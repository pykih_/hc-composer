<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

require_once HUMANE_COMPOSER_DIR . 'models/backlinks_model.php';
require_once HUMANE_COMPOSER_DIR . 'db/backlinks_db.php';

use Humane_Acquire\Model_Forms;
use Humane_Spaces\Model_Playlist;
use Humane_Acquire\Controller_Forms;
use Humane_Acquire\Controller_Playlists;

/**
 * Contains all hooks and other logic for Backlinks
 * 
 * @package Humane Sites
 * @subpackage Backlinks
 */
class Controller_Backlinks {
	/**
	 * Attaches the hooks for backlinks
	 * 
	 * @return void
	 */
	public static function init() {
        add_action( 'admin_menu', array( __CLASS__, 'add_submenu' ) );
    }
	/**
	 * Returns the breadcrumb slug and title
	 * 
	 * A user may reach the backlinks page from various different locations. 
	 * Each location is denoted by a type variable in the URL and this type URL
	 * then figures out the link of the page to use in the "Back to Page"  breadcrumb
	 * rendered on the Backlinks page.
	 * 
	 * @param string $type Contains the type for which backlinks page is being rendered
	 * 
	 * @return array Containing the title and slug of the breadcrumb
	 */
	public static function get_breadcrumb($type){
		if($type === "10000fts"){
			return array("title" => "10000ft for Data", "slug" => "humane_10000ft_for_data");
		}if($type === "humane_feed"){
			return array("title" => "Humane Feeds", "slug" => "humane_feeds");
		}elseif($type === "form"){
			return array("title" => "Forms", "slug" => "humane_forms");
		}elseif($type === "nudge"){
			return array("title" => "Nudges", "slug" => "humane_nudges");
		}elseif($type === "playlist"){
			return array("title" => "Playlists", "slug" => "humane_playlists");
		}elseif($type === "post"){
			return array("title" => "Posts", "slug" => "wp_posts_posts");
		}elseif($type === "aggregation"){
			return array("title" => "Aggregations", "slug" => "humane_aggregations");
		}else if($type === "event"){
			return array("title" => "Events", "slug" => "humane_events");
		}elseif($type === "datastory"){
			return array("title" => "Datastories", "slug" => "wp_posts_datastories");
		}elseif($type === "product"){
			return array("title" => "Products", "slug" => "wp_posts_products");
		}elseif($type === "subscription"){
			return array("title" => "Subscriptions", "slug" => "wp_posts_subscriptions");
		}elseif($type === "show_page"){
			return array("title" => "Page", "slug" => "wp_posts_page", "type" => "show");
		}else if($type === "show_event"){
			return array("title" => "Event", "slug" => "wp_posts_event", "type" => "show");
		}else if($type === "show_post"){
			return array("title" => "Post", "slug" => "wp_posts_post", "type" => "show");
		} else if($type === "datastory"){
			return array("title" => "DataStories", "slug" => "wp_posts_datastories");
		}else if($type === "email_template"){
			return array("title" => "Email Templates", "slug" => "humane_email_templates");
		}else if($type === "category"){
			return array("title" => "Categories", "slug" => "wp_terms_categories");
		}else if($type === "post_tag"){
			return array("title" => "Tags", "slug" => "wp_terms_tags");
		}else if($type === "writing_format"){
			return array("title" => "🟩 Formats", "slug" => "wp_terms_formats");
		}else if($type === "author"){
			return array("title" => "Authors", "slug" => "wp_users_writers");
		}else if($type === "dataset"){
			return array("title" => "Datasets", "slug" => "humane_datasets");
		}
	}
	/**
	 * Renders the show page for Backlinks
	 * 
	 * @return void
	 */
	public static function show() {		
		$object_table = DB::post_type_to_table_mapper($_GET["type"]);
		$id = $_GET["id"];
		global $wpdb;
		if($object_table === $wpdb->prefix . 'posts')
			$object = $wpdb->get_results($wpdb->prepare("SELECT * from $object_table WHERE ID = %s", $id))[0];
		else
			$object = $wpdb->get_results($wpdb->prepare("SELECT * from $object_table WHERE id = %s", $id))[0];
		$title = $object->post_title ?? $object->name ?? $object->sheet_name;
		$breadcrumb = self::get_breadcrumb($_GET["type"]);
		add_filter("humane_post_table_ids", array(__CLASS__, "filter_posts"));
		require_once HUMANE_COMPOSER_DIR . 'views/backlinks/show.html.php';
	}
	/**
	 * Filters the values to show in the backlinks table
	 * 
	 * @param array $items List of posts
	 * 
	 * @return array Filtered list of posts
	 */
	public static function filter_posts($items){
		$object_id = $_GET["id"];
		$object_table = DB::post_type_to_table_mapper($_GET["type"]);
		$backlink = new Model_Backlinks(array( 'object_table' => $object_table, 'object_id' => $object_id ));
		$results = maybe_unserialize($backlink->referenced_in);
		return $results;
	}
	/**
	 * Adds the submenu items for view pages
	 * 
	 * @return void
	 */
	public static function add_submenu() {
        /* pykih_non_existent is a parent key that does not exist. This allows us to create a page in WordPress without adding it in the left side menu of WordPress */
		$hook = add_submenu_page( '', 'References', 'References', 'manage_options', 'humane_backlinks', array( __CLASS__, 'show'));
		add_action( "load-$hook", array(__CLASS__, 'add_show_options') );

	}
	/**
	 * Hook function that is required to display column checkboxes in Screen Options
	 *  
	 * @return void
	 */
	public static function add_show_options(){
		$table = new Posts_Table;
	}
}

Controller_Backlinks::init();