<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Handles the logic for Login URL
 * 
 * @package Humane Sites
 * @subpackage Login URL
 * 
 * @todo Figure out how this works. Seems to be picked up from somewhere.
 */
class Controller_Login_Url {

    public static $wp_login_php;
    public static $wp_login_url;
    public static $wp_redirect_url;

	public static function init() {

        self::$wp_login_url = 'dtxkvqoz8u';
        self::$wp_redirect_url = '404';

        add_action( 'plugins_loaded', array( __CLASS__, 'plugins_loaded' ), 9999 );
		add_action( 'wp_loaded', array( __CLASS__, 'wp_loaded' ) );
		add_action( 'setup_theme', array( __CLASS__, 'setup_theme' ), 1 );

        //redirect from custom login url to wp-admin on login
		add_filter( 'site_url', array( __CLASS__, 'site_url' ), 10, 4 );
		add_filter( 'network_site_url', array( __CLASS__, 'network_site_url' ), 10, 3 );
        add_filter( 'wp_redirect', array( __CLASS__, 'wp_redirect' ), 10, 2 ); 
        
        //replace wp-login.php with link in email sent to new user for login
        add_filter( 'site_option_welcome_email', array( __CLASS__, 'welcome_email' ) );

		add_filter( 'user_request_action_email_content', array( __CLASS__, 'user_request_action_email_content' ), 999, 2 ); //filter the login url in any email with login link sent
	}
	public function user_request_action_email_content( $email_text, $email_data ) {
		$email_text = str_replace( '###CONFIRM_URL###', esc_url_raw( str_replace( self::new_login_slug() . '/', 'wp-login.php', $email_data['confirm_url'] ) ), $email_text );
		return $email_text;
	}

	private static function use_trailing_slashes() {
		return ( '/' === substr( get_option( 'permalink_structure' ), - 1, 1 ) );
	}

	private static function user_trailingslashit( $string ) {
		return self::use_trailing_slashes() ? trailingslashit( $string ) : untrailingslashit( $string );
	}

	private static function wp_template_loader() {
		global $pagenow;
		$pagenow = 'index.php';

		if ( ! defined( 'WP_USE_THEMES' ) ) {
			define( 'WP_USE_THEMES', true );
		}

		wp();
		require_once( ABSPATH . WPINC . '/template-loader.php' );
		die;
	}

	private static function new_login_slug() {
        $slug = self::$wp_login_url;
        return $slug;
	}

	private static function new_redirect_slug() {
		$slug = self::$wp_redirect_url; //redirect to 404 not found page on opening wp-admin / wp-login.php 
        return $slug;
	}

	public static function new_login_url( $scheme = null ) {
		$url = apply_filters( 'wps_hide_login_home_url', home_url( '/', $scheme ) );
		if ( get_option( 'permalink_structure' ) ) {
			return self::user_trailingslashit( $url . self::new_login_slug() );
		} else {
			return $url . '?' . self::new_login_slug();
		}
	}

	public static function new_redirect_url( $scheme = null ) {
		if ( get_option( 'permalink_structure' ) ) {
			return self::user_trailingslashit( home_url( '/', $scheme ) . self::new_redirect_slug() );
		} else {
			return home_url( '/', $scheme ) . '?' . self::new_redirect_slug();
		}
	}

	public static function plugins_loaded() {

		global $pagenow;

		if ( ! is_multisite()
		     && ( strpos( rawurldecode( $_SERVER['REQUEST_URI'] ), 'wp-signup' ) !== false
		          || strpos( rawurldecode( $_SERVER['REQUEST_URI'] ), 'wp-activate' ) !== false ) && apply_filters( 'wps_hide_login_signup_enable', false ) === false ) {

			wp_die( 'This feature is not enabled.', 'wps-hide-login' ) ;

		}

		$request = parse_url( rawurldecode( $_SERVER['REQUEST_URI'] ) );

		if ( ( strpos( rawurldecode( $_SERVER['REQUEST_URI'] ), 'wp-login.php' ) !== false
		       || ( isset( $request['path'] ) && untrailingslashit( $request['path'] ) === site_url( 'wp-login', 'relative' ) ) )
		     && ! is_admin() ) {

			self::$wp_login_php = true;

			$_SERVER['REQUEST_URI'] = self::user_trailingslashit( '/' . str_repeat( '-/', 10 ) );

			$pagenow = 'index.php';

		} elseif ( ( isset( $request['path'] ) && untrailingslashit( $request['path'] ) === home_url( self::new_login_slug(), 'relative' ) )
		           || ( ! get_option( 'permalink_structure' )
		                && isset( $_GET[ self::new_login_slug() ] )
		                && empty( $_GET[ self::new_login_slug() ] ) ) ) {

			$pagenow = 'wp-login.php';

		} elseif ( ( strpos( rawurldecode( $_SERVER['REQUEST_URI'] ), 'wp-register.php' ) !== false
		             || ( isset( $request['path'] ) && untrailingslashit( $request['path'] ) === site_url( 'wp-register', 'relative' ) ) )
		           && ! is_admin() ) {

			self::$wp_login_php = true;

			$_SERVER['REQUEST_URI'] = self::user_trailingslashit( '/' . str_repeat( '-/', 10 ) );

			$pagenow = 'index.php';
		}

	}

	public static function setup_theme() {
		global $pagenow;

		if ( ! is_user_logged_in() && 'customize.php' === $pagenow ) {
			wp_die( 'This has been disabled', 403 );
		}
	}

	public static function wp_loaded() {

		global $pagenow;

		$request = parse_url( rawurldecode( $_SERVER['REQUEST_URI'] ) );

		if ( ! isset( $_POST['post_password'] ) ) {

			if ( is_admin() && ! is_user_logged_in() && ! defined( 'DOING_AJAX' ) && $pagenow !== 'admin-post.php' && $request['path'] !== '/wp-admin/options.php' ) {
				wp_safe_redirect( self::new_redirect_url() );
				die();
			}

			if ( $pagenow === 'wp-login.php'
			     && $request['path'] !== self::user_trailingslashit( $request['path'] )
			     && get_option( 'permalink_structure' ) ) {

				wp_safe_redirect( self::user_trailingslashit( self::new_login_url() )
				                  . ( ! empty( $_SERVER['QUERY_STRING'] ) ? '?' . $_SERVER['QUERY_STRING'] : '' ) );

				die;

			} elseif ( self::$wp_login_php ) {

				if ( ( $referer = wp_get_referer() )
				     && strpos( $referer, 'wp-activate.php' ) !== false
				     && ( $referer = parse_url( $referer ) )
				     && ! empty( $referer['query'] ) ) {

					parse_str( $referer['query'], $referer );

					@require_once WPINC . '/ms-functions.php';

					if ( ! empty( $referer['key'] )
					     && ( $result = wpmu_activate_signup( $referer['key'] ) )
					     && is_wp_error( $result )
					     && ( $result->get_error_code() === 'already_active'
					          || $result->get_error_code() === 'blog_taken' ) ) {

						wp_safe_redirect( self::new_login_url()
						                  . ( ! empty( $_SERVER['QUERY_STRING'] ) ? '?' . $_SERVER['QUERY_STRING'] : '' ) );
						die;
					}
				}
				self::wp_template_loader();
			} elseif ( $pagenow === 'wp-login.php' ) {
				global $error, $interim_login, $action, $user_login;
				$redirect_to = admin_url();
				$requested_redirect_to = '';
				if ( isset( $_REQUEST['redirect_to'] ) ) {
					$requested_redirect_to = $_REQUEST['redirect_to'];
				}

				if ( is_user_logged_in() ) {
					$user = wp_get_current_user();
					if ( ! isset( $_REQUEST['action'] ) ) {
						$logged_in_redirect = apply_filters( 'whl_logged_in_redirect', $redirect_to, $requested_redirect_to, $user );
						wp_safe_redirect( $logged_in_redirect );
						die();
					}
				}
				@require_once ABSPATH . 'wp-login.php';
				die;
			}

		}

	}

	public static function site_url( $url, $path, $scheme, $blog_id ) {
		return self::filter_wp_login_php( $url, $scheme );
	}

	public static function network_site_url( $url, $path, $scheme ) {
		return self::filter_wp_login_php( $url, $scheme );
	}

	public static function wp_redirect( $location, $status ) {
		if ( strpos( $location, 'https://wordpress.com/wp-login.php' ) !== false ) {
			return $location;
		}
		return self::filter_wp_login_php( $location );
	}

	public static function filter_wp_login_php( $url, $scheme = null ) {
		if ( strpos( $url, 'wp-login.php?action=postpass' ) !== false ) {
			return $url;
		}
		if ( strpos( $url, 'wp-login.php' ) !== false && strpos( wp_get_referer(), 'wp-login.php' ) === false ) {
			if ( is_ssl() ) {
				$scheme = 'https';
			}
			$args = explode( '?', $url );
			if ( isset( $args[1] ) ) {
				parse_str( $args[1], $args );
				if ( isset( $args['login'] ) ) {
					$args['login'] = rawurlencode( $args['login'] );
				}
				$url = add_query_arg( $args, self::new_login_url( $scheme ) );
			} else {
				$url = self::new_login_url( $scheme );
			}
		}
		return $url;
	}

	public static function welcome_email( $value ) {
		return $value = str_replace( 'wp-login.php', trailingslashit(self::$wp_login_url), $value );
	}

}
if(get_option("humane_login_url_enabled") !== "no"){
	Controller_Login_Url::init();
}