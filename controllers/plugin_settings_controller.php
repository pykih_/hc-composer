<?php

namespace Humane_Sites;
// require_once HUMANE_COMPOSER_DIR . "models/plugin_settings_model.php";

if (!defined('ABSPATH')) { exit();
}

/**
 * All hooks regarding Settings functionality
 * are added in this class
 * 
 * @package    Humane Acquire
 * @subpackage Options
 */
class Controller_Common_Plugin_Options
{
    /**
     * Attaches the hooks for settings
     * 
     * @return void
     */
    public static function init()
    {
        add_action("admin_menu", array(__CLASS__, "create_admin_pages"));
    }

    /**
     * Add submenu pages for views
     * 
     * @return void
     */
    public static function create_admin_pages()
    {
        add_submenu_page(
            'options-general.php',
            '🟩 Humane Settings', 
            '🟩 Humane Settings', 
            'manage_options', 
            'analytics_settings', 
            array(__CLASS__, 'analytics_settings')
        );

    }

    /**
     * Analytics Settings.
     * 
     * Settings to allow user to connect with Google Analytics and
     * Plausible
     * 
     * @return void
     */
    public static function analytics_settings()
    {
        include HUMANE_COMPOSER_DIR . 'views/plugin_settings/analytics.html.php';
    }
}

Controller_Common_Plugin_Options::init();
