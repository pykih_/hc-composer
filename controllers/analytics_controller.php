<?php

namespace Humane_Sites;


if (!defined('ABSPATH')) exit();

/**
 * Contains all functions related to analytics
 * 
 * @package Humane Sites
 * @subpackage Analytics
 */
class Controller_Analytics{
    /**
     * Sends a  curl request with a bearer token attached
     * 
     * Uses the saved plausible_bearer token to send an authenticated
     * CURL request to Plausible Analytics to fetch data for the current site
     * 
     * @param string $url URL to send the request to
     * 
     * @return string CURL Response
     */
    public static function curl_with_bearer($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $bearer = get_option("plausible_bearer");
        if (empty($bearer)) return 0;
        $headers = array(
            "Authorization: Bearer " . $bearer,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }

    /**
     * Gets the current live visitors for the site
     * 
     * @return string CURL Response
     */
    public static function get_current_live_visitors()
    {
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        $current_visitors = "https://plausible.io/api/v1/stats/realtime/visitors?site_id=$domain";
        $response = self::curl_with_bearer($current_visitors);
        if(strpos($response, "error") !== false){
            $response = "ERROR";
        }
        return $response;
    }

    /**
     * Gets the analytics for a site between two given dates
     * 
     * @param string $start_date
     * @param string $end_date
     * @param bool $blog_id Blog ID for which data is required
     * 
     * @return array Analytics data
     */
    public static function get_site_level_analytics_by_date($start_date, $end_date, $blog_id = false)
    {
        if(!$blog_id) $blog_id = get_current_blog_id();
        $domain = get_blog_option($blog_id, "siteurl");
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        $date = $start_date . "," . $end_date;
        $data_url = "https://plausible.io/api/v1/stats/aggregate?site_id=$domain&period=custom&date=$date&metrics=pageviews";
        $response = self::curl_with_bearer($data_url);
        $data = json_decode($response, true);
        $data = $data["results"];
        foreach ($data as $index => $datum) {
            $data[$index]["name"] = ucwords(str_replace("_", " ", $index));
            if ($index === "visit_duration") {
                $data[$index]["value"] = Controller_Date::secondsToTime($datum["value"]);
            }
        }
        return $data;
    }
    /**
     * Gets the overall analytics for the current site
     * 
     * @return array Analytics data
     */
    public static function get_site_level_analytics()
    {
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        $date = current_time('Y-m-d');
        $data_url = "https://plausible.io/api/v1/stats/aggregate?site_id=$domain&period=30d&metrics=visitors,pageviews,bounce_rate,visit_duration&compare=previous_period";
        $response = self::curl_with_bearer($data_url);
        $data = json_decode($response, true);

        $data = $data["results"];
        foreach ($data as $index => $datum) {
            $data[$index]["name"] = ucwords(str_replace("_", " ", $index));
            if ($index === "visit_duration") {
                $data[$index]["value"] = Controller_Date::secondsToTime($datum["value"]);
            }
        }
        return $data;
    }
    /**
     * Gets the analytics of the top pages of the current site
     * 
     * @return array Analytics data
     */
    public static function get_top_pages_analytics()
    {
        $filters_json = '{"goal":null,"source":null,"utm_medium":null,"utm_source":null,"utm_campaign":null,"referrer":null,"page":null}';
        $date = current_time('Y-m-d');
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=event:page&metrics=visitors,pageviews,bounce_rate,visit_duration";
        $response = self::curl_with_bearer($data_url);
        $data = json_decode($response, true);
        return $data["results"];
    }
    /**
     * Gets the analytics of the sources of the current site
     * 
     * @return array Analytics data
     */
    public static function get_sources_analytics($id = false)
    {
        if ($id) {
            $uri = get_permalink($id);
            $uri = parse_url($uri);
            $uri = $uri["path"];
            $pageID = get_option('page_on_front');
            if ($id == $pageID) $uri = "/";
        }
        $date = current_time('Y-m-d');
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        if (empty($uri)) {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:source&metrics=visitors,bounce_rate,visit_duration";
        } else {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:source&metrics=visitors,bounce_rate,visit_duration&filters=event:page==$uri";
        }
        $response = self::curl_with_bearer($data_url);
        $data = json_decode($response, true);
        return $data["results"];
    }
    /**
     * Gets the device analytics of the current site
     * 
     * @return array Analytics data
     */
    public static function get_device_analytics($id = false)
    {
        $data = array();
        if ($id) {
            $uri = get_permalink($id);
            $uri = parse_url($uri);
            $uri = $uri["path"];
            $pageID = get_option('page_on_front');
            if ($id == $pageID) $uri = "/";
        }
        $date = current_time('Y-m-d');
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        if (empty($uri)) {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:device&metrics=visitors,bounce_rate,visit_duration";
        } else {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:device&metrics=visitors,bounce_rate,visit_duration&filters=event:page==$uri";
        }
        $response = self::curl_with_bearer($data_url);
        $result = json_decode($response, true);
        $data["screen-sizes"] = $result["results"];
        if (empty($uri)) {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:browser&metrics=visitors,bounce_rate,visit_duration";
        } else {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:browser&metrics=visitors,bounce_rate,visit_duration&filters=event:page==$uri";
        }
        $response = self::curl_with_bearer($data_url);
        $result = json_decode($response, true);
        $data["browsers"] = $result["results"];
        if (empty($uri)) {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:os&metrics=visitors,bounce_rate,visit_duration";
        } else {
            $data_url = "https://plausible.io/api/v1/stats/breakdown?site_id=$domain&period=30d&property=visit:os&metrics=visitors,bounce_rate,visit_duration&filters=event:page==$uri";
        }
        $response = self::curl_with_bearer($data_url);
        $result = json_decode($response, true);
        $data["operating-systems"] = $result["results"];
        return $data;
    }
    /**
     * Gets the analytics for a single page of the current site
     * 
     * @param int $id ID of the page
     * @param bool $uri URL of the page
     * 
     * @return array Analytics data
     */
    public static function get_page_level_analytics($id, $uri = false)
    {
        if (!$uri) {
            $uri = get_permalink($id);
            $uri = parse_url($uri);
            $uri = $uri["path"];
            $pageID = get_option('page_on_front');
            if ($id == $pageID) $uri = "/";
        }
        $domain = get_site_url();
        $domain = str_replace("https://", "", $domain);
		$domain = str_replace("http://", "", $domain);
        $date = current_time('Y-m-d');
        $data_url = "https://plausible.io/api/v1/stats/aggregate?site_id=$domain&period=30d&metrics=visitors,pageviews,bounce_rate,visit_duration&compare=previous_period&filters=event:page==$uri";
        $response = self::curl_with_bearer($data_url);
        $data = json_decode($response, true);

        $data = $data["results"];
        foreach ($data as $index => $datum) {
            $data[$index]["name"] = ucwords(str_replace("_", " ", $index));
            if ($index === "visit_duration") {
                $data[$index]["value"] = Controller_Date::secondsToTime($datum["value"]);
            }
        }
        return $data;
    }
}
