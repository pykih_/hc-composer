<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * Handles all Date related logic and transformations
 */
class Controller_Date {
    /**
     * Convert seconds into d h m s format
     * 
     * @param int $inputSeconds Number of seconds to convert
     * 
     * @return string
     * 
     * @example 1000000 -> 11 d, 13 h, 46 m, 40 s
     */
    public static function secondsToTime($inputSeconds) {
        $secondsInAMinute = 60;
        $secondsInAnHour = 60 * $secondsInAMinute;
        $secondsInADay = 24 * $secondsInAnHour;

        // Extract days
        $days = floor($inputSeconds / $secondsInADay);

        // Extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);

        // Extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);

        // Extract the remaining seconds
        $remainingSeconds = $minuteSeconds % $secondsInAMinute;
        $seconds = ceil($remainingSeconds);

        // Format and return
        $timeParts = [];
        $sections = [
            'd' => (int)$days,
            'h' => (int)$hours,
            'm' => (int)$minutes,
            's' => (int)$seconds,
        ];

        foreach ($sections as $name => $value){
            if ($value > 0){
                $timeParts[] = $value. ' '.$name;
            }
        }

        return implode(', ', $timeParts);
    }

    /**
     * Converts a timestamp to a date string
     * 
     * @param int $date Timestamp
     * 
     * @return string Date string
     * 
     * @example 1658221476 -> Jul 19, 2022 at 09:04
     */
    public static function to_full($date){
        if(empty(get_option('timezone_string'))) date_default_timezone_set(get_option('gmt_offset'));
        else date_default_timezone_set(get_option('timezone_string'));
        if(empty($date)) return $date;
        $formatted_date = date('M j, Y', $date);
        $formatted_time = date('H:i', $date);
        $fulldate = $formatted_date." at ".$formatted_time;
        return $fulldate;
    }

    /**
     * Converts the given start time and end time to a human readable format
     * 
     * Mainly used for events to idicate if an event has already happened, 
     * if it is going to happen or if it is currently live.
     * 
     * @param int $start_time
     * @param int $end_time
     * @param string $timezone
     * 
     * @return string Returns the date formatted for a card
     */
    public static function to_card($start_time, $end_time = false, $timezone = false){
        $from_time = date_timestamp_get($start_time);
        $current_date= date_create();
        if(!$timezone) $timezone = wp_timezone_string();
        if($timezone) $current_date= date_create(null, new \DateTimeZone($timezone));

        $current_time = date_timestamp_get($current_date);
        if($end_time){
            $to_time = date_timestamp_get($end_time);

            $future = ($current_time < $from_time);

            $duration_in_minutes = round((abs($current_time - $from_time))/60);
            $duration_in_seconds = round(abs($current_time - $from_time));

            if($current_time >= $from_time && $current_time <= $to_time) return '<div class="check_date hc-one-line hc-supernormal-xs-bold hc-text-red">&bull; LIVE &bull;</div>';

            if(!$future){
                return "Event ended " . self::time_ago_in_words($start_time, false, true, true, $timezone);
            }

            if(date_format($current_date, "d") == date_format($start_time, "d") && date_format($current_date, "Y") == date_format($start_time, "Y")){
                return 'Premiering today at '.date_format($start_time, "H:i").' IST';
            }

            $distance_in_days = round($duration_in_minutes/1440);

            if($distance_in_days <= 1) return 'Scheduled for tomorrow, '.date_format($start_time, "H:i").' IST';

            $distance_in_years = round($distance_in_days/365);

            if(date_format($current_date, "Y") == date_format($start_time, "Y")){
                return 'Scheduled for '.date_format($start_time, "d M, H:i").' IST';
            }

            if(date_format($current_date, "Y") < date_format($start_time, "Y")){
                return 'Scheduled for '.date_format($start_time, "d M Y, H:i").' IST';
            }
        }else{
            if(date_format($current_date, "d") == date_format($start_time, "d") && date_format($current_date, "M") == date_format($start_time, "M") && date_format($current_date, "Y") == date_format($start_time, "Y")){
                return 'Today at '.date_format($start_time, "H:i").' IST';
            }
            return "Event ended " . self::time_ago_in_words($start_time, false, true, true, $timezone);
        }
    }

    /**
     * Return the timeago string for a given start time and end time
     * 
     * @param int $start_time
     * @param int $end_time
     * @param bool $include_seconds
     * @param bool $include_months
     * @param bool $timezone
     * 
     * @return string Time Ago type string
     */
    public static function time_ago_in_words($start_time, $end_time = false, $include_seconds = true, $include_months = true, $timezone = false) {

        $start_time = date_timestamp_get($start_time);

        if($timezone && !$end_time) {
            $tz = new \DateTimeZone( $timezone );
            $end_time= date_create(null, $tz);
        }
        $end_time = date_timestamp_get($end_time);
        $future = ($end_time < $start_time);

        $duration_in_minutes = round((abs($end_time - $start_time))/60);
        $duration_in_seconds = round(abs($end_time - $start_time));

        if ($future) {
            return self::future($duration_in_minutes, $include_seconds, $duration_in_seconds);
        }

        if ($duration_in_minutes <= 1) {
            if ($include_seconds) {
                if ($duration_in_seconds < 5) return 'less than 5 seconds seconds ago';
                if ($duration_in_seconds < 10) return 'less than 10 seconds seconds ago';
                if ($duration_in_seconds < 20) return 'less than 20 seconds seconds ago';
                if ($duration_in_seconds < 40) return 'half a minute ago';
                if ($duration_in_seconds < 60) return 'less than a minute ago';
                return '1 minute ago';
            }
            return ($duration_in_minutes == 0 ? 'less than a minute ago' : '1 minute ago');
        }

        if ($duration_in_minutes <= 45) return $duration_in_minutes .  ' minutes ago';

        if ($duration_in_minutes <= 90) return '1 hour ago';

        if ($duration_in_minutes <= 1440) return round($duration_in_minutes/60) .  ' hours ago';

        if ($duration_in_minutes <= 2880) return '1 day ago';

        $distance_in_days = round($duration_in_minutes/1440);

        if (!$include_months || $distance_in_days <= 30) return round($distance_in_days) .  ' days ago';

        $distance_in_months = round($distance_in_days/30);

        if ($distance_in_days < 345) {
            if($distance_in_months > 1) return round($distance_in_days/30) .  ' months ago';
            else return '1 month ago';
        }

        $distance_in_years = round($distance_in_days/365);
        if($distance_in_years > 1) return round($distance_in_days/365) .  ' years ago';
        else return '1 year ago';

    }

    /**
     * Returns a future formatted date string
     * 
     * Used mainly for events about to happen in a given duration
     * in the future
     * 
     * @param int $duration_in_minutes
     * @param bool $include_seconds
     * @param int $duration_in_seconds
     * 
     * @return string Date string in future format
     */
    private static function future($duration_in_minutes, $include_seconds, $duration_in_seconds)
    {
        if ($duration_in_minutes <= 1) {
            if ($include_seconds) {
                if ($duration_in_seconds < 5) return 'in less than 5 seconds';
                if ($duration_in_seconds < 10) return 'in less than 10 seconds';
                if ($duration_in_seconds < 20) return 'in less than 20 seconds';
                if ($duration_in_seconds < 40) return 'in half a minute';
                if ($duration_in_seconds < 60) return 'in less than a minute';
                return 'in a minute';
            }
            return ($duration_in_minutes == 0 ? 'in less than a minute' : 'in 1 minute');
        }

        if ($duration_in_minutes <= 45) return 'in '.$duration_in_minutes.' minutes';

        if ($duration_in_minutes <= 90) return 'in about 1 hour';

        if ($duration_in_minutes <= 1440) return 'in about '.round($duration_in_minutes / 60).' hours';

        if ($duration_in_minutes <= 2880) return 'in 1 day';

        $distance_in_days = round($duration_in_minutes / 1440);

        if ($distance_in_days <= 30) return 'in '.round($duration_in_minutes / 1440).' days';

        $distance_in_months = round($distance_in_days / 30);

        if ($distance_in_days < 345) {
            if($distance_in_months > 1) return 'in '. round($distance_in_days / 30) .  ' months';
            else return 'in 1 month';
        }

        $distance_in_years = round($distance_in_days / 365);
        if($distance_in_years > 1) return 'in '. round($distance_in_days / 365) .  ' years';
        else return 'in 1 year';
    }
}