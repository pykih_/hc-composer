<?php
namespace Humane_Sites;

require_once HUMANE_COMPOSER_DIR . 'models/posts_model.php';
require_once HUMANE_COMPOSER_DIR . 'api/wp_block_api.php';

use Humane_Spaces\Model_Playlist;
use Humane_Spaces\Model_Link_Juicer;
use Humane_Content\Model_Feeds;
use Humane_Acquire\Model_Login_Wall;
use Humane_Acquire\Model_Email_Templates;
use Humane_Commerce\Model_Subscriptions;
use Humane_Acquire\Model_Restrict_Content;

if (! defined('ABSPATH') ) {
    exit();
}

/**
 * Handles all logic related to Posts
 *
 * @package    Humane Sites
 * @subpackage Posts
 */
class Controller_Posts
{

    /**
     * Attaches all hooks related to Posts
     *
     * @return void
     */
    public static function init()
    {
        add_action('admin_menu', array( __CLASS__, 'add_submenu' ), 3);
        add_action('admin_enqueue_scripts', array( __CLASS__, 'enqueue_admin_scripts_styles' ));
        add_action('enqueue_block_editor_assets', array( __CLASS__, 'enqueue_block_scripts' ));
        add_action('wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts_styles' ));
        add_filter('post_row_actions', array( __CLASS__, 'add_duplicate_post_link' ), 10, 2);
        add_filter('page_row_actions', array( __CLASS__, 'add_duplicate_post_link' ), 10, 2);
        add_action('admin_action_add_duplicate_post_as_draft', array( Model_Posts::class, 'add_duplicate_post_as_draft' ));
        add_action('init', array( __CLASS__, 'make_preview_dynamic' ));
        add_action('manage_posts_custom_column', array( __CLASS__, 'render_column' ));
        add_action('manage_posts_columns', array( __CLASS__, 'register_column' ));
        add_action('manage_pages_custom_column', array( __CLASS__, 'render_column' ));
        add_action('manage_pages_columns', array( __CLASS__, 'register_column' ));
        add_action('admin_init', array( __CLASS__, 'set_user_screen_options' ));
        add_action('init', array( __CLASS__, 'add_taxonomy_to_pages' ));
        add_filter('post_type_link', array( __CLASS__, 'add_post_id_in_wp_block' ), 98, 2);
        add_action('init', array( __CLASS__, 'add_rewrite_rule_for_wp_block' ), 99);
        add_filter('manage_edit-wp_block_columns', array( __CLASS__, 'humane_remove_columns' ));
        add_action('admin_init', array( __CLASS__, 'humane_add_author_caps' ));        
        add_filter('wp_img_tag_add_loading_attr', array( __CLASS__, 'skip_lazy_loading_image_by_alt' ), 10, 3);    
        add_filter('the_content', array( __CLASS__, 'hc_dynamic_js_based_on_content' ), 11);
        add_action('wp_enqueue_scripts', array( __CLASS__, 'dequeue_unused_scripts' ), 99);
        add_action('admin_init', array( __CLASS__, 'hc_flush_cache_for_admin' ));
        add_filter('wpmeteor_enabled', array( __CLASS__, 'skip_wpmeteor_enabled_script' ), 10);
    }

    /**
     * Adds category and tag taxonomies to Pages
     *
     * @return void
     */
    public static function add_taxonomy_to_pages()
    {
        register_taxonomy_for_object_type('category', 'page');
        register_taxonomy_for_object_type('post_tag', 'page');
        register_taxonomy_for_object_type('category', 'wp_block');
        register_taxonomy_for_object_type('post_tag', 'wp_block');
        add_post_type_support('wp_block', 'author');
    }

    /**
     * Sets the default screen options for a user
     *
     * @return void
     */
    public static function set_user_screen_options()
    {
        $hidden     = array( 'manageedit-postcolumnshidden', 'manageedit-pagecolumnshidden', 'manageedit-aggregationcolumnshidden', 'manageedit-eventcolumnshidden' );
        $meta_value = array(
        'comments',
        'wpseo-score-readability',
        'wpseo-score',
        'wpseo-linked',
        'wpseo-links',
        'wpseo-title',
        'wpseo-metadesc',
        'wpseo-focuskw',
        'reads',
        'leads',
        'ratings',
        );
        $user       = wp_get_current_user();
        foreach ( $hidden as $meta_key ) {
            if (! get_user_meta($user->ID, 'default_screens_set', true) ) {
                update_user_meta($user->ID, $meta_key, $meta_value);
                update_user_meta($user->ID, 'default_screens_set', true);
            }
        }
    }
    /**
     * Registers the custom column for Posts.
     *
     * @param array $columns Existing columns.
     *
     * @return array Columns with custom column added.
     */
    public static function register_column( $columns )
    {
        $post_type = get_post_type();
        if ($post_type === 'wp_block' ) {
            $columns['card_id']           = 'Card ID';
        }
        $columns['backlinks'] = '🟩 <span class="vers dashicons dashicons-admin-links" title="Backlinks"><span class="screen-reader-text">Backlinks</span></span>';
        // if ($post_type !== 'wp_block' ) {
            $columns['writing_format'] = '🟩 Formats';
        // }
        return $columns;
    }
    /**
     * Renders the column based on name
     *
     * @param string $column_id Name of the column
     *
     * @return void
     */
    public static function render_column( string $column_id )
    {
        $post = get_post();
        if ($column_id == 'writing_format' ) {
            echo get_the_term_list($post->ID, 'writing_format');
        } elseif ($column_id == 'backlinks' ) {
            global $wpdb;
            $object_table = $wpdb->prefix . 'posts';
            if (get_post_type($post->ID) === 'wp_block' ) {
                $tag       = '<!-- wp:block {"ref":' . $post->ID . '}';
                $search    = '%' . $wpdb->esc_like($tag) . '%';
                $instances = $wpdb->get_results("SELECT * FROM $object_table WHERE post_content LIKE '$search' and post_type NOT IN ('revision', 'attachment', 'nav_menu_item')");

                $count_instances = count($instances);
                echo '<p class="reuseable_instance_label">';
                if ($count_instances > 0 ) {
                    echo '<strong>';
                    echo sprintf(
                        _n('Used in %s post:', 'Used in %s posts:', $count_instances, 'humane-club'),
                        $count_instances
                    );
                       echo '</strong>';
                } else {
                    esc_html_e('—', 'humane-club');
                }
                echo '</p>';
                if ($instances ) {
                    $count = 0;
                    foreach ( $instances as $instance ) {
                        echo '<p class="reuseable_instance_paragraph"><a href="' . get_edit_post_link($instance->ID) . '">' . $instance->post_title . ' [' . get_post_type($instance->ID) . ']</a></p>';
                        $count++;
                    }
                }
            } else {

                $references    = new Model_Backlinks(
                    array(
                    'object_table' => $object_table,
                    'object_id'    => $post->ID,
                    )
                );
                $referenced_in = maybe_unserialize($references->referenced_in);
                if (is_array($referenced_in) ) {
                    $count_references = count($referenced_in);
                } else {
                    $count_references = '—';
                }
                echo '<a href="' . admin_url() . 'admin.php?page=humane_backlinks&id=' . $post->ID . '&type=post">' . $count_references . '</a>';
            }
            // echo $count_references;
        } elseif ($column_id == 'reads' ) {
            // $id = (int)$_GET["id"];
            $latest_reads = self::get_latest_leads($post->ID, 'reads');
            echo count($latest_reads);
        } elseif ($column_id == 'leads' ) {
            $latest_leads = self::get_latest_leads($post->ID, 'leads');
            echo count($latest_leads);
        } elseif ($column_id == 'ratings' ) {
            $latest_ratings = self::get_latest_leads($post->ID, 'ratings');
            echo count($latest_ratings);
        } elseif ($column_id == 'autogenerated' ) {
            $login_page = get_post_meta($post->ID, 'humane_autogenerated_page', true);
            echo $login_page ? 'Yes' : 'No';
        } elseif ($column_id == 'playlist' ) {
            $playlist_speciality = get_post_meta($post->ID, 'playlist_speciality', true);
            echo $playlist_speciality ? $playlist_speciality : '--';
        } elseif ($column_id == 'card_id' ) {
            echo $post->ID;
        }

    }

    /**
     * Gets the leads based on attributes
     *
     * @param int    $id        ID of the post for which rows are required
     * @param string $condition Condition for which rows are needed
     * @param bool   $counting  If true, only returns count of the rows. Otherwise returns the rows.
     *
     * @return array
     */
    public static function get_latest_leads( $id = false, $condition = false, $counting = false )
    {
        if ($id ) {
            $rows = Model_User_Events::get_user_events_by_post_id($id);
        } else {
            $rows = Model_User_Events::get_all();
        }
        if ($condition ) {
            $rows = array_filter(
                $rows,
                function ( $row ) use ( $condition ) {
                    if ($condition === 'reads' ) {
                        if ($row->action === 'bookmarked' || $row->action === 'read' ) {
                                return true;
                        } else {
                            return false;
                        }
                    } elseif ($condition === 'ratings' ) {
                        if ($row->action === 'upvote' || $row->action === 'downvote' ) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        if ($row->action === 'bookmarked' || $row->action === 'read' ) {
                            return false;
                        } elseif ($row->action === 'form_submission' ) {
                            return true;
                        }
                    }
                }
            );
        }
        $count = count($rows);
        if ($counting ) {
            return $rows;
        }
        $posts_per_page = 20;
        $offset         = 0;
        $rows           = array_slice($rows, $offset, $posts_per_page);
        return $rows;
    }

    /**
     * Renders the social shares for share icons for a given post
     *
     * @param int  $id     ID of the post
     * @param bool $markup If true, returns markup. Otherwise returns count.
     *
     * @return void
     */
    public static function show_socials( $id = false, $markup = true )
    {
        if ($id === false ) {
            $id = get_the_ID();
        }
        $url     = urlencode(get_the_permalink($id));
        $title   = urlencode(html_entity_decode(get_the_title($id), ENT_COMPAT, 'UTF-8'));
        $media   = urlencode(get_the_post_thumbnail_url($id, 'full'));
        $socials = array( 'facebook', 'twitter', 'linkedin' );
        $socials = array_filter(
            $socials,
            function ( $a ) {
                $option = get_option("enable_$a");
                if ($option === 'show' ) {
                    return true;
                }
                return false;
            }
        );
        if (count($socials) > 0 && $markup ) {
            include HUMANE_COMPOSER_DIR . 'views/posts/social_share.html.php';
        }
        if (! $markup ) {
            return count($socials);
        }
    }


    /**
     * Renders the actions of a post
     *
     * 1. Upvote
     * 2. Downvote
     * 3. Bookmark
     * 4. Read
     *
     * @param int $id ID of the post
     *
     * @return void
     */
    public static function show_actions( $id = false )
    {
        if ($id === false ) {
            $id = get_the_ID();
        }
        if (is_user_logged_in() ) {
            include HUMANE_COMPOSER_DIR . 'views/posts/_post_actions.html.php';
        }
    }

    /**
     * Renders the preview markup based on attributes
     *
     * @param array  $attributes
     * @param string $content
     *
     * @return void
     *
     * @todo Move this to a Blocks Controller
     */
    public static function render_preview_callback( $attributes, $content )
    {
        $response_post = Controller_Posts::get_data_from_post_id(get_the_ID(), false, false);
        ob_start();
        include HUMANE_COMPOSER_DIR . 'views/posts/preview_block.html.php';
        $markup = ob_get_clean();
        return $markup;
    }

    /**
     * Makes the preview block dynamic
     *
     * @return void
     *
     * @todo Move this to a Blocks Controller
     */
    public static function make_preview_dynamic()
    {
        register_block_type(
            'humane-blocks/preview',
            array(
            'render_callback' => array( __CLASS__, 'render_preview_callback' ),
            'attributes'      => array(
            'content' => array(
            'type' => 'string',
                    ),
                    'id'      => array(
                        'type' => 'string',
                    ),
            ),
            )
        );
    }

    /**
     * Gets the list of post types based on usage
     *
     * @param string $usage       Usage for which post types is required
     * @param string $filter_type Type of array required
     *
     * @return array
     */
    public static function filter_existing_post_type( $usage, $filter_type = 'value' )
    {
        $post_types = json_decode(file_get_contents(HUMANE_COMPOSER_DIR . 'assets/posts/json/post_types.json'), true);
        if ($post_types ) {
            $post_types = array_filter(
                $post_types,
                function ( $a ) use ( $usage ) {
                    return $a[ $usage ];
                }
            );
            $post_types = array_keys($post_types);
        }
        $existing_types = get_post_types();
        $post_types     = array_intersect($post_types, $existing_types);
        if ($filter_type === 'keys' ) {
            $final_types = array();
            foreach ( $post_types as $key => $value ) {
                $final_types[ $value ] = ucwords($value);
            }
            $final_types = apply_filters('filter_existing_post_type', $final_types, $usage, $filter_type);
            return $final_types;
        }
        $post_types = apply_filters('filter_existing_post_type', $post_types, $usage, $filter_type);
        return $post_types;
    }

    /**
     * Enqueues the scripts and styles required on admin side
     *
     * @return void
     */
    public static function enqueue_admin_scripts_styles()
    {
        wp_enqueue_style('humane-core-css', HUMANE_COMPOSER_URL . 'assets/core/dist/core.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/core/dist/core.min.css'), 'all');
        wp_enqueue_script('flatpickr.min.js', HUMANE_COMPOSER_URL . 'assets/utilities/js/flatpickr.min.js');
        wp_enqueue_style('flatpickr.min.css', HUMANE_COMPOSER_URL . 'assets/utilities/css/flatpickr.min.css');

        wp_enqueue_script('selectize.min.js', HUMANE_COMPOSER_URL . 'assets/utilities/js/selectize.min.js', array( 'jquery' ));
        wp_enqueue_style('selectize.min.css', HUMANE_COMPOSER_URL . 'assets/utilities/css/selectize.min.css');

        wp_enqueue_script('humane-core-js', HUMANE_COMPOSER_URL . 'assets/core/dist/core.min.js', array( 'jquery' ), md5_file(HUMANE_COMPOSER_DIR . 'assets/core/dist/core.min.js'), true);

        $ajaxurl = admin_url('admin-ajax.php');
        $ajaxurl = is_ssl() ? str_ireplace('http://', 'https://', $ajaxurl) : str_ireplace('https://', 'http://', $ajaxurl);
        wp_localize_script(
            'humane-core-js',
            'Humane_Sort_View',
            array(
            'ajaxurl' => $ajaxurl,
            'nonce'   => array(
            'sort_view' => wp_create_nonce('pykih_sort_view'),
            ),
            )
        );

    }

    /**
     * Enqueues the scripts for the blocks
     *
     * @return void
     *
     * @todo Move this to a Blocks Controller
     */
    public static function enqueue_block_scripts()
    {
        wp_enqueue_script('humane-dashboard-js', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.js', array( 'jquery' ), '', false);
        wp_enqueue_style('humane-dashboard-css', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/dist/dashboard.min.css'), false);
        wp_register_script('flatpickr.min.js', HUMANE_COMPOSER_URL . 'assets/utilities/js/flatpickr.min.js');
        wp_register_style('flatpickr.min.css', HUMANE_COMPOSER_URL . 'assets/utilities/css/flatpickr.min.css');
    }

    /**
     * Enqueue scripts and styles required on the frontend
     *
     * @return void
     */
    public static function enqueue_scripts_styles()
    {
        wp_enqueue_style('humane-core-css', HUMANE_COMPOSER_URL . 'assets/core/dist/core.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/core/dist/core.min.css'), 'all');
        wp_enqueue_script('humane-dashboard-js', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.js', array( 'jquery' ), '', false);
        wp_enqueue_script('humane-core-js', HUMANE_COMPOSER_URL . 'assets/core/dist/core.min.js', array( 'jquery' ), md5_file(HUMANE_COMPOSER_DIR . 'assets/core/dist/core.min.js'), true);
        $ajaxurl = admin_url('admin-ajax.php');
        $ajaxurl = is_ssl() ? str_ireplace('http://', 'https://', $ajaxurl) : str_ireplace('https://', 'http://', $ajaxurl);
        wp_localize_script(
            'humane-core-js',
            'Humane_Sort_View',
            array(
            'ajaxurl' => $ajaxurl,
            'nonce'   => array(
            'sort_view' => wp_create_nonce('pykih_sort_view'),
            ),
            )
        );

    }

    /**
     * Adds the submenu
     *
     * @return void
     */
    public static function add_submenu()
    {
        add_submenu_page('', 'Show Post', 'Show Post', 'manage_options', 'wp_posts_post', array( __CLASS__, 'show' ));

        $hook = add_submenu_page('', 'Reads (Engage)', 'Reads (Engage)', 'manage_options', 'humane_post_reads', array( __CLASS__, 'index_posts_reads' ));
        add_action("load-$hook", array( __CLASS__, 'add_show_options' ));

        $hook = add_submenu_page('', 'Reads (Pitch)', 'Reads (Pitch)', 'manage_options', 'humane_page_reads', array( __CLASS__, 'index_page_reads' ));
        add_action("load-$hook", array( __CLASS__, 'add_show_options' ));
    }

    /**
     * Required by WP to display column checkboxes in Screen Options
     *
     * @return void
     */
    public static function add_show_options()
    {
        $table = new User_Events_Table();
    }

    /**
     * Renders the post reads
     *
     * @return void
     */
    public static function index_posts_reads()
    {
        add_filter('filter_table_data', array( __CLASS__, 'filter_post_reads' ));
        include HUMANE_COMPOSER_DIR . 'views/posts/index_posts_reads.html.php';
    }

    /**
     * Filters the list of IDs to only shows reads of posts
     *
     * @param array $data Data to be rendered
     *
     * @return array
     */
    public static function filter_post_reads( $data )
    {

        $data = array_filter(
            $data,
            function ( $datum ) {
                $id   = $datum->wp_post_id;
                $type = get_post_type($id);
                return $type === 'post';
            }
        );
        return $data;
    }

    /**
     * Renders the page reads
     *
     * @return void
     */
    public static function index_page_reads()
    {
        add_filter('filter_table_data', array( __CLASS__, 'filter_page_reads' ));
        include HUMANE_COMPOSER_DIR . 'views/posts/index_page_reads.html.php';
    }

    /**
     * Filters the list of IDs to only shows reads of pages
     *
     * @param array $data Data to be rendered
     *
     * @return array
     */
    public static function filter_page_reads( $data )
    {
        $data = array_filter(
            $data,
            function ( $datum ) {
                $id   = $datum->wp_post_id;
                $type = get_post_type($id);
                return $type === 'page';
            }
        );
        return $data;
    }

    /**
     * Renders the show page for a post
     *
     * @return void
     */
    public static function show()
    {
        $id        = $_GET['id'];
        $page      = get_post($id);
        $page_name = $page->post_title;
        include HUMANE_COMPOSER_DIR . 'views/posts/show.html.php';
    }

    /**
     * Adds the duplicate link to action list for post_row_actions
     *
     * @param array  $actions
     * @param object $post
     *
     * @return array
     */
    public static function add_duplicate_post_link( $actions, $post )
    {
        $user           = wp_get_current_user();
        $role           = (array) $user->roles;
        $author_matches = $user->ID === get_post_field('post_author', $post->ID);
        if (current_user_can('edit_posts') && ( ! in_array('contributor', $role) || $author_matches ) ) {
            $actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=add_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce') . '" title="Duplicate this post" rel="permalink">Duplicate</a>';
        }
        return $actions;
    }

    /**
     * Checks if the page has been autogenerated using a script
     *
     * Mostly includes login and user profile related pages
     *
     * @param int $page_id
     *
     * @return bool True if page is autogenerated
     *              False otherwise
     */
    public static function is_login_wall_page( $page_id )
    {
        $is_login_wall_page = get_post_meta($page_id, 'humane_autogenerated_page', true);
        if ($is_login_wall_page ) {
            return true;
        }
        return false;
    }

    /**
     * Prepares relevant data posts cards in paginated view
     *
     * @param int  $post_id           ID of the post
     * @param bool $check_restriction If restriction of the post needs to be checked
     * @param bool $apply_filter      If filters need to be applied on post content
     *
     * @return array Array containing all data required for a post
     */
    public static function get_data_from_post_id( $post = array(), $check_restriction = true, $apply_filter = true )
    {
        $response_post = $post;

        // if post is empty array, prepare data of current post
        if (! is_object($post) ) {
            if (! empty($post) ) {
                $post_id = $post;
            } else {
                $post_id = get_the_ID();
            }
            $response_post = get_post($post_id);
            if (empty($post_id) ) {
                return array();
            };
        }
        $post_id = $response_post->ID;

        // set author data for cards
        $author_id                 = $response_post->post_author ?? '';
        $response_post->author_bio = get_the_author_meta('bio', $author_id);
        $response_post->avatar     = get_avatar_url($author_id);
        // $response_post->avatar     = str_replace('.jpg', '-220x220.jpg', $response_post->avatar);
        // $response_post->avatar     = str_replace('.png', '-220x220.png', $response_post->avatar);

        // set post date for cards
        $date                 = $response_post->post_date;
        $ptime                = explode(' ', $date)[1];
        $response_post->ptime = date_format(date_create($ptime), 'H:i');
        $response_post->time  = date_format(date_create($date), 'H:i');
        $date                 = explode(' ', $date)[0];
        $response_post->date  = date_format(date_create($date), 'M d, Y');
        // set post reading time
        $reading_time = Model_Posts::get_reading_time($post_id);
        if (isset($reading_time) && ! empty($reading_time) ) {
            $response_post->reading_time = $reading_time >= 1 ? $reading_time : false;
        }

        // set author name for posts cards
        $author                = get_user_by('ID', $response_post->post_author);
        $author                = Model_Users::to_s($response_post->post_author);
        $response_post->author = $author;

        // is it read post
        if (class_exists('\Humane_Acquire\Model_Login_Wall') && Model_Login_Wall::is_read_post($post_id) ) {
            $response_post->read_class = 'is-read-post';
        }

        // set card title
        $title                      = get_the_title($post_id);
        $response_post->title       = $title;
        $format                     = get_post_format($post_id);
        $response_post->format      = $format;
        $response_post->post_format = $format;
        $html                       = $response_post->post_content;
        if ($apply_filter ) {
            $response_post->post_content = apply_filters('the_content', $response_post->post_content);
        }
        $image_id             = get_post_thumbnail_id($post_id);
        $image_meta           = get_post_meta($image_id);
        $writing_format       = get_the_terms($post_id, 'writing_format');
        $category             = get_the_terms($post_id, 'category');
        $response_post->feeds = get_the_terms($post_id, 'humane_feed');

        $category_id                    = get_post_meta($post_id, 'humane_primary_category', true);
        $response_post->category        = get_cat_name($category_id);
        $response_post->category_id     = $category_id;
        $response_post->category_object = get_term($category_id, 'category');
        $response_post->category_link   = get_term_link((int) $response_post->category_id);
        if (is_wp_error($response_post->category_link) ) {
            $response_post->category_link = false;
        }
        if (is_array($category) && empty($category_id) ) {
            $response_post->category        = $category[0]->name;
            $response_post->category_id     = $category[0]->term_id;
            $response_post->category_object = $category[0];
            $response_post->category_link   = get_term_link((int) $response_post->category_id);
            if (is_wp_error($response_post->category_link) ) {
                $response_post->category_link = false;
            }
        }

        if (is_object($response_post->category_object) && property_exists($response_post->category_object, 'parent') && $response_post->category_object->parent ) {
            $response_post->category_parent = get_term($response_post->category_object->parent);
        }
        if (is_array($writing_format) ) {
            $response_post->writing_format      = $writing_format[0]->name;
            $response_post->writing_format_id   = $writing_format[0]->term_id;
            $response_post->writing_format_link = get_term_link((int) $response_post->writing_format_id, 'writing_format');
            if (is_wp_error($response_post->writing_format_link) ) {
                $response_post->writing_format_link = false;
            }
        } else {
            $response_post->writing_format = false;
        }

        if ($format === 'video' ) {
            $response_post = self::get_video_data($response_post, $html, $apply_filter);
        } else {
            $response_post = self::get_gallery_data($response_post, $html, $apply_filter);
        }
        if ($apply_filter ) {
            $html = apply_filters('the_content', $html);
        }

        // set card image
        $response_post->thumbnail        = esc_attr(get_the_post_thumbnail_url($post_id, 'thumbnail-53'));
        $response_post->image            = esc_attr(get_the_post_thumbnail_url($post_id, 'small-53'));
        $response_post->medium_featured  = esc_attr(get_the_post_thumbnail_url($post_id, 'small-53'));
        $response_post->large_featured   = esc_attr(get_the_post_thumbnail_url($post_id, 'large-53'));
        $response_post->cta_label        = get_post_meta($post_id, 'humane_cta_label', true);
        $response_post->cta_url          = get_post_meta($post_id, 'humane_cta_url', true);
        $response_post->card_interaction = get_post_meta($post_id, 'humane_post_card_interaction', true);
        if (empty($response_post->card_interaction) || $response_post->card_interaction === 'global' ) {
            $global_card_interaction = '';
            if ($response_post->post_type == 'wp_block' || $response_post->post_type == 'post' || $response_post->post_type == 'event' ) {
                $global_card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
            }
            if ($response_post->post_type == 'page' ) {
                $global_card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
            }
            if ($global_card_interaction ) {
                $response_post->card_interaction = $global_card_interaction;
            }
        }
        $response_post->preview_interaction = get_post_meta($post_id, 'humane_preview_card_interaction', true);
        if (empty($response_post->preview_interaction) || $response_post->preview_interaction === 'global' ) {
            $global_preview_interaction = '';
            if ($response_post->post_type == 'wp_block' || $response_post->post_type == 'post' || $response_post->post_type == 'event' ) {
                $global_preview_interaction = get_option('settings_content_pack_preview_card_interaction_for_posts');
            }
            if ($response_post->post_type == 'page' ) {
                $global_preview_interaction = get_option('settings_content_pack_preview_card_interaction_for_pages');
            }
            if ($global_preview_interaction ) {
                $response_post->preview_interaction = $global_preview_interaction;
            }
        }

        $response_post = Controller_Breadcrumbs::get_breadcrumbs_data($post_id, $response_post);

        $date                 = $response_post->post_date;
        $ptime                = explode(' ', $date)[1];
        $response_post->ptime = date_format(date_create($ptime), 'H:i');

        $date                = explode(' ', $date)[0];
        $response_post->date = date_format(date_create($date), 'M d, Y');

        $modified             = $response_post->post_modified;
        $mtime                = explode(' ', $modified)[1];
        $response_post->mtime = date_format(date_create($mtime), 'H:i');

        $modified                = explode(' ', $modified)[0];
        $response_post->modified = date_format(date_create($modified), 'M d, Y');

        $response_post->hide_byline       = get_post_meta($post_id, 'humane_hide_byline', true);
        $response_post->hide_cover_image  = get_post_meta($post_id, 'humane_hide_cover', true);
        $response_post->hide_updated_at   = get_post_meta($post_id, 'humane_hide_updated_at', true);
        $response_post->hide_published_at = get_post_meta($post_id, 'humane_hide_published_at', true);
        $response_post->hide_progress     = get_post_meta($post_id, 'humane_hide_progress', true);
        $response_post->url               = get_permalink($post_id);

        $response_post->posts_meta_data = get_post_meta($post_id);
        $response_post->data_connect    = $post_id;
        if ($response_post->post_type == 'aggregation' ) {
            $response_post->posts_meta_data                   = get_post_meta($post_id);
            $response_post->image                             = $response_post->posts_meta_data['humane_aggregations_featured_image'][0];
            $response_post->thumbnail                         = $response_post->posts_meta_data['humane_aggregations_featured_image'][0];
            $response_post->avatar                            = $response_post->posts_meta_data['humane_icon'][0];
            $response_post->posts_meta_data['aggregationURL'] = get_post_meta($post_id, 'humane_aggregations_redirect_to_url', true);

            $response_post->posts_meta_data['aggregationDomain'] = get_post_meta($post_id, 'humane_aggregations_domain', true);
            $author                                   = get_post_meta($post_id, 'humane_aggregations_author', true);
            $response_post->posts_meta_data['author'] = $author;
            $response_post->author                    = $author;
            $response_post->author_avatar             = false;
            $response_post->author_bio                = false;
            $agg_reading_time                         = get_post_meta($post_id, 'humane_reading_time', true);
            if (isset($agg_reading_time) && ! empty($agg_reading_time) ) {
                $response_post->reading_time = (int) $agg_reading_time >= 1 ? (int) $agg_reading_time : false;
            }
            $aggregation_date_time = get_post_meta($post_id, 'humane_aggregations_published_at', true);
            if (isset($aggregation_date_time) && $aggregation_date_time && ! empty($aggregation_date_time)) {
                $aggregation_date    = explode(' ', $aggregation_date_time)[0];
                $response_post->date = date_format(date_create($aggregation_date), 'M d, Y');
                $response_post->time = date_format(date_create($aggregation_date_time), 'H:i');
            } else {
                $response_post->date = get_the_date('M d, Y', $post_id);
                $response_post->time = get_the_date('H:i', $post_id);
            }
        }
        if ($response_post->post_type == 'page' ) {
            $response_post->reading_time = '';
        }
        $response_post->show_author = ! $response_post->hide_byline || ! $response_post->hide_updated_at || ! $response_post->hide_published_at;
        $author_url                 = sanitize_title($response_post->author);
        $response_post->author_url  = get_site_url() . "/u/$author_url";

        $response_post->is_online_event = false;

        // prepare events data
        if ($response_post->post_type === 'event' ) {
            $response_post->posts_meta_data = get_post_meta($post_id);
            $timezone                       = empty(get_post_meta($post_id, 'humane_events_timezone', true)) ? 'Asia/Kolkata' : get_post_meta($post_id, 'humane_events_timezone', true);
            $start_date                     = date_create(get_post_meta($post_id, 'humane_events_start_time', true), new \DateTimeZone($timezone));
            $end_date                       = date_create(get_post_meta($post_id, 'humane_events_end_time', true), new \DateTimeZone($timezone));
            $response_post->event_date      = Controller_Date::to_card($start_date, $end_date, $timezone);
            $response_post->event_location  = get_post_meta($post_id, 'humane_events_address', true);
            $response_post->avatar          = get_avatar_url($response_post->post_author);
            // $response_post->avatar          = str_replace('.jpg', '-220x220.jpg', $response_post->avatar);
            // $response_post->avatar          = str_replace('.png', '-220x220.png', $response_post->avatar);
            $response_post->event_data      = self::get_events_data($post_id, $response_post);
            if ($response_post->event_data['web_location_link'] ) {
                $response_post->is_online_event = true;
            }
        }
        if ($response_post->post_type === 'product' ) {
            $response_post->product_data = self::get_products_data($post_id, $response_post);
        }
        if ($response_post->post_type === 'subscription' ) {
            $response_post->subscription_data = self::get_subscriptions_data($post_id, $response_post);
        }
        $response_post->restricted   = self::check_if_restricted($post_id);
        $response_post->presentation = self::get_post_format_custom($post_id);
        if (! empty($response_post->feeds) ) {
            $response_post->avatar = get_post_meta($post_id, 'humane_avatar', true);
        }
        $response_post = apply_filters('get_data_from_post_id', $response_post);
        return $response_post;
    }
    public static function check_if_restricted( $id )
    {
        $restricted = array();
        if (class_exists('\Humane_Commerce\Model_Subscriptions') ) {
            $restricted = Model_Subscriptions::check_if_subscribed($id);
        }
        if(class_exists('\Humane_Acquire\Model_Restrict_Content')) {
            $login_restricted = Model_Restrict_Content::_validate_auto_restrict(get_post_type($id));
        }
        if (! $login_restricted ) {
            $login_restricted = Model_Playlist::check_if_restricted($id);
        }
        if (! $login_restricted ) {
            $login_restricted = get_post_meta($id, 'humane_login_restriction', true);
        }
        return $restricted['restricted'] || ( $login_restricted && ! is_user_logged_in() );
    }
    /**
     * Adds the Products data for a given ID
     *
     * @param int   $post_id
     * @param array $response_post
     *
     * @return array Respose with Products data added to it
     */
    public static function get_products_data( $post_id, $response_post )
    {
        $product_data             = array();
        $product_data['price']    = get_post_meta($post_id, 'humane_price', true);
        $product_data['currency'] = get_post_meta($post_id, 'humane_currency', true);
        $product_data['discount'] = get_post_meta($post_id, 'humane_discount', true);
        $product_data['gst']      = get_post_meta($post_id, 'humane_gst', true);

        $email_template                 = get_post_meta($post_id, 'humane_email_template_id', true);
        $email_template                 = new Model_Email_Templates($email_template);
        $product_data['download']       = $email_template->attachment_file;
        $product_data['product_id']     = $post_id;
        $product_data['specifications'] = maybe_unserialize(get_post_meta($post_id, 'humane_product_specification', true));
        $product_data['specifications'] = array_filter(
            $product_data['specifications'],
            function ( $a ) {
                if (empty($a['value']) ) {
                    return false;
                }
                return true;
            }
        );
        return $product_data;
    }

    /**
     * Adds the Subscription data for a given ID
     *
     * @param int   $post_id
     * @param array $response_post
     *
     * @return array Respose with Subscription data added to it
     */
    public static function get_subscriptions_data( $post_id, $response_post )
    {
        $subscription_data                    = array();
        $subscription_data['price']           = get_post_meta($post_id, 'humane_price', true);
        $subscription_data['currency']        = get_post_meta($post_id, 'humane_currency', true);
        $subscription_data['period']          = get_post_meta($post_id, 'humane_period', true);
        $email_template                       = get_post_meta($post_id, 'humane_email_template_id', true);
        $email_template                       = new Model_Email_Templates($email_template);
        $subscription_data['download']        = $email_template->attachment_file;
        $subscription_data['plan_id']         = get_post_meta($post_id, 'humane_sku_id', true);
        $subscription_data['subscription_id'] = $post_id;
        return $subscription_data;
    }

    /**
     * Adds the Events data for a given ID
     *
     * @param int   $post_id
     * @param array $response_post
     *
     * @return array Respose with Events data added to it
     */
    public static function get_events_data( $post_id, $response_post )
    {
        $timezone   = empty(get_post_meta($post_id, 'humane_events_timezone', true)) ? 'Asia/Kolkata' : get_post_meta($post_id, 'humane_events_timezone', true);
        $event_data = array();
        $start_date = date_create(get_post_meta($post_id, 'humane_events_start_time', true), new \DateTimeZone($timezone));
        $end_date   = date_create(get_post_meta($post_id, 'humane_events_end_time', true), new \DateTimeZone($timezone));
        if (! empty($end_date) ) {
            $event_data['text_time'] = date_format($end_date, 'M j, Y');
        } else {
            $event_data['text_time'] = date_format($start_date, 'M j, Y');
        }
        $event_data['time_ago_in_words'] = Controller_Date::to_card($start_date, $end_date, $timezone);
        $event_data['day']               = date_format($start_date, 'j');
        $event_data['year']              = date_format($start_date, 'Y');
        $event_data['day_words']         = date_format($start_date, 'l');
        $event_data['month']             = substr(strtoupper(date_format($start_date, 'M')), 0, 3);
        $event_data['time']              = date_format($start_date, 'H:i');
        $diff                            = $end_date->diff($start_date);
        if ($diff->d < 1 ) {
            if ($diff->h < 1 ) {
                $event_data['duration'] = $diff->i . ' minute(s)';
            } elseif ($diff->i < 1 ) {
                $event_data['duration'] = $diff->h . ' hour(s)';
            } else {
                $event_data['duration'] = $diff->h . ' hour(s) ' . $diff->i . ' minute(s)';
            }
        } elseif ($diff->h < 1 && $diff->i < 1 ) {
            $event_data['duration'] = $diff->d . ' day(s)';
        } elseif ($diff->h < 1 ) {
            $event_data['duration'] = $diff->d . ' day(s) ' . $diff->i . ' minute(s)';
        } elseif ($diff->i < 1 ) {
            $event_data['duration'] = $diff->d . ' day(s) ' . $diff->h . ' hour(s)';
        } else {
            $event_data['duration'] = $diff->d . ' day(s) ' . $diff->h . ' hour(s) ' . $diff->i . ' minute(s)';
        }
        if (date_format($start_date, 'M j, Y') == date_format($end_date, 'M j, Y') ) {
            $event_data['date_time'] = date_format($start_date, 'l, M j, Y') . ' at ' . date_format($start_date, 'H:i') . ' - ' . date_format($end_date, 'H:i');
        } elseif (date_format($start_date, 'Y') == current_time('Y') || date_format($end_date, 'Y') == current_time('Y') ) {
            $start_format = 'l, M j, Y, H:i';
            $end_format   = 'l, M j, Y, H:i';
            if (date_format($start_date, 'Y') == current_time('Y') ) {
                $start_format = 'l, M j, H:i';
            }
            if (date_format($end_date, 'Y') == current_time('Y') ) {
                $end_format = 'l, M j, H:i';
            }
            $event_data['date_time'] = date_format($start_date, $start_format) . ' - ' . date_format($end_date, $end_format);
        } else {
            $event_data['date_time'] = date_format($start_date, 'l, M j, Y, H:i') . ' - ' . date_format($end_date, 'l, M j, Y, H:i');
        }

        $event_data['location']      = get_post_meta($post_id, 'humane_events_address', true);
        $event_data['location_link'] = get_post_meta($post_id, 'humane_events_maps_link', true);
        $title                       = get_the_title($post_id);
        $format                      = (int) get_post_meta($post_id, 'humane_formats', true);
        $format                      = get_term_by('ID', $format, 'writing_format');
        $format_name                 = sanitize_title($format->name);
        // if ( $format ) {
        //     $file = HUMANE_CONTENT_URL . 'data/events/' . "[$format_name] " . sanitize_title( $title ) . '-' . get_current_blog_id() . "-$post_id.ics";
        // } else {
        $file = HUMANE_CONTENT_URL . 'data/events/' . sanitize_title($title) . '-' . get_current_blog_id() . "-$post_id.ics";
        // }
        $event_data['ics_link']          = $file;
        $event_data['end_date']          = $end_date->getTimestamp();
        $event_data['web_location_link'] = get_post_meta($post_id, 'humane_events_video', true);
        $event_data['timezone']          = get_post_meta($post_id, 'humane_events_timezone', true);
        $event_data['date_time_svg']     = Controller_Icons::get_svg_icons('date-time');
        $event_data['location_svg']      = Controller_Icons::get_svg_icons('location');

        $event_data['heading'] = $response_post->post_title;
        $terms                 = get_the_terms($response_post->ID, 'writing_format');
        $writing_format        = '';
        if ($terms ) {
            $writing_format = $terms[0]->name;
        }
        $event_data['format'] = $writing_format;

        return $event_data;
    }

    /**
     * Adds the video data for a given HTML
     *
     * Extracts the videos from a given HTML and adds it to the response object
     *
     * @param array  $response_post
     * @param string $html
     * @param bool   $apply_filter
     *
     * @return array Respose with video data added to it
     */
    public static function get_video_data( $response_post, $html, $apply_filter = true )
    {
        if ($apply_filter ) {
            $html = apply_filters('the_content', $html);
        }
        $domain_document = new \DOMDocument();
        $domain_document->loadHTML($html);
        if ($html ) {
            $xpath                = new \DOMXPath($domain_document);
            $src                  = $xpath->evaluate('string(//iframe/@src)');
            $response_post->video = $src;
            if (! $response_post->video ) {
                $src                        = $xpath->evaluate('string(//video/@src)');
                $response_post->video       = $src;
                $response_post->isTypeVideo = true;
            }
        }
        return $response_post;
    }

    /**
     * Adds the gallery data for a given HTML
     *
     * Extracts the galleries from a given HTML and adds it to the response object
     *
     * @param array  $response_post
     * @param string $html
     * @param bool   $apply_filter
     *
     * @return array Respose with gallery data added to it
     */
    public static function get_gallery_data( $response_post, $html, $apply_filter = true )
    {
        $post_id    = $response_post->ID;
        $post       = get_post($post_id);
        $html       = $post->post_content;
        $video_html = $html;
        if ($apply_filter ) {
            $video_html = apply_filters('the_content', $html);
        }
        if (! empty($video_html) ) {
            $doc = new \DOMDocument();
            $doc->loadHTML($video_html);
        }

        $response_post->format = 'gallery';
        if (! empty($html) ) {
            $docHTML = new \DOMDocument();
            $docHTML->loadHTML($html);
        }
        if (isset($docHTML) ) {
            $xpath       = new \DOMXPath($docHTML);
            $urlXpath    = $xpath->query('//img/@src');
            $content     = array();
            $pure_images = array();
            foreach ( $urlXpath as $key ) {
                if (! is_object($key) && empty($key) ) {
                    continue;
                }
                $src      = $key->nodeValue;
                $class    = $key->ownerElement->getAttribute('class');
                $image_id = str_replace('wp-image-', '', $class);
                $meta     = wp_get_attachment_metadata($image_id);
                if (is_array($meta) ) {
                    $width = $meta['width'];
                }
                if (isset($width) ) {
                    $image_tag = "<div style='width: {$width}px; background:var(--N300);' width='{$width}px'><img data-src='$src' class='humane-lazyload'></div>";
                } else {
                    $image_tag = "<div style='background:var(--N300);'><img data-src='$src' class='humane-lazyload'></div>";
                }
                array_push($pure_images, "<img data-src='$src' class='humane-lazyload' />");
                array_push($content, $image_tag);
            }
            $captionXpath = $xpath->query("//figure[contains(@class, 'wp-block-image')]");
            $captions     = array();
            foreach ( $captionXpath as $node ) {
                $caption = $node->getElementsByTagName('figcaption');
                $caption = $caption->item(0);
                if (is_object($caption) && $caption->ownerDocument ) {
                    $caption = $caption->ownerDocument->saveHTML($caption); // Get HTML of DOMElement.
                } else {
                    $caption = '';
                }
                array_push($captions, $caption);
            }
            $url = false;
            if (is_object($urlXpath->item(0)) ) {
                $url = $urlXpath->item(0)->nodeValue;
            }
            if (! $url ) {
                $url = esc_attr(get_the_post_thumbnail_url($post_id, 'small-53'));
            }
            if ($doc ) {
                $xpath = new \DOMXPath($doc);
            }
            $urlXpath = $xpath->query('//iframe/@src');
            foreach ( $urlXpath as $key ) {
                if (! is_object($key) && empty($key) ) {
                    continue;
                }
                $src = $key->nodeValue;
                if (strpos($src, 'youtube') !== false ) {
                    $iframe = "<iframe width='580' height='348' allow='fullscreen; picture-in-picture' frameborder='0' src='$src'></iframe>";
                    array_push($content, $iframe);
                }
            }
            $show_images = true;
            if (count($content) === 0 || ! $show_images ) {
                $response_post->image  = esc_attr(get_the_post_thumbnail_url($post_id, 'small-53'));
                $post_meta             = get_post_meta(get_post_thumbnail_id($post_id));
                $response_post->format = false;
            } else {
                $response_post->gallery          = $content;
                $response_post->gallery_captions = $captions;
                $response_post->gallery_images   = $pure_images;
            }
        }
        return $response_post;
    }

    /**
     * Returns the built-in WP post format for a post
     *
     * @param object $post
     *
     * @return string Post Format
     */
    public static function get_post_format_custom( $post = null )
    {
        $post = get_post($post);

        if (! $post ) {
            return false;
        }

        if (! post_type_supports($post->post_type, 'post-formats') ) {
            return false;
        }

        $_format = get_the_terms($post->ID, 'post_format');

        if (empty($_format) ) {
            return false;
        }

        $format = reset($_format);
        return str_replace('post-format-', '', $format->slug);
    }
    /**
     * Add rewrite rule for wp block.
     *
     * @return void
     */
    public static function add_rewrite_rule_for_wp_block()
    {
        add_rewrite_rule('evergreen-notes/([0-9]+)?$', 'index.php?post_type=wp_block&p=$matches[1]', 'top');
    }
    /**
     * Add post id in wp block url.
     *
     * @param string $post_link post url
     * @param object $post      post object
     *
     * @return string Post url
     */
    public static function add_post_id_in_wp_block( $post_link, $post )
    {
        // if ( $post && 'wp_block' === $post->post_type ) {
        //     return str_replace( basename( $post_link ), $post->ID, $post_link );
        // }
        return $post_link;
    }
    /**
     * Registers the custom column for Pages.
     *
     * @param array $columns Existing columns.
     *
     * @return array Columns with custom column added.
     */
    public static function humane_remove_columns( $columns )
    {
        unset($columns['wpseo-score']);
        unset($columns['wpseo-title']);
        unset($columns['wpseo-linked']);
        unset($columns['wpseo-metadesc']);
        unset($columns['wpseo-focuskw']);
        unset($columns['wpseo-links']);
        unset($columns['wpseo-score-readability']);
        return $columns;

    }
    /**
     * Get reusable instance.
     *
     * @param int $reuseable_post_id reuseable post id.
     *
     * @return array All instances of reusable block.
     */
    public static function get_reusable_instance( $reuseable_post_id )
    {
        write_log($reuseable_post_id, "reuseable_post_id");
        global $wpdb;
        $tag             = '<!-- wp:block {"ref":' . $reuseable_post_id . '}';
        $search          = '%' . $wpdb->esc_like($tag) . '%';
        $current_post_id = get_the_ID();
        if (! empty($current_post_id) ) {
            $instances = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts WHERE ID!=$current_post_id and post_content LIKE '$search' and post_type NOT IN ('revision', 'attachment', 'nav_menu_item')");
            write_log($instances, "instances of posts");
            return $instances;
        }
        return $instances = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts WHERE post_content LIKE '$search' and post_type NOT IN ('revision', 'attachment', 'nav_menu_item')");
    }

    public static function get_post_references($response_post_id)
    {
        //$list_of_references = array();
        global $wpdb;
        // $object_table = $wpdb->prefix . 'posts';
        // $references  = new Model_Backlinks(
        //     array(
        //         'object_table' => $object_table,
        //         'object_id'    => $response_post_id,
        //     )
        // );
        // $referenced_in = maybe_unserialize( $references->referenced_in );
        // foreach($referenced_in as $ref) {
        //     array_push($list_of_references, array(
        //         'ref_id'   => $ref,
        //         'ref_title' => get_the_title($ref),
        //         'ref_link' => get_the_permalink($ref)
        //     ));
        // };

        //find call category pages where post is used
        // $category = get_the_terms( $response_post_id, "category" );
        // foreach($category as $cat){
        //     // $category_link = get_term_link((int)$category_id);
        //     // if(is_wp_error($category_link)) $category_link = false;
        //     array_push($list_of_references, array(
        //         'ref_id'   => $cat->term_id,
        //         'ref_title' => $cat->name,
        //         'ref_link' => get_term_link((int)$cat->term_id)
        //     ));
        // }


        $link_index_results = Model_Link_Juicer::get_links_for_single_post($response_post_id);
    
        foreach($link_index_results as $ref){
            unset($ref->id, $ref->anchor);
        }
        write_log($link_index_results, "link_index_results");
        $link_index_results = array_unique($link_index_results, SORT_REGULAR);

        $link_index_results = array_filter(
            $link_index_results, function ($refs) {
                if(get_the_title($refs->link_from) !== 'Uncategorized') {
                    return $refs;
                }
            }
        );

        // foreach($link_index_results as $link){
        //     array_push($list_of_references, array(
        //         'ref_id'   => $link->link_from,
        //         'ref_title' => get_the_title($link->link_from),
        //         'ref_link' => get_the_permalink($link->link_from)
        //     ));
        // }

        $count_instances = count($link_index_results);
        ob_start();
        ?>
        <?php
        if ($count_instances === 0 ) {
            echo '<div class="hc-re-grey">None yet</div>';
        } else {
            ?>
            <div class="views-container hc-px-0 hc-mx-20-mobile ">
            <div data-id="0" class="view-container ">
                <div class="inner-view-container">
                    <div class="automate-grid dashboard-grid1">
                        <?php
                        foreach ( $link_index_results as $instance ) {
                            $current_post_id  = $instance->link_from;
                            $author_id        = get_post_field('post_author', $current_post_id);
                            $currnt_post_type = get_post_type($current_post_id);
                            $card_interaction = get_post_meta($current_post_id, 'humane_post_card_interaction', true);
                            if (empty($card_interaction) || $card_interaction === 'global' ) {
                                if ($currnt_post_type === 'post' || $currnt_post_type === 'event' ) {
                                    $card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
                                }
                                if ($currnt_post_type === 'page' ) {
                                    $card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
                                }
                            }
                            // $restricted = Controller_Posts::check_if_restricted( $current_post_id );
                            if (( $card_interaction === 'modal') || ( $card_interaction === 'modal' && $currnt_post_type === 'event' ) ) {
                                $intrection_class = 'hc-modal-trigger';
                            } else {
                                $intrection_class = '';
                            }
                            $new_tab = false;
                            if ($currnt_post_type === 'aggregation' ) {
                                $new_tab = true;
                            }
                            if (empty($intrection_class) ) {
                                ?>
                            <a class="" href="<?php echo get_the_permalink($current_post_id); ?>" <?php echo $new_tab ? "target='_blank'" : ''; ?>>
                                <?php
                            }
                            ?>
                            <?php if(!empty($intrection_class)) : ?>
                        <div data-connect="<?php echo $current_post_id; ?>" class="<?php echo esc_attr($intrection_class); ?> hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">
                        <?php else: ?>
                        <div class="hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">    
                        <?php endif; ?>
                        <div class="automate-grid-element hc-border-rounded-top-12 pykslice-image-container hc-row-4">
                            <?php
                            $image = get_the_post_thumbnail_url($current_post_id, 'small-53');

                            if (! empty($image) ) :
                                ?>
                            <img class="automate-grid-element-img hc-border-rounded-top-12 hc-width-fit-container hc-cover hc-border-top-brightness-86 hc-border-bottom-brightness-86" src="<?php echo $image; ?>" />
                        <?php else : ?>
                            <div class="hc-width-fit-container hc-bg-brightness-93 hc-row-4"></div>
                        <?php endif; ?>
                        </div>
                        <div class="gallery-info hc-m-12">
                            <div data-lines="3" class="gallery_card_headline hc-truncate pykslice-card-title hc-mb-16" style="overflow: hidden; text-overflow: ellipsis; -webkit-box-orient: vertical; display: -webkit-box; -webkit-line-clamp: 3;">
                                <?php echo get_the_title($current_post_id); ?>          
                            </div>
                            <div class="gallery_sub-title-container sub-title-container hc-sub-title-container">
                                <div class="gallery_date_time">
                                <div class="hc-one-line hc-supernormal-xs"><?php echo get_the_date('M j, Y', $current_post_id); ?></div>
                                </div>
                                <div class="hc-card-footer aggregations_icon_and_domain">
                                <div class="aggregations_icon"><img class="hc-dp-xs hc-flex-no-shrink" src="<?php echo get_avatar_url($author_id); ?>"></div>
                                <div class="hc-one-line hc-supernormal-xs hc-gallery-author-info">By <?php echo get_the_author_meta('display_name', $author_id); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                            <?php
                            if (empty($intrection_class) ) {
                                echo '</a>';
                            }
                            ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            </div>
            <?php
        }
        $preview_markup = ob_get_clean();
        return $preview_markup;
        
        //return $list_of_references;
    }
    
    public static function get_reusable_link_to_this_card( $response_post_id )
    {
        $instances       = self::get_reusable_instance($response_post_id);
        $count_instances = count($instances);
        ob_start();
        ?>
        <h3>Links to this Evergreen Note</h3>
        <?php
        if ($count_instances === 0 ) {
            echo '<div class="hc-re-grey">None yet</div>';
        } else {
            ?>
            <div class="hc-fy">
            <div class="hc-fx hc-justify-content-end">
            <div class="hc-show-540">
                <div class="hc-filter-overlay"></div>
                <div class="dashboard-header topbar perspectives-dashboard-header hc-mobile-filters hc-side-header main-dropdown ">
                    <div class="hc-fx hc-flex-between hc-m-20 hc-mb-0 hc-show-540">
                    <div class="hc-filters-title">Filters</div>
                    <div class="hc-mobile-close">
                        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="28" height="28" rx="14" fill="#1A1A1A"></rect>
                            <path d="M21 8.41L19.59 7L14 12.59L8.41 7L7 8.41L12.59 14L7 19.59L8.41 21L14 15.41L19.59 21L21 19.59L15.41 14L21 8.41Z" fill="white"></path>
                        </svg>
                    </div>
                    </div>
                    <input type="hidden" value="Humane_Sites\Controller_Dashboard::build_perspective_json" class="controller-class">
                </div>
            </div>
            </div>
            <div class="views-container hc-px-0 hc-mx-20-mobile ">
            <div data-id="0" class="view-container ">
                <div class="inner-view-container">
                    <div class="automate-grid dashboard-grid1">
                        <?php
                        foreach ( $instances as $instance ) {
                            $current_post_id  = $instance->ID;
                            $author_id        = get_post_field('post_author', $current_post_id);
                            $currnt_post_type = get_post_type($current_post_id);
                            $card_interaction = get_post_meta($current_post_id, 'humane_post_card_interaction', true);
                            if (empty($card_interaction) || $card_interaction === 'global' ) {
                                if ($currnt_post_type === 'post' || $currnt_post_type === 'event' ) {
                                    $card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
                                }
                                if ($currnt_post_type === 'page' ) {
                                    $card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
                                }
                            }
                            $restricted = Controller_Posts::check_if_restricted($current_post_id);
                            if (( $card_interaction === 'modal' && ! $restricted ) || ( $card_interaction === 'modal' && $currnt_post_type === 'event' ) ) {
                                $intrection_class = 'hc-modal-trigger';
                            } else {
                                $intrection_class = '';
                            }
                            $new_tab = false;
                            if ($currnt_post_type === 'aggregation' ) {
                                $new_tab = true;
                            }
                            if (empty($intrection_class) ) {
                                ?>
                            <a class="" href="<?php echo get_the_permalink($current_post_id); ?>" <?php echo $new_tab ? "target='_blank'" : ''; ?>>
                                <?php
                            }
                            ?>
                            <?php if(!empty($intrection_class)) : ?>
                        <div data-connect="<?php echo $current_post_id; ?>" class="<?php echo esc_attr($intrection_class); ?> hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">
                        <?php else: ?>
                        <div class="hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">    
                        <?php endif; ?>
                        <div class="automate-grid-element hc-border-rounded-top-12 pykslice-image-container hc-row-4">
                            <?php
                            $image = get_the_post_thumbnail_url($current_post_id, 'small-53');

                            if (! empty($image) ) :
                                ?>
                            <img class="automate-grid-element-img hc-border-rounded-top-12 hc-width-fit-container hc-cover hc-border-top-brightness-86 hc-border-bottom-brightness-86" src="<?php echo $image; ?>" />
                        <?php else : ?>
                            <div class="hc-width-fit-container hc-bg-brightness-93 hc-row-4"></div>
                        <?php endif; ?>
                        </div>
                        <div class="gallery-info hc-m-12">
                            <div data-lines="3" class="gallery_card_headline hc-truncate pykslice-card-title hc-mb-16" style="overflow: hidden; text-overflow: ellipsis; -webkit-box-orient: vertical; display: -webkit-box; -webkit-line-clamp: 3;">
                                <?php echo $instance->post_title; ?>          
                            </div>
                            <div class="gallery_sub-title-container sub-title-container hc-sub-title-container">
                                <div class="gallery_date_time">
                                <div class="hc-one-line hc-supernormal-xs"><?php echo get_the_date('M j, Y', $current_post_id); ?></div>
                                </div>
                                <div class="hc-card-footer aggregations_icon_and_domain">
                                <div class="aggregations_icon"><img class="hc-dp-xs hc-flex-no-shrink" src="<?php echo get_avatar_url($author_id); ?>"></div>
                                <div class="hc-one-line hc-supernormal-xs hc-gallery-author-info">By <?php echo get_the_author_meta('display_name', $author_id); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                            <?php
                            if (empty($intrection_class) ) {
                                echo '</a>';
                            }
                            ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <?php
        }
        $preview_markup = ob_get_clean();
        return $preview_markup;
    }
    public static function get_reusable_content( $reuseable_post_id )
    {
        $reuseable_title           = get_the_title($reuseable_post_id);
        $instances                 = self::get_reusable_instance($reuseable_post_id);
        $block_content             = apply_filters('the_content', get_post_field('post_content', $reuseable_post_id));
        $count_instances           = count($instances);
        $author_id                 = get_post_field('post_author', $reuseable_post_id);
        $author_name               = get_the_author_meta('display_name', $author_id);
        $hc_reusable_top_content   = "<div class='hc-fx hc-re-column-gap-10 hc-text-brightness-46 hc-supernormal-s hc-reusable-top-content'>";
        $hc_reusable_top_content  .= sprintf(
            '<a class="hc-flex-no-shrink"  href="%s"><img src="%s" class="hc-border-radius-50" width="20px" height="20px"></a>',
            get_author_posts_url($author_id),
            get_avatar_url($author_id, ['size' => '20'])
        );
        $hc_reusable_top_content .= '<div class="hc-fy hc-flex-evenly">';
        $hc_reusable_top_content .= '<div class="hc-fx hc-re-column-gap-10 hc-flex-wrap">';
        $hc_reusable_top_content .= '<div class="hc-supernormal-s">';
        $hc_reusable_top_content  .= sprintf(
            '<a class="hc-text-primary-main hc-re-text-color" href="%s">%s</a>',
            get_author_posts_url($author_id),
            ucwords($author_name)
        );
        $hc_reusable_top_content .= '</div>';
        if(!empty($reuseable_title)) {
            $hc_reusable_top_content .= '<div class="hc-reusable-dot"></div>';
            $hc_reusable_top_content .= sprintf(
                '<a href=" ' . get_the_permalink($reuseable_post_id). '"> %s</a>',
                $reuseable_title
            );
        }
        $hc_reusable_top_content .= '<div class="hc-reusable-dot"></div>';
        $hc_reusable_top_content .= '<div class="hc-re-text-color hc-supernormal-s hc-mr-20 hc-no-wrap">';
        $hc_reusable_top_content .= get_the_date('M j, Y', $reuseable_post_id);
        $hc_reusable_top_content .= '</div>';
        $hc_reusable_top_content .= '</div>';
        $hc_reusable_top_content .= '</div>';
        $hc_reusable_top_content .= '</div>';
        $card_for_append          = "<span class='hc-supernormal-s'>";
        if ($count_instances > 0 ) {
            $share_img           = sprintf('<img class="hc-mr-8 hc-size-20" src="%s"/><span class="hc-text-brightness-46">%s</span>', get_parent_theme_file_uri() . '/assets/core/images/arrow.png', $count_instances);
            $card_for_append    .= " <a class='hc-reusable-block-a'> " . $share_img . '</a>';
            $reuseable_dropdown  = "<div class='hc-reusable-block-dropdown'><div>";
            $reuseable_dropdown .= "<span class='hc-mb-20 hc-p-8 hc-text-brightness-46'>Linked references</span>";
            $count               = 0;
            foreach ( $instances as $instance ) {
                $reuseable_dropdown .= '<div class="reusable_instance_paragraph"><span class="hc-reusable-dot"></span><a target="_blank" href="' . get_permalink($instance->ID) . '">' . $instance->post_title . '</a></div>';
                $count++;
            }
        }
        $reuseable_dropdown .= '</div></div>';
        $card_for_append    .= '</span>';
        $content_for_append  = $card_for_append;
        $resuable_color = Model_Options::get_styling_option('reusable_block_color');
        if (! empty($reuseable_dropdown) ) {
            $content_for_append .= $reuseable_dropdown;
        }
        $content_for_append = "<div class='hc-reusable-block-container'>$content_for_append</div>";
        $block_content1     = '<div class="hc-p-20 hc-re-border hc-mb-20" style="background-color:'. $resuable_color .'">';
        $block_content1    .= "$hc_reusable_top_content<div class='hc-reusable-middle-content hc-my-20 hc-border-rounded-8 hc-text-color-black' style='background-color:$resuable_color'> $block_content $reusable_bottom_title_content </div>";
        $total_item         = '';
        if ($count_instances > 0 ) {
            $total_item = $content_for_append;
        }
        $share_link      = "<div class='hc-ml-20'><span class='humane_clip_to_board'>";
        $share_link     .= sprintf('<img class="hc-mr-8 hc-size-20" src="%s"/>', get_parent_theme_file_uri() . '/assets/core/images/link.png');
        $share_link     .= "<span class='hc-text-primary-main hc-taxt-copy'>Copied</span></span>";
        $share_link     .= sprintf("<span class='clip_to_board_a' style='display:none'>%s</span>", get_the_permalink($reuseable_post_id), get_the_permalink($reuseable_post_id));
        $share_link     .= '</div>';
        $block_content1 .= "<div class='hc-fy hc-flex-evenly'><div class='hc-fx hc-reusable-bottom-content'>$total_item $share_link</div></div>";
        $block_content1 .= '</div>';
        return $block_content1;
    }
    /**
     * Add capabilities to author, so he/she can edit other author page also.
     *
     * @param int $reuseable_post_id reuseable post id.
     *
     * @return void
     */
    public static function humane_add_author_caps()
    {
        // Get the author role.
        $role = get_role('author');
        $role->add_cap('edit_pages');
        $role->add_cap('edit_published_pages');
        $role->add_cap('edit_posts'); 
        $role->add_cap('edit_others_posts');  
        $role->add_cap('publish_posts');  
        $role->add_cap('publish_pages');  
        $role->add_cap('edit_published_posts');   
        $role->add_cap('edit_published_pages');       
    }
    /**
     * Get img alt tag.
     *
     * @param int $thumb_id thumb id.
     *
     * @return img_alt img alt tag.
     */
    public static function humane_get_alt_tag($post_id)
    {
        if('aggregation' === get_post_type($post_id)) {
            $img_url = get_post_meta($post_id, 'humane_aggregations_featured_image', true);
            $cache_name  = md5('img_attachment_id'.$img_url);
            $cache_group = 'group_img_attachment_id';
            $thumb_id  = wp_cache_get($cache_name, $cache_group);
            if (empty($thumb_id) ) {
                $thumb_id = attachment_url_to_postid($img_url);
                wp_cache_set($cache_name, $thumb_id, $cache_group, 60 * HOUR_IN_SECONDS);
            }    
        } else{
            $thumb_id = get_post_thumbnail_id($post_id);
        }
        if(0 === $thumb_id ) {
            $img_alt = get_the_title($post_id);
            $img_alt = preg_replace('%\s*[-_\s]+\s*%', ' ',  $img_alt);
            // Sanitize the title:  capitalize first letter of every word (other letters lower case):
            $img_alt = ucwords(strtolower($img_alt));
            return $img_alt;
        }
        $img_alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
        if(!empty($img_alt)) {
            return $img_alt;
        }
        $img_alt = get_post($thumb_id)->post_title;
        // Sanitize the title:  remove hyphens, underscores & extra spaces:
        $img_alt = preg_replace('%\s*[-_\s]+\s*%', ' ',  $img_alt);
        // Sanitize the title:  capitalize first letter of every word (other letters lower case):
        $img_alt = ucwords(strtolower($img_alt));
        return $img_alt;
    }
    /**
     * Get img alt tag by url.
     *
     * @param int $img_url img url.
     *
     * @return img_alt img alt tag.
     */
    public static function humane_get_alt_tag_by_url($img_url)
    {
        $cache_name  = md5('img_attachment_id'.$img_url);
        $cache_group = 'group_img_attachment_id';
        $thumb_id  = wp_cache_get($cache_name, $cache_group);
        if (empty($thumb_id) ) {
            $thumb_id = attachment_url_to_postid($img_url);
            wp_cache_set($cache_name, $thumb_id, $cache_group, 60 * HOUR_IN_SECONDS);
        }
        if(0 === $thumb_id) {
            $img_alt = basename($img_url);
            $img_alt = substr($img_alt, 0, strpos($img_alt, "-"));

            // Sanitize the title:  capitalize first letter of every word (other letters lower case):
            $img_alt = ucwords(strtolower($img_alt));
            return $img_alt;
        }
        $img_alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
        if(!empty($img_alt)) {
            return $img_alt;
        }
        $img_alt = get_post($thumb_id)->post_title;
        // Sanitize the title:  remove hyphens, underscores & extra spaces:
        $img_alt = preg_replace('%\s*[-_\s]+\s*%', ' ',  $img_alt);
        // Sanitize the title:  capitalize first letter of every word (other letters lower case):
        $img_alt = ucwords(strtolower($img_alt));
        return $img_alt;
    }
    /**
     * Skip lazy loading image by alt tag.
     *
     * @param string $value   lazy.
     * @param string $image   current image.
     * @param string $context current context.
     *
     * @return $value lazy loaded or not.
     */
    public static function skip_lazy_loading_image_by_alt( $value, $image, $context )
    {
        if ('the_content' === $context ) {
            if (false !== strpos($image, ' alt="hcskiplazy"') ) {
                return false;
            }
        }
        return $value;
    }
    /**
     * Dynamic js based on content.
     *
     * @param string $content post content.
     *
     * @return $content post content.
     */
    public static function hc_dynamic_js_based_on_content($content)
    {
        if (stristr($content, 'datawrapper.dwcdn') ) {
            wp_enqueue_script('datawrapper-dwcdn', 'https://datawrapper.dwcdn.net/lib/embed.min.js', array(), md5_file('https://datawrapper.dwcdn.net/lib/embed.min.js'));
        }
        if (stristr($content, 'hc_datasheet_enabled') ) {
            wp_enqueue_style('humane-evidence-handsontable-css', 'https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css', array(), 1.1, false);
        }
        // if( stristr( $content, 'hc-contact-form-wrapper' ) ){
        wp_enqueue_script('flatpickr.min.js');
        wp_enqueue_style('flatpickr.min.css');
        // }
        return $content;
    }
    public static function dequeue_unused_scripts()
    {
        wp_dequeue_style('ct_public_css');
    }
    public static function hc_flush_cache_for_admin()
    {
        wp_cache_flush();
    }
    /**
     * To address the issue of images loading after the first interaction, which resulted in Googlebot being unable to crawl the images, the solution is to skip the WP Meteor enabled script when the post format is set to 'gallery'.
     * 
     * @param  bool $value The original value indicating whether WP Meteor script is enabled.
     * @return bool The modified value indicating whether WP Meteor script should be skipped.
     */
    public static function skip_wpmeteor_enabled_script($value)
    {
        global $post;
        if ($post && get_post_format($post->ID) === 'gallery' ) {
            return false;
        }
        return $value;
    }
}
Controller_Posts::init();
