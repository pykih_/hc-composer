<?php 

namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

use stdClass;

/**
 * Handles all logic for custom breadcrumbs
 * 
 * @package Humane Sites
 * @subpackage Breadcrumbs
 */
class Controller_Breadcrumbs{
    
    /**
     * Creates the custom breadcrumb for a given post.
     * 
     * For a given post, if user has enabled custom breadcrumbs and given
     * a template for it in the Post Sidebar Settings, this function is used to calculate
     * the value of the breadcrumb. 
     * 
     * @param int $post_id Post ID for which custom breadcrumb is required
     * @param array $response_post Response Object of the post
     * 
     * @return array Modified Response Object of the post with breadcrumb info
     *               attached to it
     */
    public static function get_breadcrumbs_data($post_id, $response_post){
        
        $breadcrumb_settings = get_post_meta( $post_id );
        // print_r($breadcrumb_settings);
        $enable_breadcrumb = (array_key_exists('humane_show_breadcrumb', $breadcrumb_settings) && !empty( $breadcrumb_settings["humane_show_breadcrumb"][0] ) ) ?? false;
        $response_post->has_breadcrumb = (function_exists( 'yoast_breadcrumb')  &&  $enable_breadcrumb) ? "breadcrumb" : "no-breadcrumb";
        $response_post->enable_breadcrumb = $enable_breadcrumb;
        
        if($enable_breadcrumb){
            $custom_breadcrumb = $breadcrumb_settings["humane_custom_breadcrumb"][0];
            $arr = explode("[",$custom_breadcrumb);
            $custom_breadcrumb_final = array();
            foreach ($arr as $custom_brd) {
                if($custom_brd){
                    $custom_brd = explode("]",$custom_brd)[0];
                    $menu_arr = explode("-",$custom_brd);
                    $type = $menu_arr[0];
                    //in case when id is specified eg: [post-12]
                    if(sizeof($menu_arr) > 1){
                        // in case [cat-1] or [tag-1]
                        if($type == "cat" || $type == "tag"){
                            $id = $menu_arr[1];
                            //create breadcrumb by id and type
                            $temp_breadcrumb = self::get_tag_category_breadcrumb($id, $type);
    
                            if(!empty($temp_breadcrumb)) array_push($custom_breadcrumb_final, $temp_breadcrumb);
                        }
                        // in case [post-1] or [page-1]
                        elseif($type == "post" || $type == "page"){
                            //create breadcrumb by id
                            $post_number = $menu_arr[1];
                            $custom_breadcrumb_post = get_post($post_number);
                            $title = $custom_breadcrumb_post->post_title;
                            $frontpage_id = get_option( 'page_on_front' );
                            if($custom_breadcrumb_post->ID == $frontpage_id) $title = "Home";
                            $temp = (object) array('name' => $title, 'link' => get_permalink($custom_breadcrumb_post));
                            array_push($custom_breadcrumb_final, $temp);
                        }
                    }
                    //in case when id is not specified eg: [post]
                    else{
                        // in case [cat] or [tag]
                        if($type == "cat" || $type == "tag"){
                            //get category/tags associated with post/page
                            if($type == "cat")
                                $terms = get_the_terms(get_the_ID(), "category");
                            if($type == "tag")
                                $terms = get_the_terms(get_the_ID(), "post_tag");
                            if(!empty($terms)){
                                //create breadcrumb by id and type
                                $id = $terms[0]->term_id;
                                $temp_breadcrumb = self::get_tag_category_breadcrumb($id, $type);
    
                                if(!empty($temp_breadcrumb))
                                    array_push($custom_breadcrumb_final, $temp_breadcrumb);
                            }
                        }
                        // in case [post] or [page]
                        elseif($type == "post" || $type == "page"){
                            //create breadcrumb by id and type
                            $post_number = get_the_ID();
                            $custom_breadcrumb_post = get_post($post_number);
                            if($custom_breadcrumb_post){
                                if($custom_breadcrumb_post->post_type == "post"){
                                    $temp = (object) array('name' => $custom_breadcrumb_post->post_title, 'link' => get_permalink($custom_breadcrumb_post));
                                    array_push($custom_breadcrumb_final, $temp);
                                }else{
                                    $name_link_object = self::get_page_breadcrumb_link($post_number);
                                    array_push($custom_breadcrumb_final, $name_link_object);
                                }
                            }
                        }
                        elseif($type == "home"){
                            $temp = (object) array('name' => "Home", 'link' => get_site_url());
                            array_push($custom_breadcrumb_final, $temp);
                        }
                        //in case of text eg: [text]
                        else{
                            $temp = (object) array('name' => $custom_brd,
                            'link' => "");
                            array_push($custom_breadcrumb_final, $temp);
                        }
                    }
                }
            }
            $response_post->custom_breadcrumb_final = $custom_breadcrumb_final;
            if($custom_breadcrumb) $response_post->has_breadcrumb = count($response_post->custom_breadcrumb_final) > 0 ? "breadcrumb" : "no-breadcrumb";
        }
        return $response_post;
    }

    /**
     * Gets the name of the term and permalink of the term
     * 
     * @param int $id ID of the term
     * @param string $type Taxonomy of the term
     * 
     * @return array Breadcrumb information for a category / tag
     */
    public static function get_tag_category_breadcrumb($id, $type){
		if($type == "tag") $term = get_term_by('id', $id, 'post_tag');
		if($type == "cat") $term = get_term_by('id', $id, 'category');
		$breadcrumb = (object) array('name' => $term->name, 'link' => get_term_link($term));
		return $breadcrumb;
    }
    
    /**
     * Gets the post title and permalink of a given page
     * 
     * For the homepage, post title is always returns as "Home"
     * 
     * @param int $post_id ID of the page
     * 
     * @return array Breadcrumb info for a given page 
     */
    public static function get_page_breadcrumb_link($post_id){
        $post = get_post($post_id);
        $heading = $post->post_title;
        $frontpage_id = get_option( 'page_on_front' );
        if($post_id == $frontpage_id) $heading = "Home";
        $name_link_object = (object) array('name' => $heading, 'link' => get_permalink($post_id));
        return $name_link_object;
    }
}