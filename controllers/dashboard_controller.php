<?php

namespace Humane_Sites;

use Humane_Spaces\Model_Playlist;
use Humane_Spaces\Controller_Automated_Pages;
use Humane_Acquire\Model_Forms;

if (!defined('ABSPATH')) exit();

/**
 * Contains all logic and hooks related to dashboards
 * 
 * @package Humane Sites
 * @subpackage Dashboard
 */
class Controller_Dashboard{
    /**
	 * @var array Holds the entire perspective JSON for an automated page
	 */
	public static $perspectives_json = array();

	/**
	 * @var string Holds the taxonomy value for which automated block needs to be generated
	 */
	public static $taxonomy;

	/**
	 * @var string Holds the term ID for which automated block needs to be generated
	 */
	public static $term_id;

    /**
     * Attaches all hooks related to dashboards
     * 
     * @return void
     */
    public static function init()
    {
        add_action('init', array(__CLASS__, 'hc_add_image_sizes') );
		add_action('wp_head', array(__CLASS__, 'initial_perspectives') );
        add_filter('admin_body_class', array(__CLASS__, 'set_admin_page_as_body_class'));
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_admin_scripts_styles'));
        add_action('wp_enqueue_scripts', array(__CLASS__, 'enqueue_frontend_scripts_styles'));
        add_action('upload_mimes', array(__CLASS__, 'add_file_types_to_uploads'));
        add_filter('recovery_mode_email', array(__CLASS__, 'recovery_email_update'), 10, 2);
        /* Admin Script to generate mandatory pages for the site if they are missing */
        add_action('admin_init', array( __CLASS__, 'generate_pages_from_json' ));
        add_action('admin_menu', array(__CLASS__, "create_admin_pages"));
        add_action('admin_bar_menu', array(__CLASS__, 'add_toolbar_items'), 100);
        add_action('wp_ajax_pykih_sort_view', array( __CLASS__, 'ajax_sort_list_view' ) );
        add_action('wp_ajax_nopriv_pykih_sort_view', array( __CLASS__, 'ajax_sort_list_view' ) );
        add_action('admin_init', array(__CLASS__, 'humane_generate_csv') );
       // add_filter('wp_preload_resources', array(__CLASS__, 'hc_add_preload_resources'));
        add_filter('wp_calculate_image_sizes', array(__CLASS__, 'hc_content_image_sizes_attr'), 10, 2 );
        add_filter('intermediate_image_sizes_advanced', array(__CLASS__, 'remove_default_image_sizes') );
    }

    
    /**
     * AJAX Handler for sorting / filtering a list view
     * 
     * @return void
     * 
     * @todo Change method name to reflect the function it does
     */
    public static function ajax_sort_list_view() {
        if ( ! wp_verify_nonce( $_POST['nonce'], 'pykih_sort_view' ) ) {
            echo $response;
            wp_die();
        }
        if(empty($_POST["controller"])){
            $func = "self::build_perspective_json";
        }else{
            $func = stripslashes($_POST["controller"]);
        }
        $attributes = $_POST["hidden_values"]["extra_attributes"];
        $playlist = new Model_Playlist($attributes["playlist_id"]);
 
        $view_options = $playlist->view_options;
        if(is_string($view_options)) $view_options = json_decode($playlist->view_options, true);
        if(get_option("humane_use_default_views") === "on" && $view_options["override_settings"] !== "yes"){
            $attributes["show_filters"] = get_option("humane_default_filters_sidebar") === "on";
            $attributes["refresh_dashboard"] = get_option("humane_default_refresh_dashboard") === "on";
        }
        $filter_side = filter_var($attributes["show_filters"], FILTER_VALIDATE_BOOLEAN);
        $hide_filter_counts = filter_var($attributes["hide_filter_counts"], FILTER_VALIDATE_BOOLEAN);
        $refresh_dashboard = filter_var($attributes["refresh_dashboard"], FILTER_VALIDATE_BOOLEAN);
        $perspectives_json = $func($_POST["type"], false, 20, true, $attributes);
        $rows = $perspectives_json['data'];
        $id = $perspectives_json["id"];
        $response = array(
            'status' => 'ok'
        );
        $selected_view = $_POST["view"] ?? "0";
        $items_per_page = $_POST["hidden_values"]["items_per_page"];
        ob_start();
        require HUMANE_COMPOSER_DIR.'views/dashboard/views.html.php';
        $response["html"] = ob_get_clean();
        $postfix = "";
        if($refresh_dashboard){
            ob_start();
            require HUMANE_COMPOSER_DIR.'views/dashboard/filter.html.php';
            $response["filters_html"] = ob_get_clean();
            $postfix = "-mobile";
            ob_start();
            require HUMANE_COMPOSER_DIR.'views/dashboard/filter.html.php';
            $response["filters_mobile_html"] = ob_get_clean();
            $response["filters_data"] = $perspectives_json["filters"];
        }
        $response["rows"] = $rows;
        $response["count"] = $perspectives_json["filters"]["filtered_count"];
        $response["total_count"] = $perspectives_json["filters"]["total_count"];
        wp_send_json($response);
        return;
    }

	/**
     * Sets the instance variables based on the current page.
     * 
     * Checks the URL of the current page and sets the values for taxonomy and
     * term ID and renders the script tag required for automated pages to work
     * 
     * @todo Move this to Humane Sites so that it can be used even without
     *       the Humane Spaces plugin
     * 
	 * @return void
     * 
     * @uses Controller_Dashboard::build_perspective_json()
     * @uses Controller_Dashboard::$taxonomy
     * @uses Controller_Dashboard::$term_id
     * 
     * @see Controller_Dashboard::init() - Used By
	 */
	public static function initial_perspectives(){
        global $wp_query;
        $final_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $final_url = str_replace(get_option("siteurl"), "", $final_url);
        $check = $final_url;
        $check = rtrim($check, '/') . '/';
        $check = explode("/", $check);
        $check = $check[1];
        $check = explode("?", $check)[0];
        $check_page = explode("/", $final_url);
        $check_page = $check_page[count($check_page) - 1];
        $check_page = explode("?", $check_page)[0];
        if($check !== "reading-list"  && $check !== "what-all-did-you-read"  && $check !== "wish-list"  && $check !== "orders"  && $check !== "subscriptions" && $check !== "search") return;
        if($check === "orders"){
            self::$taxonomy = "orders";
            self::$term_id = get_current_user_id();
        }elseif($check === "search"){
            self::$taxonomy = "search";
            self::$term_id = $_POST["search"] ?? $_GET["search"];
        }elseif($check === "subscriptions"){
            self::$taxonomy = "subscriptions";
            self::$term_id = get_current_user_id();
        }elseif($check === "reading-list"){
            self::$taxonomy = "reading-list";
            self::$term_id = get_current_user_id();
        }elseif($check === "what-all-did-you-read"){
            self::$taxonomy = "what-all-did-you-read";
            self::$term_id = get_current_user_id();
        }elseif($check === "wish-list"){
            self::$taxonomy = "wish-list";
            self::$term_id = get_current_user_id();
        }
        $ajaxurl = admin_url( 'admin-ajax.php' );
		$ajaxurl = is_ssl() ? str_ireplace( 'http://', 'https://', $ajaxurl ) : str_ireplace( 'https://', 'http://', $ajaxurl );
		if(defined("ICL_LANGUAGE_CODE") && !empty(ICL_LANGUAGE_CODE)){
            $ajaxurl = $ajaxurl . '?lang=' . ICL_LANGUAGE_CODE;
        }
        self::build_perspective_json(self::$taxonomy, self::$term_id);
        ?>
            <script type="text/javascript">
                window.perspectives["<?php echo self::$taxonomy . "-" . self::$term_id; ?>"] = {};
                window.perspectives["<?php echo self::$taxonomy . "-" . self::$term_id; ?>"].perspectives_json = <?php echo json_encode(self::$perspectives_json); ?>;
                window.perspectives["<?php echo self::$taxonomy . "-" . self::$term_id; ?>"].humane_perspectives = {
                    "filters": {}
                };
                window.ajaxurl = "<?php echo $ajaxurl; ?>";
            </script>
        <?php
    }

    /**
     * Build the perspective JSON for an automated block.
     * 
     * @param string $taxonomy Taxonomy of the automated page
     * @param string $term_id Term ID of the automated page
     * @param int $limit Number of rows per page
     * @param bool $ajax Flag that denotes if the function is called after an ajax request
     * @param array $extra_attributes Any extra attributes that need to be used
     * 
     * @return array Perspectives JSON used to render the dashboard
     * 
     * @uses Controller_Dashboard::get_filter_values()
     * 
     * @see \Humane_Sites\Controller_Posts::ajax_sort_list_view() - Used By
     * @see \Humane_Theme\Controller_Posts::humane_search() - Used By
     * @see Model_Playlist::_save() - Used By
     * @see Controller_Blocks::render_automated_callback() - Used By
     * @see Controller_Dashboard::initial_perspectives() - Used By
     */
    public static function build_perspective_json($taxonomy = false, $term_id = false, $limit = 20, $ajax = false, $extra_attributes = array(), $calling_function = ""){
        global $wp_query;
        $perspectives_json = array();
        /* Setting the value from $_POST when called during AJAX */
        if(empty($taxonomy)){
            $taxonomy = $_POST["hidden_values"]["taxonomy"];
            $term_id = $_POST["hidden_values"]["term_id"];
            $limit = $_POST["hidden_values"]["items_per_page"];
        }
        $sort = array();
        $filters = array();
        if ($_POST['sort'] && $_POST['column']) {
            $sort['sort'] = $_POST['sort'];
            $sort['column'] = $_POST['column'];
        }
        if(isset($_POST['filters']) && !empty($_POST['filters'])) $filters = $_POST['filters'];
		$pageno = $_POST["page"] ?? 1;
		$map = array(
			"category" => "selected_categories",
			"author" => "selected_authors",
			"post_tag" => "selected_tags",
            "writing_format" => "selected_formats",
            "post_subscription" => "selected_subscription",
            "humane_feed" => "selected_feeds"
        );
        $post_types = false;
        $load_perspectives = false;

        /* Different filters applied to data based on the taxonomy */
        if($taxonomy === "playlist"){
            add_filter("get_filtered_post_list_$term_id", function($data, $extra_data) use($term_id){
                if($extra_data["term_id"] !== $term_id) return $data;
                $playlist = new Model_Playlist($term_id);
                $results = maybe_unserialize($playlist->results);
                $blocked_stories = maybe_unserialize($playlist->blocked_stories);
                $pinned_stories = maybe_unserialize($playlist->pinned_stories);
                if(!is_array($blocked_stories)) $blocked_stories = array();
                $data = array_filter($data, function($datum) use($results, $blocked_stories,$pinned_stories){
                    if(is_array($results) && in_array($datum, $results) && !in_array($datum, $blocked_stories)) return true;
                    return false;
                });
                
                if(is_array($pinned_stories)){
                               foreach (array_reverse($pinned_stories) as $value) {
                                   if(in_array($value, $data)){
                                       $index = array_search($value, $data);
                                       unset($data[$index]);
                                   }
                                   array_unshift($data, intval($value));
                               }
                           }
                return $data;
            }, 10, 2);
            $load_perspectives = true;
            $speciality = false;
        } elseif($taxonomy === "checkout"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $cart = get_user_option('humane_user_cart', $term_id);
                $cart = maybe_unserialize($cart);
                $data = array_filter($data, function($datum) use($cart){
                    if(in_array($datum, $cart)) return true;
                    return false;
                });
                return $data;
            });
        }elseif($taxonomy === "orders"){
            add_filter("get_filtered_post_list_$term_id", function($data) use($term_id){
                $library = get_user_option("humane_user_library",$term_id);
                $library = maybe_unserialize($library);
                $data = array_filter($data, function($datum) use($library){
                    if(in_array($datum, $library)) return true;
                    return false;
                });
                return $data;
            });
        }elseif($taxonomy === "subscriptions"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $subscriptions = get_user_option("humane_user_subscriptions", $term_id);
                $subscriptions = array_keys($subscriptions);
                $subscriptions = maybe_unserialize($subscriptions);
                $data = array_filter($data, function($datum) use($subscriptions){
                    if(in_array($datum, $subscriptions)) return true;
                    return false;
                });
                return $data;
            });
        }elseif($taxonomy === "search"){
            $post_types = array("post");
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $searched_posts = get_posts(array(
                    's' => $term_id,
                    'post_type' => array("post"),
                    'fields' => 'ids',
                    'posts_per_page' => -1
                ));
                $searched_posts = array_filter($searched_posts, function($datum) use($data){
                    if(in_array($datum, $data)) return true;
                    return false;
                });
                return $searched_posts;
            });
        }elseif($taxonomy === "reading-list"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $bookmarked_posts = Model_User_Events::get_user_action_events_by_created_by($term_id, "bookmarked");
                $bookmarked_posts = array_map(function($post){
                    return $post->wp_post_id;
                }, $bookmarked_posts);
                $data = array_filter($data, function($datum) use($bookmarked_posts){
                    if(in_array($datum, $bookmarked_posts)) return true;
                    return false;
                });
                return $data;
            });
        }elseif($taxonomy === "what-all-did-you-read"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $posts = Model_User_Events::get_user_action_events_by_created_by($term_id, "read");
                $posts = array_map(function($post){
                    return $post->wp_post_id;
                }, $posts);
                $data = array_filter($data, function($datum) use($posts){
                    if(get_post_meta($datum, "playlist_speciality", true)) return false;
                    elseif(in_array($datum, $posts)) return true;
                    return false;
                });
                return $data;
            });
        }elseif($taxonomy === "wish-list"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $wishlist_posts = get_user_option("humane_user_wishlist", $term_id);
                $wishlist_posts = maybe_unserialize($wishlist_posts);
                $data = array_filter($data, function($datum) use($wishlist_posts){
                    if(in_array($datum, $wishlist_posts)) return true;
                    return false;
                });
                return $data;
            });
        }else if($taxonomy === "post_subscription"){
            add_filter("get_filtered_post_list", function($data) use($term_id){
                $subscription_id = get_term_meta($term_id, "subscription_post_id", true);
                $term_slug = get_post_field("post_name", $subscription_id);
                $term = get_term_by("slug", $term_slug, "post_subscription");
                $playlists = get_term_meta($term->term_id, "subscribe_playlists",true);
                $posts = array();
                foreach($playlists as $playlist){
                    $posts = array_merge($posts, maybe_unserialize($playlist->results));
                }
                $posts = array_unique($posts);
                $posts = array_filter($posts, function($a){
                    $format = get_post_format($a) ? : 'standard';
                    if(get_post_type($a) === "post" && $format === "standard") return true;
                    return false;
                });
                $data = array_filter($data, function($datum) use($posts){
                    if(in_array($datum, $posts)) return true;
                    return false;
                });
                return $data;
            });
        }else{
            add_filter("get_filtered_post_list_playlist", function($playlist) use($taxonomy, $term_id, $map){
                $key = $map[$taxonomy];
                if(is_array($playlist->$key)) $playlist->$key[] = $term_id;
                else $playlist->$key = array($term_id);
                $playlist->type = Controller_Posts::filter_existing_post_type("automated");
                return $playlist;
            });
            $load_perspectives = true;
            $speciality = true;
        }

        /* Get Playlist ID if it's an automated playlist */
        if($speciality){
            $playlist_id = Model_Playlist::get_id_by_speciality($taxonomy . $term_id);
        }else{
            $playlist_id = $term_id;
        }
        $playlist = new Model_Playlist($playlist_id);
        
        /* Load written perspectives files only for unique Playlists and not other dashboards */
        if($load_perspectives){     
            if(is_array($playlist->selected_views) && !empty($playlist->selected_views)){
                $set_views = $playlist->selected_views;
            }
            $default_views = get_option("humane_automated_default_views");
            $use_default_views = get_option("humane_use_default_views");
            $view_options = $playlist->view_options;
            if(is_string($view_options)) $view_options = json_decode($playlist->view_options, true);
            if($use_default_views === "on" && $view_options["override_settings"] !== "yes" && is_array($default_views) && !empty($default_views)){
                $set_views = $default_views;
            }
            if(!empty($set_views)){
                $set_views = array_filter($set_views, function($a){
                    return $a["enabled"] === "on";
                });
                $set_views = array_keys($set_views);
            }
            $filename = HUMANE_SPACES_DIR . "playlist_data/" . get_current_blog_id() . "_playlist-$playlist_id.json";
            $file_exists = file_exists($filename);
            if(!$file_exists) self::write_perspective_json($playlist_id, $limit);
            $loaded_filters = json_decode(file_get_contents($filename), true);
            if(!empty($loaded_filters['meta_filters'])){
                $meta_filters = array(
                    "relation" => "AND"
                );
                $type = $loaded_filters['special_type'];
                $sort_order = array_keys($loaded_filters['meta_filters']);
                $new_order = get_option("$type-sort_order");
                if(!empty($new_order)){
                    $new_order = json_decode(stripslashes($new_order), true);
                    $sort_order = array_map(function($a) use($type) {
                        return str_replace("$type-", "", $a);
                    }, $new_order);
                }
                foreach($loaded_filters['meta_filters'] as $meta_key => $meta_filter){
                    if(isset($_POST['filters'][$meta_key]) && !empty($_POST['filters'][$meta_key])){
                        $meta_filter['value'] = $_POST['filters'][$meta_key];
                        if($meta_filter['datatype'] === "date" && is_array($meta_filter['value'])){
                            if(!isset($meta_filter['value'][1])){
                                $meta_filter['value'][1] = current_time('Y-m-d H:s');
                            }else if(!isset($meta_filter['value'][1])){
                                $meta_filter['value'][0] = wp_date('Y-m-d H:s', 0);
                            }
                        }
                        if($meta_filter['compare'] === "LIKE"){
                            $meta_filter['value'] = $_POST['filters'][$meta_key][0];
                        }
                        if($meta_filter['delimiter']){
                            $sub_filters = array(
                                "relation" => "OR"
                            );
                            $values = $_POST['filters'][$meta_key];
                            foreach($values as $value){
                                $meta_filter["value"] = $value;
                                $sub_filters[] = $meta_filter;
                            }
                            $meta_filters[] = $sub_filters;
                        }else{
                            $meta_filters[] = $meta_filter;
                        }
                    }
                    unset($loaded_filters['filters'][0]['filters'][$meta_key]);
                }
                $saved_meta = array();
                foreach($sort_order as $meta_key){
                    $meta_filter = $loaded_filters['meta_filters'][$meta_key];
                    if(!$meta_filter) continue;
                    $show_field = get_option($type . "-" . $meta_key);
                    if($show_field !== "off"){
                        $loaded_filters['filters'][0]['filters'][$meta_key] = $meta_filter;
                        $saved_meta[$meta_key] = $meta_filter;
                    }
                }
            }
        }

        /* Default views to be displayed in the dashboard */
        if(empty($set_views)) $set_views = array("gallery");

        /* Get data based on filters, sort, and other attributes */
        $data = Model_Posts::get_from_py_posts_view($sort, $filters, $pageno, $limit, $post_types, array("publish"), true, $meta_filters, array(
            "taxonomy" => $taxonomy,
            "term_id" => $term_id,
            "loaded_filters" => $loaded_filters["meta_filters"]
        ));
        $total_count = $data["total_count"];
        $filtered_count = $data["count"];
        $all_data_ids = $data["all_results"];
        $data = $data["results"];
        $authors = array();
        $categories = array();
        $formats = array();
        $tags = array();

        /* If AJAX request, the refresh dashboard option is set, and the count after filtering is different from the total count, get new filter values */
        if($ajax && filter_var($extra_attributes["refresh_dashboard"], FILTER_VALIDATE_BOOLEAN) && $filtered_count !== (int)$_POST["hidden_values"]["total_count"]){
            $loaded_filters = self::get_filter_values($all_data_ids, $playlist, $saved_meta);
        }

        /* Get dropdown options */
        $authors_data = get_users(
			array(
				"role__in" => array("contributor", "author", "editor", "administrator")
			)
		);
        foreach($authors_data as $author) {
			if($author->ID === $term_id) continue;
            array_push($authors, array(
                "key" => $author->ID,
                "alias" => Model_Users::to_s($author->ID)
            ));
        }
        foreach(get_categories() as $category) {
			if($category->term_id === $term_id) continue;
            array_push($categories, array(
                "key" => $category->term_id,
                "alias" => $category->name
            ));
        }
        foreach(get_terms("writing_format", array('hide_empty' => false)) as $format) {
			if($format->term_id === $term_id) continue;
            array_push($formats, array(
                "key" => $format->term_id,
                "alias" => $format->name
            ));
        }
        
        /* Default sort data */
        $sort_data = array(
            array("key" => "ASC", "alias" => "Oldest to Latest"), 
            array("key" => "DESC", "alias" => "Latest to Oldest")
		);
        $perspectives_json['id'] = $taxonomy . "-" . $term_id;
        $perspectives_json['controller_class'] = "Humane_Sites\Controller_Dashboard::build_perspective_json";
        $perspectives_json['data'] = $data;
        $perspectives_json['all_data_ids'] = $all_data_ids;
        $perspectives_json['hidden_values'] = array(
            "taxonomy" => $taxonomy,
            "term_id" => $term_id,
            "items_per_page" => $limit,
            "total_count" => $total_count,
            "extra_attributes" => $extra_attributes
        );
        $view_options = $playlist->view_options;
        if(is_string($view_options)) $view_options = json_decode($playlist->view_options, true);
        $GLOBALS['pinned_stories'] = $playlist->pinned_stories;
        /* Create the views array for the dashboard */
        $views = array(
            "gallery" => array(
                "id" => "ID",
                "scope" => "Gallery",
                "page" => $pageno,
                "new_tab" => $type === "aggregation" ? true : false,
                "mapped_values" => array(
                    "image" => function($row){
                        return $row->featured_image;
                    },
                    "title" => "post_title",
                    "url" => function($row){
                        if($row->post_type === "aggregation"){
                            $redirect_url  = get_post_meta( $row->ID, 'humane_aggregations_redirect_to_url', true );
                            return $redirect_url;
                        }
                        return get_permalink($row->ID);
                    },
                    "pinned" => function($row){
                       if(is_array($GLOBALS['pinned_stories']) && in_array($row->ID,$GLOBALS['pinned_stories'])){
                        return true;
                       } else{
                        return false;
                       }
                    },
                    "top-left" => function($row){
                        // if($row->post_type === "wp_block"){
                        //     return "";
                        // }
                        $terms = get_the_terms($row->ID, "writing_format");
                        if(is_array($terms)) return $terms[0]->name;
                        return "";
                    },
                    "top-right" => function($row){
                        $post_type = get_post_type($row->ID);
                        if(get_post_type($row->ID) === "page") return;
                        if(in_array($post_type, apply_filters("automated_top_right", array()))) return;
                        $reading_time = Model_Posts::get_reading_time($row->ID); 
                        $reading_time = $reading_time < 1 ? "Less than 1" : $reading_time;
                        return $reading_time . " min";
                    },
                    "new_tab" => function($row){
                        if($row->post_type === "aggregation"){
                            return true;
                        }
                        return false;
                    },
                    "subtitle" => function($row){
                        $author_id = get_post_field('post_author', $row->ID );
                        $recent_author = get_user_by( 'ID', $author_id );
                        // Get user display name.
                    $author_display_name = $recent_author->display_name;
                        return $author_display_name;
                    },
                    "show_date" => function($row){
                        $hide_date =  get_post_meta( $row->ID, 'humane_hide_published_at', true );
                        if( '1' === $hide_date ) {
                            return 'hide';
                        } else{
                            return 'show';
                        }
                    },
                    "show_byline" => function($row){
                        $hide_byline =  get_post_meta( $row->ID, 'humane_hide_byline', true );
                        if( '1' === $hide_byline  ) {
                            return 'hide';
                        } else{
                            return 'show';
                        }
                    },
                    "aggregations_icon" => function($row){
                      return self:: get_aggregations_icon($row->ID);
                    },
                    "aggregations_domain" => function($row){
                        if($row->post_type === "aggregation"){
                            return get_post_meta( $row->ID, 'humane_aggregations_domain', true );
                        }
                    },
                    "show_min" => function($row){
                        if($row->post_type === "aggregation"){
                            return 'hide';
                        } else{
                            return 'show';
                        }
                    },

                    "hint" => function($row){
                        return date("M j, Y", strtotime($row->post_date));
                    },
                    'current_id'=> function($row){
                        
                            return $row->ID;
                    },
                    'current_post_type'=> function($row){
                        
                       return $row->post_type;
                },
                'intrection_class'=> function($row){
                    $card_interaction = get_post_meta($row->ID, "humane_post_card_interaction", true);
                    if( empty( $card_interaction ) || $card_interaction === 'global'){
                        if($row->post_type === 'wp_block' || $row->post_type === 'post' || $row->post_type === 'event' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
                        }
                        if( $row->post_type === 'page' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
                        }
                    }
                    $restricted = Controller_Posts::check_if_restricted($row->ID);
                    if(( $card_interaction === 'modal' && !$restricted ) || ( $card_interaction === 'modal' && $row->post_type === 'event' )  ){
                        $intrection_class = 'hc-modal-trigger';
                    } else{
                        $intrection_class = '';
                    }
                    return $intrection_class;
             },
             'category'=> function($row){
                $cat_id = get_post_meta($row->ID, 'humane_primary_category', true);
                if(empty($cat_id) || $cat_id=== '-1'){
                    $categories = get_the_category( $row->ID );
                    if ( is_array( $categories ) && array_key_exists( 0, $categories ) ) {
                        $cat_name   = $categories[0]->name;
                        return $cat_name;
                    }
                } else{
                    return  get_cat_name( $cat_id );
                }
               
            },
            'alt_tag'=> function($row){
                   return Controller_Posts::humane_get_alt_tag($row->ID);
            },
            "is_from_datawrapper" => function($row){
               $is_from_datawrapper  = get_post_meta( $row->ID, 'humane_is_from_datawrapper', true );
               return $is_from_datawrapper;
            },
            "image_srcset" => function($row){
                return self:: get_img_attachment_srcset_by_post_id($row->ID);
            },
            "image_sizes" => function($row){
                return self:: get_img_attachment_sizes_by_post_id($row->ID);
            },
                )
                
            ),
            "newsfeed" => array(
                "id" => "ID",
                "scope" => "Newsfeed",
                "page" => $pageno,
                "profile_pic" => function($row){
                    global $wpdb;
                    $thumbnail_id = $wpdb->prepare("SELECT meta_value from {$wpdb->prefix}postmeta WHERE meta_key='_thumbnail_id' AND post_id='{$row->ID}'");
                    $thumbnail_id = $wpdb->get_var($thumbnail_id);
                    return wp_get_attachment_url( $thumbnail_id, 'thumbnail' );
                },
                "container" => array(
                    "attributes" => function($row){
                        if($row->post_type === "aggregation") return null;
                        $type = $row->post_type;
                        $connect = $row->ID;
                        return "data-post_type='$type'  data-connect='$connect' class='humane_card_container'";
                    }
                ),
                "title" => array(
                    "text" => "post_title",
                    "url" => function($row){
                        if($row->post_type === "aggregation"){
                            $redirect_url  = get_post_meta( $row->ID, 'humane_aggregations_redirect_to_url', true );
                            return $redirect_url;
                        }
                        return get_permalink($row->ID);
                    },
                    "new_tab" => function($row){
                        if($row->post_type === "aggregation"){
                            return true;
                        }
                        return false;
                    }
                ),
                'current_id'=> function($row){
                        
                    return $row->ID;
                },
                "author_first_name" => function($row){
                    $hide_byline =  get_post_meta( $row->ID, 'humane_hide_byline', true );
                    if( '1' !== $hide_byline ) {
                       return $row->author_name;
                    }
                }, 
                "byline" => function($row) use($taxonomy, $term_id){
                    ?>
                        <?php 
                            $category = get_the_terms( $row->ID, "category" );
                    
                            $category_id = get_post_meta($row->ID, 'humane_primary_category', true);
                            $category_name = get_cat_name($category_id);
                            $category_object = get_term($category_id, "category");
                            $category_link = get_term_link((int)$category_id);
                            if(is_wp_error($category_link)) $category_link = false;
                            
                            if(is_array($category) && !$category_id){
                                $category_name = $category[0]->name;
                                $category_id = $category[0]->term_id;
                                $category_object = $category[0];
                                $category_link = get_term_link((int)$category_id);
                                if(is_wp_error($category_link)) $category_link = false;
                            }
                        ?>
                        <?php if(!empty($category_name) && $category_name !== "Uncategorized"): ?>
                            • &nbsp; <a href="<?php echo $category_link; ?>"><?php echo $category_name; ?></a>
                        <?php endif; ?>
                    <?php
                },
                "pinned" => function($row){
                    if(is_array($GLOBALS['pinned_stories']) && in_array($row->ID,$GLOBALS['pinned_stories'])){
                     return true;
                    } else{
                     return false;
                    }
                 },
                "badge" => function($row) use($taxonomy, $term_id){
                        $bookmark_svg = Controller_Icons::get_bookmark_icon($row->ID);
                        $bookmark_svg = apply_filters("automated_page_bookmark_show", $bookmark_svg);
                        $cart_svg = Controller_Icons::get_cart_icon($row->ID);
                        $bookmark_remove_svg = Controller_Icons::get_svg_icons('bookmarked');
                        $bookmarked_posts = get_user_option( 'bookmarked_posts', get_current_user_id());
                        $render = is_array($bookmarked_posts) && in_array($row->ID, $bookmarked_posts);
                    ?>
                        <?php if(Controller_Options::is_login_wall_activated() && false): ?>
                            <div class="carousel-element-icon-container">
                                <?php if ($bookmark_svg) : ?>
                                    <div class="carousel-element-author-bookmark-container sales-hint">
                                        <?php if(!$render): ?>
                                            <div class="carousel-element-bookmark sales-hint" bookmark-post="<?php echo $row->ID; ?>">
                                                <?php echo $bookmark_svg; ?>
                                            </div>
                                        <?php else: ?>
                                            <div class="carousel-element-bookmark sales-hint" bookmark-post="<?php echo $row->ID; ?>">
                                                <?php echo $bookmark_remove_svg; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="hint hc-mt-20">
                            <?php if($row->post_type === "event"): ?>
                                <?php 
                                    $event_time = get_post_meta($row->ID, "humane_events_start_time", true);
                                    $event_time = date_create($event_time);
                                    $count_leads = count(Model_User_Events::get_user_events_by_post_id($row->ID,"form_submission", false)); 
                                ?>
                                Event on <?php echo date_format($event_time, "d M Y") . " at " . date_format($event_time, "H:i") ?> 
                                <?php $count_leads !== 0 ? '• ' .$count_leads . ' reservations' : "" ?>
                            <?php elseif($row->post_type === "product"): ?>
                                <?php 
                                if(function_exists('wc_get_product')){
                                    $product_data = wc_get_product( $row->ID );
                                    $currency = get_woocommerce_currency_symbol(); 
                                    $price = $product_data->price;
                                    $show_price = "Price: ". $currency . " ". $price;
                                } else{
                                    $price = get_post_meta($row->ID, "humane_price", true);
                                    $currency = get_post_meta($row->ID, "humane_currency", true);
                                    echo 'Price:'.$price .' '.$currency;
                                } 
                                ?>
                            <?php elseif($row->post_type === "subscription"): ?>
                                <?php 
                                    $price = get_post_meta($row->ID, "humane_price", true);
                                    $currency = get_post_meta($row->ID, "humane_currency", true);
                                    $subscriptions = get_user_option("humane_user_subscriptions", $term_id);
                                    $subscription = $subscriptions[$row->ID];
                                    $current_time = time();
                                ?>
                                <?php if($subscription["status"] === "active"): ?>
                                    <span class="hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-green hc-supernormal-xs hc-text-brightness-97 hc-width-fit-content hc-fx hc-center">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php elseif($subscription["status"] === "pending" || $subscription["status"] === "authenticated"): ?>
                                    <span class="badge rounded-pill bg-warning">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php else: ?>
                                    <span class="hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-red hc-supernormal-xs hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php endif; ?>
                                <?php if($subscription["status"] === "cancelled"): ?>
                                    Ends on <?php echo date('F j, Y', $subscription["end_at"]); ?> •
                                <?php else: ?>
                                    Renews on <?php echo date('F j, Y', $subscription["end_at"]);  ?> •
                                <?php endif; ?>
                                Price: <?php echo $price . " " . $currency ?>
                            <?php endif; ?>
                        </div>
                        <?php $modified_term_list = self::get_modified_term_list( $row->ID, 'post_tag', '<div class="hc-mt-8 hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-brightness-97 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">', '</div><div class="hc-mt-8 hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-brightness-97 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">', '</div>', array($term_id), $taxonomy );
                        // var_dump($modified_term_list);
                        if($modified_term_list){
                        ?>
                        <div class="terms-container"> 
                            <div class="hc-fx hc-terms hc-col-12">
                                <?php echo $modified_term_list; ?>
                            </div>
                        </div>
                    <?php
                        }
                },
                "action" => function($row){
                    return "Published";
                },
                "image_src" => function($row){
                    if($row->post_type === "aggregation"){
                        return $row->featured_image;
                    }
                    return get_the_post_thumbnail_url($row->ID, "small-53");
                },
                "post_type" => "post_type",
                "status" => "post_status",
                "category" => "category",
                "tag" => "post_tag",
                "format" => "post_format",
                "date" => array(
                    "post_date" => "post_date",
                    "post_modified" => "post_modified"
                ),
                "show_date" => function($row){
                    $hide_date =  get_post_meta( $row->ID, 'humane_hide_published_at', true );
                    if( '1' === $hide_date ) {
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                "show_byline" => function($row){
                    $hide_byline =  get_post_meta( $row->ID, 'humane_hide_byline', true );
                    if( '1' === $hide_byline ) {
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                "aggregations_icon" => function($row){
                    return self:: get_aggregations_icon($row->ID);
                },
                "aggregations_domain" => function($row){
                    if($row->post_type === "aggregation"){
                        return get_post_meta( $row->ID, 'humane_aggregations_domain', true );
                    }
                },
                "show_min" => function($row){
                    if($row->post_type === "aggregation"){
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                'intrection_class'=> function($row){
                    $card_interaction = get_post_meta($row->ID, "humane_post_card_interaction", true);
                    if( empty( $card_interaction ) || $card_interaction === 'global'){
                        if( $row->post_type === 'wp_block' || $row->post_type === 'post' || $row->post_type === 'event' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
                        }
                        if( $row->post_type === 'page' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
                        }
                    }
                    $restricted = Controller_Posts::check_if_restricted($row->ID);
                    if(( $card_interaction === 'modal' && !$restricted ) || ( $card_interaction === 'modal' && $row->post_type === 'event' )  ){
                        $intrection_class = 'hc-modal-trigger';
                    } else{
                        $intrection_class = '';
                    }
                    return $intrection_class;
             },
             'alt_tag'=> function($row){
                return Controller_Posts::humane_get_alt_tag($row->ID);
             },
             "image_srcset" => function($row){
                return self:: get_img_attachment_srcset_by_post_id($row->ID);
            },
            "image_sizes" => function($row){
                return self:: get_img_attachment_sizes_by_post_id($row->ID);
            },
            ),
            "list" => array(
                "id" => "ID",
                "scope" => "List",
                "page" => $pageno,
                "profile_pic" => function($row){
                    global $wpdb;
                    $thumbnail_id = $wpdb->prepare("SELECT meta_value from {$wpdb->prefix}postmeta WHERE meta_key='_thumbnail_id' AND post_id='{$row->ID}'");
                    $thumbnail_id = $wpdb->get_var($thumbnail_id);
                    return wp_get_attachment_url( $thumbnail_id, 'thumbnail' );
                },
                "container" => array(
                    "attributes" => function($row){
                        if($row->post_type === "aggregation") return null;
                        $type = $row->post_type;
                        $connect = $row->ID;
                        return "data-post_type='$type'  data-connect='$connect' class='humane_card_container'";
                    }
                ),
                "title" => array(
                    "text" => "post_title",
                    "url" => function($row){
                        if($row->post_type === "aggregation"){
                            $redirect_url  = get_post_meta( $row->ID, 'humane_aggregations_redirect_to_url', true );
                            return $redirect_url;
                        }
                        return get_permalink($row->ID);
                    },
                    "new_tab" => function($row){
                        if($row->post_type === "aggregation"){
                            return true;
                        }
                        return false;
                    }
                ),
                'current_id'=> function($row){
                        
                    return $row->ID;
                },
                "author_first_name" => function($row){
                    $hide_byline =  get_post_meta( $row->ID, 'humane_hide_byline', true );
                    if( '1' !== $hide_byline ) {
                       return $row->author_name;
                    }
                }, 
                "byline" => function($row) use($taxonomy, $term_id){
                    ?>
                        <?php 
                            $reading_time = Model_Posts::get_reading_time($row->ID);
                            $category = get_the_terms( $row->ID, "category" );
                    
                            $category_id = get_post_meta($row->ID, 'humane_primary_category', true);
                            $category_name = get_cat_name($category_id);
                            $category_object = get_term($category_id, "category");
                            $category_link = get_term_link((int)$category_id);
                            if(is_wp_error($category_link)) $category_link = false;
                            
                            if(is_array($category) && !$category_id){
                                $category_name = $category[0]->name;
                                $category_id = $category[0]->term_id;
                                $category_object = $category[0];
                                $category_link = get_term_link((int)$category_id);
                                if(is_wp_error($category_link)) $category_link = false;
                            }
                        ?>
                        <?php if($reading_time): ?>
                            • <?php echo $reading_time; ?> min read
                        <?php endif; ?>
                        <?php if(!empty($category_name) && $category_name !== "Uncategorized"): ?>
                            • &nbsp; <a href="<?php echo $category_link; ?>"><?php echo $category_name; ?></a>
                        <?php endif; ?>
                    <?php
                },
                "pinned" => function($row){
                    if(is_array($GLOBALS['pinned_stories']) && in_array($row->ID,$GLOBALS['pinned_stories'])){
                     return true;
                    } else{
                     return false;
                    }
                 },
                "badge" => function($row) use($taxonomy, $term_id){
                        $bookmark_svg = Controller_Icons::get_bookmark_icon($row->ID);
                        $bookmark_svg = apply_filters("automated_page_bookmark_show", $bookmark_svg);
                        $cart_svg = Controller_Icons::get_cart_icon($row->ID);
                        $bookmark_remove_svg = Controller_Icons::get_svg_icons('bookmarked');
                        $bookmarked_posts = get_user_option( 'bookmarked_posts', get_current_user_id());
                        $render = is_array($bookmarked_posts) && in_array($row->ID, $bookmarked_posts);
                    ?>
                        <?php if(Controller_Options::is_login_wall_activated() && false): ?>
                            <div class="carousel-element-icon-container">
                                <?php if ($bookmark_svg) : ?>
                                    <div class="carousel-element-author-bookmark-container sales-hint">
                                        <?php if(!$render): ?>
                                            <div class="carousel-element-bookmark sales-hint" bookmark-post="<?php echo $row->ID; ?>">
                                                <?php echo $bookmark_svg; ?>
                                            </div>
                                        <?php else: ?>
                                            <div class="carousel-element-bookmark sales-hint" bookmark-post="<?php echo $row->ID; ?>">
                                                <?php echo $bookmark_remove_svg; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="hint hc-mt-20">
                            <?php if($row->post_type === "event"): ?>
                                <?php 
                                    $event_time = get_post_meta($row->ID, "humane_events_start_time", true);
                                    $event_time = date_create($event_time);
                                    $count_leads = count(Model_User_Events::get_user_events_by_post_id($row->ID,"form_submission", false)); 
                                ?>
                                Event on <?php echo date_format($event_time, "d M Y") . " at " . date_format($event_time, "H:i") ?> 
                                <?php $count_leads !== 0 ? '• '. $count_leads . ' reservations' : "" ?>
                            <?php elseif($row->post_type === "product"): ?>
                                <?php 
                                if(function_exists('wc_get_product')){
                                    $product_data = wc_get_product( $row->ID );
                                    $currency = get_woocommerce_currency_symbol(); 
                                    $price = $product_data->price;
                                    $show_price = "Price: ". $currency . " ". $price;
                                } else{
                                    $price = get_post_meta($row->ID, "humane_price", true);
                                    $currency = get_post_meta($row->ID, "humane_currency", true);
                                    echo 'Price:'.$price .' '.$currency;
                                } 
                                ?>
                            <?php elseif($row->post_type === "subscription"): ?>
                                <?php 
                                    $price = get_post_meta($row->ID, "humane_price", true);
                                    $currency = get_post_meta($row->ID, "humane_currency", true);
                                    $subscriptions = get_user_option("humane_user_subscriptions", $term_id);
                                    $subscription = $subscriptions[$row->ID];
                                    $current_time = time();
                                ?>
                                <?php if($subscription["status"] === "active"): ?>
                                    <span class="hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-green hc-supernormal-xs hc-text-brightness-97 hc-width-fit-content hc-fx hc-center">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php elseif($subscription["status"] === "pending" || $subscription["status"] === "authenticated"): ?>
                                    <span class="badge rounded-pill bg-warning">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php else: ?>
                                    <span class="hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-red hc-supernormal-xs hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">
                                        <?php echo ucwords($subscription["status"]); ?>
                                    </span>
                                <?php endif; ?>
                                <?php if($subscription["status"] === "cancelled"): ?>
                                    Ends on <?php echo date('F j, Y', $subscription["end_at"]); ?> •
                                <?php else: ?>
                                    Renews on <?php echo date('F j, Y', $subscription["end_at"]);  ?> •
                                <?php endif; ?>
                                Price: <?php echo $price . " " . $currency ?>
                            <?php endif; ?>
                        </div>
                        <?php $modified_term_list = self::get_modified_term_list( $row->ID, 'post_tag', '<div class="hc-mt-8 hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-brightness-97 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">', '</div><div class="hc-mt-8 hc-mr-8 hc-badge hc-border-rounded-16 hc-bg-brightness-97 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center">', '</div>', array($term_id), $taxonomy );
                        // var_dump($modified_term_list);
                        if($modified_term_list){
                        ?>
                        <div class="terms-container"> 
                            <div class="hc-fx hc-terms hc-col-12">
                                <?php echo $modified_term_list; ?>
                            </div>
                        </div>
                    <?php
                        }
                },
                "action" => function($row){
                    return "Published";
                },
                "image_src" => function($row){
                    if($row->post_type === "aggregation"){
                        return $row->featured_image;
                    }
                    return get_the_post_thumbnail_url($row->ID, "small-53");
                },
                "post_type" => "post_type",
                "status" => "post_status",
                "category" => "category",
                "tag" => "post_tag",
                "format" => "post_format",
                "date" => array(
                    "post_date" => "post_date",
                    "post_modified" => "post_modified"
                ),
                "show_date" => function($row){
                    $hide_date =  get_post_meta( $row->ID, 'humane_hide_published_at', true );
                    if( '1' === $hide_date ) {
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                "show_byline" => function($row){
                    $hide_byline =  get_post_meta( $row->ID, 'humane_hide_byline', true );
                    if( '1' === $hide_byline ) {
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                "aggregations_icon" => function($row){
                    return self:: get_aggregations_icon($row->ID);
                },
                "aggregations_domain" => function($row){
                    if($row->post_type === "aggregation"){
                        return get_post_meta( $row->ID, 'humane_aggregations_domain', true );
                    }
                },
                "show_min" => function($row){
                    if($row->post_type === "aggregation"){
                        return 'hide';
                    } else{
                        return 'show';
                    }
                },
                'intrection_class'=> function($row){
                    $card_interaction = get_post_meta($row->ID, "humane_post_card_interaction", true);
                    if( empty( $card_interaction ) || $card_interaction === 'global'){
                        if( $row->post_type === 'wp_block' || $row->post_type === 'post' || $row->post_type === 'event' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_posts');
                        }
                        if( $row->post_type === 'page' ){
                            $card_interaction = get_option('settings_content_pack_post_card_interaction_for_pages');
                        }
                    }
                    $restricted = Controller_Posts::check_if_restricted($row->ID);
                    if(( $card_interaction === 'modal' && !$restricted ) || ( $card_interaction === 'modal' && $row->post_type === 'event' )  ){
                        $intrection_class = 'hc-modal-trigger';
                    } else{
                        $intrection_class = '';
                    }
                    return $intrection_class;
                },
                'alt_tag'=> function($row){
                    return Controller_Posts::humane_get_alt_tag($row->ID);
                },
                "image_srcset" => function($row){
                    return self:: get_img_attachment_srcset_by_post_id($row->ID);
                },
                "image_sizes" => function($row){
                    return self:: get_img_attachment_sizes_by_post_id($row->ID);
                },
            ),
            "dictionary" => array(
                "id" => "ID",
                "page" => $pageno,
                "scope" => "Dictionary",
                "post_type" => "post_type",
                "status" => "post_status",
                "category" => "category",
                "tag" => "post_tag",
                "format" => "post_format",
                "container" => array(
                    "attributes" => function($row){
                        if($row->post_type === "aggregation") return null;
                        $type = $row->post_type;
                        $connect = $row->ID;
                        return "data-post_type='$type'  data-connect='$connect' class='humane_card_container'";
                    }
                ),
                "title" => array(
                    "text" => "post_title",
                    "url" => function($row){
                        if($row->post_type === "aggregation"){
                            $redirect_url  = get_post_meta( $row->ID, 'humane_aggregations_redirect_to_url', true );
                            return $redirect_url;
                        }
                        return get_permalink($row->ID);
                    },
                    "new_tab" => function($row){
                        if($row->post_type === "aggregation"){
                            return true;
                        }
                        return false;
                    }
                ),
                'current_id'=> function($row){
                    return $row->ID;
                },
                "action" => function($row){
                    return "Published";
                },

            ),
            "map" => array(
                "style_url" => $view_options ? $view_options["mapbox_style"] : "",
                "latitude_key" => $view_options ? $view_options["latitude_key"] : "",
                "longitude_key" => $view_options ? $view_options["longitude_key"] : "",
                "view_options" => $view_options,
                "popup_template" => $view_options ? $view_options["popup_template"] : "",
                "id" => "ID",
                "scope" => "Map",
                "primary_key" => $view_options ? $view_options["primary_key"] : "",
            )
        );
        $final_views = array();

        /* Filter views based on settings */
        foreach($set_views as $set_view){
            if($views[$set_view]){
                $final_views[] = $views[$set_view];
            }
        }
        $perspectives_json['views'] = $final_views;
        $perspectives_json['filters'] = array(
            "title" => "FILTERS",
            "filtered_count" => $filtered_count,
            "total_count" => $total_count,
            "filters" => array(
                array(
                    "title" => "All filters", 
                    "filters" => array(
                        "search" => array(
                            "initial" => "",
                            "label" => "Search",
                            "placeholder" => "Search by post title",
                            "scope" => "Search",
                            "datatype" => "string"
                        ),
                        "selected_authors" => array(
                            "initial" => "",
                            "label" => "Author",
                            "multiple" => true,
                            "placeholder" => "Select an author to filter by",
                            "data" => $authors,
                            "scope" => "Dropdown",
                            "datatype" => "string"
                        ),
                        "selected_categories" => array(
                            "initial" => "",
                            "label" => "Category",
                            "multiple" => true,
                            "placeholder" => "Select a category to filter by",
                            "data" => $categories,
                            "scope" => "Dropdown",
                            "datatype" => "string"
                        ),
                        "selected_formats" => array(
                            "initial" => "",
                            "label" => "Format",
                            "multiple" => true,
                            "placeholder" => "Select a format to filter by",
                            "data" => $formats,
                            "scope" => "Dropdown",
                            "datatype" => "string"
                        ),
                        "start_date" => array(
                            "initial" => "",
                            "label" => "Start date",
                            "placeholder" => "Search by start date",
                            "scope" => "Datepicker",
                            "datatype" => "date"
                        ),
                        "end_date" => array(
                            "initial" => "",
                            "label" => "End date",
                            "placeholder" => "Search by end date",
                            "scope" => "Datepicker",
                            "datatype" => "date"
                        )
                    )
                )
            )
        );
        if($load_perspectives){
            $loaded_filters['filtered_count'] = $filtered_count;
            $loaded_filters['total_count'] = $total_count;
            $perspectives_json['filters'] = $loaded_filters;
        }

        /* Sort section */
        $sorts = HUMANE_SPACES_DIR . "assets/playlists/json/sorts.json";
        $sorts = json_decode(file_get_contents($sorts), true);
        $sorts = apply_filters("humane_automated_default_sorts", $sorts);
        $selected_sorts = $playlist->selected_sorts;
        $global_default_sorts = get_option("humane_automated_default_sorts");
        if($use_default_views === "on" && $view_options["override_settings"] !== "yes"){
            $selected_sorts = $global_default_sorts;
        }
        if(empty($selected_sorts)) $selected_sorts = $sorts;
        if(is_array($selected_sorts["data"])){
            $selected_sorts["data"] = array_filter($selected_sorts["data"], function($filter_key) use($selected_sorts){
                if($selected_sorts["data"] && $selected_sorts["data"][$filter_key]){
                    $enabled = $selected_sorts["data"][$filter_key]["enabled"];
                    if($enabled !== "off") return true;
                    else return false;
                }
                return false;
            }, ARRAY_FILTER_USE_KEY);
        }
        if(!empty($selected_sorts["data"])){
            $selected_sorts["data"] = array_values($selected_sorts["data"]);
            $perspectives_json['sort'] = array(
                "title" => "SORT",
                "description" => "",
                "sorts" => $selected_sorts
            );
        }

        /* Add sort section only if sort is activated in Playlist */
        if($playlist && $playlist->automated_sort_active === "off") unset($perspectives_json['sort']);
        self::$perspectives_json = $perspectives_json;
        return $perspectives_json;
    }

       
    /**
     * Generates a list of each term of a post.
     * 
     * @param int $id ID of the Post for which term list has to be generated
     * @param string $taxonomy Taxonomy for which terms need to be found
     * @param string $before String before each term
     * @param string $sep Separator between each term
     * @param string $after String after each term 
     * @param array $exclude Terms that need to be excluded
     * @param string $active_taxonomy Current automated page taxonomy to compare with
     * 
     * @return string Term list based on the arguments passed
     * 
     * @see Controller_Dashboard::build_perspective_json() - Used By
     */
    public static function get_modified_term_list( $id = 0, $taxonomy = '', $before = '', $sep = '', $after = '', $exclude = array(), $active_taxonomy = "category") {
        if($active_taxonomy !== $taxonomy) $exclude = array();
        $terms = get_the_terms( $id, $taxonomy );
    
        if ( is_wp_error( $terms ) )
            return $terms;
    
        if ( empty( $terms ) )
            return false;
    
        foreach ( $terms as $term ) {
    
            if(!in_array($term->term_id,$exclude)) {
                $link = get_term_link( $term, $taxonomy );
                if ( is_wp_error( $link ) )
                    return $link;
                if(empty($term->name)) continue;
                $term_links[] = '<a target="_blank" href="' . $link . '" rel="tag">' . $term->name . '</a>';
            }
        }
    
        if( !isset( $term_links ) )
            return false;
    
        return $before . join( $sep, $term_links ) . $after;
    }

    /**
     * Creates the filter section of the perspectives JSON.
     * 
     * Goes through the Post IDs and calculates count for the following dropdown values
     * 
     * 1. Categories
     * 2. Tags
     * 3. Formats
     * 4. Authors
     * 
     * Furthermore, if all data IDs belong to the same post type, fetches the meta filters JSON
     * for that specific post type and calculates count for each meta value that is a dropdown 
     * as well
     * 
     * @param array $all_data_ids List of Post IDs for which filter values need to be generated
     * @param object $playlist Playlist object
     * @param array $saved_meta_filters Meta filters 
     * 
     * @return object Filters section of Perspectives JSON
     * 
     * @uses \Humane_Spaces\Controller_Automated_Pages::create_searchable_field()
     * @uses Controller_Dashboard::sort_dropdown_values()
     * @uses Model_Users::to_s()
     * 
     * @see Controller_Dashboard::write_perspective_json() - Used By
     * @see Controller_Dashboard::build_perspective_json() - Used By
     */
    public static function get_filter_values($all_data_ids, $playlist, $saved_meta_filters = false){
        $use_default_views = get_option("humane_use_default_views");
        $view_options = $playlist->view_options;
        if(is_string($view_options)) $view_options = json_decode($playlist->view_options, true);
        $selected_filters = $playlist->selected_filters;
        $global_default_filters = get_option("humane_automated_default_filters");
        if($use_default_views === "on" && $view_options["override_settings"] !== "yes"){
            $selected_filters = $global_default_filters;
        }
        

        /* Categories count */
        $categories = array();
        if(is_array($selected_filters) && $selected_filters["selected_categories"]){
            $post_categories = wp_get_object_terms($all_data_ids, "category", array(
                'fields' => 'ids',
                'exclude' => (int) str_replace($playlist->speciality, "", $playlist->speciality_id)
            ));
            foreach($post_categories as $category) {
                $category = get_term($category, "category");
                $count = 0;
                foreach($all_data_ids as $post_id){
                    if(has_term($category->term_id, "category", $post_id)) $count++;
                }
                array_push($categories, array(
                    "key" => $category->term_id,
                    "alias" => $category->name,
                    "external_count" => $count
                ));
            }
            usort($categories, array(__CLASS__, "sort_dropdown_values"));
        }
        /* Tags count */
        $tags = array();
        if(is_array($selected_filters) && $selected_filters["selected_tags"]){
            $post_tags = wp_get_object_terms($all_data_ids, "post_tag", array(
                'fields' => 'ids',
                'exclude' => (int) str_replace($playlist->speciality, "", $playlist->speciality_id)
            ));
            foreach($post_tags as $tag) {
                $tag = get_term($tag, "post_tag");
                $count = 0;
                foreach($all_data_ids as $post_id){
                    if(has_term($tag->term_id, "post_tag", $post_id)) $count++;
                }
                array_push($tags, array(
                    "key" => $tag->term_id,
                    "alias" => $tag->name,
                    "external_count" => $count
                ));
            }
            usort($tags, array(__CLASS__, "sort_dropdown_values"));
        }
        /* Formats count */
        $formats = array();
        if(is_array($selected_filters) && $selected_filters["selected_formats"]){
            $post_formats = wp_get_object_terms($all_data_ids, "writing_format", array(
                'fields' => 'ids',
                'exclude' => (int) str_replace($playlist->speciality, "", $playlist->speciality_id)
            ));
            foreach($post_formats as $format) {
                $format = get_term($format, "writing_format");
                $count = 0;
                foreach($all_data_ids as $post_id){
                    if(has_term($format->term_id, "writing_format", $post_id)) $count++;
                }
                array_push($formats, array(
                    "key" => $format->term_id,
                    "alias" => $format->name,
                    "external_count" => $count
                ));
            }
            usort($formats, array(__CLASS__, "sort_dropdown_values"));
        }
        /* Authors count */
        $authors = array();
        if(is_array($selected_filters) && $selected_filters["selected_authors"]){
            $post_authors = array();
            foreach($all_data_ids as $datum){
                $post_authors[] = get_post_field('post_author', $datum);
            }
            $post_authors = array_unique($post_authors);
            foreach($post_authors as $author) {
                $count = 0;
                foreach($all_data_ids as $post_id){
                    $post_author_id = get_post_field( 'post_author', $post_id );
                    if($author === $post_author_id) $count++;
                }
                $author = get_user_by( 'ID', $author );
                array_push($authors, array(
                    "key" => $author->ID,
                    "alias" => Model_Users::to_s($author->ID),
                    "external_count" => $count
                ));
            }
            usort($authors, array(__CLASS__, "sort_dropdown_values"));
        }
        /* Create searchable field only if search is activated */
        foreach($all_data_ids as $datum){
            Controller_Automated_Pages::create_searchable_field($datum);
        }
        /* Default filters */
        $default_filters = array(
            "search" => array(
                "initial" => "",
                "label" => "Search",
                "placeholder" => "Search by post title",
                "scope" => "Search",
                "datatype" => "string"
            ),
            "selected_authors" => array(
                "initial" => "",
                "label" => "Author",
                "multiple" => true,
                "placeholder" => "Select an author to filter by",
                "data" => $authors,
                "scope" => "Dropdown",
                "datatype" => "string"
            ),
            "selected_categories" => array(
                "initial" => "",
                "label" => "Category",
                "multiple" => true,
                "placeholder" => "Select a category to filter by",
                "data" => $categories,
                "scope" => "Dropdown",
                "datatype" => "string"
            ),
            "selected_tags" => array(
                "initial" => "",
                "label" => "Tag",
                "multiple" => true,
                "placeholder" => "Select a tag to filter by",
                "data" => $tags,
                "scope" => "Dropdown",
                "datatype" => "string"
            ),
            "selected_formats" => array(
                "initial" => "",
                "label" => "Format",
                "multiple" => true,
                "placeholder" => "Select a format to filter by",
                "data" => $formats,
                "scope" => "Dropdown",
                "datatype" => "string"
            ),
            "start_date" => array(
                "initial" => "",
                "label" => "Start date",
                "placeholder" => "Search by start date",
                "scope" => "Datepicker",
                "datatype" => "date"
            ),
            "end_date" => array(
                "initial" => "",
                "label" => "End date",
                "placeholder" => "Search by end date",
                "scope" => "Datepicker",
                "datatype" => "date"
            )
        );
        $default_filters = array_filter($default_filters, function($filter_key) use($selected_filters){
            if($selected_filters && $selected_filters[$filter_key]){
                $enabled = $selected_filters[$filter_key]["enabled"];
                if($enabled !== "off") return true;
                else return false;
            }
            return true;
        }, ARRAY_FILTER_USE_KEY);
		$filters = array(
			"title" => "All filters", 
			"filters" => $default_filters
		);
        /* If all posts are of a specific post type, use meta filter JSON
        for that post type */

		$meta_filters = array();
        $all_post_type = false;
        foreach($all_data_ids as $datum){
            $post_type = get_post_type($datum);
            if($all_post_type === false){
                $all_post_type = $post_type;
            }else if($all_post_type !== $post_type){
                $all_post_type = "all";
            }
        }
		if($all_post_type !== "all"){
			$filename = HUMANE_COMPOSER_DIR . "assets/posts/filters/". $all_post_type .".json";
            $filename = apply_filters("filters_file_name", $filename, $all_post_type);
			if(file_exists($filename)){
				$meta_filters = json_decode(file_get_contents($filename), true);
                if($saved_meta_filters) $meta_filters = $saved_meta_filters;
				foreach($meta_filters as $filter_key => $meta_filter){
					if($meta_filter['scope'] === 'Dropdown'){
						$meta_values = array();
						foreach($all_data_ids as $post_id){
                            $meta_value = get_post_meta($post_id, $meta_filter['key'], true);
                            if(is_string($meta_value) && $meta_filter["delimiter"]){
                                $meta_value = explode($meta_filter["delimiter"], $meta_value);
                            }
                            if(is_array($meta_value)){
                                $meta_value = array_map(function($a){ return trim($a); }, $meta_value);
                                $meta_values = array_merge($meta_values, $meta_value);
                            }else{
                                $meta_values[] = strval($meta_value);
                            }
						}
						$meta_values = array_count_values($meta_values);
                        unset($meta_values[""]);
						$final_values = array();
						foreach($meta_values as $meta_key => $meta_value){
							$final_values[] = array(
								"key" => strval($meta_key),
								"alias" => strval($meta_key),
								"external_count" => $meta_value
							);
						}
                        usort($final_values, array(__CLASS__, "sort_dropdown_values"));
						$meta_filters[$filter_key]['data'] = $final_values;
					}else if($meta_filter['scope'] === 'Slider'){
						$max = 0;
						$min = 10000000;
						foreach($all_data_ids as $post_id){
							$meta_value = get_post_meta($post_id, $meta_filter['key'], true);
							if(empty($meta_value)) $meta_value = 0;
							if($meta_value < $min){
								$min = $meta_value;
							}
							if($meta_value > $max){
								$max = $meta_value;
							}
						}
						$meta_filters[$filter_key]['minimum'] = $min;
						$meta_filters[$filter_key]['maximum'] = $max;
					}
				}
			}
		}
		$filters['filters'] = array_merge($filters['filters'], $meta_filters);
        $perspectives_json = array(
            "title" => "Filters",
            "filters" => array($filters),
			"special_type" => $all_post_type,
			"meta_filters" => $meta_filters
        );
        return $perspectives_json;
    }
        
    /**
     * Finds the difference between counts.
     * 
     * Used for sorting a dropdowns value
     * 
     * @param array $option_one
     * @param array $option_two
     * 
     * @return int Difference between counts of dropdown
     * 
     * @see Controller_Dashboard::get_filter_values() - Used By
     */
    public static function sort_dropdown_values($option_one, $option_two){
        return $option_two["external_count"] - $option_one["external_count"];
    }

    /**
     * Writes the Perspectives JSON to a local file for quick access.
     * 
     * Takes the ID of a Playlist and generates the filters part of the 
     * Perspectives JSON from the settings of the Playlist and global 
     * Automated Block settings 
     * 
     * @param int $id ID of the Playlist
     * @param int $limit Number of rows per page
     * 
     * @return array Perspectives JSON array for the given Playlist
     * 
     * @uses \Humane_Spaces\Model_Playlist
     * @uses Model_Posts::get_from_py_posts_view()
     * @uses Controller_Dashboard::get_filter_values() Calculates count values for each filter
     * 
     * @see Controller_Dashboard::build_perspective_json() - Used By
     * @see Model_Playlist::_save - Used By
     */
    public static function write_perspective_json($id = false, $limit = 20){
		if(!$id) $id = $_GET['id'];
		$perspectives_json = array();
        $filters = array();
		$sort = array();
        global $playlistdata;
        $playlist = $playlistdata;
        if(empty($playlist)){
            $playlist = new Model_Playlist($id);
        }
        
		add_filter("get_filtered_post_list_$id", function($data) use($id, $playlist){
            $playlist = new Model_Playlist($id);
            $results = maybe_unserialize($playlist->results);
            $blocked_stories = maybe_unserialize($playlist->blocked_stories);
            if(!is_array($blocked_stories)) $blocked_stories = array();
            $data = array_filter($data, function($datum) use($results, $blocked_stories){
                if(is_array($results) && in_array($datum, $results) && !in_array($datum, $blocked_stories)) return true;
                return false;
            });
            return $data;
		});
        $pageno = 1;
        $data = Model_Posts::get_from_py_posts_view($sort, $filters, $pageno, $limit, $type, array("publish"), true, false, array(
            "taxonomy" => "playlist",
            "term_id" => $id
        ));
        $total_count = $data["total_count"];
        $filtered_count = $data["count"];
        $all_data_ids = $data["all_results"];
        $data = $data["results"];
        if(!is_array($all_data_ids)) $all_data_ids = array();
		$perspectives_json = self::get_filter_values($all_data_ids, $playlist);
		$file = HUMANE_SPACES_DIR . "playlist_data/" . get_current_blog_id() . "_playlist-$id.json";
        $success = file_put_contents($file, json_encode($perspectives_json), FILE_TEXT );
        return $perspectives_json;
    }
    /**
     * Creates a body class based on the current admin page.
     *
     * @param string $classes The current admin body classes.
     * @return string
     */
    public static function set_admin_page_as_body_class($classes)
    {
        /**
         * For safety, always check for get_current_screen before using it.
         * May not be needed for how late `admin_body_class` is filtered.
         */
        if (!function_exists('get_current_screen')) {
            return $classes;
        }

        $screen = get_current_screen();

        /**
         * Build an array of the parts you want.
         *
         * $screen->base gives you the page pase, so "edit.php"'s base is "edit"
         * get_query_var: https://codex.wordpress.org/Function_Reference/get_query_var
         */
        $vars   = [
            "cognitively",
            $screen->base,
            $_GET['post_type'] ?? "",
            $_GET['post_status'] ?? "",
            $_GET['page'] ?? "",
            $_GET['view'] ?? "",
            "admin-page"
            // etc.
        ];

        // array_filter will remove any "empty" elements.
        $vars  = array_filter($vars);
        $class = implode('-', $vars);

        // Don't add our class more than once.
        if (false !== strpos($classes, $class)) {
            return $classes;
        }
        $current_user = wp_get_current_user();
        $is_pykih = is_super_admin() ? "pykih-email" : "not-pykih-email";
        $classes .= " {$class} {$is_pykih}";
        return $classes;
    }
    /**
     * Enqueues all scripts and styles required in the admin side
     * of dashboard
     * 
     * @return void
     */
    public static function enqueue_admin_scripts_styles(){
        $screen = get_current_screen();

        foreach (glob(HUMANE_COMPOSER_DIR . 'assets/dashboard/js/vendor/*.js') as $file) {
            $file = explode("/", $file);
            $file = $file[count($file) - 1];
            wp_enqueue_script($file, HUMANE_COMPOSER_URL . 'assets/dashboard/js/vendor/' . $file, array('jquery'));
        }
        wp_enqueue_script('humane-dashboard-js', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/dist/dashboard.min.js'), 'all');
        foreach (glob(HUMANE_COMPOSER_DIR . 'assets/dashboard/css/vendor/*.css') as $file) {
            $file = explode("/", $file);
            $file = $file[count($file) - 1];
            wp_enqueue_style($file, HUMANE_COMPOSER_URL . 'assets/dashboard/css/vendor/' . $file);
        }

        wp_enqueue_script('humane-tinymce', HUMANE_COMPOSER_URL. 'assets/utilities/js/tinymce.min.js');
        wp_enqueue_script('humane-theme-sort-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/sortable.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/sortable.min.js'), false);

        wp_enqueue_style('humane-dashboard-css', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.css', array(),  md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/dist/dashboard.min.css'), false);
        wp_enqueue_style('humane-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/humane.min.css', array(),  md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/humane.min.css'), false);
        wp_enqueue_style('humane-charts-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneCharts.min.css', array(),  md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneCharts.min.css'), false);
        wp_enqueue_style('humane-mapbox-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/mapbox.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/mapbox.min.css'), false);
        wp_enqueue_script('humane-mapbox-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/mapbox.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/mapbox.min.js'), false);
    }
    
    /**
     * Enqueues all scripts and styles required in the frontend side
     * of dashboard
     * 
     * @return void
     */
    public static function enqueue_frontend_scripts_styles(){
        wp_enqueue_script('humane-dashboard-js', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/dist/dashboard.min.js'), 'all');
        wp_enqueue_script('humane-query-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneQuery.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneQuery.min.js'), false);
        wp_enqueue_script('axios', "https://unpkg.com/axios/dist/axios.min.js", array(), false, false);

        wp_enqueue_style('dashicons');
        wp_enqueue_script('humane-element-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneElement.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneElement.min.js'), false);
        wp_localize_script( 'humane-element-js', 'map_object',
		array( 
			'token' => get_option("humane_mapbox_access_token")
		)
	    );
        wp_enqueue_script('humane-helper-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneHelper.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneHelper.min.js'), false);

        wp_enqueue_script('humane-connector-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneConnector.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneConnector.min.js'), false);
        wp_enqueue_script('humane-maps-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneMaps.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneMaps.min.js'), false);

        wp_enqueue_script('humane-charts-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneCharts.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneCharts.min.js'), false);
        wp_enqueue_script('humane-vizhelper-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneVizHelper.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneVizHelper.min.js'), false);

        wp_enqueue_script('humane-control-css', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneControl.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneControl.min.js'), false);
        wp_enqueue_style('humane-control-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneControl.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneControl.min.css'), false);
        wp_enqueue_style('humane-intro-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/intro.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/intro.min.css'), false);

        wp_enqueue_script('humane-slice-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumaneSlice.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumaneSlice.min.js'), false);
        wp_enqueue_style('humane-slice-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneSlice.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneSlice.min.css'), false);
        wp_enqueue_style('humane-maps-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneMaps.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneMaps.min.css'), false);
        wp_enqueue_style('humane-charts-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneCharts.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneCharts.min.css'), false);
        wp_enqueue_style('humane-vizhelper-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneVizHelper.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneVizHelper.min.css'), false);

        wp_enqueue_style('humane-element-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneElement.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneElement.min.css'), false);
        wp_enqueue_style('humane-charts-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumaneCharts.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumaneCharts.min.css'), false);
		wp_register_style('humane-mapbox-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/mapbox.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/mapbox.min.css') );
        if(!empty(get_option("humane_mapbox_access_token"))){
            wp_register_script('humane-mapbox-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/mapbox.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/mapbox.min.js'), false);
        }
        wp_enqueue_script('humane-jquery-mobile-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/jquery-mobile.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/jquery-mobile.min.js'), false);
        wp_enqueue_script('humane-jquery-sticky-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/jquery.sticky.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/jquery.sticky.js'), false);

        wp_register_script('humane-perspective-js', HUMANE_COMPOSER_URL. 'assets/utilities/js/HumanePerspectives.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/js/HumanePerspectives.min.js'), true);
        wp_enqueue_style('humane-perspective-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/HumanePerspectives.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/HumanePerspectives.min.css'), false);
        wp_enqueue_style('humane-dashboard-css', HUMANE_COMPOSER_URL . 'assets/dashboard/dist/dashboard.min.css', array(),  md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/dist/dashboard.min.css'), false);
        wp_enqueue_style('humane-css', HUMANE_COMPOSER_URL. 'assets/utilities/css/humane.min.css', array(),  md5_file(HUMANE_COMPOSER_DIR . 'assets/utilities/css/humane.min.css'), false);

        wp_enqueue_script('humane-synscroll-js', 'https://pyk-building-blocks.s3.ap-south-1.amazonaws.com/lib/syncscroll.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/dashboard/fonts/avenir-next/avenir-next-windows.css'), false);
        $google_analytics_id = \Humane_Sites\Controller_Options::get_humane_google_analytics_id(); 
        wp_localize_script( 'humane-dashboard-js', 'dashboard_object',
		array( 
			'google_analytics_id' => $google_analytics_id
		)
	    );
        
    }

    /**
     * Add dashboard URL to toolbar
     * 
     * @param object $admin_bar Admin Bar object
     * 
     * @return void
     */
    public static function add_toolbar_items($admin_bar){
        $image_url = HUMANE_COMPOSER_URL . "assets/images/humane-logo-sq.png";
        $user = wp_get_current_user();
        $roles = (array) $user->roles;
        if ($roles[0] !== "subscriber") {
            $args = array(
                'id' => 'custom-ui',
                'title' => "Dashboard",
                'href' => admin_url(),
                'meta' => array()
            );
            $admin_bar->add_node($args);
        }
    }
    
    /**
     * Add submenu page for dashboard
     * 
     * @return void
     */
    public static function create_admin_pages()
    {
        add_submenu_page('', __('Humane Club'), __('Humane Club'), 'manage_options', 'humane_dashboard', array(__CLASS__, 'index'));
    }

    /**
     * Renders index page to show analytics
     * 
     * @todo This view is broken, needs to be fixed
     * 
     * @return void
     */
    public static function index()
    {
        $analytics_data = Controller_Analytics::get_site_level_analytics();
        $top_page_analytics_data = Controller_Analytics::get_top_pages_analytics();
        $source_analytics_data = Controller_Analytics::get_sources_analytics();
        $device_analytics_data = Controller_Analytics::get_device_analytics();
        require HUMANE_COMPOSER_DIR . "views/dashboard/index.php";
    }

    /**
     * Generates mandatory pages that are required in every site
     * 
     * @return void
     */
    public static function generate_pages_from_json()
    {
        $all_pages = json_decode(file_get_contents(HUMANE_COMPOSER_DIR .'assets/dashboard/json/autogenerate.json'), true);
        self::auto_generate_pages($all_pages);
    }

    /**
     * Generates a given list of pages
     * 
     * @param array $all_pages List of pages to generate
     * 
     * @return void
     */
    public static function auto_generate_pages($all_pages) {
        // Create post object
        if(class_exists("\Humane_Spaces\Model_Playlist")) remove_action( 'save_post', array(Model_Playlist::class, "update_all_playlists_by_type"), 10, 1 );
        foreach ($all_pages as $page_args) {
            $page = get_page_by_title(wp_strip_all_tags($page_args["title"]));       
            if(empty($page) || ($page_args["regenerate"] ?? false)){
                $my_page = array(
                    'post_title'    => wp_strip_all_tags( $page_args["title"] ),
                    'post_content'  => $page_args["description"],
                    'post_status'   => 'publish',
                    'post_author'   => get_current_user_id(),
                    'post_type'     => 'page',
                );
                if(isset($page_args['parent'])) $my_page['post_parent'] = $page_args['parent'];
                if(isset($page_args['slug'])) $my_page['post_name'] = $page_args['slug'];

                // Insert the post into the database
                $id = wp_insert_post( $my_page );
                if($page_args["autogenerated"] === "false"){
                    update_post_meta($id, "humane_autogenerated_page", false);
                }else{
                    update_post_meta($id, "humane_autogenerated_page", true);
                }
                foreach($page_args["metadata"] as $key => $value){
                    update_post_meta($id, $key, $value);
                }
                update_post_meta($id, "updated_by", get_current_user_id());
            }
        }
        if(class_exists("\Humane_Spaces\Model_Playlist")) add_action( 'save_post', array(Model_Playlist::class, "update_all_playlists_by_type"), 10, 1 );
    }

    /**
     * Sets the recovery email to "the@humane.club"
     * 
     * @param string $email
     * @param string $url
     * 
     * @return string
     */
    public static function recovery_email_update($email, $url)
    {
        $email['to'] = 'the@humane.club';
        return $email;
    }

    /**
     * Adds allowance for SVG file type to be uploaded to WP Gallery
     * 
     * @param array $file_types Existing allowed file types
     * 
     * @return array
     */
    public static function add_file_types_to_uploads($file_types)
    {
        $new_filetypes = array();
        $new_filetypes['svg'] = 'image/svg+xml';
        $file_types = array_merge($file_types, $new_filetypes);
        return $file_types;
    }
    public static function humane_generate_csv(){
        if (isset($_GET['form_report'])) {
            $form_id  = $_GET['id'];
            global $wpdb;
            $csv_output = '';                                           //Assigning the variable to store all future CSV file's data
            $table = "{$wpdb->prefix}_py_user_events";            //For flexibility of the plugin and convenience, lets get the prefix
            $separator =",";
            $values = $wpdb->get_results($wpdb->prepare( "SELECT details_json FROM  $table where py_user_form_id=%d",$form_id));       //This here
            $i=0;
            $form = new Model_Forms($form_id);
            $form_fields = json_decode($form->fields_json);
            foreach ($form_fields as $key => $field) {
                if($field->type == "dropdown"){
                    $options = Model_Forms::get_form_dropdown($key);
                    $field->options = $options;
                    $form_fields->$key = $field;
                }
            }
            foreach ($form_fields as $key => $field):
                if( $field->show == "on"){
                    if('extra_field' === $key){
                        $data = explode(',', $field->label);
                        $assoc = array();
                        $j=0;
                        foreach ($data as $i => $value) {
                            if($j=== 0){
                                $j++;
                                continue;
                            }
                                $assoc[$value] = $value;
                                $j++;
                        }
                        $field->options= $assoc;
                        $field->label = current($data); 
                    }
                    $title = $field->label;
                    $csv_output = $csv_output.$title.',';
                }
            endforeach;
            $csv_output .= "\n";    
            foreach ($values as $rowr) {
                $data = maybe_unserialize($rowr->details_json);
                unset($data['contacted_at']);
                unset($data['source_page']);
                $fields = array_values((array) $data);
                $csv_output .= implode($separator, $fields);      //Generating string with field separator
                $csv_output .= "\n";    //Yeah...
                $i++;
            }
            $filename = "form_entries";
            $generatedDate = date('d-m-Y His'); 
            $csv = $csv_output;
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"" . $filename . "_" . $generatedDate . ".csv\";" );
            header("Content-Transfer-Encoding: binary");
            echo $csv;
            exit;
          }
    
    }
    public static function hc_add_preload_resources ($preload_resources) {
        $preload_resources[] = array(
            'href' => HUMANE_COMPOSER_URL. 'assets/dashboard/fonts/inter/inter.css?ver=1',
            'as' => 'font',
            'type' => 'font/woff2',
            'crossorigin' => true,
            'id'=> 'inter.css'
        );
        $preload_resources[] = array(
            'href' => HUMANE_COMPOSER_URL. 'assets/dashboard/fonts/raleway/raleway.css?ver=1',
            'as' => 'font',
            'type' => 'font/woff2',
            'crossorigin' => true,
            'id'=> 'raleway.css'
        );
        $preload_resources[] = array(
            'href' => HUMANE_COMPOSER_URL. 'assets/dashboard/fonts/avenir-next/avenir-next-windows.css?ver=1',
            'as' => 'font',
            'type' => 'font/woff2',
            'crossorigin' => true,
            'id'=> 'avenir-next-windows.css'
        );
        $preload_resources[] = array(
            'href' =>  HUMANE_COMPOSER_URL. 'assets/dashboard/fonts/avenir/avenir.css?ver=1',
            'as' => 'font',
            'type' => 'font/woff2',
            'crossorigin' => true,
            'id'=> 'avenir.css'
        );
        return $preload_resources;
    }
    public static function hc_add_image_sizes(){
        add_image_size( 'thumbnail-50', 50, 50,true );
    }
    public static function get_aggregations_icon($current_post_id){
        if( get_post_type($current_post_id) === "aggregation"){
            $domain_attachment_url = get_post_meta( $current_post_id, 'humane_aggregations_domain_attachment_50', true );
            if(!empty($domain_attachment_url)){
                return $domain_attachment_url;
            } else{
                return get_post_meta( $current_post_id, 'humane_aggregations_icon', true );
            }
            
        } else{
            $author_id = get_post_field('post_author', $current_post_id );
            $user_photo = get_user_meta($author_id, "humane_user_profile_photo", true);
            $cache_name  = md5( 'img_attachment_id'.$user_photo );
            $cache_group = 'group_img_attachment_id';
            $attachment_id  = wp_cache_get( $cache_name, $cache_group );
            if ( empty( $attachment_id ) ) {
                $attachment_id = attachment_url_to_postid( $user_photo );
                wp_cache_set( $cache_name, $attachment_id, $cache_group, 60 * HOUR_IN_SECONDS );
            }
            $user_photo    = wp_get_attachment_image_src(  $attachment_id,'thumbnail-50');
            if(is_array($user_photo)){
                $author_icon = $user_photo[0];
                return $author_icon;
            }
            if(empty($author_icon)) $author_icon = get_avatar_url($author_id, ['size' => '50']);
            return $author_icon;
        }
    }

public static function hc_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 460 <= $width ) {
		$sizes = '(max-width: 400px) 89vw, (max-width: 460px) 82vw, 460px';
	}
	return $sizes;
}
public static function remove_default_image_sizes( $sizes) {
    unset( $sizes['1536x1536']);
    unset( $sizes['2048x2048']);
    return $sizes;
}
public static function get_img_attachment_srcset_by_post_id($current_post_id){
    $img_attachment_id = self::hc_get_attachment_id($current_post_id);
   return wp_get_attachment_image_srcset( $img_attachment_id, 'thumbnail-53' );
}
public static function hc_get_attachment_id($current_post_id){
    if( get_post_type($current_post_id) === "aggregation"){
        $featured_image_url  = get_post_meta( $current_post_id, 'humane_aggregations_featured_image', true );
        $cache_name  = md5( 'img_attachment_id'.$featured_image_url );
		$cache_group = 'group_img_attachment_id';
        $img_attachment_id  = wp_cache_get( $cache_name, $cache_group );
		if ( empty( $img_attachment_id ) ) {
			$img_attachment_id = attachment_url_to_postid( $featured_image_url );
			wp_cache_set( $cache_name, $img_attachment_id, $cache_group, 60 * HOUR_IN_SECONDS );
		}
    } else{
        $img_attachment_id = get_post_thumbnail_id($current_post_id);
    }
    return $img_attachment_id;
}
public static function get_img_attachment_sizes_by_post_id($current_post_id){
    $attachment_id = self::hc_get_attachment_id($current_post_id);
   return wp_get_attachment_image_sizes( $attachment_id, 'thumbnail-53' );
}
}

Controller_Dashboard::init();