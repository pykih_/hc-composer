<?php

namespace Humane_Sites;

if (!defined('ABSPATH')) {
    exit();
}

require HUMANE_COMPOSER_DIR . 'models/modal_model.php';

/**
 * Handles the logic for Content Selection Modal
 * 
 * @package Humane Sites
 * @subpackage Content Selection Modal
 */
class Controller_Content_Selection_Modal
{

    /**
     * @var string JSON used to render the modal
     */
    public static $reference_json;

    /**
     * Attaches the hooks for Content Selection Modal
     * 
     * @return void
     */
    public static function init(){
        self::$reference_json = json_decode(file_get_contents(HUMANE_COMPOSER_DIR . 'assets/modal/json/selected-tabs.json'), true);
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_post_selection_modal_assets'));
        add_action('wp_ajax_pykih_get_tab_objects', array(__CLASS__, 'ajax_get_tab_objects'));
        add_action('wp_ajax_pykih_add_objects', array(__CLASS__, 'ajax_add_objects'));
        add_action('wp_ajax_trigger_modal_window', array( __CLASS__, 'ajax_trigger_modal_window' ) );

    }
    
	public static function ajax_trigger_modal_window() {
		if ( ! isset( $_POST['nonce'] ) ) {
			wp_die();
		}

		if ( ! wp_verify_nonce( $_POST['nonce'], 'trigger_modal_window' )
			|| ! current_user_can( 'edit_posts' ) ) {
			wp_die();
        }

        /* Get selected objects from Slice */
        $selected = $_POST["selected"];
        if(!is_array($selected)) $selected = array();

		$results = array();
		if ( ! empty( $selected ) ) {
            /* IDs need to be integers */
			$int_selected= array_map('intval', $selected);
            $current = array();
            /* Get all normal post's data */
            $current = get_posts( array(
                'post_type' => array('post','page','event', 'datastory', 'aggregation'),
                'post_status' => 'any',
                'include' => $int_selected,
                'posts_per_page' => -1,
                'ignore_sticky_posts'	=> true,
                'no_found_rows'		=> true
            ) );
		} else {
			$current = array();
        }
        /* Set type which will lock in the tab on left side of modal with green dot */
		if(! empty( $current ) && $current[0]->post_type) $results["type"] = $current[0]->post_type;
        else $results["type"] = "all";
        $results["data"] = $selected;
		if ( ! empty( $current ) ) {
			ob_start();
            $items = $current;
            $items = self::add_author_date_to_items($items);
            require HUMANE_COMPOSER_DIR . 'views/modal/index_selected-objects.html.php';
			$results['current'] = ob_get_clean();
		} else {
			$results['current'] = '<h3 class="modal-window-notice" style="text-align: center;">'. 'No articles selected yet. Click <a href="#" class="add-to-slice-link">here</a> to get started.'  . '</h3>';
        }
 		echo wp_send_json( $results );
		wp_die();
    }

	
	/**
	 * Add author data to items.
	 * 
	 * @todo change method name.
     * 
	 * @param object $items post items.
     * 
	 * @return object post items.
	 */
    public static function add_author_date_to_items($items){
        if(empty($items)) return $items;
        foreach ($items as $key => $item) {
            $item->date = $item->post_modified;
            $author = get_the_author_meta("first_name", $item->post_author);
            if(empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
            $item->author = $author;
            $items[$key] = $item;
        }
        return $items;
    }

    /**
     * Enqueue the JS and CSS for modal to work
     * 
     * @return void
     */
    public static function enqueue_post_selection_modal_assets(){
        // Edit Slice Custom scripts
        wp_enqueue_style('humane-modal-css', HUMANE_COMPOSER_URL . 'assets/modal/dist/modal.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/modal/dist/modal.min.css'), false);
        wp_enqueue_script('humane-modal-js', HUMANE_COMPOSER_URL . 'assets/modal/dist/modal.min.js', array('jquery'), md5_file(HUMANE_COMPOSER_DIR . 'assets/modal/dist/modal.min.js'), false);
        wp_enqueue_media();

        wp_localize_script(
            'humane-modal-js',
            'Humane_Blocks_Params',
            array(
                'nonce' => array(
                    "trigger_modal_window" => wp_create_nonce( 'trigger_modal_window' ),
                    'get_tab_objects' => wp_create_nonce('pykih_get_tab_objects'),
                    'add_objects' => wp_create_nonce('pykih_add_objects'),
                ),
                'text' => array(
                    'please_wait' => __('Please wait...', 'cognitively'),
                    'no_objects' => __('No articles added to slice yet. Click <a href="#"     class="add-to-slice-link">here</a> to get started.', 'cognitively'),
                )
            )
        );
    }

    /**
     * AJAX handler for adding objects in the modal
     * 
     * @return void
     */
    public static function ajax_add_objects(){
        if (!isset($_POST['nonce'])) {
            wp_die();
        }

        if (!wp_verify_nonce($_POST['nonce'], 'pykih_add_objects')) {
            wp_die();
        }

        if (!isset($_POST['post_ids']) || empty($_POST['post_ids'])) {
            wp_die();
        }

        $post_ids = $_POST['post_ids'];
        $results = Model_Content_Selection_Modal::render_selected_objects_view($post_ids);
        $results = apply_filters("ajax_add_objects", $results);
        echo wp_send_json($results);
        wp_die();
    }

    /**
     * AJAX handler that returns the markup based on tab selected
     * 
     * @return void
     */
    public static function ajax_get_tab_objects(){
        if (!isset($_POST['nonce'])) {
            wp_die();
        }

        if (!wp_verify_nonce($_POST['nonce'], 'pykih_get_tab_objects') || !current_user_can('edit_posts')) {
            wp_die();
        }
        $post_type = sanitize_text_field($_POST['post_type']);
        $action = sanitize_text_field($_POST['button_action'] ?? false);
        $page = $_POST['current_page'];
        if(empty($page)){
            $page =1;
        }
        $searchFor = $_POST['searchFor'];
        $selected = $_POST['selected'] ?? array();
        if (!empty($action)) {
            if ('next' === $action) $page = $page + 1;
            else $page = $page - 1;
        }

        if ($page < 1) wp_die();
        $per_page = 24;
        $offset = ($page - 1) * $per_page;

        // Necessary variables for the template

        $rendered_values = Model_Content_Selection_Modal::tab_view($searchFor, $selected, $post_type, $offset);

        $items = $rendered_values["items"];
        $total_count = $rendered_values["total_count"];
        $response = array();

        ob_start();
        if ($post_type == "attachment") Model_Content_Selection_Modal::render_media_gallery_view($items);
        else if ($post_type === "product") Model_Content_Selection_Modal::render_product_view($items);
        else if ($post_type === "subscription") Model_Content_Selection_Modal::render_subscription_view($items);
        else if ($post_type == "page") Model_Content_Selection_Modal::render_page_view($items);
        else Model_Content_Selection_Modal::render_default_view($items);
        if ($total_count > count($items)) require HUMANE_COMPOSER_DIR . 'views/modal/_pagination.html.php';
        $response["html"] = ob_get_clean();
        ob_end_clean();
        $response["num_items"] = count($items);
        $response = apply_filters("ajax_get_tab_objects", $response, $selected, $offset, $per_page, $searchFor, $post_type, $page);
        echo wp_send_json($response);
        wp_die();
    }

    /**
     * Renders the markup of the modal based on arguments passed
     * 
     * @param string $reference_json
     * @param string $special_class
     * @param array $attributes
     * 
     * @return string Markup of the modal
     */
    public static function create_post_modal_markup($reference_json = false, $special_class = "content-selection-modal", $attributes = array()){
        $reference_json = apply_filters("create_post_modal_markup", $reference_json);
        if ($reference_json === false) $reference_json = self::$reference_json;
        require HUMANE_COMPOSER_DIR . 'views/modal/edit.html.php';
    }
}
Controller_Content_Selection_Modal::init();