<?php

namespace Humane_Sites;

if (!defined('ABSPATH')) exit;

require_once HUMANE_COMPOSER_DIR . 'models/users_model.php';

use Humane_Spaces\Model_Playlist;
use Humane_Acquire\Controller_Playlists;

/**
 * Handles all User related logic
 * 
 * @package Humane Sites
 * @subpackage Users
 */
class Controller_Users
{
    /**
     * Attaches all hooks related to Users
     * 
     * @return void
     */
    public static function init(){
        add_action('admin_menu', array(__CLASS__, 'add_submenu'), 10);
        // add example_role capabilities, priority must be after the initial role definition
        add_action('admin_init', array(__CLASS__, 'add_custom_capability'), 11);
        // Add custom user photo field.
        add_action( 'show_user_profile', array( __CLASS__, 'user_photo_field' ));
        add_action( 'edit_user_profile', array( __CLASS__, 'user_photo_field' ));
        add_action( 'user_new_form', array( __CLASS__, 'user_photo_field' ));
        add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 1);
        //save user profile image
        add_action('profile_update', array( __CLASS__, 'save_user_meta' ));
        add_action('init', array( Model_Users::class, 'update' ));
        add_action('wpmu_new_user', array( __CLASS__, 'save_user_photo_field' ));
        add_action('wpmu_new_user', array( __CLASS__, 'init_user_fields' ));
        add_filter( 'get_avatar_url', array( __CLASS__, 'modify_get_avatar_function' ), 10, 3);  
        //remove selected edit user fields
        remove_action( 'admin_color_scheme_picker', array( __CLASS__, 'admin_color_scheme_picker' ));
        add_filter( 'wpmu_signup_user_notification', array( __CLASS__, 'hc_skp_auto_activate_users' ), 10, 4);  
        add_action( 'user_new_form', array( __CLASS__, 'hc_skp_confirmation_email' ), 9 );
        add_filter('map_meta_cap', array( __CLASS__, 'restore_users_edit_caps'), 99, 4);
        remove_all_filters('enable_edit_any_user_configuration');
        add_filter('enable_edit_any_user_configuration', '__return_true');
    }

    /**
     * Enqeueues styles and scripts for Users
     * 
     * @return void
     */
    public static function enqueue_scripts(){
        wp_enqueue_script( 'humane-user-profile-js', HUMANE_COMPOSER_URL . 'assets/users/dist/users.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/users/dist/users.min.js'), 'all' );
        wp_enqueue_style( 'humane-user-profile-css', HUMANE_COMPOSER_URL . 'assets/users/dist/users.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/users/dist/users.min.css'), 'all' );
    }

    /**
     * Allows admin to upload user photo from inside WordPress instead of Gravatar
     * 
     * @return void
     */
    public static function user_photo_field($user){
        require HUMANE_COMPOSER_DIR . 'views/users/edit.html.php';
    }

    /**
     * Modifies the avatar URL to use custom profile photo field
     * 
     * @param string $avatar_url
     * @param mixed $id_or_email
     * @param mixed $args
     * 
     * @return string Avatar URL
     */
    public static function modify_get_avatar_function( $avatar_url, $id_or_email, $args ) {
        // If there's no avatar, the default will be used, which results in 404 response
        $profile_pic = get_user_meta($id_or_email, 'humane_user_profile_photo', true);
        if( !empty($profile_pic) ){
            return $profile_pic;
        }
        // Return img html
        return $avatar_url;
    }

    /**
     * Initializes all custom fields of a user when they register
     * 
     * @param int $user_id
     * 
     * @return void
     */
    public static function init_user_fields($user_id){
        $keys = array(
            'humane_user_profile_photo',
            'first_name',
            'last_name'
        );
        foreach($keys as $key){
            $val = get_user_meta($user_id, $key);
            if(!isset($val) || empty($val)) update_user_meta($user_id, $key, "");
        }
    }
    /**
     * Saves meta values of a user
     * 
     * @param int $user_id
     * 
     * @return void
     */
    public static function save_user_meta($user_id){
        if( current_user_can('edit_users') ){
            $profile_pic = empty($_POST['humane_user_profile_photo']) ? '' : $_POST['humane_user_profile_photo'];
            update_user_meta($user_id, 'humane_user_profile_photo', $profile_pic);
            $designation = empty($_POST['humane_user_designation']) ? '' : $_POST['humane_user_designation'];
            update_user_option($user_id, 'humane_user_designation', $designation);
        }
    }
    /**
     * Adds / removes custom capabilites for a user role
     * 
     * @return void
     */
    public static function add_custom_capability(){
        remove_role( 'humane_seo_manager' );
        $seo_role = add_role("humane_seo_manager", "SEO Team", array(
            'edit_posts' => true,
            'publish_posts' => true,
            'edit_others_posts' => true,
            'edit_published_posts' => true,
            'edit_pages' => true,
            'edit_others_pages' => false,
            'delete_posts' => false,
            'delete_others_posts' => false,
            'delete_pages' => false,
            'delete_others_pages' => false,
            'read' => true,
            'aioseo_manage_seo' => true
        ));
        $u = new \WP_User(get_current_user_id());
        $role = get_role('administrator');
        if ($role) {
            $role->add_cap('manage_options', true);
            $role->add_cap('manage_options_admin', true);
            $role->add_cap('manage_options_editor', true);
        }
        $role = get_role('editor');
        if ($role) {
            $role->add_cap('manage_options_editor', true);
        }
        $role = get_role('author');
        if ($role) {
            $role->add_cap('manage_options', true);
        }
        $role = get_role('contributor');
        if ($role) {
            $role->remove_cap('manage_options');
        }
    }

    /**
     * Adds admin page for customer
     * 
     * @return void
     */
    public static function add_submenu(){
        add_submenu_page('', __('Show Customer'), __('Show Customer'), 'manage_options', 'wp_users_customer', array(__CLASS__, 'show_customer'));
    }
   //Save user profile image a user metafield.
   public static function save_user_photo_field($user_id){
        if( current_user_can('edit_users') ){ 
            $first_name = empty($_POST['first_name']) ? '' : $_POST['first_name'];
            update_user_meta($user_id, 'first_name', $first_name);
            $last_name = empty($_POST['last_name']) ? '' : $_POST['last_name'];
            update_user_meta($user_id, 'last_name', $last_name);
            $profile_pic = empty($_POST['humane_user_profile_photo']) ? '' : $_POST['humane_user_profile_photo'];
            update_user_meta($user_id, 'humane_user_profile_photo', $profile_pic);
            $designation = empty($_POST['humane_user_designation']) ? '' : $_POST['humane_user_designation'];
            update_user_option($user_id, 'humane_user_designation', $designation);
        }
    }
    /**
     * Renders show page for a customer
     * 
     * @return void
     */
    public static function show_customer(){
        $user_id = $_GET["id"];
        if (!$user_id) wp_redirect(admin_url() . "admin.php?page=wp_users_administrators");
        require HUMANE_COMPOSER_DIR . 'views/users/show.html.php';
    }
    public static function hc_skp_confirmation_email(){
        if ( current_user_can( 'manage_network_users' ) ){
            return false;
        }
    ?>
        <table class="form-table">
        <tr class="user-first-name-wrap">
            <th><label for="first_name">First Name</label></th>
            <td><input type="text" name="first_name" id="first_name" value="" class="regular-text"></td>
        </tr>
        <tr class="user-last-name-wrap">
            <th><label for="last_name">Last Name</label></th>
            <td><input type="text" name="last_name" id="last_name" value="" class="regular-text"></td>
        </tr>
            <tr>
                <th scope="row"><?php _e('Skip Confirmation Email') ?></th>
                <td><input type = "checkbox" name = "noconfirmation" value = "1" checked /> Add the user without sending an email that requires their confirmation.</td>
            </tr>

        </table>
    <?php
    }
    public static function hc_skp_auto_activate_users($user, $user_email, $key, $meta){
        if(!current_user_can('manage_options'))
            return false;
            if ( current_user_can( 'manage_network_users' ) ){
                return false;
            }
            if(isset($_POST['noconfirmation']) && !empty($_POST['noconfirmation'])){
                remove_action( 'register_new_user', 'wp_send_new_user_notifications' );
	            remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10, 2 );
                $new_user = wpmu_activate_signup($key);
                if ( is_wp_error( $new_user ) ) {
                    $redirect = add_query_arg( array( 'update' => 'addnoconfirmation' ), 'user-new.php' );
                } elseif ( ! is_user_member_of_blog( $new_user['user_id'] ) ) {
                    $redirect = add_query_arg( array( 'update' => 'created_could_not_add' ), 'user-new.php' );
                } else {
                    $redirect = add_query_arg(
                        array(
                            'update'  => 'addnoconfirmation',
                            'user_id' => $new_user['user_id'],
                        ),
                        'user-new.php'
                    );
            }
            wp_redirect( $redirect );
            die();
        }
    }
    /**
     * restore edit_users, delete_users, create_users capabilities for non-superadmin users under multisite
     * (code is provided by http://wordpress.org/support/profile/sjobidoo)
     * 
     * @param type $caps
     * @param type $cap
     * @param type $user_id
     * @param type $args
     * @return type
     */
    public static function restore_users_edit_caps($caps, $cap, $user_id, $args) {
        foreach ($caps as $key => $capability) {
            if ($capability != 'do_not_allow')
                continue;
            switch ($cap) {
                case 'edit_user':
                case 'edit_users':
                    $caps[$key] = 'edit_users';
                    break;
                case 'delete_user':
                case 'delete_users':
                    $caps[$key] = 'delete_users';
                    break;
                case 'create_users':
                    $caps[$key] = $cap;
                    break;
            }
        }
        return $caps;
    }
}

Controller_Users::init();
