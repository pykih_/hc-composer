<?php

namespace Humane_Sites;

require_once HUMANE_COMPOSER_DIR . "models/settings_model.php";

use Humane_Acquire\Model_Forms;

if (!defined('ABSPATH')) exit();

/**
 * Handles all core Settings related logic
 * 
 * @package Humane Sites
 * @subpackage Settings
 */
class Controller_Options
{
    /**
     * @var array Dropdown options for colors
     */
    public static $color_options;

    /**
     * @var array Dropdown options for weights
     */
    public static $weight_options;

    /**
     * Attaches all hooks related to options
     * 
     * @return void
     */
    public static function init(){
        self::$color_options = array(
            "var(--primary-dark)" => "Primary Dark",
            "var(--primary-main)" => "Primary Main",
            "var(--primary-pastel)" => "Primary Pastel",
            "var(--primary-faded)" => "Primary Faded",
            "var(--secondary-main)" => "Secondary Main",
            "var(--red)" => "Red"
        );
        self::$weight_options = array(
            "var(--regular)" => "400",
            "var(--medium)" => "500",
            "var(--semi-bold)" => "600",
            "var(--bold)" => "800",
            "var(--extra-bold)" => "900"
        );
        add_action("admin_menu", array(__CLASS__, "create_admin_pages"));
        add_action('admin_init', array(Model_Options::class, 'update'), 1);
        add_action('admin_head', array(__CLASS__, 'set_theme_variables'));
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_scripts_styles'));
        add_filter('wp_mail_from', array(__CLASS__, 'change_support_email'), 10, 1);
        add_filter('wp_mail_from_name', array(__CLASS__, 'change_support_name'), 10, 1);
    }

    /**
     * Changes the default support mail name
     * 
     * @param string $from_name
     * 
     * @return string Support name
     */
    public static function change_support_name($from_name){
        if(!empty(get_option("support_email"))){
            return get_bloginfo('name');
        }
        return $from_name;
    }
    
    /**
     * Changes the default support mail email address
     * 
     * @param string $from_email
     * 
     * @return string Support email
     */
    public static function change_support_email($from_email){
        if(!empty(get_option("support_email"))){
            return get_option("support_email");
        }
        return $from_email;
    }

    /**
     * Enqueues all scripts and styles required for settings
     * 
     * @return void
     */
    public static function enqueue_scripts_styles(){
        wp_enqueue_script('humane-options-css', HUMANE_COMPOSER_URL . 'assets/settings/dist/settings.min.js', array("jquery"), md5_file(HUMANE_COMPOSER_DIR . 'assets/settings/dist/settings.min.js'), 'all');
    }

    /**
     * Sets the CSS variables for theme based on default and saved values
     * 
     * @return void
     */
    public static function set_theme_variables(){
        $default = json_decode(file_get_contents(HUMANE_COMPOSER_DIR . 'assets/settings/json/defaults.json'), true);

        //get theme settings from options table
        $theme_settings = maybe_unserialize(get_option("humane_theme_settings"));
        $carousel_card_size = "220px";
        ?>
            <style>
                /**
                Set root variables that are used throughout the theme and in live preview
                */
                :root {
                    /**
                    Navigation Root Variables
                    */
                    <?php foreach ($default as $key => $value) : ?>--<?php echo $key; ?>: <?php echo Model_Options::get_or_default($theme_settings, $default, $key); ?>;
                    <?php endforeach; ?>--content-carousel-card-size: <?php echo $carousel_card_size ?>;
                }
            </style>
        <?php
    }

    /**
     * Creates the admin pages for settings
     * 
     * @return void
     */
    public static function create_admin_pages(){
        add_submenu_page('', 
            'Site', 
            'Site', 
            'manage_options_editor', 
            'site_settings', 
            array(__CLASS__, 'site_settings')
        );
        add_submenu_page(
            '',
            'Style Guide', 
            'Style Guide', 
            'manage_options_admin', 
            'styleguide_settings', 
            array(__CLASS__, 'styleguide_settings')
        );
    }

    /**
     * Contains all general site related settings
     * 
     * @return void
     */
    public static function site_settings(){
        require HUMANE_COMPOSER_DIR . 'views/settings/site.html.php';
    }

    /**
     * Contains all theme style related settings
     * 
     * @return void
     */
    public static function styleguide_settings(){
        $color_options = self::$color_options;
        $weight_options = self::$weight_options;
        require HUMANE_COMPOSER_DIR . 'views/settings/styleguide.html.php';
    }

    /**
     * Checks if login wall is activated
     * 
     * @return bool
     */
    public static function is_login_wall_activated(){
        $login_wall = get_option("turn_on_login_wall");
        if(isset($login_wall) && $login_wall == "yes") {
            return true;
        }
        else return false;
    }

    /**
     * Checks if sign up is activated
     * 
     * @return bool
     */
    public static function is_sign_up_activated(){
        $value = get_option("turn_on_sign_up");
        if($value !== "no") {
            return true;
        }
        else return false;
    }

    /**
     * Gets the GTM Tracking ID
     * 
     * @return string
     */
    public static function get_google_tag_manager_ID(){
        $google_tag_manager_ID = get_option("humane_gtm_tracking_id");
        if($google_tag_manager_ID) return $google_tag_manager_ID;
        return false;
    }

    /**
     * Checks if preview card is activated
     * 
     * @return bool
     */
    public static function is_preview_card_activated(){
        // $disable_preview_card = get_option("disable_preview_card");
        // if(isset($disable_preview_card) && $disable_preview_card) return false;
        return true;
    }

    /**
     * Returns all color values set in the theme
     * 
     * @return array
     */
    public static function get_all_theme_pallete_colors(){
        $theme_settings = get_option("humane_theme_settings");
        $colors = [
            "primary-dark",
            "primary-main",
            "primary-pastel",
            "primary-faded",
            "secondary-main",
            "red",    
            "brightness-7",
            "brightness-20",
            "brightness-46",
            "brightness-60",
            "brightness-86",
            "brightness-93",
            "brightness-97",
            "brightness-100",
        ];
        $pallete_colors = array();
        foreach ($colors as $color_key) {
            $color_value = isset($theme_settings[$color_key]) ? $theme_settings[$color_key] : Model_Options::get_default($color_key);
            $pallete_color = array(
                'name' => $color_key,
                'slug' => $color_key,
                'color' => $color_value,
            );
            array_push($pallete_colors, $pallete_color);
        }
        return $pallete_colors;
    }

    /**
     * Checks which logo needs to be shown: Rectange or Square
     * 
     * @param string $option
     * 
     * @return bool
     */
    public static function show_rect_logo($option){
        $rect_logo = get_option($option);
        if(isset($rect_logo) && $rect_logo == "yes") {
            return true;
        }
        else return false;
    }
    /**
     * Get google ad id.
     * 
     * @return string
     */
    public static function get_google_ad_id(){
        $google_ad_id = get_option("humane_google_ad_id");
        if($google_ad_id) return $google_ad_id;
        return false;
    }
    /**
     * Gets the google analytics id
     * 
     * @return string
     */
    public static function get_humane_google_analytics_id(){
        $humane_google_analytics_id = get_option("humane_google_analytics_id");
        if($humane_google_analytics_id) return $humane_google_analytics_id;
        return false;
    }
}

Controller_Options::init();
