<?php
    namespace Humane_Sites;
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

    if ( ! defined( "ABSPATH" ) ) exit;
    if(class_exists("WP_List_Table")){

        /**
         * Extends the core WP_List_Table class
         */
        class Humane_List_Table extends \WP_List_Table {
            /**
             * @var string Name of the database table
             */
            public $table_name;

            /**
             * @var string Slug of the page to be opened 
             *             on click of the name
             */
            public $name_url_slug;

            /**
             * @var string Slug of the edit page
             */
            public $edit_slug;

            /**
             * @var string Slug of the show page
             */
            public $show_slug;

            /**
             * @var string Slug of the index page 
             */
            public $slug;

            /**
             * @var string Nonce for deleting
             */
            public $delete_nonce;

            /**
             * @var bool True if bulk actions are enabled
             */
            public $enable_bulk_actions; 

            /**
             * @var string Column for string to be searched in
             */
            public $search_column;

            /**
             * @var string Type of the backlink
             */
            public $backlink_type;

            /**
             * Returns the array of bulk actions for custom tables
             * 
             * Currently only has bulk delete
             * 
             * @return array Array of bulk actions
             */
            function get_bulk_actions() {
                if(!$this->enable_bulk_actions) return array();
                $actions = array(
                  'bulk-delete'    => 'Delete'
                );
                return $actions;
            }

            /**
             * Returns the markup of the checkbox
             * 
             * @param object $item
             * 
             * @return string Checkbox markup
             */
            function column_cb($item) {
                if(!$this->enable_bulk_actions) return;
                return sprintf(
                    '<input type="checkbox" name="item[]" value="%s" />', $item->id
                );    
            }

            /**
             * Deletes multiple records based on selected items
             * 
             * @return void
             */
            function process_bulk_action() {
                // Detect when a bulk action is being triggered
                if ( 'bulk-delete' === $this->current_action() ) {
            
                    // In our file that handles the request, verify the nonce.
                    $nonce = esc_attr( $_GET['nonce'] );
                    if ( wp_verify_nonce( $nonce, "bulk-delete-nonce" ) ) {
                        $delete_ids = $_GET['item'];
                        // loop over the array of record IDs and delete them
                        foreach ( $delete_ids as $id ) {
                            $this->delete_record($id);
                        }
                        // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
                        // add_query_arg() return the current url
                        if(isset($_GET["id"]))
                            wp_redirect( sprintf("?page=%s&id=%s", $_GET["page"], $_GET["id"]) );
                        else
                            wp_redirect( sprintf("?page=%s", $_GET["page"]) );
                        exit;
                    }
                }

            }
            
            /**
             * Deletes a record based on given ID
             * 
             * @param int $id ID of record to be deleted
             * 
             * @return void
             */
            protected function delete_record($id){}

            /**
             * Adds actions based on slugs set in this object
             * 
             * @param string $value
             * @param object $item
             * 
             * @return string 
             */
            function add_actions($value, $item){
                $name_url_slug = $this->name_url_slug;   
                $id = $item->id;
                $actions = array(
                    'edit'      => sprintf('<a href="?page=%s&id=%s">Edit</a>', $this->edit_slug, $item->id),
                    'show'      => sprintf('<a class="%s" href="?page=%s&id=%s">Show</a>', $show_class, $this->show_slug, $item->id),
                    'delete'    => sprintf('<a href="?page=%s&nonce=%s&id_deleted=%s">Delete</a>', $this->slug, $this->delete_nonce,$item->id),
                );
                if(empty($this->edit_slug)) unset($actions['edit']);
                if(empty($this->show_slug)) unset($actions['show']);
                return sprintf('%1$s %2$s', "<strong><a class='row-title' href='/wp-admin/admin.php?id=$id&amp;page=$name_url_slug'>$value</a></strong>", $this->row_actions($actions) );
            }

            /**
             * Renders the name column along with actions
             * 
             * @param object $item
             * 
             * @return string 
             */
            function column_name($item) {
                $value = $item->name;
                return $this->add_actions($value, $item);
            }

            /**
             * Generates the table navigation above or below the table and removes the
             * _wp_http_referrer and _wpnonce because it generates a error about URL too large
             * 
             * @param string $which 
             * 
             * @return void
             */
            function display_tablenav( $which ) {
                ?>
                <div class="tablenav <?php echo esc_attr( $which ); ?>">
                    <?php if($which === "top"): ?>
                        <div class="alignleft actions">
                            <input type="hidden" name="page" value="<?php echo $_GET["page"]; ?>" />
                            <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" />
                            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("bulk-delete-nonce"); ?>" />
                            <?php $this->bulk_actions(); ?>
                        </div>
                        <?php if(!empty($this->search_column)): ?>
                            <div class="alignright hc-ml-8">
                                <?php $this->search_box('Search', 'search_id'); ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php
                    $this->extra_tablenav( $which );
                    $this->pagination( $which );
                    ?>
                    <br class="clear" />
                </div>
                <?php
            }

            /**
             * Renders a column value
             * 
             * Default renderer that is used if a renderer for a given
             * column is not present in the table object
             * 
             * @param object $item
             * @param string $column_name
             * 
             * @return string
             */
            function column_default( $item, $column_name ) {
                return $item->$column_name;
            }

            /**
             * Renders the backlinks column
             * 
             * @param object $item
             * 
             * @return string 
             */
            function column_backlinks($item){
                global $wpdb;
                $object_table = $wpdb->prefix . $this->table_name;
                $references = new Model_Backlinks(array('object_table' => $object_table , 'object_id' => $item->id));
                $referenced_in  = maybe_unserialize($references->referenced_in); 
                if(is_array($referenced_in)) {
                    $count_references = count($referenced_in);
                } else {
                    $count_references = "—";
                }
                echo '<a href="' . admin_url() . 'admin.php?page=humane_backlinks&id=' . $item->id . '&type=' . $this->backlink_type . '">' . $count_references . '</a>';
            }

            /**
             * Renders the created by column
             * 
             * @param object $item
             * 
             * @return string
             */
            function column_created_by( $item ) {
                $name = Model_Users::to_s($item->created_by);
                $url = get_author_posts_url($item->created_by);
                return "<a href='$url'>$name</a>";
            }

            /**
             * Renders the updated by column
             * 
             * @param object $item
             * 
             * @return string
             */
            function column_updated_by( $item ) {
                $name = Model_Users::to_s($item->updated_by);
                $url = get_author_posts_url($item->updated_by);
                return "<a href='$url'>$name</a>";
            }

            /**
             * Renders the created at column
             * 
             * @param object $item
             * 
             * @return string
             */
            function column_created_at($item) {
                return date("F j, Y H:s", $item->created_at);
            }

            /**
             * Renders the updated at column
             * 
             * @param object $item
             * 
             * @return string
             */
            function column_updated_at($item) {
                return date("F j, Y H:s", $item->updated_at);
            }

            /**
             * Gets the data to be displayed in the table
             * 
             * @param array $args Arguments to filter the data
             * 
             * @return array Items to be rendered in the table
             */
            function get_data($args = array()){
                global $wpdb;

                $where = "";
                $count = 0;
                  /* -- Pagination parameters -- */
                //Number of elements in your table?

                //How many to display per page?
                $perpage = 20;

                //Which page is this?
                $paged = !empty($_GET["paged"]) ? $_GET["paged"] : "";
                //Page Number
                if(empty($paged) || !is_numeric($paged) || $paged <= 0 ){ 
                    $paged=1; 
                } 
                if (isset($args['where'])) {
                    $where = "";
                    foreach($args['where'] as $column => $condition) {
                        $val = $condition['value'];
                        if(is_array($val)){
                            $val = "(" . implode(",", $val) . ")";
                        }
                        if(isset($condition['special_key'])) $column = $condition['special_key'];
                        
                        if($condition['type'] === 'string' || $condition['type'] === 'date') {
                            $value = "'{$val}'";
                        } else {
                            $value = "{$val}";
                        }
                        $operator = "";
                        if (isset($condition['operator']) && $count) {
                            $operator = $condition['operator'] ?? "AND";
                        }
                        $count++;
                        $comparator = "=";
                        if (isset($condition['comparator'])) {
                            $comparator = $condition['comparator'] ?? "=";
                        }
                        $where .= " {$operator} {$column} {$comparator} {$value}";
                    }
                }
                $limit = isset($args['limit']) ? $args['limit'] : $perpage;
                $offset = isset($args['offset']) ? $args['offset'] : $perpage * ($paged - 1);
                $order = isset($args['order']) ? $args['order'] : 'desc';
                $order_by = isset($args['order_by']) ? (string) $args['order_by'] : 'id';
                $columns = $args["columns"] ?? "*";
                if($_GET['type'] === 'dataset'){
                    $table_name = $wpdb->prefix.'_py_10000fts';
                }elseif($_GET['type'] === 'nudge'){
                    $table_name = $wpdb->prefix.'_py_playlists';
                } else{
                    $table_name = $wpdb->prefix.$this->table_name;
                }
                if(isset($args['where'])){
                    if(empty($_GET["s"])){
                        $result = $wpdb->prepare(
                            "SELECT {$columns} from {$table_name} WHERE {$where} ORDER BY {$order_by} {$order}"
                        );
                    }else{
                        $search = $wpdb->esc_like($_GET["s"]); 
                        $searchColumn = $this->search_column;
                        $result = $wpdb->prepare(
                            "SELECT {$columns} from {$wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' AND {$where} ORDER BY {$order_by} {$order}", $search
                        );
                    }
                }else{
                    if(empty($_GET["s"])){
                        $result = $wpdb->prepare(
                            "SELECT {$columns} from {$wpdb->prefix}{$this->table_name} ORDER BY {$order_by} {$order}"
                        );
                    }else{
                        $search = $wpdb->esc_like($_GET["s"]);
                        $searchColumn = $this->search_column;
                        $result = $wpdb->prepare(
                            "SELECT {$columns} from {$wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' ORDER BY {$order_by} {$order}", $search
                        );
                    }
                }
                $returned_result = $wpdb->get_results($result);
                $returned_result = apply_filters("filter_table_data", $returned_result);
                $totalitems = count($returned_result);
                
                //How many pages do we have in total? 
                $totalpages = ceil($totalitems / $perpage); //adjust the query to take pagination into account 
                /* -- Register the pagination -- */ 
                $this->set_pagination_args( array(
                    "total_items" => $totalitems,
                    "total_pages" => $totalpages,
                    "per_page" => $perpage,
                ) );
                /* -- Fetch the items -- */
                $returned_result = array_slice($returned_result, $offset, $perpage);
                return $returned_result;
            }

            function prepare_items() {
                $columns = $this->get_columns();
                /** Process bulk action */
		        $this->process_bulk_action();
                $hidden = array();
                $sortable = array();
                $this->_column_headers = $this->get_column_info();
                $this->items = $this->get_data();
            }
        }
    }

?>