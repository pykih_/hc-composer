<?php

namespace Humane_Sites;

use Humane_Sites\Controller_Dashboard;

if (!defined('ABSPATH')) exit;

require_once HUMANE_COMPOSER_DIR . 'db/user_events_db.php';
require_once HUMANE_COMPOSER_DIR . 'models/user_events_model.php';
require_once HUMANE_COMPOSER_DIR . 'notifications/user_events_notification.php';
require_once HUMANE_COMPOSER_DIR . 'views/user_events/index.php';


use Humane_Sites\Model_Users;

/**
 * Handles all logic related to User Events
 * 
 * @package Humane Sites
 * @subpackage User Events
 */
class Controller_User_Events
{
    /**
     * Attaches hooks for User Events
     * 
     * @return void
     */
    public static function init()
    {
        add_action('init', array(__CLASS__, 'start_session'), 1);
        add_action('admin_init', array(__CLASS__, 'delete'), 1);
        add_action('template_redirect', array(__CLASS__, 'capture_datapoint'), 10);
    }

    /**
     * Handles deleting of a User Events
     * 
     * @return void
     */
    public static function delete()
    {
        if (wp_verify_nonce($_GET['nonce'] ?? "", 'humane_user_events_destroy')) {
            $deleted_form = $_GET['id_deleted'];
            $delete = Model_User_Events::destroy($deleted_form);
        }
    }

    /**
     * Gets domain from a URL
     * 
     * @param string $url
     * 
     * @return string Domain
     */
    public static function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) return $regs['domain'];
    }

    /**
     * Starts a session for the current user
     * 
     * @return void
     */
    public static function start_session()
    {
        if (!session_id()) {
            session_start([
                'read_and_close' => true,
            ]);
            $referrer = $_SERVER["HTTP_REFERER"] ?? "";
            $referrer_domain = self::get_domain($referrer);
            $current_domain = get_site_url();
            $current_domain = self::get_domain($current_domain);
            if ($current_domain === $referrer_domain) $referrer = "No Referer";
            $_SESSION["referral_source"] = $referrer ?? "No Referer";
        }
    }

    /**
     * Adds the current page to the user flow array in the session
     * 
     * @return void
     */
    public static function capture_datapoint()
    {
        $stack = $_SESSION["user_flow"] ?? array();
        $id = get_the_ID();
        if (!empty($id)) array_push($stack, $id);
        while (count($stack) > 20) array_shift($stack);
        $_SESSION["user_flow"] = $stack;
    }
}

Controller_User_Events::init();
