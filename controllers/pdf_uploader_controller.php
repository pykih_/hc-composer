<?php
namespace Humane_Sites;

class Controller_PDF_Uploader{

    public static function init(){
        add_action('admin_menu', array(__CLASS__, 'pdf_uploader_admin_menu'));
        add_action('init', array(__CLASS__, 'handle_media_library_pdf_upload'));
        add_shortcode('media_library_pdf_uploader', array(__CLASS__, 'media_library_pdf_uploader_form'));
    }


    // Shortcode to display the upload form
    public static function media_library_pdf_uploader_form(){
        if (!current_user_can('upload_files')) {
            return 'You do not have permission to upload files.';
        }

        $html = '<form action="' . esc_url($_SERVER['REQUEST_URI']) . '" method="post" enctype="multipart/form-data">';
        $html .= wp_nonce_field('pdf_nonce', 'pdf_nonce_field');
        $html .= '<input type="file" class="hc-p-20" name="media_library_pdf" accept="application/pdf"><br><br>';
        $html .= '<input type="submit" name="media_library_pdf_upload" value="Upload" class="button button-primary">';
        $html .= '</form>';

        return $html;
    }
    // Handle the file upload
    public static function handle_media_library_pdf_upload() {
        if (isset($_POST['media_library_pdf_upload']) && isset($_FILES['media_library_pdf'])) {
            if (!wp_verify_nonce($_POST['pdf_nonce_field'], 'pdf_nonce')) {
                die('Security check failed.');
            }

            require_once(ABSPATH . 'wp-admin/includes/media.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            $file_id = media_handle_upload('media_library_pdf', 0);
            if (is_wp_error($file_id)) {
                echo "Error uploading file: " . $file_id->get_error_message();
            } else {
                echo "File upload successful! File ID: " . $file_id;
            }
        }
    }

    // Add a new menu to the admin interface
    public static function pdf_uploader_admin_menu() {

        // add_submenu_page(
        //     'options-general.php',
        //     '🟩 PDF Uploader', 
        //     '🟩 PDF Uploader', 
        //     'manage_options', 
        //     'pdf_uploader_settings_page', 
        //     array(__CLASS__, 'pdf_uploader_settings_page')
        // );

        add_media_page(
            '🟩 Large Files', 
            '🟩 Large Files', 
            'manage_options', 
            'pdf_uploader_settings_page', 
            array(__CLASS__, 'pdf_uploader_settings_page')
        );

    }

    // Admin page content
    public static function pdf_uploader_settings_page() {
        echo '<div class="wrap"><h2>Upload Large Files to Media Library</h2><p class="hc-mb-20">We only support uploading images and videos to your WordPress website that are no more than 3 MB. In fact, it is recommended that it be smaller than 500 kb each because image and video size impacts your website performance.</p><p>However, we have created this functionality to upload PDF reports which can go up to 10 MB in size.</p><br/>';
        echo Controller_PDF_Uploader::media_library_pdf_uploader_form();
        echo '</div>';
    }
} 

Controller_PDF_Uploader::init();

