<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
/**
 * Contains all hooks and logics for Form Fields
 * 
 * @package Humane Sites
 * @subpackage Form Fields
 */
class Controller_Form_Fields {

    /**
     * Attaches all hooks for form fields
     * 
     * @return void
     */
    public static function init(){
        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_assets'));
        add_action( 'wp_ajax_get_dropdown_list', array( __CLASS__, 'get_dropdown_list' ) );
        add_action( 'wp_ajax_get_cities', array( __CLASS__, 'get_cities' ) );
    }

    /**
     * Enqueue script for form fields
     * 
     * @return void
     */
    public static function enqueue_assets(){
        wp_enqueue_script('humane-form-js', HUMANE_COMPOSER_URL . 'assets/form_fields/dist/form_fields.min.js', array('jquery'), md5_file(HUMANE_COMPOSER_DIR . 'assets/form_fields/dist/form_fields.min.js'), false);
        wp_localize_script( 'humane-form-js',
        'Humane_Form_Fields_Params', array(
            'nonce' => array(
				'get_dropdown_list' => wp_create_nonce( 'get_dropdown_list' ),
                'get_cities' => wp_create_nonce('get_cities')
            )
        ) );
    }
    /**
	 * Get the values to display in the cities dropdown
     * 
	 * @return void
	 */
	public static function get_cities() {
        $response = json_encode( array() );
		if ( ! isset( $_POST['nonce'] ) ) {
			echo $response;
			wp_die();
		}
		if ( ! wp_verify_nonce( $_POST['nonce'], 'get_cities' ) ) {
			echo $response;
			wp_die();
		}
		$response = array();
        $cities = array();
        $countries = $_POST["country"];
        $all_countries = json_decode(file_get_contents(HUMANE_COMPOSER_DIR.'assets/form_fields/json/countries.min.json'), true);
        if(!is_array($countries)) $countries = array($countries);
        foreach($countries as $country){
            $all_cities = $all_countries[$country];
            foreach($all_cities as $key => $city ){
                $cities[] = array("value" => $city, "text" => $city);
            }
        }
        
        $response['results'] = $cities;
        echo wp_send_json($response);
		wp_die();
	}

    /**
	 * Get value for searchable dropdowns
     * 
     * Used mainly for dropdowns which may have a lot of 
     * values to display normally. Currently used for tags
     * and posts.
     * 
	 * @return void
	 */
	public static function get_dropdown_list() {
        $response = json_encode( array() );
		if ( ! isset( $_POST['nonce'] ) ) {
			echo $response;
			wp_die();
		}
		if ( ! wp_verify_nonce( $_POST['nonce'], 'get_dropdown_list' ) ) {
			echo $response;
			wp_die();
		}
		$response = array();
		if($_POST["type"] === "selected_tags"){
			$tags = get_terms("post_tag", array(
				'hide_empty' => false,
				'fields' => 'id=>name'
			));
			foreach($tags as $tag_id => $tag_name){
				$response['results'][] = array("value" => $tag_id, "text" => $tag_name);
			};
		}else{
			$posts = get_posts( array(
				'posts_per_page' => -1,
				'ignore_sticky_posts'	=> true,
				'no_found_rows'		=> true,
				'post_type' => Controller_Posts::filter_existing_post_type("playlist"),
				'fields' => "ids"
			));
			foreach ($posts as $post) {
				$post_title = html_entity_decode(get_the_title($post));
				$post_type = get_post_type($post);
				if (strpos(strtolower($post_title), strtolower($_POST["search"])) !== false) {
					$response['results'][] = array("value"=> $post, "text"=> $post_title, "post_type" => $post_type);
				}
			}
		}

		echo wp_send_json($response);
		wp_die();
	}

    /**
     * Formats a given form field value to prevent issues
     * 
     * @param mixed $value Value to be formatted
     * @param array $attributes Attributes of the form field
     * 
     * @return mixed Formatted value
     */
    public static function format_value($value, $attributes){
        if(!isset($value)) $value = null;
        $value = $value ?? $attributes["default"] ?? "";
        if(is_string($value)) $value = stripslashes($value);
        if($attributes["multiple"] && !is_array($value)){
            $value = explode(",", $value);
        }
        if(is_array($value)){
            $value = array_map(function($a){
                return trim($a);
            }, $value);
            $value = array_filter($value, function($a){
                return !empty($a);
            });
        }
        return $value;
    }

    /**
     * Checks the email of the user
     * 
     * @return bool True if user belongs to pykih,
     *              False otherwise
     */
    public static function check_pykih_user(){
        $is_pykih = is_super_admin() ? true : false;
        return $is_pykih;
    }

    /**
     * Renders the description of a form field
     * 
     * @param array $attributes
     * 
     * @return void
     */
    public static function description($attributes){
        ?>
            <p class="hc-mt-8 hc-text-brightness-60 hc-supernormal-xs">
                <?php echo esc_html( $attributes['description'] ) ?>
            </p>
        <?php
    }

    /**
     * Renders the label for a given form field based on attributes
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_label( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        if(empty($attributes["caption_class"])) $attributes["caption_class"] = "hc-mb-8";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/label.php';
    }

    /**
     * Renders the form field for a hidden input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_hidden( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/hidden.php';
    }

    /**
     * Renders the form field for a text based input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_text( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/text.php';
    }

    /**
     * Renders the form field for a password input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_password( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/password.php';
    }

    /**
     * Renders the form field for a URL input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_url( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/url.php';
    }

    /**
     * Renders the form field for a button input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_button( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/button.php';
    }

    /**
     * Renders the form field for select dropdown input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_select( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/select.php';
    }

    /**
     * Renders the form field for a document input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_attach_document( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/document.php';
    }

    /**
     * Renders the form field for a file input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_file( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/file.php';
    }

    /**
     * Renders the form field for a photo input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_photo( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/photo.php';
    }

    /**
     * Renders the form field for a color input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_color( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/color.php';
    }

    /**
     * Renders the form field for a checkbox input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_checkbox( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/checkbox.php';
    }

    /**
     * Renders the form field for a date input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_date( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/date.php';
    }

    /**
     * Renders the form field for a datetime input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_datetime( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/datetime.php';
    }

    /**
     * Renders the form field for a toggle input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_toggle( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        require HUMANE_COMPOSER_DIR . 'views/form_fields/toggle.php';
    }

    /**
     * Renders the form field for a round toggle input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_toggle_round( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        require HUMANE_COMPOSER_DIR . 'views/form_fields/toggle_round.php';
    }

    /**
     * Renders the form field for an email input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_email( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $attributes["type"] = "email";
        self::input_text($name, $label, $is_required, $value, $attributes);
    }

    /**
     * Renders the form field for a textarea input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_textarea( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/textarea.php';
    }

    /**
     * Renders the form field for a number input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_number( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/number.php';
    }

    /**
     * Renders the form field for a radio button input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_radio( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/radio.php';
    }
    /**
     * Renders the form field for a location input
     * 
     * Used countries and cities JSON along with some custom JS.
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_location( $name, $label, $is_required, $value, $attributes = array() ) {
        $all_countries = json_decode(file_get_contents(HUMANE_COMPOSER_DIR.'assets/form_fields/json/countries.min.json'), true);
        $countries = array();
        $value = Controller_Form_Fields::format_value($value, $attributes);
        foreach($all_countries as $country => $cities ){
            $countries[$country] = $country;
        }
        $attributes["options"] = $countries;
        if(!$attributes["multiple"]){
            $temp_value = array($value);
        }else{
            $temp_value = $value;
        }
        $cities = array();
        foreach($temp_value as $temp_country){
            $all_cities = $all_countries[$temp_country];
            foreach($all_cities as $key => $city ){
                $cities[$city] = $city;
            }
        }
        $attributes["city_options"] = $cities;
        require HUMANE_COMPOSER_DIR . 'views/form_fields/location.php';
    }

    /**
     * Renders the form field for select with option grouping input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_select_optgroup( $name, $label, $is_required, $value, $attributes = array() ) {
        if($attributes["humane_team_only"] && self::check_pykih_user()) return; 
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/select_grouped.php';
    }

    /**
     * Renders the form field for a Content Selection Modal input
     * 
     * @param string $name
     * @param string $label
     * @param bool $is_required
     * @param mixed $value
     * @param array $attributes
     * 
     * @return void
     */
    public static function input_selection_upload( $name, $label, $is_required, $value, $attributes ) {
        $render_class = ($attributes["render"] ?? true) ? "" : "hc-display-none";
        require HUMANE_COMPOSER_DIR . 'views/form_fields/selection_upload.php';
    }
}

Controller_Form_Fields::init();