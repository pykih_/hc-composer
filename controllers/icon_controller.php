<?php 

namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

use stdClass;

/**
 * Handles the logic for icons
 */
class Controller_Icons{

    /**
     * Returns the empty or filled bookmark icon
     * 
     * Returns empty icon if user has not bookmarked this post,
     * Returns full icon if user has bookmarked this post.
     * 
     * @param int $post_id ID for which bookmark icon is needed
     * @param int $user_id User for which bookmarked check is run
     * 
     * @return string SVG of the icon
     */
    public static function get_bookmark_icon($post_id, $user_id = false){
        if(Controller_Options::is_login_wall_activated()){
            if($user_id === false) $user_id = get_current_user_id();
            if(!$user_id) return false;
            $bookmarked_posts = get_user_option( 'bookmarked_posts', $user_id);
            if(is_array($bookmarked_posts) && !in_array($post_id, $bookmarked_posts)){
                return self::get_svg_icons("bookmark");
            }else{
                return self::get_svg_icons("bookmarked");
            }
        }else{
            return false;
        }
    }

    /**
     * Returns the empty or filled cart icon
     * 
     * Returns empty icon if user has not put this post in cart,
     * Returns full icon if user has put this post in cart.
     * 
     * @param int $post_id ID for which bookmark icon is needed
     * @param int $user_id User for which bookmarked check is run
     * 
     * @return string SVG of the icon
     */
    public static function get_cart_icon($post_id, $user_id = false){
        if(get_post_type($post_id) !== "product") return false;
        if($user_id === false) $user_id = get_current_user_id();
        $cart = get_user_option('humane_user_cart', $user_id);
        $cart = maybe_unserialize($cart);
        if(!in_array($post_id, $cart)){
            return self::get_svg_icons("cart");
        }else{
            return self::get_svg_icons("cart-remove");
        }
    }

    /**
     * Renders an SVG icon based on name and other parameters
     * 
     * @param string $icon_name Name of the icon 
     * @param int $height Height of the icon
     * @param string $fill Fill color of the icon 
     * @param string $stroke Stroke color
     * 
     * @return string SVG string of the icon
     */
    public static function get_svg_icons($icon_name, $height = 30,  $fill = "black", $stroke = "none"){
        if(empty($icon_name)) return false;
        $svg_icon = HUMANE_COMPOSER_DIR . 'assets/dashboard/images/'.$icon_name.'.svg';
        $svg_icon = file_get_contents( $svg_icon );
        ob_start();
        ?>
            <span class="hc-icon-container" style="height: <?php echo $height; ?>px; fill: <?php echo $fill; ?>; stroke: <?php echo $stroke; ?>">
                <?php echo $svg_icon; ?>
            </span>
        <?php
        $svg_icon = ob_get_clean();
        return $svg_icon;
    }


    /**
     * Renders a badge that shows publish status
     * 
     * @param string $value Status value
     * 
     * @return string Markup of the publish status
     */
    public static function publish_status($value) {
        $colors = array(
            'publish' =>'#00990F',
            'autocreated' => 'var(--brightness-46)',
			'future' =>'#BEF549',
			'draft' =>'#828282',
			'homepage' =>'#FFA800'
        );
        $titles = array(
            'publish' => 'Published',
            'autocreated' => 'Auto Created',
            'future' => 'Scheduled',
            'draft' => 'Draft',
            'homepage' => 'Homepage'
        );
        $color = $colors[$value];
        $title = $titles[$value];
        return "<div title='$title' style='background-color: $color; color: white !important;' class='hc-mr-8 hc-mb-8 hc-badge hc-border-rounded-16 hc-bg-brightness-86 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center'>$title</div>";
    }

    /**
     * Renders the icon of a view in the automated block dashboard
     * 
     * @param string $value Name of the view
     * 
     * @return string
     */
    public static function view_icon($value){
        $view_icons = array(
            "Table" => '
            <svg enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m21.5 23h-19c-1.378 0-2.5-1.122-2.5-2.5v-17c0-1.378 1.122-2.5 2.5-2.5h19c1.378 0 2.5 1.122 2.5 2.5v17c0 1.378-1.122 2.5-2.5 2.5zm-19-21c-.827 0-1.5.673-1.5 1.5v17c0 .827.673 1.5 1.5 1.5h19c.827 0 1.5-.673 1.5-1.5v-17c0-.827-.673-1.5-1.5-1.5z"/><path d="m23.5 8h-23c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h23c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m23.5 13h-23c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h23c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m23.5 18h-23c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h23c.276 0 .5.224.5.5s-.224.5-.5.5z"/><path d="m6.5 23c-.276 0-.5-.224-.5-.5v-15c0-.276.224-.5.5-.5s.5.224.5.5v15c0 .276-.224.5-.5.5z"/><path d="m12 23c-.276 0-.5-.224-.5-.5v-15c0-.276.224-.5.5-.5s.5.224.5.5v15c0 .276-.224.5-.5.5z"/><path d="m17.5 23c-.276 0-.5-.224-.5-.5v-15c0-.276.224-.5.5-.5s.5.224.5.5v15c0 .276-.224.5-.5.5z"/></svg>',
            "Map" => '
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 480.1 480.1" style="enable-background:new 0 0 480.1 480.1;" xml:space="preserve">
                <g>
                    <g>
                        <path d="M240.135,0.05C144.085,0.036,57.277,57.289,19.472,145.586l-2.992,0.992l1.16,3.48
                            c-49.776,122.766,9.393,262.639,132.159,312.415c28.673,11.626,59.324,17.594,90.265,17.577
                            c132.548,0.02,240.016-107.416,240.036-239.964S372.684,0.069,240.135,0.05z M428.388,361.054l-12.324-12.316V320.05
                            c0.014-1.238-0.26-2.462-0.8-3.576l-31.2-62.312V224.05c0-2.674-1.335-5.172-3.56-6.656l-24-16
                            c-1.881-1.256-4.206-1.657-6.4-1.104l-29.392,7.344l-49.368-21.184l-6.792-47.584l18.824-18.816h40.408l13.6,20.44
                            c1.228,1.838,3.163,3.087,5.344,3.448l48,8c1.286,0.216,2.604,0.111,3.84-0.304l44.208-14.736
                            C475.855,208.053,471.889,293.634,428.388,361.054z M395.392,78.882l-13.008,8.672l-36.264-7.256l-23.528-7.832
                            c-1.44-0.489-2.99-0.551-4.464-0.176l-29.744,7.432l-13.04-4.344l9.664-19.328h27.056c1.241,0.001,2.465-0.286,3.576-0.84
                            l27.68-13.84C362.382,51.32,379.918,63.952,395.392,78.882z M152.44,33.914l19.2,12.8c0.944,0.628,2.01,1.048,3.128,1.232
                            l38.768,6.464l-3.784,11.32l-20.2,6.744c-1.809,0.602-3.344,1.83-4.328,3.464l-22.976,38.288l-36.904,22.144l-54.4,7.768
                            c-3.943,0.557-6.875,3.93-6.88,7.912v24c0,2.122,0.844,4.156,2.344,5.656l13.656,13.656v13.744l-33.28-22.192l-12.072-36.216
                            C57.68,98.218,99.777,56.458,152.44,33.914z M129.664,296.21l-36.16-7.24l-13.44-26.808v-18.8l29.808-29.808l11.032,22.072
                            c1.355,2.712,4.128,4.425,7.16,4.424h51.472l21.672,36.12c1.446,2.407,4.048,3.879,6.856,3.88h22.24l-5.6,28.056l-30.288,30.288
                            c-1.503,1.499-2.349,3.533-2.352,5.656v20l-28.8,21.6c-2.014,1.511-3.2,3.882-3.2,6.4v28.896l-9.952-3.296l-14.048-35.136V304.05
                            C136.065,300.248,133.389,296.97,129.664,296.21z M105.616,419.191C30.187,362.602-1.712,264.826,25.832,174.642l6.648,19.936
                            c0.56,1.687,1.666,3.14,3.144,4.128l39.88,26.584l-9.096,9.104c-1.5,1.5-2.344,3.534-2.344,5.656v24
                            c-0.001,1.241,0.286,2.465,0.84,3.576l16,32c1.108,2.21,3.175,3.784,5.6,4.264l33.6,6.712v73.448
                            c-0.001,1.016,0.192,2.024,0.568,2.968l16,40c0.876,2.185,2.67,3.874,4.904,4.616l24,8c0.802,0.272,1.642,0.412,2.488,0.416
                            c4.418,0,8-3.582,8-8v-36l28.8-21.6c2.014-1.511,3.2-3.882,3.2-6.4v-20.688l29.656-29.656c1.115-1.117,1.875-2.54,2.184-4.088
                            l8-40c0.866-4.333-1.944-8.547-6.277-9.413c-0.515-0.103-1.038-0.155-1.563-0.155h-27.472l-21.672-36.12
                            c-1.446-2.407-4.048-3.879-6.856-3.88h-51.056l-13.744-27.576c-1.151-2.302-3.339-3.91-5.88-4.32
                            c-2.54-0.439-5.133,0.399-6.936,2.24l-10.384,10.344V192.05c0-2.122-0.844-4.156-2.344-5.656l-13.656-13.656v-13.752l49.136-7.016
                            c1.055-0.153,2.07-0.515,2.984-1.064l40-24c1.122-0.674,2.062-1.614,2.736-2.736l22.48-37.464l21.192-7.072
                            c2.393-0.785,4.271-2.662,5.056-5.056l8-24c1.386-4.195-0.891-8.72-5.086-10.106c-0.387-0.128-0.784-0.226-1.186-0.294
                            l-46.304-7.72l-8.136-5.424c50.343-16.386,104.869-14.358,153.856,5.72l-14.616,7.296h-30.112c-3.047-0.017-5.838,1.699-7.2,4.424
                            l-16,32c-1.971,3.954-0.364,8.758,3.59,10.729c0.337,0.168,0.685,0.312,1.042,0.431l24,8c1.44,0.489,2.99,0.551,4.464,0.176
                            l29.744-7.432l21.792,7.256c0.312,0.112,0.633,0.198,0.96,0.256l40,8c2.08,0.424,4.244-0.002,6.008-1.184l18.208-12.144
                            c8.961,9.981,17.014,20.741,24.064,32.152l-39.36,13.12l-42.616-7.104l-14.08-21.12c-1.476-2.213-3.956-3.547-6.616-3.56h-48
                            c-2.122,0-4.156,0.844-5.656,2.344l-24,24c-1.782,1.781-2.621,4.298-2.264,6.792l8,56c0.403,2.769,2.223,5.126,4.8,6.216l56,24
                            c1.604,0.695,3.394,0.838,5.088,0.408l28.568-7.144l17.464,11.664v27.72c-0.014,1.238,0.26,2.462,0.8,3.576l31.2,62.312v30.112
                            c0,2.122,0.844,4.156,2.344,5.656l16.736,16.744C344.921,473.383,204.549,493.415,105.616,419.191z"/>
                    </g>
                </g>
            </svg>
            ',
            "Gallery" => '
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="512" height="512"><g id="Grid"><path d="M40,23H24a1,1,0,0,0-1,1V40a1,1,0,0,0,1,1H40a1,1,0,0,0,1-1V24A1,1,0,0,0,40,23ZM39,39H25V25H39Z"/><path d="M40,1H24a1,1,0,0,0-1,1V18a1,1,0,0,0,1,1H40a1,1,0,0,0,1-1V2A1,1,0,0,0,40,1ZM39,17H25V3H39Z"/><path d="M40,45H24a1,1,0,0,0-1,1V62a1,1,0,0,0,1,1H40a1,1,0,0,0,1-1V46A1,1,0,0,0,40,45ZM39,61H25V47H39Z"/><path d="M18,23H2a1,1,0,0,0-1,1V40a1,1,0,0,0,1,1H18a1,1,0,0,0,1-1V24A1,1,0,0,0,18,23ZM17,39H3V25H17Z"/><path d="M18,1H2A1,1,0,0,0,1,2V18a1,1,0,0,0,1,1H18a1,1,0,0,0,1-1V2A1,1,0,0,0,18,1ZM17,17H3V3H17Z"/><path d="M18,45H2a1,1,0,0,0-1,1V62a1,1,0,0,0,1,1H18a1,1,0,0,0,1-1V46A1,1,0,0,0,18,45ZM17,61H3V47H17Z"/><path d="M62,23H46a1,1,0,0,0-1,1V40a1,1,0,0,0,1,1H62a1,1,0,0,0,1-1V24A1,1,0,0,0,62,23ZM61,39H47V25H61Z"/><path d="M62,1H46a1,1,0,0,0-1,1V18a1,1,0,0,0,1,1H62a1,1,0,0,0,1-1V2A1,1,0,0,0,62,1ZM61,17H47V3H61Z"/><path d="M62,45H46a1,1,0,0,0-1,1V62a1,1,0,0,0,1,1H62a1,1,0,0,0,1-1V46A1,1,0,0,0,62,45ZM61,61H47V47H61Z"/><path d="M5,6h5a1,1,0,0,0,0-2H5A1,1,0,0,0,5,6Z"/><path d="M13,6h1a1,1,0,0,0,0-2H13a1,1,0,0,0,0,2Z"/></g></svg>
            ',
            "List" => '
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 394.971 394.971" style="enable-background:new 0 0 394.971 394.971;" xml:space="preserve">
                <g>
                    <g>
                        <g>
                            <path d="M56.424,146.286c-28.277,0-51.2,22.923-51.2,51.2s22.923,51.2,51.2,51.2s51.2-22.923,51.2-51.2
                                S84.701,146.286,56.424,146.286z M56.424,227.788L56.424,227.788c-16.735,0-30.302-13.567-30.302-30.302
                                s13.567-30.302,30.302-30.302c16.735,0,30.302,13.567,30.302,30.302S73.16,227.788,56.424,227.788z"/>
                            <path d="M379.298,187.037H143.151c-5.771,0-10.449,4.678-10.449,10.449s4.678,10.449,10.449,10.449h236.147
                                c5.771,0,10.449-4.678,10.449-10.449S385.069,187.037,379.298,187.037z"/>
                            <path d="M56.424,0c-28.277,0-51.2,22.923-51.2,51.2s22.923,51.2,51.2,51.2s51.2-22.923,51.2-51.2S84.701,0,56.424,0z
                                M56.424,81.502c-16.735,0-30.302-13.567-30.302-30.302s13.567-30.302,30.302-30.302S86.726,34.465,86.726,51.2
                                S73.16,81.502,56.424,81.502z"/>
                            <path d="M143.151,61.649h236.147c5.771,0,10.449-4.678,10.449-10.449s-4.678-10.449-10.449-10.449H143.151
                                c-5.771,0-10.449,4.678-10.449,10.449S137.38,61.649,143.151,61.649z"/>
                            <path d="M56.424,292.571c-28.277,0-51.2,22.923-51.2,51.2c0,28.277,22.923,51.2,51.2,51.2s51.2-22.923,51.2-51.2
                                C107.624,315.494,84.701,292.571,56.424,292.571z M86.726,343.771c0,16.735-13.567,30.302-30.302,30.302v0
                                c-16.735,0-30.302-13.567-30.302-30.302c0-16.735,13.567-30.302,30.302-30.302S86.726,327.036,86.726,343.771L86.726,343.771z"/>
                            <path d="M379.298,333.322H143.151c-5.771,0-10.449,4.678-10.449,10.449s4.678,10.449,10.449,10.449h236.147
                                c5.771,0,10.449-4.678,10.449-10.449S385.069,333.322,379.298,333.322z"/>
                        </g>
                    </g>
                </g>
            </svg>
            '
        );
        return $view_icons[$value] ?? $view_icons["Table"];
    }

    /**
     * Renders the icon of a datatype used in automated dashboard
     * 
     * @param string $value Name of the datatype
     * 
     * @return string Markup of the icon
     */
    public static function datatype($value){
        $datatype_icons = array(
            "number" => '<svg width="24" height="24" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4 9H20" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M4 15.0001H20" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M10 3L8 21" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M16 3L14 21" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            ',
            "multi-select" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2.75 6C2.75 5.17 3.42 4.5 4.25 4.5C5.08 4.5 5.75 5.17 5.75 6C5.75 6.83 5.08 7.5 4.25 7.5C3.42 7.5 2.75 6.83 2.75 6ZM2.75 12C2.75 11.17 3.42 10.5 4.25 10.5C5.08 10.5 5.75 11.17 5.75 12C5.75 12.83 5.08 13.5 4.25 13.5C3.42 13.5 2.75 12.83 2.75 12ZM4.25 16.5C3.42 16.5 2.75 17.18 2.75 18C2.75 18.82 3.43 19.5 4.25 19.5C5.07 19.5 5.75 18.82 5.75 18C5.75 17.18 5.08 16.5 4.25 16.5ZM21.25 19H7.25V17H21.25V19ZM7.25 13H21.25V11H7.25V13ZM7.25 7V5H21.25V7H7.25Z" fill="black"/>
                </svg>
            ',
            "string" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2.5 7.5V4.5H15.5V7.5H10.5V19.5H7.5V7.5H2.5ZM12.5 9.5H21.5V12.5H18.5V19.5H15.5V12.5H12.5V9.5Z" fill="black"/>
                </svg>
            ',
            "text" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2.5 7.5V4.5H15.5V7.5H10.5V19.5H7.5V7.5H2.5ZM12.5 9.5H21.5V12.5H18.5V19.5H15.5V12.5H12.5V9.5Z" fill="black"/>
                </svg>
            ',
            "url" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M11 15H7C5.35 15 4 13.65 4 12C4 10.35 5.35 9 7 9H11V7H7C4.24 7 2 9.24 2 12C2 14.76 4.24 17 7 17H11V15ZM17 7H13V9H17C18.65 9 20 10.35 20 12C20 13.65 18.65 15 17 15H13V17H17C19.76 17 22 14.76 22 12C22 9.24 19.76 7 17 7ZM16 11H8V13H16V11Z" fill="black"/>
                </svg>
            ',
            "email" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12V13.43C22 15.4 20.47 17 18.5 17C17.31 17 16.19 16.42 15.54 15.53C14.64 16.44 13.38 17 12 17C9.24 17 7 14.76 7 12C7 9.24 9.24 7 12 7C14.76 7 17 9.24 17 12V13.43C17 14.22 17.71 15 18.5 15C19.29 15 20 14.22 20 13.43V12C20 7.66 16.34 4 12 4C7.66 4 4 7.66 4 12C4 16.34 7.66 20 12 20H17V22H12C6.48 22 2 17.52 2 12ZM9 12C9 13.66 10.34 15 12 15C13.66 15 15 13.66 15 12C15 10.34 13.66 9 12 9C10.34 9 9 10.34 9 12Z" fill="black"/>
                </svg>
            ',
            "select" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C6.48 2 2 6.48 2 12C2 17.52 6.48 22 12 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 12 2ZM12 4C16.41 4 20 7.59 20 12C20 16.41 16.41 20 12 20C7.59 20 4 16.41 4 12C4 7.59 7.59 4 12 4ZM8 11L12 15L16 11H8Z" fill="black"/>
                </svg>
            ',
            "date" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M19 4H18V2H16V4H8V2H6V4H5C3.9 4 3 4.9 3 6V20C3 21.1 3.9 22 5 22H19C20.1 22 21 21.1 21 20V6C21 4.9 20.1 4 19 4ZM19 20H5V10H19V20ZM5 6V8H19V6H5ZM7 12H17V14H7V12ZM14 16H7V18H14V16Z" fill="black"/>
                </svg>
            ',
            "image" => '<svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M20 0H2C1 0 0 1 0 2V16C0 17.1 0.9 18 2 18H20C21 18 22 17 22 16V2C22 1 21 0 20 0ZM20 15.92C19.9861 15.9409 19.9624 15.9618 19.9426 15.9794C19.9339 15.987 19.9261 15.9939 19.92 16H2V2.08L2.08 2H19.91C19.9309 2.01394 19.9518 2.03761 19.9694 2.05744C19.977 2.06605 19.9839 2.07394 19.99 2.08V15.92H20ZM7.5 9.5L10 12.51L13.5 8L18 14H4L7.5 9.5Z" fill="black"/>
                </svg>        
            ',
            "accounting" => '<svg width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.38997 7.9C4.11997 7.31 3.38997 6.7 3.38997 5.75C3.38997 4.66 4.39997 3.9 6.08997 3.9C7.86997 3.9 8.52997 4.75 8.58997 6H10.8C10.73 4.28 9.67997 2.7 7.58997 2.19V0H4.58997V2.16C2.64997 2.58 1.08997 3.84 1.08997 5.77C1.08997 8.08 2.99997 9.23 5.78997 9.9C8.28997 10.5 8.78997 11.38 8.78997 12.31C8.78997 13 8.29997 14.1 6.08997 14.1C4.02997 14.1 3.21997 13.18 3.10997 12H0.909973C1.02997 14.19 2.66997 15.42 4.58997 15.83V18H7.58997V15.85C9.53997 15.48 11.09 14.35 11.09 12.3C11.09 9.46 8.65997 8.49 6.38997 7.9Z" fill="black"/>
                </svg>
            ',
            "percent" => '<svg width="12" height="16" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 4C0 6.20914 1.7269 8 3.85714 8C5.98738 8 7.71429 6.20914 7.71429 4C7.71429 1.79086 5.98738 0 3.85714 0C1.7269 0 0 1.79086 0 4ZM14.1428 16C12.0126 16 10.2857 14.2091 10.2857 12C10.2857 9.79086 12.0126 8 14.1428 8C16.2731 8 18 9.79086 18 12C18 14.2091 16.2731 16 14.1428 16ZM14.1429 14.6667C15.5631 14.6667 16.7143 13.4728 16.7143 12C16.7143 10.5272 15.5631 9.33333 14.1429 9.33333C12.7227 9.33333 11.5715 10.5272 11.5715 12C11.5715 13.4728 12.7227 14.6667 14.1429 14.6667ZM3.85713 6.66667C5.2773 6.66667 6.42856 5.47276 6.42856 4C6.42856 2.52724 5.2773 1.33333 3.85713 1.33333C2.43697 1.33333 1.28571 2.52724 1.28571 4C1.28571 5.47276 2.43697 6.66667 3.85713 6.66667ZM9.89914 7.64145L14.5103 1.87916L13.5858 1.36897L3.85711 14.1245L4.78156 14.6346L9.89914 7.64145Z" fill="black"/>
                </svg>        
            ',
            "latitude" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C15.31 2 18 4.69 18 8C18 12.5 12 19 12 19C12 19 6 12.5 6 8C6 4.69 8.69 2 12 2ZM19 22V20H5V22H19ZM8 8C8 5.79 9.79 4 12 4C14.21 4 16 5.79 16 8C16 10.13 13.92 13.46 12 15.91C10.08 13.47 8 10.13 8 8ZM10 8C10 6.9 10.9 6 12 6C13.1 6 14 6.9 14 8C14 9.1 13.11 10 12 10C10.9 10 10 9.1 10 8Z" fill="black"/>
                </svg>
            '
        );
        return $datatype_icons[$value] ?? $datatype_icons["string"];
    }
}