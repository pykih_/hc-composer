<?php 

namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

use stdClass;

/**
 * Handles all logic related to Fonts
 * 
 * @package Humane Sites
 * @subpackage Fonts
 */
class Controller_Fonts{
    
    /**
     * Returns all currently stored font files 
     * 
     * @return object Contains list of font files to enqueue
     */
    public static function find_all()
    {
        $dir = HUMANE_COMPOSER_DIR . "assets/dashboard/fonts/";
        $css_files_enqueue = new stdClass;

        // Open a directory, and read its contents
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    $not_read = ['.', '..'];
                    if(!in_array($file, $not_read)){
                        $fontdir = HUMANE_COMPOSER_DIR . "assets/dashboard/fonts/$file";
                        if(is_dir($fontdir))
                        {
                            $font_dir = opendir($fontdir);
                            while (($font_file_name = readdir($font_dir)) !== false){
                                //read css file
                                $pattern = "/.css/i";
                                if(preg_match($pattern, $font_file_name)){
                                    //do something
                                    $fontfile = HUMANE_COMPOSER_URL . "assets/dashboard/fonts/$file/$font_file_name";
                                    $enqueue_file = strtoupper($file);
                                    $css_files_enqueue->{$enqueue_file} = $fontfile;
                                }
                            }
                            closedir($font_dir);
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $css_files_enqueue;
    }
}