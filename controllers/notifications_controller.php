<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) exit;

require_once HUMANE_COMPOSER_DIR . 'db/notifications_db.php';
require_once HUMANE_COMPOSER_DIR . 'models/notifications_model.php';

use Humane_Acquire\Model_Forms;
// use Humane_Acquire\Model_Email_Templates;
use Humane_Spaces\Model_Playlist;
use Humane_Spaces\Controller_Automated_Pages;
use WhichBrowser\Parser;

class Controller_Notifications {
    public static function init() {
        // add_filter("send_notification", array(__CLASS__, "send_notification"), 10, 6);
        // add_filter("send_in_blue_notification", array(__CLASS__, "send_in_blue_notification"), 10, 6);

        add_action('wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 1);
        add_action('admin_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 1);

        add_action('wp_ajax_update_subscriber_list', array(__CLASS__, 'update_subscriber_list'));
        add_action('wp_ajax_add_remove_subscribers', array(__CLASS__, 'add_remove_subscribers'));
    }
    public static function update_subscriber_list(){
        $response = array();
        if ( ! isset( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'update_subscriber_list' ) ) {
			echo $response;
            wp_die();
        }
        $selected_users = $_POST["selected_users"];
        /* Making specifically for form right now, will need to be generalised later on if needed */
        $action = $_POST["object_action"];
        $object = $_POST["object"];
        $current_user_id = $_POST["current_user_id"];
        $notification = new Model_Notifications(array(
            "object_table" => $object,
            "object_id" => (int) $_POST["id"],
            "py_user_events_action" => $action
        ));
        $row_id = $_POST["id"];
        $notification->set_wp_users_ids($selected_users);
        $notification->save();
        ob_start();
        include_once HUMANE_COMPOSER_DIR . "views/notifications/show.html.php";
        $markup = ob_get_clean();
        $response["status"] = "ok";
        $response["markup"] = $markup;
        wp_send_json($response);
        return;
    }
    public static function add_remove_subscribers(){
        $response = array();
        if ( ! isset( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'add_remove_subscribers' ) ) {
			echo $response;
            wp_die();
        }
        $user_id = $_POST["user_id"];
        /* Making specifically for form right now, will need to be generalised later on if needed */
        $action = $_POST["object_action"];
        $object = $_POST["object"];
        $notification = new Model_Notifications(array(
            "object_table" => $object,
            "object_id" => (int) $_POST["id"],
            "py_user_events_action" => $action
        ));
        $selected_users = $notification->get_wp_users_ids();
        if(empty($selected_users)) $selected_users = array();
        if($_POST["type"] === "add"){
            if(!in_array($user_id, $selected_users)){
                $selected_users[] = (int) $user_id;
            }
        }else{
            if (($key = array_search($user_id, $selected_users)) !== false) {
                unset($selected_users[$key]);
            }
        }
        $notification->set_wp_users_ids($selected_users);
        $notification->save();
        $browser = Model_User_Events::get_browser_data();
        $ip = $_SERVER['REMOTE_ADDR'];
        $location_data = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"), true);
        $current_time = current_time('timestamp');
        $user_event_args = array(
			'created_by' => $user_id,
            'wp_post_id' => $_POST["id"],
            'object_table' => $object,
            'details_json' => $details_json,
            'ip_address' => $location_data['ip'],
            'browser' => $browser['name'].' '.$browser['version'],
            'device' => $browser['platform'],
            'os' => $browser['platform'],
            'is_client_facing_action' => 1,
            'lat' => explode(",", $location_data['loc'])[0],
            'longitude' => explode(",", $location_data['loc'])[1],
			'action' => $action,
			'created_at' => $current_time
        );
        Model_User_Events::_save(0, $user_event_args);
        return;
    }
    public static function enqueue_scripts(){
        $ajaxurl = admin_url( 'admin-ajax.php' );
		$ajaxurl = is_ssl() ? str_ireplace( 'http://', 'https://', $ajaxurl ) : str_ireplace( 'https://', 'http://', $ajaxurl );
        wp_enqueue_style( 'humane-notifications-css', HUMANE_COMPOSER_URL . 'assets/notifications/dist/notifications.min.css', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/notifications/dist/notifications.min.css'), 'all' );
        wp_enqueue_script( 'humane-notifications-js', HUMANE_COMPOSER_URL . 'assets/notifications/dist/notifications.min.js', array(), md5_file(HUMANE_COMPOSER_DIR . 'assets/notifications/dist/notifications.min.js'), 'all' );
        wp_localize_script( 'humane-notifications-js', 'Humane_Notification_Params', array(
            'ajaxurl' => $ajaxurl,
            'pluginURL' => HUMANE_COMPOSER_URL,
			'nonce' => array(
                'update_subscriber_list' => wp_create_nonce( 'update_subscriber_list' ),
                'add_remove_subscribers' => wp_create_nonce( 'add_remove_subscribers' )
            )
		) );
    }
    public static function get_form_replacements($form_data, $post_id, $form_id, $location_data, $user_id = 0, $object_table = "posts"){
        if($object_table === "posts" || $object_table === "py_forms"){
            $post_data = get_post($post_id);
            $page_url = $form_data["source_page"];
            $post_title = $post_data->post_title;
            $post_url = get_permalink($post_data);
        }else if($object_table === "py_playlists"){
            $playlist = new Model_Playlist($post_id);
            $post_title = $playlist->name;
            $speciality = $playlist->speciality;
            $speciality_id = $playlist->speciality_id;
            $speciality_id = intval(str_replace($speciality,"",$speciality_id));
            $playlist_page_id = Controller_Automated_Pages::automated_page_exists($speciality_id, $speciality);
            $post_url = get_permalink($playlist_page_id);
        }
        $device_data = new Parser(getallheaders());
        $form = new Model_Forms($form_id);
        // $email_template_object = new Model_Email_Templates($form->py_email_template_id);
       
        if(empty($form_data)){
            $form_data = get_user_meta($user_id);
        }
        $replacements = array(
            "form.submission.id" => $form_id,
            "form.submission.from_page_title" => $post_title,
            "form.submission.from_page_a_tag" => "<a href='$post_url'>$post_title</a>",
            "form.submission.from_page_url" => $page_url,
            "form.submission.ip_address" => $location_data["ip"],
            "form.submission.browser" => $device_data->browser->toString(),
            "form.submission.device" => $device_data->device->toString(),
            "form.submission.os" => $device_data->os->toString(),
            "form.submission.date" => explode(", ", $form_data["contacted_at"])[0],
            "form.submission.time" => explode(", ", $form_data["contacted_at"])[1],
            "form.lead.first_name" => $form_data["first_name"],
            "form.lead.last_name" => $form_data["last_name"],
            "form.lead.email" => $form_data["email"],
            "form.lead.job_title" => $form_data["job_title"],
            "form.lead.bio" => $form_data["tell_us_about_yourself"],
            "form.lead.linkedin_url" => $form_data["linkedin"],
            "form.lead.twitter_url" => $form_data["twitter"], 
            "form.lead.phone_number" => $form_data["phone_number"],
            "form.lead.company.name" => $form_data["company_name"],
            "form.lead.company.size" => $form_data["company_size"],
            "form.lead.company.industry" => $form_data["industry"],
            "form.lead.company.url" => $form_data["website_url"],
            "form.lead.street_address" => $form_data["street_address"],
            "form.lead.city" => $form_data["city"],
            "form.lead.country" => $form_data["country"],
            "form.lead.zip" => $form_data["zip_postal_code"],
            "form.title" => $form->name,
            "subscription.price" => $form_data["original_price"],
            "subscription.name" => $form_data["title"],
            "subscription.id" => $form_data["id"],
            "subscription.date" => $form_data["date"],
            "subscription.currency" => $form_data["currency"],
            "self.attachment.a_tag" => "<a href='$email_template_object->attachment_file'>$email_template_object->attachment_name</a>",
            "self.attachment.url" => $email_template_object->attachment_file,
            "self.attachment.title" => $email_template_object->attachment_name,
            "site.domain" => get_site_url()
        );
        return $replacements;
    }
    public static function create_contact($user_id, $apiInstance){
        $user = get_user_by("id", $user_id);
        $user_email = $user->data->user_email;       
        $createContact = new \SendinBlue\Client\Model\CreateContact();
        $createContact['email'] = $user_email;
        $createContact['attributes'] = array(
            "FIRSTNAME" => get_the_author_meta("first_name", $user_id),
            "LASTNAME" => get_the_author_meta("last_name", $user_id)
        );
        $createContact['updateEnabled'] = true;
        $result = $apiInstance->createContact($createContact);
        return $result;
    }
    
   
    public static function send_transaction_email($playlist, $associated_object, $user_id, $notification){
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', get_option("sendinblue_apikey"));
        $apiInstance = new \SendinBlue\Client\Api\TransactionalEmailsApi(
            new \GuzzleHttp\Client(),
            $config
        );
        $user = get_user_by("id", $user_id);
        $user_email = $user->data->user_email;   
        $replacements = self::get_form_replacements(array(), $playlist->id, "", array(), "", $associated_object);
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail['sender'] =  array('name' => get_bloginfo( 'name' ) . ' Support', 'email' => get_option('support_email'));
        $sendSmtpEmail['name'] = $playlist->name;
        $sendSmtpEmail['to'] = array(
            array(
                'name' => '{{contact.FIRSTNAME}} {{contact.LASTNAME}}',
                'email' => $user_email
            )
        );
        $sendSmtpEmail['templateId'] = (int) get_option('send_in_blue_template');
        $speciality = $playlist->speciality;
		$speciality_id = $playlist->speciality_id;
		$speciality_id = intval( str_replace( $speciality,"", $speciality_id ) );
		$page_id = Controller_Automated_Pages::automated_page_exists($speciality_id, $speciality);
		if($page_id) $playlist_url = get_permalink($page_id);
        $extra_data = $notification->get_extra_data();
        if(!is_array($extra_data)) $extra_data = array();
        if(is_array($extra_data["previous_post_list"]) && count($extra_data["previous_post_list"])){
            $diff = array_diff($playlist->results, $extra_data["previous_post_list"]);
            $items = $playlist->get_latest_post_list(5, $diff);
        } else $items = $playlist->get_latest_post_list(5); 
        $extra_data["previous_post_list"] = $playlist->results;
        if(count($items)){
            $sendSmtpEmail['params'] = array(
                "title" => $replacements["form.submission.from_page_title"],
                "page_url" => $replacements["form.submission.from_page_a_tag"],
                "playlist_url" => $playlist_url,
                "keyword" => $playlist->name,
                "items" => $items
            );
            $apiInstance->sendTransacEmail($sendSmtpEmail);
        }
        return $extra_data;
    }
    public static function send_in_blue_notification($associated_object, $associated_action, $post_id, $form_id, $form_data, $location_data, $user_id = 0){
        if($associated_object === "py_forms") $id = $form_id;
        else $id = $post_id;
        if($associated_object === "py_playlists"){
            $playlist = new Model_Playlist($id);
            $name = $playlist->name;
        }else{
            $name = get_the_title($id);
        }
        $notification = new Model_Notifications(array(
            "object_table" => $associated_object,
            "object_id" => $id,
            "py_user_events_action" => $associated_action
        ));
        $user_list = maybe_unserialize($notification->wp_users_ids);
        if(!is_array($location_data)) $location_data = array();
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', get_option("sendinblue_apikey"));
        $apiInstance = new \SendinBlue\Client\Api\ContactsApi(
            new \GuzzleHttp\Client(),
            $config
        );
        $contact_list = array();
        if(!is_array($user_list)) $user_list = array();
        foreach($user_list as $user_id){
            $extra_data = self::send_transaction_email($playlist, $associated_object, $user_id, $notification);
        }
        if(is_array($extra_data)){
            $notification->set_extra_data($extra_data);
            $notification->save();
        }
    }
    public static function send_notification($associated_object, $associated_action, $post_id, $form_id, $form_data, $location_data, $user_id = 0){
        if($associated_object === "py_forms") $id = $form_id;
        else $id = $post_id;
        $notification = new Model_Notifications(array(
            "object_table" => $associated_object,
            "object_id" => $id,
            "py_user_events_action" => $associated_action
        ));
        $user_list = maybe_unserialize($notification->wp_users_ids);
        // $email_template = new Model_Email_Templates(array(
        //     "associated_object" => $associated_object,
        //     "associated_action" => $associated_action
        // ));
        if(!is_array($location_data)) $location_data = array();
        foreach($user_list as $user_id){
            $user = get_user_by("id", $user_id);
            $user_email = $user->data->user_email;
            if($form_id) $form = new Model_Forms($form_id);
            if($post_id) $post = get_post($post_id);
            $replacements = self::get_form_replacements($form_data, $post_id, $form_id, $location_data, $user_id, $associated_object);
            $email_template->send_email($replacements, $user_email);
        }
        return true;
    }
}

Controller_Notifications::init();