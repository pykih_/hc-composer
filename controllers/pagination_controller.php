<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * Handles all pagination related logic
 * 
 * @package Humane Sites
 * @subpackage Pagination
 */
class Controller_Pagination {

    /**
     * Renders the markup for pagination
     * 
     * @param mixed $total_rows
     * @param int $current_page_number
     * @param int $rows_per_page
     * 
     * @return void
     */
    public static function addPagination ($total_rows, $current_page_number = 1, $rows_per_page = 20) {
        if(!$rows_per_page) $rows_per_page = 20;
        $total_pages = ceil($total_rows / $rows_per_page);
        if($current_page_number > $total_pages) $current_page_number = $total_pages;
        if($total_pages <= 1) return;
        ?>
            <div class="paginate-container ajax-disable">
                <div data-value="1" class="start-button paginate-action paginate-block block-start <?php echo $current_page_number > 1 ? "": "inactive"?>">
                    <svg class="start-icon" height="24px" viewBox="0 0 28 28" width="24px">
                        <g>
                            <path d="M10.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.9,6.999c0.39,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L10.456,16z"></path>
                            <path d="M17.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.899,6.999c0.391,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L17.456,16z"></path>
                        </g>
                    </svg>
                </div>
                <div class="previous-button paginate-action paginate-block <?php echo $current_page_number > 1 ? "": "inactive"?>" data-value="<?php echo $current_page_number - 1 ?>">
                    <svg height="24px" class="previous-icon" viewBox="0 0 28 28" width="24px">
                        <g>
                            <path clip-rule="evenodd" d="M11.262,16.714l9.002,8.999  c0.395,0.394,1.035,0.394,1.431,0c0.395-0.394,0.395-1.034,0-1.428L13.407,16l8.287-8.285c0.395-0.394,0.395-1.034,0-1.429  c-0.395-0.394-1.036-0.394-1.431,0l-9.002,8.999C10.872,15.675,10.872,16.325,11.262,16.714z" fill-rule="evenodd"></path>
                        </g>
                    </svg>
                </div>
                <div class="current-page paginate-block">
                    <?php echo $current_page_number; ?>
                </div>
                <div class="total-pages paginate-block">
                    of <?php echo $total_pages; ?>
                </div>
                <div data-value="<?php echo $current_page_number + 1 ?>" class="next-button paginate-action paginate-block <?php echo $current_page_number < $total_pages ? "": "inactive"?>">
                    <svg height="24px" class="next-icon" viewBox="0 0 28 28" width="24px">
                        <g>
                            <path clip-rule="evenodd" d="M11.262,16.714l9.002,8.999  c0.395,0.394,1.035,0.394,1.431,0c0.395-0.394,0.395-1.034,0-1.428L13.407,16l8.287-8.285c0.395-0.394,0.395-1.034,0-1.429  c-0.395-0.394-1.036-0.394-1.431,0l-9.002,8.999C10.872,15.675,10.872,16.325,11.262,16.714z" fill-rule="evenodd"></path>
                        </g>
                    </svg>
                </div>
                <div data-value="<?php echo $total_pages; ?>" class="end-button paginate-action paginate-block block-end <?php echo $current_page_number < $total_pages ? "": "inactive"?>">
                    <svg class="end-icon" height="24px" viewBox="0 0 28 28" width="24px">
                        <g>
                            <path d="M10.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.9,6.999c0.39,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L10.456,16z"></path>
                            <path d="M17.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.899,6.999c0.391,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L17.456,16z"></path>
                        </g>
                    </svg>
                </div>
            </div>
        <?php
    }
}