<?php
    namespace Humane_Sites;
    if ( ! defined( "ABSPATH" ) ) exit;

    /**
     * Admin table for a list of posts
     *  
     * @package Humane Sites
     * @subpackage Posts
     */
    class Posts_Table extends Humane_List_Table {
        function __construct() {
            parent::__construct( array(
                'ajax'   => false // We won't support Ajax for this table
            ) );
            $this->table_name = "posts";
        }

        function get_columns(){
            if($_GET['type'] === 'dataset' || $_GET['type'] === 'nudge' ){
                $columns = array(
                    "name" => "Name",
                    "created_by" => "Created By",
                    "created_at" => "Published At"
                );
            } else{
                $columns = array(
                    "post_title" => "Name",
                    "post_author" => "Created By",
                    "post_date" => "Published At"
                );
            }
            
            return $columns;
        }
        function column_name($item) {
            $value = $item->name;
            $id = $item->id;
            if($_GET['type'] === 'nudge'){
                $name_url = admin_url() . "admin.php?page=humane_playlist_edit&id=$id";
            } else{
                $name_url = admin_url() . "admin.php?page=humane_10000ft_for_data_edit&id=$id";
            }
            
            $actions = array(
                'edit'      => sprintf('<a href="%s">Edit</a>', $name_url)
            );
            return sprintf('%1$s %2$s', "<strong><a class='row-title' href='$name_url'>$value</a></strong>", $this->row_actions($actions) );
        }
        function column_created_by($item) {
            $name = Model_Users::to_s($item->updated_by);
            $recent_author = get_user_by( 'login', $item->updated_by );
            // Get user ID.
        $author_id = $recent_author->ID;
            $url = get_author_posts_url($author_id);
            return "<a href='$url'>$name</a>";
        }
        function column_post_title($item) {
            $value = $item->post_title;
            $id = $item->ID;
            $name_url = get_permalink($id);
            $actions = array(
                'edit'      => sprintf('<a href="%s">Edit</a>', get_edit_post_link($id))
            );
            return sprintf('%1$s %2$s', "<strong><a class='row-title' href='$name_url'>$value</a></strong>", $this->row_actions($actions) );
        }

        function column_post_author($item){
            $name = Model_Users::to_s($item->post_author);
            $url = get_author_posts_url($item->post_author);
            return "<a href='$url'>$name</a>";
        }
        
        function prepare_items() {
            $columns = $this->get_columns();
            $hidden = array();
            $sortable = array(
                "post_title" => "post_title"
            );
            $this->_column_headers = $this->get_column_info();
            $ids = array();
            $ids = apply_filters("humane_post_table_ids", $ids);
            if(!is_array($ids)) $ids = array();
            $ids = array_map(function($a){ return intval($a); }, $ids);
            $items = $this->get_data(array(
                "where" => array(
                    "ID" => array(
                        "comparator" => "IN",
                        "value" => $ids
                    )
                )
            ));
            $this->items = $items;
        }
    }

?>