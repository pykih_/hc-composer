<?php
function add_humane_core_menu()
{
    add_submenu_page(
        'themes.php',
        '🟩 Super Normal',
        '🟩 Super Normal',
        'manage_options',
        'admin.php?page=site_settings',
        ''
    );

    remove_submenu_page(
        'humane_core',
        'humane_core'
    );
}

function wp_remove_menus()
{
    remove_menu_page('edit-comments.php');   // removes comments tab
    remove_submenu_page('options-general.php', 'options-discussion.php');  // removes Discussion tab
    
    if (! is_super_admin() ) {
        remove_menu_page('plugins.php'); //Plugins
        remove_menu_page('ultimate-blocks-settings'); //removes Ultimate Blocks
        //remove_menu_page('aioseo'); //removes AIOSEO tab
        remove_menu_page('aioseo-search-statistics'); //removes AIOSEO tab
        remove_menu_page('wpcode'); //removes WPCode plugin tab
        remove_menu_page('internal_link_juicer'); //removes Internal Link Juicer
        remove_menu_page('aioseo-redirects'); // Removes Redirection option supported by AIOSEO plugin
        remove_submenu_page('options-general.php', 'wprocket');  // removes WPRocket 
        remove_menu_page('edit.php?post_type=wpm-testimonial'); // removes Strong Testimonial plugins tab
    }
}

    add_action('admin_menu', 'add_humane_core_menu', 20);
    add_action('admin_init', 'wp_remove_menus');
?>
