<?php
/**
 * Plugin Name:     humane-core
 * Plugin URI: https://pykih.com
 * Description: Add additional features to plugin.
 * Version: 0.1
 * Author: Pykih
 * Author URI: https://pykih.com
 * Text Domain: hc-composer
 * Domain Path: languages
 *
 * @package         humane\core
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! function_exists( 'humane_core_setup_constants' ) ) {

	/** Humane core setup constants.
	 * Setup plugin constants.
	 *
	 * @return void
	 */
	function humane_core_setup_constants() {
		// Plugin version.
		define( 'HUMANE_SITES_VER', '0.1' );

		// Plugin path.
		if ( ! defined( 'HUMANE_COMPOSER_DIR' ) ) {
			define( 'HUMANE_COMPOSER_DIR', plugin_dir_path( __FILE__ ) . 'hc-composer/' );
		}

		// Plugin URL.
		if ( ! defined( 'HUMANE_COMPOSER_URL' ) ) {
			define( 'HUMANE_COMPOSER_URL', plugin_dir_url( __FILE__ ) . 'hc-composer/' );
		}
	}
}
if ( ! function_exists( 'humane_core_includes' ) ) {
	/** Humane core include files.
	 * Include all model and controller files.
	 *
	 * @return void
	 */
	function humane_core_includes() {
		require_once HUMANE_COMPOSER_DIR . 'db/db.php';
		require_once HUMANE_COMPOSER_DIR . 'models/model.php';
		require_once HUMANE_COMPOSER_DIR . 'vendor/autoload.php';
		foreach ( glob( HUMANE_COMPOSER_DIR . 'controllers/*.php' ) as $filename ) {
			require_once $filename;
		}
		require_once HUMANE_COMPOSER_DIR . 'admin/menu.php';
		require_once HUMANE_COMPOSER_DIR . 'admin/common_function.php';
	}
}

humane_core_setup_constants();
humane_core_includes();
