<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) exit;

use Humane_Acquire\Model_Forms;

/**
 * Controller to create forms
 */
class Notifications_User_Events {

    public static $blacklisted_emails;

    /**
	 * Init method
	 * @return void
	 */
    public static function init()
    {
        add_action( 'send_form_notification', array(__CLASS__, 'send'), 10, 1);
    }
    public static function send($event){
        $event = [self::format($event)];
        $zapier_hook_url = get_option("zapier_form_hook_url");
        $request = wp_remote_post($zapier_hook_url,array(
            'method'      => 'POST',
            'headers'     => array(
                "Content-Type" => "application/json"
            ),
            'body'        =>  json_encode($event),
        ));
    }

    public static function format($event){
        $form_id = $event->py_user_form_id;
        $form = new Model_Forms();
        $form = $form->find_by_id($form_id);
        $fields_json = json_decode($form->fields_json, true);
        $event_details = maybe_unserialize($event->details_json);
        $form_submission = [];
        $form_submission['id'] = $event->id;
        $form_submission['Name'] = $event_details['first_name'].' '.$event_details['last_name'];
        $form_submission['Form ID'] = $form->id;
        $form_submission['Form Name'] = $form->name;
        $form_submission['Submission Time'] = $event_details['contacted_at'];
        if($form->action == 'downloaded_content'){
            $form_submission['Form Type'] = "Download Content";
        }else{
            $form_submission['Form Type'] = "Book an appointment";
        }
        foreach ($fields_json as $key => $field_value) {
            if($event_details[$key] ?? false){
                $form_submission[$field_value["label"]] = $event_details[$key];
            }
        }
        return $form_submission;
    }
}

Notifications_User_Events::init();