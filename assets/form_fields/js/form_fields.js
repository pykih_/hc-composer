(function () {
    jQuery(document).ready(function ($) {
        let delayTimer;
        // $(document).on("focus", `.autofill`, function (e) {
        //     let selectize = $(`.autofill.${auto}`)[0].selectize;
        //     selectize.clearOptions();
        // });
        $(document).on("input", `.autofill .selectize-input input`, function (e) {
            clearTimeout(delayTimer);
            delayTimer = setTimeout(() => {
                $.ajax({
                    url: ajaxurl,
                    type: "POST",
                    dataType: "json",
                    data: {
                        action: "get_dropdown_list",
                        nonce: Humane_Form_Fields_Params.nonce.get_dropdown_list,
                        search: $(this).val(),
                        type: $(this).closest(".hc-select-container").find("select").attr("name").replace("[]", "")
                    }
                })
                    .done((response)=>{
                        if (response.results) {
                            let selectize = $(this).closest(".hc-select-container").find("select")[0].selectize;
                            selectize.load(function (callback) {
                                callback(response.results);
                            });
                        }
                    })
                    .error(function (error) {
                        console.warn(error);
                    });
            }, 1000);
        });
        $(document).on("input", `.hc-country-select`, function (e) {
            clearTimeout(delayTimer);
            console.log($(this).val(), this);
            delayTimer = setTimeout(() => {
                $.ajax({
                    url: ajaxurl,
                    type: "POST",
                    dataType: "json",
                    data: {
                        action: "get_cities",
                        nonce: Humane_Form_Fields_Params.nonce.get_cities,
                        country: $(this).val()
                    }
                })
                    .done((response)=>{
                        if (response.results) {
                            let value = $(this).val();
                            console.log(value, "VALUE");
                            if(value === "")
                                $(".hc-city-select-container").addClass("hc-display-none");
                            else
                                $(".hc-city-select-container").removeClass("hc-display-none");
                            let selectize = $(this).closest(".hc-location-container").find(".hc-city-select")[0].selectize;
                            selectize.load(function (callback) {
                                callback(response.results);
                            });
                        }
                    })
                    .error(function (error) {
                        console.warn(error);
                    });
            }, 1000);
        });
    });
})();
