(function () {

    jQuery(document).ready(function ($) {
        /* Object selection handler i.e when user clicks on a row in objects list table */
        $(document).on("click", ".hc-form-modal .object-row", function (e) {
            if ($(this).hasClass("slice-row")) return;
            let post_id = $(this).attr("data-post_id"),
                post_title = $(this).attr("data-title"),
                post_url = $(this).attr("data-url");
            /* Get selected post ID and replace value in input box */
            let id = $(this).closest(".hc-form-modal").attr("data-id");
            $(`#${id}`).val(post_id);
            $(`#${id}_label`).val(post_title);
            /* Close modal window */
            $(".visible-content-selection-modal").addClass("hidden-content-selection-modal");
            $(".visible-content-selection-modal").removeClass("visible-content-selection-modal");

        });

        $(document).on("click", ".hc-input-photo", function(){
            $(this).closest(".custom-file").find(".hc-hidden-file").click();
        });

        $(document).on("click", ".hc-input-clear", function(){
            let input = $(this).next();
            $(input).val("").trigger("input");
            $(this).closest(".hc-input-text").removeClass("hc-input-text-filled")
        });

        $(document).on("input", ".hc-input-text input, .hc-input-text textarea", function(){
            if($(this).val() === ""){
                $(this).closest(".hc-input-text").removeClass("hc-input-text-filled")
            }else{
                $(this).closest(".hc-input-text").addClass("hc-input-text-filled")
            }
        });

        $(document).on("input", ".hc-hidden-file", function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = (e)=>{
                    $(this).parent().find(".hc-input-photo").attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        }); 
        /* Object selection handler i.e when user clicks on a row in objects list table */
        $(document).on("click", ".hc-form-modal .slice-row .remove-object-wrapper", function (e) {
            /* Get selected post ID and replace value in input box */
            let id = $(".visible-content-selection-modal").closest(".hc-form-modal").attr("data-id");
            $(`#${id}`).val("");
            $(`#${id}_label`).val("");
            /* Close modal window */
            $(".visible-content-selection-modal").addClass("hidden-content-selection-modal");
            $('.visible-content-selection-modal').removeClass("visible-content-selection-modal");

        });

        /* Modal opening and initializing */
        $(document).on(
            "click",
            ".select-post-button",
            function (e) {
                let id = $(this).attr("data-id");
                let selected = $(`#${id}`).val();
                if (selected) selected = [selected];
                else selected = [];
                var modal_id = $(this).attr("data-modal-id");

                $.ajax({
                    url: ajaxurl,
                    type: "POST",
                    data: {
                        action: "trigger_modal_window",
                        nonce: Humane_Blocks_Params.nonce.trigger_modal_window,
                        selected: selected
                    }
                })
                    .done(function (response) {
                        if (response) {
                            $(`#${modal_id} .input-objects-wrapper`).html(response.current);
                            changePostType(response.type, window.modalJSONs[modal_id]);
                            $(".media-menu-item.input-objects").click();
                            $(".media-frame-title-text").html("Select Post / Page");
                            
                            $(`#${modal_id}`).removeClass("hidden-content-selection-modal");
                            $(`#${modal_id}`).addClass("visible-content-selection-modal");
                            if(response.data.length === 0) $(`#${modal_id} .add-post-to-slice`).click();
                            else $(`#${modal_id} .input-objects`).click();
                        }
                    })
                    .error(function (err) {
                        console.warn(err);
                    });
            }
        );
        if ($('.set_custom_images:not(.bound)').length > 0) {
            if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
                $('.set_custom_images').addClass("bound").on('click', function (e) {
                    e.preventDefault();
                    var button = $(this);
                    var id = button.prev();
                    var custom_uploader = wp.media({
                        title: 'Attach an Image to Slice',
                        button: {
                            text: 'Attach this Image'
                        },
                        multiple: false  // Set this to true to allow multiple files to be selected
                    })
                        .on('select', function () {
                            var attachment = custom_uploader.state().get('selection').first().toJSON();
                            id.val(attachment.url);
                        })
                        .open();
                    return false;
                });
            }
        }

        $(document).on("click", ".remove_custom_images", function (e) {
            $("#process_custom_images").val('');
        });

        window.tinymce && window.tinymce.init({
            selector: '.py-form-tinymce',
            toolbar_mode: 'floating',
            menubar:false,
            plugins: ["lists","textcolor","image", "link"],
            statusbar: false,
            toolbar: 'bold italic underline | bullist numlist | alignleft aligncenter alignright | outdent indent | forecolor backcolor | image link | removeformat',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            }
        });

        $(document).on("click", ".placeholder-modal-opener", function(){
            $(".placeholder-modal").removeClass("open");
            $(this).parent().find(".placeholder-modal").addClass("open");
        });

        $(document).on("click", function(e){
            $(".placeholder-modal").removeClass("open");
            if($(e.target).closest(".placeholder-modal")) $(e.target).closest(".placeholder-modal").addClass("open");
            if($(e.target).hasClass("placeholder-modal-opener")) {
                $(e.target).parent().find(".placeholder-modal").addClass("open");
            }
        });
        $(document).on("click", ".placeholder-modal .placeholder-pills .hc-badge", function(){
            let placeholderValue = $(this).attr("data-replace");
            let parent = $(this).closest(".input-wrapper");

            placeholderValue = `{{${placeholderValue}}}`;
            if(tinymce && tinymce.activeEditor && $.contains(parent[0], tinymce.activeEditor.editorContainer)){
                tinymce.activeEditor.execCommand('mceInsertContent', false, placeholderValue);
            }
            let replace_target = parent.find("[data-replace-target='true']")[0];
            if(replace_target){
                if (replace_target.selectionStart || replace_target.selectionStart == '0') {
                    var startPos = replace_target.selectionStart;
                    var endPos = replace_target.selectionEnd;
                    replace_target.value = replace_target.value.substring(0, startPos)
                        + placeholderValue
                        + replace_target.value.substring(endPos, replace_target.value.length);
                } else {
                    replace_target.value += placeholderValue;
                }
            }
        });
        $(document).on("click", ".hc-toggle button.hc-btn-structure", function(e){
            e.stopPropagation();
            let selected_value = $(this).attr("data-value");
            $(this).closest(".hc-toggle").parent().find(".toggle-hidden").val(selected_value);
            $(this).addClass("hc-humane-btn-primary");
            $(this).removeClass("hc-humane-btn-secondary");
            $(this).parent().children().each(function(){
                if($(this).attr("data-value") !== selected_value){
                    $(this).removeClass("hc-humane-btn-primary");
                    $(this).addClass("hc-humane-btn-secondary");
                }
            });
        });
        if(typeof $("select.selectize").selectize === "function"){
            $("select.selectize").selectize({
                plugins: ['remove_button'],
                allowEmptyOption: true,
                create: false
            });
        }
        if(typeof $("select.selectize-color").selectize === "function"){
            $("select.selectize-color").selectize({
                plugins: ['remove_button'],
                allowEmptyOption: true,
                create: false,
                render: {
                    item: function(item) {
                        return `<div class="color-option hc-brand-reading hc-flex-align-center hc-fx hc-p-8">
                            <div class="color-circle hc-mr-8" style="background-color: var(--${item.text.trim()})"></div>${item.text}
                        </div>`;
                    },
                    option: function(item) {
                    return `<div class="color-option hc-brand-reading hc-flex-align-center hc-fx hc-p-8">
                        <div class="color-circle hc-mr-8" style="background-color: var(--${item.text.trim()})"></div>${item.text}
                    </div>`;
                    }
                }
            });
        }
        if(jQuery('.date-picker').length>0){
            $('.date-picker').flatpickr({
                static: true
             });
        }
      if(jQuery('datetime-picker').length>0){
        $('.datetime-picker').flatpickr({
            enableTime: true,
            static: true
        });
      }
    });

})();