(function () {

    jQuery(document).ready(function ($) {
        /* Search JS */
        let searchTimeout;
        let persistent = false;
        $(document).on("keydown", function(e){
            if(e.keyCode == 72){
                if ($(e.target).is('input') || $(e.target).is('textarea')) return true;
                $("#hc-header-search-dropdown").css("display", "block");
                $()
                clearTimeout(searchTimeout);
                setSearchContent("");
                setTimeout(() => { $(".hc-backend-header-search").focus(); }, 100);
                persistent = true;
            }
            if(e.keyCode == 27){
                if ($(e.target).is('input') || $(e.target).is('textarea')) return true;
                $(".hc-backend-header-search").val("");
                $("#hc-header-search-dropdown").css("display", "");
                persistent = false;
            }
            if(e.keyCode == 13){
                let count = $(".hc-result-content-container").attr("data-count");
                if(count === "1"){
                    $(".hc-result-content-container .hc-search-result")[0].click();
                }
            }
        });
        
        $(document).on("mouseenter", ".hc-header-trigger", function(e){
            if(!persistent) setSearchContent("");
            setTimeout(() => { $(".hc-backend-header-search").focus(); }, 100);
        });
        
        $(document).on("mouseleave", ".hc-header-trigger", function(e){
            if(!persistent) $(".hc-backend-header-search").val("");
        });


        $(document).on("input", ".hc-backend-header-search", function(){
            let search = $(this).val();
            setSearchContent(search);
        });
        function setSearchContent(search){
            $(".hc-result-content-container").addClass("hc-display-none");
            $(".hc-no-matches").addClass("hc-display-none");
            if(search === ""){
                $(".hc-search-content-container").removeClass("hc-display-none");
                return;
            }

            search = search.toLowerCase();
            let count = 0;
            $(".hc-result-content-container .hc-header-search-item").each(function(index, item){
                $(item).removeClass("hc-display-none");
                $(item).removeClass("hc-search-result");
                let content = $(item).attr("data-search").toLowerCase();
                if(!content.includes(search)) $(item).addClass("hc-display-none");
                else{
                    $(item).addClass("hc-search-result");
                    count++;
                } 
            });
            $(".hc-search-content-container").addClass("hc-display-none");
            if(count) $(".hc-result-content-container").removeClass("hc-display-none");
            else $(".hc-no-matches").removeClass("hc-display-none");
            $(".hc-result-content-container").attr("data-count", count);
        }
        $(document).on("click", function(e){
            if($("#hc-header-search-dropdown")[0] && !$("#hc-header-search-dropdown")[0].contains(e.target)){
                $(".hc-backend-header-search").val("");
                $("#hc-header-search-dropdown").css("display", "");
                persistent = false;
            }
        })
        $(document).on("click", ".hc-tab-trigger", function(){
            $(this).parent().find(".hc-tab-trigger").removeClass("hc-backend-tab-active");
            $(this).addClass("hc-backend-tab-active");
            let href = $(this).attr("href");
            $(this).closest(".hc-tab-group").parent().find("> .hc-tab-content > .hc-tab-container").removeClass("active");
            $(href).addClass("active");
        });
        $(document).on("click", ".user-event-modal-close", function(){
            $(".user-event-modal-container").removeClass("open");
        });
        $(document).on("click", ".user-event-modal-opener", function(){
            $(".user-event-modal-container").addClass("open");
            let data = JSON.parse($(this).attr("data-form-details"));
            let table = `
                <div class="event-details-table hc-fy">
                    ${
                        Object.keys(data).map((d)=>{
                            if(data[d] === "") return "";
                            return `
                                <div class="event-details-row hc-fx hc-mr-8 hc-mb-8">
                                    <div class="event-cell hc-flex-no-shrink hc-mr-8 hc-col-2 hc-capitalize">${d.replaceAll("_", " ")}</div>
                                    <div class="event-cell hc-mr-8">${data[d]}</div>
                                </div>
                            `;
                        }).join("")
                    }
                </div>
            `;
            $(".user-event-modal-details").html(table);
        });

        $(document).on("click", function(e){
            if($(".user-event-modal")[0] && !$(".user-event-modal")[0].contains(e.target) && !e.target.classList.contains("user-event-modal-opener"))  $(".user-event-modal-container").removeClass("open");
        });
    

        $('.custom-file-input').on('change', (e) => {
            $(e.target).siblings('.custom-file-label').html(e.target.value);
        });

        let customer_filters_container = document.getElementById("cognitively_customer_filter_content");
        if(customer_filters_container && window.customerData){
            let filterStored = false, sortStored = false;
            let prettyToFilterMap = {},filterToSortMap = {};
            let mappedCustomerData = JSON.parse(JSON.stringify(window.customerData));
            mappedCustomerData.forEach((a)=> {
                Object.keys(a).forEach((key)=>{
                    if(a[key]["type"] === "date"){
                        prettyToFilterMap[`${(new Date(a[key]["filterable_data"])).getTime()}_${key}`] = a[key]["pretty"];
                    }else{
                        prettyToFilterMap[`${a[key]["filterable_data"]}_${key}`] = a[key]["pretty"];
                    }
                    filterToSortMap[`${a[key]["filterable_data"]}_${key}`] = a[key]["sortable_data"];
                    a[key] = a[key]["filterable_data"];
                });
            });
            function setTableData(filter, sort){
                if(!filter) filter = filterStored;
                if(!sort) sort = sortStored;
                sortStored = sort;
                filterStored = filter;
                let data = mappedCustomerData;
                if(filter) data = new Humane.Query().from(mappedCustomerData).where(filter).run();
                let clone = JSON.parse(JSON.stringify(data));
                clone.forEach((datum)=>{
                    Object.keys(datum).forEach((key)=>{
                        datum[key] =  {
                            "pretty" : prettyToFilterMap[`${datum[key]}_${key}`],
                            "sortable_data" : filterToSortMap[`${datum[key]}_${key}`]
                        }
                    });
                });
                $(".customer-filter-count").html(clone.length);
                if(clone.length !== mappedCustomerData.length){
                    $(".filter-dropdown-opener").addClass("active");
                }else{
                    $(".filter-dropdown-opener").removeClass("active");
                }
                if(sort){
                    $(".sort-dropdown-opener").addClass("active");
                    $(".humane_sort_status").html("Sorted");

                }else{
                    $(".sort-dropdown-opener").removeClass("active");
                    $(".humane_sort_status").html("Not Sorted");
                }
                $.ajax({
                    url: ajaxurl,
                    type: "POST",
                    data: {
                        action: "pykih_filter_customer_data",
                        nonce: Cognitively_Customer_Data_Params.nonce.pykih_filter_customer_data,
                        data: clone,
                        sort
                    }
                })
                .done(function (response) {
                    if (response && response.markup) {
                        $("#customer-data-table").html(response.markup);
                    }
                })
                .error(function (err) {
                    console.warn(err);
                });
            }

            $(document).on("click", function(e){
                if(!$(".humane-filter-dropdown")[0].contains(e.target) && !e.target.classList.contains("filter-dropdown-opener"))  $(".humane-filter-dropdown").removeClass("open");
            })
            $(document).on("click", ".filter-dropdown-opener", function(){
                $(".humane-filter-dropdown").addClass("open");
            });
            $(document).on("click", ".clear-cognitively-customer-filters", function(e){
                e.preventDefault();
                $(".pykcontrol_reset_filter").click();
            });
            let comp = new Humane.Control({
                data: mappedCustomerData,
                onChange: result => {
                    setTableData(result, false);
                }
            });
            Object.keys(window.customer_filters_data).forEach((filter_key)=>{
                let filter_data = window.customer_filters_data[filter_key];
                let filter_value = document.createElement("input");
                filter_value.id = `cognitively_filter_content_value_${filter_key}`;
                filter_value.setAttribute("name", `cognitively_filter_content_value_${filter_key}`);
                filter_value.setAttribute("type", "hidden");
                customer_filters_container.prepend(filter_value);
                let filter_container = document.createElement("div");
                filter_container.id = `cognitively_filter_content_${filter_key}`;
                customer_filters_container.prepend(filter_container);
                if(filter_data["initial"]) $(".filter-dropdown-opener").addClass("active")
                comp[filter_data["scope"]]({
                    column: filter_key,
                    selector: filter_container.id,
                    label: filter_data["label"],
                    width: 260,
                    height: 120,
                    multiple: filter_data["multiple"],
                    placeholder: filter_data["placeholder"],
                    showCount: true,
                    type: filter_data["type"],
                    bins : 4,
                    container_padding:{
                        left: 20,
                        right: 20,
                        top: 0,
                        bottom: 0
                    },
                    showBars: false,
                    barColor: "#ECECEC",
                    showSortFilter: false,
                    highlightColor: "#53b2fc",
                    showinOutFilter : false,
                    showInputBox : true,
                    isDouble : true,
                    dropdownWrapper : true
                });
            });
        }

        if($("#cognitively_filter_customer_content_sort")[0]){
            let sort_data = window.cognitively_customer_sort;
            $(document).on("click", function(e){
                if(!$("#cognitively_filter_customer_content_sort")[0].contains(e.target) && !e.target.classList.contains("sort-dropdown-opener"))  $("#cognitively_filter_customer_content_sort").removeClass("open");
            })
            $(document).on("click", ".sort-dropdown-opener", function(){
                $("#cognitively_filter_customer_content_sort").addClass("open");
            });
            let comp = new Humane.Control({
                data: sort_data.data,
                onChange: result => {
                    setTableData(false, result[0][0]["value"]);
                }
            });
            comp.Dropdown({
                column: "key",
                selector: "cognitively_filter_customer_content_sort",
                label: sort_data["label"],
                height: "200px",
                multiple: sort_data["multiple"],
                placeholder: sort_data["placeholder"],
                showCount: false,
                showBars: false,
                barColor: "#ECECEC",
                showSortFilter: false,
                highlightColor: "#53b2fc",
                showinOutFilter : false,
                showInputBox : true,
                isDouble : true,
                dropdownWrapper : false
            });
        }

        /* Rename an object using AJAX */
        

    });

})();