(function () {
    
    jQuery(document).ready(function ($) {
        window.addEventListener("click", (e) => {
            if(document.querySelector(".option_container.open")) {
                document.querySelector(".option_container.open").classList.remove("open");
            }
            if(e.target.classList.contains('option_container_open')) {
                e.target.nextElementSibling.classList.add("open");
            }
        });
        if(window.perspectives){
            
            function updateQueryStringParameter(uri, key, value) {
                var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
                }
                else {
                return uri + separator + key + "=" + value;
                }
            }
                    
            function unescape (str) {
                return (str + '==='.slice((str.length + 3) % 4))
                .replace(/-/g, '+')
                .replace(/_/g, '/')
            }
            
            function escape (str) {
                return str.replace(/\+/g, '-')
                .replace(/\//g, '_')
                .replace(/=/g, '')
            }
            
            function encode (str) {
                return escape(btoa(str));
            }
            
            function decode (str) {
                return atob(unescape(str));
            }
            function hc_ga_views( pathname ) {
                console.log("inside hc_ga_views", dashboard_object.google_analytics_id);
                if ( ( typeof gtag !== 'undefined'  ) && typeof dashboard_object !== 'undefined' ) {
                    if ( gtag !== '' && dashboard_object.google_analytics_id !=='' ) {
                        gtag( 'create', dashboard_object.google_analytics_id, 'auto' );
                        gtag( 'send', {hitType:'pageview', page:pathname} );
                    }
                }
            }

            window.sortView = function({
                column,
                sort,
                filters,
                page,
                view
            } = {
                column: "",
                sort: "",
                filters: {},
                page: 1,
                view: ""
            }, perspectives_json, callback = function(){}) {
                console.log(window.perspectives[perspectives_json.id], "PERSP");
                if(window.perspectives[perspectives_json.id].throttleDashboard) return;
                let container = $(`.dashboard-container[data-id='${perspectives_json.id}']`);
                container.addClass("disabled");
                container.find(".ajax-disable").addClass("disabled");
                let newJSON = {};
                Object.keys(window.perspectives).forEach((id) => {
                    newJSON[id] = window.perspectives[id].humane_perspectives;
                });
                let json = JSON.stringify(newJSON);
                json = encode(json);
                let url = updateQueryStringParameter(location.href, "perspectives", json);
                window.history.pushState({}, "", url);
                const urlParams = new URLSearchParams(window.location.search);
                let type = urlParams.get('page');
                let id = urlParams.get('id');
                let reference_type = urlParams.get('type');
                let hidden_values = perspectives_json.hidden_values;
                let controller = container.find(".controller-class").val();
                window.perspectives[perspectives_json.id].throttleDashboard = true;
                console.log(column, sort, filters, "FL");
                console.log('Humane_Sort_View are');
                console.log(Humane_Sort_View);
                $.ajax({
                    url: ajaxurl,
                    type: "POST",
                    data: {
                        action: "pykih_sort_view",
                        nonce: Humane_Sort_View.nonce.sort_view,
                        column, sort, filters, page, type, controller, id, reference_type, hidden_values, view
                    }
                }).done(function(response) {
                    container.find('.views-container').html(response["html"]);
                    console.log(response, "RESP");
                    if(response["filters_html"]){
                        container.find('.filter-desktop-container').html(response["filters_html"]);
                        container.find('.filter-mobile-container').html(response["filters_mobile_html"]);
                    }
                    if(sort){
                        container.find(".sort-dropdown-opener").addClass("active");
                        container.find(".humane_sort_status").html("Sorted");
                    }else{
                        container.find(".sort-dropdown-opener").removeClass("active");
                        container.find(".humane_sort_status").html("Not Sorted");
                    }
                    if(perspectives_json.filters){
                        if(response.filters_data) window.perspectives[perspectives_json.id].perspectives_json.filters = response.filters_data;
                        perspectives_json.filters.filtered_count = response["count"];
                        container.find(".filtered_count").html(response["count"]);
                    }
                    if(response["count"] !== response["total_count"]) container.find(".count-container").addClass("count-active");
                    else container.find(".count-container").removeClass("count-active");
                    window.perspectives[perspectives_json.id].throttleDashboard = false;
                    container.removeClass("disabled");
                    container.find(".ajax-disable").removeClass("disabled");
                    window.truncateTitles();
                    if(response.filters_data) window.createFilters(perspectives_json.id);
                    else window.perspectives[perspectives_json.id].refreshFilters = true;
                    callback();
                    let changedUrl = removeURLParameter(window.location.href, "mode");
                    jQuery(".hc-modal-trigger[data-connect], .hc-card-automated-gallery[data-connect], .hc-card-gallery-xs[data-connect], .list-table-row[data-connect]").on("click", function(e) {
                        let ids = parseInt($(this).attr("data-connect"));
                        console.log(ids, "IDS");
                        //had to comment below line as aggaregation a tag was not working in newsfeed in pagination
                        //e.preventDefault();
                        $.ajax({
                          url: Humane_Posts_Params.ajaxurl, // The AJAX URL is automatically defined by WordPress
                          type: 'POST',
                          dataType: "json",
                          data: {
                            nonce: Humane_Posts_Params.nonce.custom_ajax_handler,
                            action: 'custom_ajax_handler', // The action name you used in your PHP function
                            // Add any additional data you want to send to the server
                            ids
                          },
                          success: function(response) {
                            // Handle the response from the server
                            console.log(response);
                            window.postData = response["postData"];
                            data = window.postData;
                            window.current_post_id = ids;
                            if(data.post_type !== "aggregation"){
                                currentModalTypeDirect = data["card_interaction"] === "modal";
                                appendToURL(data.url, "modal");
                                let modal_content = document.querySelector(".hc-side-modal-container .hc-side-modal-contents");
                                modal_content.innerHTML = data["modal_markup"];
                                window.prepareModal();
                                articleContainerSideModal.open();
                            }
                          },
                          error: function(errorThrown) {
                              console.log('Error:', errorThrown);
                          }
                        });
                    })
                    // if (jQuery(".list-table-row").length > 0 ) { 
                    //     var ids = $(".list-table-row[data-connect]").map(function() {
                    //         return $(this).attr("data-connect");
                    //     }).get();
                    // }
                    // if (jQuery(".hc-card-automated-gallery").length > 0) var ids = $(".hc-card-automated-gallery[data-connect]").map(function() {
                    //     return $(this).attr("data-connect");
                    // }).get();
                    // if (jQuery(".hc-card-gallery-xs").length > 0) ids = $(".hc-card-gallery-xs[data-connect]").map(function() {
                    //     return $(this).attr("data-connect");
                    // }).get();
                    // if (jQuery(".hc-card-gallery-xs").length < 0 && jQuery(".hc-card-automated-gallery").length > 0) ids = $(".hc-card-gallery-xs[data-connect],.hc-card-automated-gallery[data-connect]").map(function() {
                    //     return $(this).attr("data-connect");
                    // }).get();
                    // $.ajax({
                    //     url: Humane_Posts_Params.ajaxurl,
                    //     type: "POST",
                    //     dataType: "json",
                    //     data: {
                    //     nonce: Humane_Posts_Params.nonce.custom_ajax_handler,
                    //     action: "custom_ajax_handler",
                    //     ids
                    //     }
                    // }).done(function(response) {
                    //     console.log(response, "response dashboard");
                    //     window.postsData = response["postsData"];
                    //     window.slicePeopleData = response["peopleData"];
                    //     document.body.classList.add("modal-data-loaded");
                    //     jQuery('.hc-taxt-copy').hide();
                    // });
                }).error(function(err) {
                    console.warn(err);
                    window.perspectives[perspectives_json.id].throttleDashboard = false;
                    container.removeClass("disabled");
                    container.find(".ajax-disable").removeClass("disabled");
                });
                

            }
            window.addEventListener("click", (e) => {
                let container = $(e.target).closest(".dashboard-container");
                let sort = container.find('.sort-container-main');
                if (e.target.classList.contains('main-sort-container')) {
                    sort.css("left", `${container.find(".main-sort-container").position().left - sort.width() + container.find('.main-sort-container').width()}px`)
                    sort.attr("data-sort-visible", true);
                    container.find(".top-header-bar").addClass("cancel-sort-dropdown-bar");
                    container.find(".cancel-sort-dropdown-bar .back-button").addClass("cancel-sort-dropdown");
                    container.find(".cancel-sort-dropdown-bar .back-button").removeClass("hide");
                    container.find(".cancel-sort-dropdown-bar .done-button").addClass("hide");
                    container.find(".cancel-sort-dropdown-bar .top-header-bar-title").html("Sort");
                    container.find(".cancel-sort-dropdown-bar .top-header-bar-title").addClass("show-grid");
                } else {
                    $(".sort-container-main").attr("data-sort-visible", false);
                    container.find(".cancel-sort-dropdown-bar .back-button").removeClass("cancel-sort-dropdown");
                    container.find(".cancel-sort-dropdown-bar .back-button").addClass("hide");
                    container.find(".cancel-sort-dropdown-bar .done-button").removeClass("hide");
                    container.find(".cancel-sort-dropdown-bar .top-header-bar-title").removeClass("show-grid");
                    container.find(".top-header-bar").removeClass("cancel-sort-dropdown-bar");
                }
            });
            $(document).on("click", ".paginate-action", function(){
                let page = $(this).attr("data-value");
                let id = $(this).closest(".dashboard-container").attr("data-id");
                window.perspectives[id].humane_perspectives.page = page;
                clearTimeout(window.perspectives[id].filterTimeout);
                window.perspectives[id].filterTimeout = setTimeout(function(){
                    window.sortView(window.perspectives[id].humane_perspectives, window.perspectives[id].perspectives_json);
                }, 1000);
                //let toppos = $(".views-container").offset().top;
                //window.scrollTo(0, toppos);
            });
            $(document).on("click", ".cancel-sort-dropdown", function(){
                let container = $(e.target).closest(".dashboard-container");
                $(".sort-container-main").attr("data-sort-visible", false);
                container.find(".cancel-sort-dropdown-bar .back-button").removeClass("cancel-sort-dropdown");
                container.find(".cancel-sort-dropdown-bar .back-button").addClass("hide");
                container.find(".cancel-sort-dropdown-bar .done-button").removeClass("hide");
                container.find(".cancel-sort-dropdown-bar .top-header-bar-title").removeClass("show-grid");
                container.find(".top-header-bar").removeClass("cancel-sort-dropdown-bar");
            });
   
            /* Creating filters */
            window.addEventListener("click", (e) => {
                let container = $(e.target).closest(".dashboard-container");
                let filter_dropdown = container.find('.filter-container-main');
                let count_container = container.find(".main-count-container");
                if (e.target.classList.contains('main-count-container') || (filter_dropdown.length && filter_dropdown.get(0).contains(e.target))) {
                    filter_dropdown.addClass("active");
                    filter_dropdown.css("left", `${count_container.position().left - filter_dropdown.width() + count_container.width()}px`);
                    container.find(".top-header-bar").addClass("cancel-filter-dropdown-bar");
                    if(container.find(".filter-container-main").hasClass("filters-changed")){
                        container.find(".cancel-filter-dropdown-bar").addClass("filters-changed");
                    }
                    container.find(".cancel-filter-dropdown-bar .back-button").addClass("cancel-filter-dropdown");
                    container.find(".cancel-filter-dropdown-bar .back-button").removeClass("hide");
                    container.find(".cancel-filter-dropdown-bar .top-header-bar-title").html("Filters");
                    container.find(".cancel-filter-dropdown-bar .top-header-bar-title").addClass("show-grid");
                    container.find(".cancel-filter-dropdown-bar .done-button").addClass("hide");
                } else {
                    $(".filter-container-main").removeClass("active");
                    container.find(".cancel-filter-dropdown-bar .back-button").removeClass("cancel-filter-dropdown");
                    container.find(".cancel-filter-dropdown-bar .back-button").addClass("hide");
                    container.find(".cancel-filter-dropdown-bar .done-button").removeClass("hide");
                    container.find(".cancel-filter-dropdown-bar").removeClass("filters-changed");
                    container.find(".cancel-filter-dropdown-bar .top-header-bar-title").removeClass("show-grid");
                    container.find(".top-header-bar").removeClass("cancel-filter-dropdown-bar");
                }
            });
            $(document).on("click", ".cancel-filter-dropdown", function(){
                let container = $(e.target).closest(".dashboard-container");
                container.find(".cancel-filter-dropdown-bar .filter-container-main").removeClass("active");
                container.find(".cancel-filter-dropdown-bar .back-button").removeClass("cancel-filter-dropdown");
                container.find(".cancel-filter-dropdown-bar .done-button").removeClass("hide");
                container.find(".cancel-filter-dropdown-bar").removeClass("filters-changed");
                container.find(".cancel-filter-dropdown-bar .top-header-bar-title").removeClass("show-grid");
                container.find(".top-header-bar").removeClass("cancel-filter-dropdown-bar");
            });

            $(document).on('click','.global_reset', (e) => {
                e.preventDefault();
                let container = $(e.target).closest(".dashboard-container");
                let id = container.attr("data-id");
                window.perspectives[id].humane_perspectives.filters = {};
                container.find(".hc-control-reset-filter").click();
            });
            const urlParams = new URLSearchParams(window.location.search);
            let stored_perspectives = urlParams.get('perspectives')
            if(stored_perspectives){
                stored_perspectives = JSON.parse(decode(stored_perspectives));
                Object.keys(stored_perspectives).forEach((id)=>{
                    window.perspectives[id].humane_perspectives = stored_perspectives[id];
                    window.sortView(window.perspectives[id].humane_perspectives, window.perspectives[id].perspectives_json);
                });
            }
            window.createFilters = function(specific_id = false){
                Object.keys(window.perspectives).forEach((id) => {
                    if(specific_id !== false && id !== specific_id) return;
                    let perspectives = window.perspectives[id];
                    let container = $(`.dashboard-container[data-id='${id}']`);
                    if(perspectives.perspectives_json['filters'] && perspectives.perspectives_json['filters']['filters']){
                        let filters = perspectives.perspectives_json['filters']['filters'];
                        let count = 0;
                        filters.forEach((tab)=>{
                            Object.keys(tab["filters"]).forEach((filter_key) =>{
                                let filter_data = tab["filters"][filter_key];
                                if(filter_data.data && filter_data.data.length <= 0) return;
                                let comp = new Humane.Control({
                                    data: filter_data.data || [],
                                    onChange: (result, changed) => {
                                        let check = JSON.stringify(window.perspectives[id].humane_perspectives.filters);
                                        if(Object.values(comp.needsReset).includes(true)){
                                            container.find('.filter-container-main').addClass("filters-changed");
                                        }else{
                                            container.find('.filter-container-main').removeClass("filters-changed");
                                        }
                                        if(!changed) $( `#perspectives-${id}-filter-${filter_key}-title`).removeClass("active-title");
                                        else $( `#perspectives-${id}-filter-${filter_key}-title`).addClass("active-title");
                                        result = result[0] ? result[0].map(r => r.value) : "";
                                        if(changed) perspectives.humane_perspectives.filters[filter_key] = result;
                                        else delete perspectives.humane_perspectives.filters[filter_key];
                                        clearTimeout(perspectives.filterTimeout);
                                        count+=1;
                                        perspectives.filterTimeout = setTimeout(function(){
                                            if(perspectives.refreshFilters || check !== JSON.stringify(perspectives.humane_perspectives.filters) || JSON.stringify(perspectives.humane_perspectives.filters) === "{}"){
                                                window.sortView(perspectives.humane_perspectives, perspectives.perspectives_json);
                                                count = 0;
                                            }
                                        }, 2000);
                                    }
                                });
                                if(filter_data["initial"]) container.find(".count-container").addClass("count-active")
                                if(filter_data["scope"] === "Search"){
                                    comp[filter_data["scope"]]({
                                        selector: window.innerWidth < 540 ? `perspectives-${id}-search-mobile` : `perspectives-${id}-search`,
                                        height: 40,
                                        width: '100%',
                                        placeholder: "Search...",
                                        version: 2,
                                        initial: {
                                            value: perspectives.humane_perspectives["filters"][filter_key]
                                        }
                                    });
                                } else {
                                    comp[filter_data["scope"]]({
                                        column: "key",
                                        selector: perspectives.humane_perspectives["filters_sidebar"] === "1" || window.innerWidth < 540 ? `perspectives-${id}-filter-${filter_key}-mobile`  : `perspectives-${id}-filter-${filter_key}`,
                                        label: filter_data["label"],
                                        height: "200px",
                                        multiple: filter_data["multiple"],
                                        placeholder: filter_data["placeholder"],
                                        hideCount: false,
                                        countKey: "external_count",
                                        initial: {
                                            value: perspectives.humane_perspectives["filters"][filter_key],
                                            leftValue: perspectives.humane_perspectives["filters"][filter_key] && perspectives.humane_perspectives["filters"][filter_key][0],
                                            rightValue: perspectives.humane_perspectives["filters"][filter_key] && perspectives.humane_perspectives["filters"][filter_key][1]
                                        },
                                        minimum: filter_data["minimum"],
                                        maximum: filter_data["maximum"],
                                        showBars: false,
                                        range: filter_data["range"],
                                        barColor: "#ECECEC",
                                        showSortFilter: false,
                                        highlightColor: "#53b2fc",
                                        showinOutFilter : false,
                                        showInputBox : true,
                                        showCount: true,
                                        isDouble : true,
                                        version: 2,
                                        dropdownWrapper : false
                                    });
                                }
                                
                            });
                        });
                    }
                })
               
            };
            window.createSorts = function(){
                Object.keys(window.perspectives).forEach((id) => {
                    let perspectives = window.perspectives[id];
                    /* Creating sorts */
                    let container = $(`.dashboard-container[data-id="${id}"]`);
                    if(perspectives.perspectives_json['sort'] && perspectives.perspectives_json['sort']['sorts']){
                        let sorts = perspectives.perspectives_json['sort']['sorts'];
                        let sort_id = `perspectives-${id}-sort`;
                        if(sorts.data.length <= 0) return;
                        let comp = new Humane.Control({
                            data: sorts.data,
                            onChange: result => {
                                result = result[0] ? result[0][0].value : "";
                                for(let i = 0; i < sorts.data.length; i++){
                                    if(sorts.data[i].key === result){
                                        container.find(".sort-title").html(`
                                            Sort by: <span class="hc-bold">${sorts.data[i].display}</span>
                                        `)
                                        break;
                                    }
                                }
                                result = result.split("-");
                                perspectives.humane_perspectives.column = result[0];
                                perspectives.humane_perspectives.sort = result[1];
                                window.sortView(perspectives.humane_perspectives, perspectives.perspectives_json);
                            }
                        });
                        comp[sorts["scope"]]({
                            column: "key",
                            selector: sort_id,
                            height: "200px",
                            multiple: false,
                            initial: {
                                value:  perspectives.humane_perspectives["sort"] || sorts["initial"]
                            },
                            dropdownWrapper : false,
                            stripped: true,
                            version: 2
                        });
                    }
                });
            }
            window.createViews = function(){
                Object.keys(window.perspectives).forEach((id) => {
                    let perspectives = window.perspectives[id];
                    /* Creating sorts */
                    let container = $(`.dashboard-container[data-id="${id}"]`);
                    if(perspectives.perspectives_json['views']){
                        let views = perspectives.perspectives_json['views'];
                        let view_id = `perspectives-${id}-views`;
                        if(views.length <= 1) return;
                        let comp = new Humane.Control({
                            data: views.map((a, i)=>{
                                return {
                                    "key": `${i}`,
                                    "alias": a.scope
                                };
                            }),
                            onChange: result => {
                                result = result[0] ? result[0][0].value : "";
                                let id = container.attr("data-id");
                                container.find(`.view-container`).addClass("view-hidden");
                                container.find(`.view-name`).html(views[parseInt(result)].scope);
                                container.find(`.view-container[data-id="${result}"]`).removeClass("view-hidden");
                                container.find(".view-dropdown-content").css("display", "none");
                                container.find(".top-header-bar").removeClass("show-grid");
                                container.find(".top-header-bar").addClass("hide");
                                container.find(".done-button").removeClass("cancel-view-dropdown");

                                window.perspectives[id].humane_perspectives.view = result;
                                let newJSON = {};
                                Object.keys(window.perspectives).forEach((id) => {
                                    newJSON[id] = window.perspectives[id].humane_perspectives;
                                });
                                let json = JSON.stringify(newJSON);
                                json = encode(json);
                                let url = updateQueryStringParameter(location.href, "perspectives", json);
                                window.history.pushState({}, "", url);
                            }
                        });
                        comp.Dropdown({
                            column: "key",
                            selector: view_id,
                            multiple: false,
                            initial: {
                                value:  perspectives.humane_perspectives["view"] || "0"
                            },
                            dropdownWrapper : false,
                            stripped: true,
                            version: 2
                        });
                    }
                });
            }
            window.createFilters();
            window.createSorts();
            window.createViews();

            $(document).on("click", ".dashboard-container .pyk-dropdown-views", function(){
                let container = $(this).closest(".dashboard-container");
                if($(this).hasClass("hc-single-view")) return;
                container.find(".view-dropdown-content").css("display", "block");
                container.find(".top-header-bar").addClass("show-grid");
                container.find(".top-header-bar").removeClass("hide");
                container.find(".top-header-bar-title").html("Views");
                container.find(".top-header-bar-title").addClass("show-grid");
                container.find(".done-button").addClass("cancel-view-dropdown");
            });
            $(document).on("click", ".dashboard-container .filter-title", function(){
                let id = $(this).attr("id");
                id = id.replace("-title", "");
                $(`#${id}`)[0].scrollIntoView({
                    behavior: "smooth",
                    block: 'nearest',
                    inline: 'start' 
                });
            });
            $(document).on("click", function(e){
                if(!$(e.target).closest(".pyk_dropdown_views").length){
                    $(".dashboard-container .view-dropdown-content").css("display", "none");
                }
            });
            $(document).on("click", ".dashboard-container .cancel-view-dropdown", function(){
                let container = $(this).closest(".dashboard-container");
                $('body').removeClass("hc-body-no-scroll-mobile");
                container.find(".view-dropdown-content").css("display", "none");
                container.find(".top-header-bar").removeClass("show-grid");
                container.find(".top-header-bar").addClass("hide");
                container.find(".top-header-bar-title").removeClass("show-grid");
                container.find(".done-button").removeClass("cancel-view-dropdown");
            });

            $(document).on("click", ".dashboard-container .hc-mobile-close, .dashboard-container .hc-filter-overlay", function(){
                let container = $(this).closest(".dashboard-container");
                $('body').removeClass("hc-body-no-scroll-mobile");
                container.find(".hc-mobile-filters").removeClass("perspectives-show");
                container.find(".hc-mobile-sort").removeClass("perspectives-show");
                container.find(".hc-filter-overlay").removeClass("perspectives-show");
            });

            $(document).on("click", ".dashboard-container .hc-filter-dropdown", function(){
                let container = $(this).closest(".dashboard-container");
                $('body').addClass("hc-body-no-scroll-mobile");
                container.find(".hc-mobile-filters").addClass("perspectives-show");
                container.find(".hc-filter-overlay").addClass("perspectives-show");
            });

            $(document).on("click", ".dashboard-container .hc-open-mobile-sort", function(){
                let container = $(this).closest(".dashboard-container");
                $('body').addClass("hc-body-no-scroll-mobile");
                container.find(".hc-mobile-sort").addClass("perspectives-show");
                container.find(".hc-filter-overlay").addClass("perspectives-show");
            });
            $(document).on("click", ".humane_clip_to_board", function(){
                var contentToCopy = $(this).parent().find('.clip_to_board_a').text();
                var hc_taxt_copy = $(this).find('.hc-taxt-copy');
                  navigator.clipboard.writeText(contentToCopy).then(function() {
                    hc_taxt_copy.show();
                    setTimeout(function () {
                      hc_taxt_copy.hide();
                    }, 3500);
                  }, function(err) {
                    console.error('Unable to copy with async ', err);
                  });
            });
            $(document).click(function(event) {
                  var container = $(".hc-reusable-block-a");
                  if (!container.is(event.target) && !container.has(event.target).length) {
                      jQuery('.hc-reusable-block-dropdown').hide();
                  }
              });
              $(document).on("click", ".hc-reusable-block-a", function(){
                $(this).parents('.hc-reusable-block-container').find('.hc-reusable-block-dropdown').show();
            });
            if($('.hc-newsfeed-row').length >0){
               var current_url = window.location.href;
               currentHash =current_url;
                $(window).scroll(function() {
                    $('.hc-newsfeed-row').each(function(){
                        var top = window.pageYOffset;
                        var distance = top - $(this).offset().top;
                        var hash = $(this).attr('data-src');
                        var domain = new URL(hash);
                        domain = domain.hostname;
                        if (domain === window.location.hostname){
                            if (distance < 30 && distance > -30 && currentHash != hash) {
                                window.location.hash = (hash);
                                currentHash = hash;
                                window.history.pushState(null, null, $(this).attr("data-src"));
                                hc_ga_views($(this).attr("data-src"));
                            }
                        }   
                    })
                    if($(window).scrollTop() === 0){
                        window.history.pushState(null, null, current_url);
                    }
                });
               
            }
        }
        if(jQuery('.dataset_scroll .humane_datasets').length>0){
            jQuery( "table" ).wrap( "<div class='humane_datasets_wrapper'></div>" );

        }
    });
})();