(function () {

    jQuery(document).ready(function ($) {

        /**
         * Open media tab for adding user profile image and bind the image
         * url in the input field.
         */
        if ($('.humane-user-profile-photo-button:not(.bound)').length > 0) {
            if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
                $('.humane-user-profile-photo-button').addClass("bound").on('click', function (e) {
                    e.preventDefault();
                    var button = $(this);
                    var id = button.prev();
                    var custom_uploader = wp.media({
                        title: 'Add user profile photo',
                        button: {
                            text: 'Attach this Image'
                        },
                        multiple: false  // Set this to true to allow multiple files to be selected
                    })
                        .on('select', function () {
                            var attachment = custom_uploader.state().get('selection').first().toJSON();
                            id.val(attachment.url);
                            var profile = document.querySelector(".user-edit-php #your-profile .user-profile-picture img");
                            if(profile){
                                profile.setAttribute("src", attachment.url);
                                profile.setAttribute("srcset", attachment.url);
                            }
                            var media = document.getElementById("humane_user_profile_photo");
                            if(media)
                                media.value = attachment.url;
                        })
                        .open();
                    return false;
                });
            }
        }
        if ($('.humane-user-profile-photo-reset-button').length > 0) {
            $('.humane-user-profile-photo-reset-button').on('click', function (e) {
                e.preventDefault();
                var media = document.getElementById("humane_user_profile_photo");
                if(media)
                    media.value = "";
            });
        }
    });
})();