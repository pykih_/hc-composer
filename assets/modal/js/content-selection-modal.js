
(function (jQuery) {
    jQuery(document).ready(function ($) {

        window.changePostType = function (type, modalJSON) {
            $(".activated").removeClass("activated");
            let types = [];
            Object.keys(modalJSON).forEach((key) => {
                Object.keys(modalJSON[key]["sections"]).forEach((tab) => {
                    modalJSON[key]["sections"][tab].tabs.forEach((tabValue) => {
                        types.push({
                            "key": tab,
                            "value": tabValue["code"]
                        });
                    })
                });
            });
            let index = types.findIndex(t => t.value === type);
            if (type === "all") {
                $(types.map(t => `.add-${t.key}-to-slice`).join(",")).removeClass("deactivated");
            } else if (index !== -1) {
                /* Activates the required tab and deactivates the rest */
                let other_types = types.filter(t => t.value !== type).map(t => `.add-${t.key}-to-slice`).join(",");
                $(other_types).addClass("deactivated");
                $(other_types).attr("title", "To select this, remove currently selected objects");
                $(`.add-${types[index].key}-to-slice`).removeClass("deactivated");
                $(`.add-${types[index].key}-to-slice`).addClass("activated");
            }
        }

        let searchStatus = false,// This variable is used to set whether the search button is clicked. If clicked, it will maintain the status, unless the page is reloaded, to let the system know that whenever you make a DB call like click on pagination, consider the search term as well in the query.
            searchFor = false,// The searched term will be stored here  for all the future DB reference until the page is reloaded.
            selectedView;//declared to store the selected view button so that whenever the search is clicked, the click of the selected view button is triggered which will take the search term in consideration

        /* Get the search string from the the search box if the search button was pressed. */
        function getSearchFor(status) {
            let searchText = $(`.visible-content-selection-modal .search-post`).val();// get the searched term
            return status && searchText ? searchText : false;// return search term or false if the search button was not clicked yet or the search term is empty.
        }

        /* get the search string to search for from the textbox and send an ajax request to search in the database */
        let searchTimeout;
        $(document).on('input', '.visible-content-selection-modal .search-post', function (e) {
            clearTimeout(searchTimeout);
            searchTimeout = setTimeout(() => {
                searchStatus = true;// set to true when the search button is clicked.
                searchFor = getSearchFor(searchStatus);// Stores the search term or false for the query to consider the search term or not.
                $(selectedView).trigger('click');// trigger the click of the selected view after the search term is set.
            }, 1000);
        });

        // Remove an object from slice
        $(document).on('click', '.visible-content-selection-modal .remove-object-button', function (e) {
            e.preventDefault();

            var $parent = $('.visible-content-selection-modal');

            /* Remove the row which contains this button */
            $(this).closest('tr').remove();

            /* Remove entire table if last object was removed */
            if ($parent.find('.input-objects-wrapper table tbody tr').length < 1) {
                $parent.find('.input-objects-wrapper table').remove();
                changePostType("all", window.modalJSONs[$parent.attr("id")]);
                $parent.find('.input-objects-wrapper').html('<h3 class="modal-window-notice" style="text-align: center;">' + Humane_Blocks_Params.text.no_objects + '</h3>');
            }
        });

        // Left menu handler
        $(document).on('click', '.visible-content-selection-modal .media-menu .media-menu-item:not(".cancel")', function (e) {
            e.preventDefault();
            /* Deactivate safety check */
            if (e.target.classList.contains('deactivated')) return;
            /* Tab type */
            var content = $(this).data('content');
            /* Add active class to only this tab */
            $(this).siblings('.media-menu-item').removeClass('active');
            $(this).addClass('active');

            /* Hide all content divs */
            $(this).closest('.visible-content-selection-modal').find('.media-frame-content-item').hide();

            /* Display only this div */
            $(this).closest('.visible-content-selection-modal').find('#' + content).show();
            /* Check for content type validity */
            if (content.indexOf("add-to-slice") !== -1) {
                $('.visible-content-selection-modal .media-frame-router').show();
                $('.visible-content-selection-modal .media-router').hide();
                $('.visible-content-selection-modal .media-frame-title-notice').show();
                $('.visible-content-selection-modal button.add-object-modal').show();
                let type = content.split("-")[3];
                $('.visible-content-selection-modal .media-frame-router').attr("data-active", type);
                $(`.visible-content-selection-modal .media-router[data-type=${type}]`).show();
                $($(`.visible-content-selection-modal .media-router[data-type=${type}]>a`)[0]).trigger('click');
                $('.visible-content-selection-modal button.hc-update-slice-modal').hide();
                $(".visible-content-selection-modal .search-form").show();

            } else {
                $('.media-frame-router').hide();
                $('.media-frame-title-notice').hide();
                $('button.add-object-modal').hide();
                $('button.hc-update-slice-modal').show();
                $(".search-form").hide();

            }
        });

        // Add object row to edit-slice screen
        $(document).on('click', '.visible-content-selection-modal .add-object-modal', function (e) {
            e.preventDefault();
            /* Get active tag type*/
            let side_type = $(".visible-content-selection-modal .media-frame-router").attr("data-active");
            /* Get tab name */
            var active_tab = $(`.visible-content-selection-modal .media-router[data-type=${side_type}] .media-menu-item.active`).data('tab');
            var $parent = $('.visible-content-selection-modal');

            /* Get current selected objects */
            var $current_objects = $('.visible-content-selection-modal .input-objects-wrapper').find('.object-row');

            /* Get their post IDs */
            var post_ids = $.map($current_objects, function (item, index) {
                return $(item).data('post_id');
            });

            /* Get newly selected objects */
            var selected = $('.visible-content-selection-modal #' + active_tab).data('selected');
            selected = selected.split(",");
            let isSingleSelect = $(".visible-content-selection-modal .media-menu .content-container .media-menu-item.active").attr("data-single-select");

            if (selected && selected[0] === "") {
                alert("Select an object first");
                return;
            }
            if (isSingleSelect === "1") {
                post_ids = selected;
            } else {
                post_ids = selected.concat(post_ids);
            }
            var $parent = $('.visible-content-selection-modal');
            console.log(post_ids, "POST");

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'pykih_add_objects',
                    nonce: Humane_Blocks_Params.nonce.add_objects,
                    post_ids: post_ids
                },
            })
                .done(function (response) {
                    window.changePostType(response.type, window.modalJSONs[$parent.attr("id")]);
                    $('.visible-content-selection-modal').data('playlist-name', response.playlist_name);
                    $('.visible-content-selection-modal').data('playlist-id', response.playlist_id);
                    $('.visible-content-selection-modal .input-objects-wrapper').html(response.markup);
                    $('.visible-content-selection-modal .object-row.selected').removeClass('selected');
                    // Remove selected data in add to slice screen
                    $('.visible-content-selection-modal .media-item-tab-content').data('selected', '');
                    $(".visible-content-selection-modal .media-menu-item.input-objects").click();
                    $(".visible-content-selection-modal .search-form").hide();

                }).error(function (error) {
                    console.warn(error);
                });
        });

        $(document).on('click', '.visible-content-selection-modal .media-router .media-menu-item', function (event) {
            event.preventDefault();

            selectedView = this;
            /* Get current tab */
            var tab = $(this).data('tab');
            /* Get selected objects */
            var $selected = $(this).closest('.visible-content-selection-modal').find('.input-objects-wrapper .object-row');
            /* Map to post ID */
            var selected = $.map($selected, function (item, index) {
                return $(item).data('post_id');
            });

            /* Set active and hide other content divs */
            $(this).addClass('active').siblings('.media-menu-item').removeClass('active');
            $(`.visible-content-selection-modal #${tab}`).data("selected", "");
            $(`.visible-content-selection-modal #${tab}`).show().siblings('.media-item-tab-content').hide();

            /* Loading text display */
            $('.visible-content-selection-modal #' + tab).html('<h3 class="tab-notice" style="text-align: center;">' + Humane_Blocks_Params.text.please_wait + '</h3>');

            /* Tab validity check */
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'pykih_get_tab_objects',
                    nonce: Humane_Blocks_Params.nonce.get_tab_objects,
                    post_type: tab,
                    current_page: 1,
                    selected: selected,
                    searchFor: getSearchFor(true)// Pass the search term to the API to consider it in the query.
                },
            })
                .done(function (response) {
                    if (response.num_items < 1) {
                        /* TODO: Improve these conditions dynamically */
                        if (tab == 'post') {
                            $('.visible-content-selection-modal .media-item-tab-content#' + tab).html('<h3 class="modal-window-notice" style="text-align: center;">' + 'No ' + tab + ' available. Please ' + '<a href="post-new.php" target="_blank">' + 'Add a ' + tab + '</a>' + ' first.' + '</h3>');
                        } else if (tab == 'page') {
                            $('.visible-content-selection-modal .media-item-tab-content#' + tab).html('<h3 class="modal-window-notice" style="text-align: center;">' + 'No ' + tab + ' available. Please ' + '<a href="post-new.php?post_type=page" target="_blank">' + 'Add a ' + tab + '</a>' + ' first.' + '</h3>');
                        } else if (tab == 'attachment') {
                            $('.visible-content-selection-modal .media-item-tab-content#' + tab).html('<h3 class="modal-window-notice" style="text-align: center;">' + 'No media available. Please ' + '<a href="media-new.php" target="_blank">' + 'Upload or add an image' + '</a>' + ' first.' + '</h3>');
                        } else if (tab == '10000ft') {
                            $('.visible-content-selection-modal .media-item-tab-content#' + tab).html(`<h3 class="modal-window-notice" style="text-align: center;">No ${tab} has been created. Please <a href="${admin_url()}admin.php?page=cognitively-list-10000ft" target="_blank">Create 10000ft</a> first.</h3>`);
                        } else {
                            $('.visible-content-selection-modal .media-item-tab-content#' + tab).html(`<h3 class="modal-window-notice" style="text-align: center;">No ${tab} has been created. Please create one first.</h3>`);
                        }
                    }
                    else $('.visible-content-selection-modal .media-item-tab-content#' + tab).html(response.html);

                    let selected = $('.visible-content-selection-modal .media-item-tab-content#' + tab).find(".object-row[data-selected='1']");
                    selected = Array.from(selected).map(e => e.getAttribute("data-post_id"));
                    selected = selected.filter(e => e.length !== 0).join(',');
                    $('.visible-content-selection-modal .media-item-tab-content#' + tab).data('selected', selected);
                })
                .error(function (err) {
                    console.warn(err);
                });

        });

        $(document).on('click', '.visible-content-selection-modal .add-to-slice-link', function (e) {
            e.preventDefault();
            $(".visible-content-selection-modal .media-menu-item.add-post-to-slice").click();
        });
        // Sorting object row handler in .visible-content-selection-modal
        $(document).on('click', '.visible-content-selection-modal .input-objects-wrapper .object-row .sort .sort-button', function (e) {
            e.preventDefault();

            var sort = $(this).data('sort');
            var $el = $(this).closest('.object-row');
            var $next_el = $el.next();
            var $prev_el = $el.prev();

            if (sort == 'up') {
                $el.insertBefore($prev_el);
            } else {
                $el.insertAfter($next_el);
            }
        });

        /*
            TODO: Check if common function can be used for pagination and tab click
            Object selector pagination handler
        */
        $(document).on('click', '.visible-content-selection-modal .objects-table-pagination .button', function (e) {
            e.preventDefault();
            let slice_id = $('.visible-content-selection-modal').attr('data-slice_id'),
                selected = $('.visible-content-selection-modal .input-objects-wrapper').find('.object-row');
            selected = $.map(selected, function (item, index) {
                return $(item).data('post_id');
            });
            let post_type = $(this).closest('.objects-table-pagination').data('post_type'),
                button_action = $(this).data('action'),
                current_page = $(this).closest('.objects-table-pagination').data('current_page'),
                rows_selected = $(this).closest('.media-item-tab-content').data('selected');
            rows_selected = typeof (rows_selected) == 'undefined' || rows_selected.length == 0 ? '' : rows_selected;
            rows_selected = rows_selected.split(',');
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'pykih_get_tab_objects',
                    nonce: Humane_Blocks_Params.nonce.get_tab_objects,
                    post_type: post_type,
                    button_action: button_action,
                    current_page: current_page,
                    selected: selected,
                    searchFor: getSearchFor(true)// Pass the search term to the API to consider it in the query.
                }
            })
                .done(function (response) {
                    if (typeof (response) != 'undefined' && response.num_items && response.html && response.html.length > 0) {
                        $('.visible-content-selection-modal .media-item-tab-content#' + post_type).html(response.html);
                        for (var key in rows_selected) {
                            $(this).closest('.media-item-tab-content').find('.object-row[data-post_id="' + rows_selected[key] + '"]').addClass('selected');
                        }
                    }

                });
        });

        /* Modal close button handler */
        $(document).on('click',
            '.visible-content-selection-modal .media-modal-close,\
		.visible-content-selection-modal .media-menu .cancel',

            function (e) {
                e.preventDefault();
                $('.visible-content-selection-modal').addClass("hidden-content-selection-modal");
                $('.visible-content-selection-modal').removeClass("visible-content-selection-modal");
            }
        );
        $(document).on('click', '.visible-content-selection-modal#forms-selection-modal .add-to-slice-link', function (e) {
            e.preventDefault();
            $(".visible-content-selection-modal .media-menu-item.add-forms-to-slice").click();
        });
    
        $(document).on('click', '.content-selection-modal-common .detach-form', function (event) {
            event.preventDefault();
            let id = $(this).closest("tr").attr("data-post_id");
            var $parent = $('.visible-content-selection-modal');
    
            if (confirm("Are you sure you want to detach this form from the slice?")) {
    
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    data: {
                        action: 'detach_form',
                        nonce: Humane_Form_Submission_Params.nonce.detach_form,
                        id,
                    },
                })
                    .done(function (response) {
                        changePostType(response.type, window.modalJSONs[$parent.attr("id")]
                        );
                        $('.content-selection-modal-common .input-objects-wrapper').html('<h3 class="modal-window-notice" style="text-align: center;">' + Editor_Params.text.no_objects + '</h3>');
    
                        $(".media-menu-item.input-objects").click();
                    })
                    .error(function (err) {
                        console.warn(err);
                    });
            }
        });
        /* Object selection handler i.e when user clicks on a row in objects list table */
        $(document).on('click', '.visible-content-selection-modal .media-item-tab-content .object-row', function (e) {

            e.preventDefault();

            var $parent = $(this).closest('.media-item-tab-content');
            var post_id = $(this).data('post_id');
            var selected = $parent.data('selected');
            selected = typeof (selected) == 'undefined' || selected.length == 0 ? '' : selected;
            selected = selected.split(',');
            var current;

            if (!$(this).hasClass('selected')) {
                let dataContent = $("visible-content-selection-modal .media-menu .content-container .media-menu-item.active").attr("data-content");
                let isSingleSelect = 0;
                isSingleSelect = $(`.visible-content-selection-modal .media-menu .content-container .media-menu-item.active`).attr("data-single-select");
                if (isSingleSelect === "1") {
                    $('.selected').removeClass('selected');
                    selected = [post_id.toString()]
                } else {
                    selected.push(post_id.toString());
                }
                $(this).addClass('selected');
                current = selected.filter(e => e.length !== 0).join(',');
                $parent.data('selected', current);
            } else {
                /* Undo selection */
                $(this).removeClass('selected');
                for (var key in selected) {
                    if (selected[key] == post_id) {
                        selected.splice(key, 1);
                    }
                }

                current = selected.join(',');
                $parent.data('selected', current)
            }
        });
    });
})(jQuery);
