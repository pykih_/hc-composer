jQuery(document).ready(function ($) {
    $(document).on("click", ".add-subsriber-select-everyone", function(){
        $.each($(".add-subscriber-modal-content input[name='subscribers']"), function(){
            $(this).prop("checked", true);
        });
    });
    $(document).on("click", ".add-subsriber-select-no-one", function(){
        $.each($(".add-subscriber-modal-content input[name='subscribers']"), function(){
            $(this).prop("checked", false);
        });
    });

    $(document).on("click", ".show-page .hc-notification-unsubscribe", function(){
        let current_user_id = $(this).attr("data-user-id");
        $(`input[name='subscribers'][data-val='${current_user_id}']`).prop("checked", false);
        $(this).addClass("hc-notification-subscribe");
        $(this).removeClass("hc-notification-unsubscribe");
        $(this).html(`<img src='${Humane_Notification_Params.pluginURL}assets/dashboard/images/notifications_24px_outlined.svg' /> Stay in the loop`);
        $(".save-subscribers").click();
    });

    $(document).on("click", ".show-page .hc-notification-subscribe", function(){
        let current_user_id = $(this).attr("data-user-id");
        $(`input[name='subscribers'][data-val='${current_user_id}']`).prop("checked", true);
        $(this).removeClass("hc-notification-subscribe");
        $(this).addClass("hc-notification-unsubscribe");
        $(this).html(`<img src='${Humane_Notification_Params.pluginURL}assets/dashboard/images/notifications_off_24px_outlined.svg' /> Mute notifications`);
        $(".save-subscribers").click();
    });
    $(document).on("click", ".index-page .hc-notification-subscribe", function(){
        let current_user_id = $(this).attr("data-user-id");
        let id = $(this).attr("data-row-id");
        let object = $(this).attr("data-row-object");
        let object_action = $(this).attr("data-row-action");
        $.ajax({
            url: Humane_Notification_Params.ajaxurl,
            type: "POST",
            data: {
                nonce: Humane_Notification_Params.nonce.add_remove_subscribers,
                action: "add_remove_subscribers",
                type: "add",
                user_id: current_user_id,
                id, object, object_action
            }
        }).done(()=>{
            $(this).removeClass("hc-notification-subscribe");
            $(this).addClass("hc-notification-unsubscribe");
            $(this).html(`<img src='${Humane_Notification_Params.pluginURL}assets/dashboard/images/notifications_off_24px_outlined.svg' /> Mute notifications`);
        });
    });

    $(document).on("click", ".humane_page_container .hc-notification-unsubscribe", function(e){
        e.preventDefault();
        e.stopPropagation();
        let current_user_id = $(this).attr("data-user-id");
        let id = $(this).attr("data-row-id");
        let object = $(this).attr("data-row-object");
        let object_action = $(this).attr("data-row-action");
        $.ajax({
            url: Humane_Notification_Params.ajaxurl,
            type: "POST",
            data: {
                nonce: Humane_Notification_Params.nonce.add_remove_subscribers,
                action: "add_remove_subscribers",
                type: "remove",
                user_id: current_user_id,
                id, object, object_action
            }
        }).done(() => {
            $(this).addClass("hc-notification-subscribe");
            $(this).removeClass("hc-notification-unsubscribe");
        });
    });

    $(document).on("click", ".humane_page_container .hc-notification-subscribe", function(e){
        e.preventDefault();
        e.stopPropagation();
        let current_user_id = $(this).attr("data-user-id");
        let id = $(this).attr("data-row-id");
        let object = $(this).attr("data-row-object");
        let object_action = $(this).attr("data-row-action");
        $.ajax({
            url: Humane_Notification_Params.ajaxurl,
            type: "POST",
            data: {
                nonce: Humane_Notification_Params.nonce.add_remove_subscribers,
                action: "add_remove_subscribers",
                type: "add",
                user_id: current_user_id,
                id, object, object_action
            }
        }).done(()=>{
            $(this).removeClass("hc-notification-subscribe");
            $(this).addClass("hc-notification-unsubscribe");
        });
    });
    $(document).on("click", ".add-subscriber-modal-opener", function(){
        $(".add-subscriber-modal").removeClass("hc-display-none");
    });
    $(document).on("click", ".add-subscriber-modal-close", function(){
        $(".add-subscriber-modal").addClass("hc-display-none");
    });
    $(document).on("click", ".add-subscriber-modal-content .save-subscribers", function(){
        let selected_users = [];
        $.each($(".add-subscriber-modal-content input[name='subscribers']:checked"), function(){
            selected_users.push($(this).attr("data-val"));
        });
        let current_user_id = $(this).attr("data-user-id");
        let object = $(this).attr("data-row-object");
        let object_action = $(this).attr("data-row-action");
        if(selected_users.indexOf(current_user_id) === -1){
            $(".hc-notification-button").addClass("hc-notification-subscribe");
            $(".hc-notification-button").removeClass("hc-notification-unsubscribe");
            $(".hc-notification-button").html(`<img src='${Humane_Notification_Params.pluginURL}assets/dashboard/images/notifications_24px_outlined.svg' /> Stay in the loop`);
        }else{
            $(".hc-notification-button").removeClass("hc-notification-subscribe");
            $(".hc-notification-button").addClass("hc-notification-unsubscribe");
            $(".hc-notification-button").html(`<img src='${Humane_Notification_Params.pluginURL}assets/dashboard/images/notifications_off_24px_outlined.svg' /> Mute notifications`);
        }
        let id = $(this).attr("data-row-id");
        $.ajax({
            url: Humane_Notification_Params.ajaxurl,
            type: "POST",
            data: {
                nonce: Humane_Notification_Params.nonce.update_subscriber_list,
                action: "update_subscriber_list",
                selected_users,
                id, object, object_action, current_user_id
            }
        }).done(function(response) {
            $(".add-subscriber-modal-content .close").click();
            $('.modal-backdrop.show').remove();
            $('body').removeClass("modal-open");
            if(response.status === "ok"){
                $(".notifications-container").html(response.markup);
            }
        });
    });
});