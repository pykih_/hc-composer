
(function (jQuery) {
    jQuery(document).ready(function ($) {

        $(document).on("input", ".color-settings input", function(){
            let name = $(this).attr("name"),
                value = $(this).val();
            document.documentElement.style.setProperty(
                `--${name}`,
                value
            );
        });
        if(window.sortable){
            let sort = window.sortable(".hc-sortable", {
                forcePlaceholderSize: true,
                placeholderClass: 'hc-sort-placeholder',
            });
            sort.forEach(function(sort_container){
                sort_container.addEventListener("sortupdate", function(e){
                    let container = $(e.target).closest(".hc-sort-container");
                    console.log(e, container, "E");
                    let items = e.detail.destination.items;
                    let sortOrder = items.map((a)=> a.querySelector(".form-control").getAttribute("name"))
                    container.find(".sort-order-field").val(JSON.stringify(sortOrder));
                });
            })
            
        }
    });
})(jQuery);
