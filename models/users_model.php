<?php

namespace Humane_Sites;

if (!defined('ABSPATH')) {
    exit();
}

use stdClass;
use Humane_Spaces\Model_Playlist;

require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');

/**
 * All CRUD operations and data related functions regarding 
 * Users functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage Users
 */
class Model_Users
{
    /**
     * Creates a new user from $_POST values
     * 
     * @return void
     */
    public static function new()
    {
        if (!wp_verify_nonce($_POST['wp_users_create'] ?? "", 'wp_users_create')) {
            return;
        }

        $user_name = $_POST["humane_user_user_name"];
        $email = $_POST["humane_user_email_id"];
        $blog_id = get_current_blog_id();
        $new_user = get_user_by("email", $email);
        $user_id = $new_user->data->ID;
        if ($user_id) {
            if (!is_user_member_of_blog($user_id, $blog_id)) {
                $result = add_user_to_blog($blog_id, $user_id, sanitize_text_field($_POST["humane_user_role"]));
                $new_user = get_user_by("id", $user_id);
            } else {
                $error_messages = ["Another user with this username / email already exists. Maybe it's your evil twin. Spooky."];
                return;
            }
        }
        if (!$user_id){
            $new_user = wp_create_user($user_name, '', $email);
            if($_POST["humane_notify_user"] === "yes" && !is_wp_error($new_user)){
                wp_new_user_notification($new_user, null, "user");
            }
        }

        if (!is_wp_error($new_user)) {
            if ($_POST["humane_user_role"] === "subscriber") {
                update_user_meta($new_user, "is_temporary", true);
                $sent_mail = apply_filters('humane_magic_signin_link_email', $new_user);
                if (!$sent_mail) {
                    wp_delete_user($new_user->user_id);
                } 
            }
            foreach ($_POST as $key => $value) {
                if (
                    $key === "humane_user_role"
                    || $key === "humane_user_designation"
                    || $key === "description"
                )
                    update_user_option($new_user, $key, $value);
                else
                    update_user_meta($new_user, $key, $value);
            }
            update_user_meta($new_user, "relationship_status", $_POST["humane_user_status"]);
            $user = new \WP_User((int)$new_user);
            $user->set_role($_POST["humane_user_role"]);
        }
        return;
    }
    /**
     * Returns the display name of a user by ID or login
     * 
     * @param int $user_id
     * @param string $type
     * 
     * @return string
     */
    public static function to_s($user_id = 0, $type = "id")
    {
        if(is_string($user_id)){
            $user = get_user_by("login", $user_id);
        } else {
            $user = get_user_by("id", $user_id);
        }
        $user_meta = get_user_meta($user_id);
        if (!empty($user_meta["first_name"][0]))
            $user_name = $user_meta["first_name"][0] . (" " . $user_meta["last_name"][0] ?? "");
        else {
            $user_name = $user->data->user_email;
        }
        return $user_name;
    }
    /**
     * Updates an existing user from $_POST values
     * 
     * @return void
     */
    public static function update()
    {
        if (!wp_verify_nonce($_POST['wp_users_update'] ?? "", 'wp_users_update')) {
            return;
        }
        // will return the attachment id of the image in the media library
        $attachment_id = \media_handle_upload('humane_user_profile_photo_file', 0);
        // test if upload succeeded
        if (!is_wp_error($attachment_id)) {
            $_POST['humane_user_profile_photo'] = wp_get_attachment_url($attachment_id);
        }
        $id = $_GET["id"] ?? $_POST["id"];
        if ($id) {
            $new_user = get_user_by("id", (int)$id);
            if ($new_user) {
                $existing_id = get_user_meta((int)$id, "humane_user_profile_photo_file", true);
                if (!empty($existing_id)) wp_delete_attachment($existing_id, true);
                foreach ($_POST as $key => $value) {
                    if (
                        $key === "humane_user_role"
                        || $key === "humane_user_designation"
                        || $key === "description"
                    )
                        update_user_option((int)$id, $key, $value);
                    else
                        update_user_meta((int)$id, $key, $value);
                }
                $user = new \WP_User((int)$id);
                do_action("user_edit_custom", (int)$id);
                if (isset($_POST["humane_user_role"])) {
                    $user->set_role($_POST["humane_user_role"]);
                }
            }
        }
        return;
    }

    /**
     * Destroys an existing user by ID
     * 
     * @return void
     */
    public static function destroy()
    {
        if (!wp_verify_nonce($_GET['nonce'] ?? "", 'wp_users_destroy')) {
            return;
        }
        $user_id = $_GET['id_deleted'];
        global $update_messages;
        global $error_messages;
        if ($user_id) {
            remove_user_from_blog($user_id, get_current_blog_id(), (int) $_POST["assign_to"]);
        }
        return;
    }

    public static function show($author_id, $page_key=false, $page=false){
        $offset = 0;
        $per_page = 10;
        if($page !== false){
            $offset = $page*$per_page;
        }else if($page_key && isset($_GET[$page_key])){
            $page = $_GET[$page_key] - 1;
            $offset = $page*$per_page;
        }
        if ( ! empty( $author_id ) ) {
            $capibilities = get_user_meta($author_id, 'wp_capabilities', true);
            if ( !empty( $capibilities['administrator'] ) ) return;
            global $post_types;
            $posts = get_posts(
                array(
                    'author'        =>  $author_id,
                    'orderby'       =>  'post_date',
                    'order'         =>  'DESC',
                    'numberposts' => $per_page,
                    'offset' => $offset,
                    'ignore_sticky_posts'	=> true,
                    'no_found_rows'		=> true
                )
            );
            $all_posts = get_posts(
                array(
                    'author'        =>  $author_id,
                    'orderby'       =>  'post_date',
                    'order'         =>  'DESC',
                    'numberposts' => -1,
                    'ignore_sticky_posts'	=> true,
                    'no_found_rows'		=> true
                )
            );
            $response_user = get_user_by( 'ID', $author_id );
            $user_bio = get_the_author_meta( 'bio', $author_id );
        }
        $wpseo_sep = Model_Playlist::get_wpseo_seperator();
        return array($wpseo_sep, $response_user, $user_bio, $author_id, $posts, $all_posts);
    }

    /**
     * Returns the data of the root author page for a site
     * 
     * @param string $check
     * @param string $folder_key
     * @param string $folder
     * 
     * @return array
     */
    public static function index($check, $folder_key = false, $folder = false){
        
        $user_args = array(
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => false,
            'has_published_posts' => apply_filters("filter_existing_post_type", array('post','aggregation', 'event'), "playlist", false)
        );
        $items = get_users($user_args);
        $all_items = get_users($user_args);
        $items = array_map(function($a){
            $obj = array();
            $obj["title"] = $a->display_name;
            $obj["description"] = get_user_meta( $a->ID, 'bio', true);
            $slug = sanitize_title($a->user_login);
            $obj["link"] = site_url()."/u/$slug";
            $obj["image"] = get_avatar_url($a->ID);
            $user_meta = get_userdata( $a->ID );
            $obj["role"] = $user_meta->roles[0];
            $posts = get_posts( array(
                'author'      => $a->ID,
                'post_type'   => apply_filters("filter_existing_post_type", array('post','aggregation', 'event'), "playlist", false),
                'orderby'     => 'date',
                'order'       => 'DESC',
                'numberposts' => -1,
                'ignore_sticky_posts'	=> true,
                'no_found_rows'		=> true
            ));
            $posts = array_filter($posts, function($a){
                $login_page = get_post_meta($a->ID, "humane_autogenerated_page");
                if($login_page) return false;
                return true;
            });
            $obj["count"] = count($posts);

            $date = explode(" ", $posts[0]->post_date)[0];
            $obj["latest"] = date_format(date_create($date),"M d, Y");
            $date = explode(" ", $posts[count($posts) - 1]->post_date)[0];
            $obj["oldest"] = date_format(date_create($date),"M d, Y");
            $authors_data = Model_Playlist::get_authors_data($posts);
            $obj["authors"] = $authors_data["authors"];
            $obj["authors_list"] = $authors_data["authors_list"];
            $obj["other_authors"] = $authors_data["other_authors"];
            $obj["author_avatars_list"] = $authors_data["author_avatars_list"];
            $obj["posts"] = array_slice($posts, 0, 10);
            return $obj;
        }, $items);
        $is_root = "author";
        $wpseo_sep = Model_Playlist::get_wpseo_seperator();
        return array($wpseo_sep, $items, $all_items, count($all_items));
    }

    /**
     * Handles pagination based on $_POST values
     * 
     * @return void
     */
    public static function return_ajax_get_authors_paged_data() {
       //pagination for individual author pages
        if($_POST["term_id"] === "author"){
            $res = self::index($taxonomy, false, $_POST["page"]);
            $items = $res[1];
            $total_count = $res[3];
            return array("folder" => array("items"=> $items, "total_count"=> $total_count));
        }else{
            $author_id = $_POST["term_id"];
            $res = self::show($author_id,false, $_POST["page"]);
            $wpseo_sep = $res[0];
            $response_user = $res[1];
            $user_bio = $res[2];
            $author_id = $res[3];
            $posts = $res[4];
            $all_posts_data = $res[5];
            $total_count = isset($all_posts_data) ? count($all_posts_data) : 0;
            return array("post" => array("items"=> $posts,"all_items"=>$all_posts_data, "total_count"=> $total_count));
        }
    }
}
