<?php
namespace Humane_Sites;

use Humane_Acquire\Model_Forms;


if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * All CRUD operations and data related functions regarding 
 * Settings functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage Settings
 */
class Model_Options extends Model {
    /**
     * @ignore
     */
    public function prepare_data() {}

    /**
     * Gets a saved theme setting value by key
     * 
     * @param string $key
     * 
     * @return string
     */
    public static function get_styling_option($key){
        $theme_settings = get_option("humane_theme_settings");
        $theme_settings = maybe_unserialize($theme_settings);
        return $theme_settings[$key];
    }

    /**
     * Updates a theme setting by key
     * 
     * @param string $key
     * @param mixed $value
     * 
     * @return true
     */
    public static function update_styling_option($key, $value){
        $theme_settings = get_option("humane_theme_settings");
        $theme_settings = maybe_unserialize($theme_settings);
        $theme_settings[$key] = $value;
        update_option("humane_theme_settings", $theme_settings);
        return true;
    }
    /**
     * Saves the options from $_POST object
     * 
     * @return bool True if option is updated,
     *              False otherwise
     */
    public static function _save(){
        do_action("modify_option_values");
        foreach($_POST as $key => $value) {
            if ($key != "humane_edit_settings") {
                if($_POST["secret_type_field"] === "styling"){
                    $update_wp_options = self::update_styling_option($key, $value);
                }else{
                    if($key == 'site_square_logo'){
                        $site_icon_id = attachment_url_to_postid($value);
                        update_option('site_icon', $site_icon_id);
                    }
                    $update_wp_options = update_option($key, $value);
                }
            }
        }
		return $update_wp_options;
    }

    /**
     * Updates the options if nonce is present
     * 
     * @return void
     */
    public static function update() {
		if( ! wp_verify_nonce( $_POST['humane_edit_settings'] ?? "", 'humane_edit_settings' ) ) {
            return;
		}
		$edit_wp_options = self::_save();
        return;
	}

    /**
     * Returns the set value of root variables in theme settings if present or returns the defalt values
     * 
     * @param array $theme_settings Saved setting values
     * @param array $default Default setting values
     * @param string $key Setting key
     * 
     * @return string Setting value
     */
    public static function get_or_default($theme_settings, $default, $key){
        $value = isset($theme_settings[$key]) ? $theme_settings[$key] : $default[$key];
        if(strpos($key, "font_size") !== false){
            if(strpos($value, "px") !== false){
                $value = (int)str_replace("px", "", $value);
                $value = $value / 16;
                $value = $value . "rem";
            }
        }
        return $value;
    }

    /**
     * Returns the default value of a setting
     * 
     * @param string $key Setting key
     * 
     * @return string Setting value
     */
    public static function get_default($key){
        $defaults = json_decode(file_get_contents(HUMANE_COMPOSER_DIR.'assets/settings/json/defaults.json'), true);
        return isset($defaults[$key]) ? $defaults[$key] : '';
    }
}