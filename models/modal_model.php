<?php

namespace Humane_Sites;

if (!defined('ABSPATH')) {
    exit();
}

/**
 * All CRUD operations and data related functions regarding 
 * Backlinks functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage Backlinks
 */
class Model_Content_Selection_Modal
{
    /**
     * Renders the default view of the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_default_view($items){
        foreach ($items as $item) {
            $post_id = $item->ID;
            $format = get_post_format($post_id);
            $author = get_the_author_meta("first_name", $item->post_author);
            if (empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
            $item->author = $author;
            $item->url = get_edit_post_link($post_id);
            $item->format = ucwords(empty($format) ? 'standard' : $format);
        }
        require HUMANE_COMPOSER_DIR . 'views/modal/index_default.html.php';
    }

    /**
     * Renders the view for a list of pages in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_page_view($items)
    {
        foreach ($items as $item) {
            $post_id = $item->ID;
            $author = get_the_author_meta("first_name", $item->post_author);
            if (empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
            $item->author = $author;
            $item->url = get_edit_post_link($post_id);
        }
        require HUMANE_COMPOSER_DIR . 'views/modal/index_page.html.php';
    }

    /**
     * Renders the view for a list of products in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_product_view($items)
    {
        foreach ($items as $item) {
            $post_id = $item->ID;
            $author = get_the_author_meta("first_name", $item->post_author);
            if (empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
            $item->author = $author;
            $price = get_post_meta($post_id, 'humane_price', true);
            $currency = get_post_meta($post_id, 'humane_currency', true);

            $item->price = $price;
            $item->currency = $currency;
            $item->url = get_edit_post_link($post_id);
        }
        require HUMANE_COMPOSER_DIR . 'views/modal/index_product.html.php';
    }

    /**
     * Renders the view for a list of subscriptions in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_subscription_view($items)
    {
        foreach ($items as $item) {
            $post_id = $item->ID;
            $author = get_the_author_meta("first_name", $item->post_author);
            if (empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
            $item->author = $author;
            $price = get_post_meta($post_id, 'humane_price', true);
            $currency = get_post_meta($post_id, 'humane_currency', true);
            $interval = get_post_meta($post_id, 'humane_interval', true);
            $period = get_post_meta($post_id, 'humane_period', true);
            if ($period === "daily")  $period = "Day(s)";
            if ($period === "weekly")  $period = "Week(s)";
            if ($period === "monthly")  $period = "Month(s)";
            if ($period === "yearly")  $period = "Year(s)";
            $item->price = $price;
            $item->currency = $currency;
            $item->period = $period;
            $item->interval = $interval;
            $item->url = get_edit_post_link($post_id);
        }
        require HUMANE_COMPOSER_DIR . 'views/modal/index_subscription.html.php';
    }

    /**
     * Renders the view for a gallery of media in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_media_gallery_view($items)
    {
        foreach ($items as $item) {
            $item->img_url = wp_get_attachment_image_src($item->ID, 'thumbnail-53')[0];
        }
        require HUMANE_COMPOSER_DIR . 'views/modal/index_media-gallery.html.php';
    }

    /**
     * Renders the view for a list of selected objects in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function render_selected_objects_view($post_ids)
    {
        $items = array();
        foreach ($post_ids as $post_id) {
            $post = get_post(intval($post_id));
            $items[] = $post;
        }
        $results = array();
        if (count($items) && $items[0]->post_type) $results["type"] = $results["type"] = $items[0]->post_type;
        else $results["type"] = "all";
        foreach ($items as $item) {
            if($item){
                $post_id = $item->ID;
                $item->date = get_the_date('Y-m-d', $item->ID);
                $author = get_the_author_meta("first_name", $item->post_author);
                if (empty($author)) $author = get_the_author_meta("user_login", $item->post_author);
                $item->author = $author;
                $item->url = get_edit_post_link($post_id);
                $item = apply_filters("render_selected_objects_view", $item);
            }
        }
        ob_start();
        require HUMANE_COMPOSER_DIR . 'views/modal/index_selected-objects.html.php';
        $results["markup"] = ob_get_clean();
        return $results;
    }

    /**
     * Renders the view for a list of objects in the modal
     * 
     * @param array $items Items to render in the modal
     * 
     * @return void
     */
    public static function tab_view($searchFor, $selected, $post_type, $offset = 0)
    {
        $selected = array_map('intval', $selected);
        add_filter('posts_where', array(__CLASS__, 'title_filter'), 10, 2); // add the search term in the db query.
        $items = get_posts(
            array(
                'post_type' => $post_type,
                'offset' => $offset,
                'posts_per_page' => 24,
                'exclude' => $selected,
                'post_title_search_term' => $searchFor, // passing the search term to the wordpress built-in db querying function.
                'suppress_filters' => FALSE, // turn it off to allow filtering the db query.
                'ignore_sticky_posts'    => true,
                'no_found_rows'        => true,
                'orderby' => 'post_date', 
                'order' => 'DESC'
            )
        );
        $total_count = count(
            get_posts(
                array(
                    'post_type' => $post_type,
                    'posts_per_page' => -1,
                    'exclude' => $selected,
                    'fields' => 'ids',
                    'post_title_search_term' => $searchFor, // passing the search term to the wordpress built-in db querying function.
                    'suppress_filters' => FALSE, // turn it off to allow filtering the db query.
                    'ignore_sticky_posts'    => true,
                    'no_found_rows'        => true
                )
            )
        );
        remove_filter('posts_where', array(__CLASS__, 'title_filter'), 10, 2); // remove the search term from the db query after the action is performed since the same query is reused at multiple places.
        return array("items" => $items, "total_count" => $total_count);
    }

    /**
     * Filters the posts bt Title in the content modal. 
     * 
     * The search string from the search box is put in a sql LIKE clause in this function and then passed to get_posts()
     * 
     * @param string $where - Contains the where clause that will be passed in the query. This where caluse needs to be appended with the like clause for the search term
     * @param object &$wp_query - stores the attributes being passed  to the wordpress built in database functions.
     * 
     * @return string $where - returns the updates query string having the like clause for the search term.
     */
    public static function title_filter($where, $wp_query)
    {
        global $wpdb; // object storing the database, table and column meta.
        $search_term = $wp_query->get('post_title_search_term'); // this is the attribute passed in the wordpress database function in the above code which stores the search term.

        if ($search_term != '' && $search_term != 'false') {
            $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql($wpdb->esc_like($search_term)) . '%\'';
        }

        return $where;
    }
}