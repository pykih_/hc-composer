<?php
namespace Humane_Sites;


if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * All CRUD operations and data related functions regarding 
 * Backlinks functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage Backlinks
 */
class Model_Backlinks extends Model {
	/**
	 * Global $wpdb
	 * @var object
	 */
	public $wpdb;

	/**
	 * Table name for this object
	 * @var string
	 */
	public $table_name = '_py_references';

	/**
	 * Object ID
	 * @var int
	 */
	public $id;

	/**
	 * Object table name of this row
	 * @var string
	 */
	public $object_table;

	/**
	 * Object ID of this row
	 * @var int
	 */
	public $object_id;

	/**
	 * This reference object count
	 * @var int
	 */
	public $count;

	/**
	 * Serialized array of pykih slices id where this object is referenced in
	 * @var string
	 */
	public $referenced_in;

	/**
	 * Construct
	 * @param mixed $args Int for object ID|Array consists of 'object_table' and 'object_id' key value pair
	 */
	public function __construct( $args = false ) {
		global $wpdb;
		$this->wpdb = $wpdb;

		if ( is_numeric( $args ) ) {
			$this->id = $args;
		} elseif ( is_array( $args ) ) {
			$this->object_table = $args['object_table'];
			$this->object_id    = $args['object_id'];
			$this->set_id_by_object();
		}

		parent::__construct( $this->id );
	}

	/**
	 * Required prepare_data function for this class
	 * @return void
	 */
	public function prepare_data() {
		if(!$this->count) $this->count = 0;
		$this->data = array(
			'id' => $this->id,
			'object_table' => $this->object_table,
			'object_id' => $this->object_id,
			'count' => $this->count,
			'referenced_in' => self::maybe_serialize($this->referenced_in),
		);

		$this->data_format = array(
			'%d', '%s', '%d', '%d', '%s'
		);
	}

	/**
	 * Set this object ID by object_table and object_id values
	 * @return void
	 */
	public function set_id_by_object() {
		$id = wp_cache_get( "{$this->object_table}_{$this->object_id}_id", '_references' );

		if ( false === $id ) {
			$sql = $this->wpdb->prepare( "SELECT id FROM {$this->wpdb->prefix}{$this->table_name} WHERE object_table = %s AND object_id = %d", $this->object_table, $this->object_id );

			$id = $this->wpdb->get_var( $sql );

			if ( is_numeric( $id ) && $id > 0 ) {
				wp_cache_set( "{$this->object_table}_{$this->object_id}_id", $id, '_references' );
			} else {
				$id = 0;
			}
		}

		if ( ! is_numeric( $id ) ) {
			$id = 0;
		}

		$this->id = $id;
	}

	/**
	 * Get ID of this object
	 * @return int
	 */
	public function get_id() {
		return (int) $this->id;
	}

	/**
	 * Get object table name of this object row
	 * @return string
	 */
	public function get_object_table() {
		return (string) $this->object_table;
	}

	/**
	 * Get object ID of this object row
	 * @return int
	 */
	public function get_object_id() {
		return (int) $this->object_id;
	}

	/**
	 * Get reference count of this object row
	 * @return int
	 */
	public function get_count() {
		return (int) $this->count;
	}

	/**
	 * Get serialized array of references of this object row
	 * @return string
	 */
	public function get_referenced_in() {
		return maybe_unserialize($this->referenced_in);
	}

	/**
	 * Set ID of this object
	 * @return void
	 */
	public function set_id( $id ) {
		$this->id = (int) $id;
	}

	/**
	 * Set object table name of this object
	 * @return void
	 */
	public function set_object_table( $object_table ) {
		$this->object_table = (string) $object_table;
	}

	/**
	 * Set object ID of this object
	 * @return void
	 */
	public function set_object_id( $object_id ) {
		$this->object_id = (int) $object_id;
	}

	/**
	 * Set reference count of this object
	 * @return void
	 */
	public function set_count( $count ) {
		$this->count = (int) $count;
	}

	/**
	 * Set serialized array of references of this object
	 * @return void
	 */
	public function set_referenced_in( $referenced_in ) {
		$this->referenced_in = (string) self::maybe_serialize($referenced_in);
	}

	/**
	 * Increment reference count of this object row
	 * @return void
	 */
	public function increment() {
		$this->count++;
	}

	/**
	 * Decrement reference count of this object row
	 * @return void
	 */
	public function decrement() {
		$this->count--;
	}

	/**
	 * Add reference to the object
	 * @param string $referenced_in_id ID this object is referenced in
	 * @return void
	 */
	public function add_reference( $referenced_in_id ) {
		$refs = ! empty( $this->referenced_in ) ? maybe_unserialize( $this->referenced_in ) : array();
		$key = array_search( $referenced_in_id, $refs, TRUE );
		if ( false === $key ) {
			$refs[] = $referenced_in_id;
			$this->referenced_in = $refs;
			$this->increment();
		}
	}

	/**
	 * Delete reference from the object
	 * @param  string $referenced_in_id ID this object is referenced in
	 * @return void
	 */
	public function delete_reference( $referenced_in_id ) {
		$refs = ! empty( $this->referenced_in ) ? maybe_unserialize( $this->referenced_in ) : array();
		$key  = array_search( $referenced_in_id, $refs, TRUE);
		if ( $key !== false && isset( $refs[ $key ] ) ) {
			unset( $refs[ $key ] );
			$this->referenced_in = self::maybe_serialize( $refs );
			$this->decrement();
		}
		$this->save();
	}

	public static function maybe_remove_postfix($str, $postfix){
		if(strpos($str, $postfix) !== false) return str_replace($postfix, "", $str);
		return $str;
	}

	public static function maybe_serialize( $data ){
		if ( is_array( $data ) || is_object( $data ) ) {
			return serialize( $data );
		}
		return $data;
	}

}