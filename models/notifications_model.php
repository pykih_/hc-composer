<?php
namespace Humane_Sites;

use Humane_Acquire\Model_Forms;
use WhichBrowser\Parser;

use stdClass;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * All CRUD operations and data related functions regarding 
 * Backlinks functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage Backlinks
 */
class Model_Notifications extends Model {
    /**
     * @var object Global $wpdb
     */
    public $wpdb;

	/**
	 * @var string Table name
	 */
	public $table_name = '_py_notifications';

	/**
	 * @var int Unique Auto-incrementing ID
	 */
	public $id;

	/**
	 * @var string Table of the notifying object
	 */
	public $object_table;

	/**
	 * @var int ID of the notifying object
	 */
	public $object_id;

	/**
	 * @var string Action of the user event when notification has to be sent
	 */
	public $py_user_events_action;

	/**
	 * @var int Stores the User ID for the person who created the form
	 */
	public $created_by;

	/**
	 * @var int Stores the timestamp for the creation of the form
	 */
	public $created_at;

	/**
	 * @var int Stores the User ID for the person who last updated the form
	 */
	public $updated_by;

	/**
	 * @var int Stores the timestamp for the updation of the form
	 */
	public $updated_at;

	/**
	 * @var string Serialized array of users to notify
	 */
	public $wp_users_ids;

	/**
	 * @var string Any extra data regarding the notification
	 */
	public $extra_data;

    public function get_id() {
		return (int) $this->id;
	}
	public function get_object_table(){
		return (string) $this->object_table;
	}
	public function get_object_id(){
		return (int) $this->object_id;
	}
	public function get_py_user_events_action(){
		return (string) $this->py_user_events_action;
	}
	public function get_wp_users_ids(){
		return maybe_unserialize($this->wp_users_ids);
	}
	public function get_extra_data(){
		return maybe_unserialize($this->extra_data);
	}
	public function get_created_by(){
		return (string) $this->created_by;
	}
	public function get_created_at(){
		return (int) $this->created_at;
	}
	public function get_updated_by(){
		return (string) $this->updated_by;
	}
	public function get_updated_at(){
		return (int) $this->updated_at;
	}
	public function set_id( $id ) {
		$this->id = (int) $id;
	}
	public function set_object_table($object_table){
		$this->object_table = (string) $object_table;
	}
	public function set_object_id($object_id){
		$this->object_id = (int) $object_id;
	}
	public function set_py_user_events_action($py_user_events_action){
		$this->py_user_events_action = (string) $py_user_events_action;
	}
	public function set_wp_users_ids($wp_users_ids){
		$this->wp_users_ids = serialize($wp_users_ids);
	}
	public function set_extra_data($extra_data){
		$this->extra_data = serialize($extra_data);
	}
	public function set_created_by($created_by){
		$this->created_by = (string) $created_by;
	}
	public function set_created_at($created_at){
		$this->created_at = (int) $created_at;
	}
	public function set_updated_by($updated_by){
		$this->updated_by = (string) $updated_by;
	}
	public function set_updated_at($updated_at){
		$this->updated_at = (int) $updated_at;
	}

    /**
	 * Constructs an object based on ID or compound key
	 * 
     * @param int|array $args
     * 
     * @return void
     */
    public function __construct( $args = false ) {
		global $wpdb;
		$this->wpdb = $wpdb;
		if ( is_numeric( $args ) ) {
			$this->id = $args;
		} elseif ( is_array( $args ) ) {
			$this->object_table 			= $args['object_table'];
			$this->object_id    			= $args['object_id'];
			$this->py_user_events_action    = $args['py_user_events_action'];
			$this->set_id_by_object();
		}
		parent::__construct( $this->id );
	}
	
	
	/**
	 * Set this object ID by object_table and object_id values
	 * 
	 * @return void
	 */
	public function set_id_by_object() {
		$id = wp_cache_get( "{$this->object_table}_{$this->object_id}_{$this->py_user_events_action}_id", '_py_notifications' );

		if ( false === $id ) {
			$sql = $this->wpdb->prepare( "SELECT id FROM {$this->wpdb->prefix}{$this->table_name} WHERE object_table = %s AND object_id = %d AND py_user_events_action = %s", $this->object_table, $this->object_id, $this->py_user_events_action );

			$id = $this->wpdb->get_var( $sql );

			if ( is_numeric( $id ) && $id > 0 ) {
				wp_cache_set( "{$this->object_table}_{$this->object_id}_{$this->py_user_events_action}_id", $id, '_py_notifications' );
			} else {
				$id = 0;
			}
		}

		if ( ! is_numeric( $id ) ) {
			$id = 0;
		}

		$this->id = $id;
	}

	/**
	 * Required prepare_data function for this class
	 * 
	 * @return void
	 */
	public function prepare_data() {
        $this->updated_at = current_time("timestamp");
        $this->updated_by = get_current_user_id();
		$this->data = array(
			'id' => $this->id,
			'object_table' => $this->object_table,
			'object_id' => $this->object_id,
			'py_user_events_action' => $this->py_user_events_action,
			'created_by' => $this->created_by,
			'created_at' => $this->created_at,
			'updated_by' => $this->updated_by,
			'updated_at' => $this->updated_at,
			'wp_users_ids' => $this->wp_users_ids,
			'extra_data' => $this->extra_data
		);
		$this->data_format = array(
			'%d', '%s', '%d', '%s', '%s', '%d', '%s', '%d', '%s', '%s'
		);
	}
	
	/**
	 * Returns all created notifications
	 * 
	 * @return void
	 */
	public static function get_all(){
        global $wpdb;
		$notifications_table = Database_Notifications::$notifications_table;

		$result = $wpdb->prepare(
			"SELECT * from {$notifications_table} ORDER BY `created_at` DESC"
		);
        return $wpdb->get_results($result);
	}
}