<?php
namespace Humane_Sites;

use Humane_Spaces\Model_Playlist;

use stdClass;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}


/**
 * All CRUD operations and data related functions regarding
 * Posts functionality are added in this class.
 *
 * @package Humane Sites
 * @subpackage Posts
 */
class Model_Posts {

	/**
	 * Destroys a given post by ID
	 *
	 * @param int $id
	 *
	 * @return void
	 */
	public static function destroy( $id ) {
		if ( ! $id ) {
			return false;
		}
		$post = wp_trash_post( $id );
	}

	/**
	 * Returns a post based on ID
	 *
	 * @param int $id
	 *
	 * @return object
	 */
	public static function find_by_id( $id ) {
		return get_post( $id );
	}

	/**
	 * Returns the current post count
	 *
	 * @return int
	 */
	public static function count() {
		$posts = get_posts(
			array(
				'numberposts'         => -1,
				'post_type'           => 'post',
				'post_status'         => 'any',
				'fields'              => 'ids',
				'suppress_filters'    => false,
				'ignore_sticky_posts' => true,
				'no_found_rows'       => true,
			)
		);
		return count( $posts );
	}

	/**
	 * Returns all posts on the site
	 *
	 * @param bool $filter
	 *
	 * @return array
	 */
	public static function find_all() {
		$posts = get_posts(
			array(
				'post_type'           => 'post',
				'numberposts'         => -1,
				'post_status'         => 'any',
				'suppress_filters'    => false,
				'ignore_sticky_posts' => true,
				'no_found_rows'       => true,
			)
		);
		return $posts;
	}

	/**
	 * Returns the guided tour link of a page
	 *
	 * @param int    $page_id
	 * @param string $key
	 *
	 * @return array
	 */
	public static function get_guided_tour_link( $page_id, $key ) {
		 $next_page     = get_post_meta( $page_id, $key, true );
		$next_page_link = false;
		$pageID         = '';
		if ( preg_match( '!\d+!', $next_page ) ) {
			preg_match_all( '!\d+!', $next_page, $pageID );
			if ( ! empty( $pageID ) ) {
				$pageID = $pageID[0][0];
			}
			if ( get_permalink( $pageID ) ) {
				$next_page_link = get_permalink( $pageID );
			}
		}
		return array(
			'link' => $next_page_link,
			'id'   => $pageID,
		);
	}

	/**
	 * Returns the title of a post by ID
	 *
	 * @param int $id
	 *
	 * @return string
	 */
	public static function get_page_name_from_id( $id = 0 ) {
		$post = get_post( $id );
		if ( $post ) {
			return $post->post_title;
		}
		return false;
	}


	/**
	 * Creates post duplicate as a draft and redirects then to the edit post screen
	 *
	 * @return void
	 */
	public static function add_duplicate_post_as_draft() {

		global $wpdb;
		if ( ! ( isset( $_GET['post'] ) || isset( $_POST['post'] ) || ( isset( $_REQUEST['action'] ) && 'add_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
			wp_die( 'No post to duplicate has been supplied!' );
		}

		/*
		* Nonce verification
		*/
		if ( ! isset( $_GET['duplicate_nonce'] ) && ! wp_verify_nonce( $_GET['duplicate_nonce'], 'py_duplicate_nonce' ) ) {
			return;
		}

		/*
		* get the original post id
		*/
		$post_id = ( isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
		/*
		* and all the original post data then
		*/
		$post = get_post( $post_id );

		/*
		* if you don't want current user to be the new post author,
		* then change next couple of lines to this: $new_post_author = $post->post_author;
		*/
		$current_user    = wp_get_current_user();
		$new_post_author = $current_user->ID;

		$post_status = 'draft';
		if ( $post->post_type == 'aggregation' ) {
			$post_status = 'publish';
		}

		/*
		* if post data exists, create the post duplicate
		*/
		if ( isset( $post ) && $post != null ) {

			/*
			* new post data array
			*/
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status'    => $post->ping_status,
				'post_author'    => $new_post_author,
				'post_content'   => wp_slash( $post->post_content ),
				'post_excerpt'   => $post->post_excerpt,
				'post_name'      => $post->post_name,
				'post_parent'    => $post->post_parent,
				'post_password'  => $post->post_password,
				'post_status'    => $post_status,
				'post_title'     => $post->post_title,
				'post_type'      => $post->post_type,
				'to_ping'        => $post->to_ping,
				'menu_order'     => $post->menu_order,
			);

			/*
			* insert the post by wp_insert_post() function
			*/
			$new_post_id = wp_insert_post( $args );

			/*
			* get all current post terms ad set them to the new post draft
			*/
			$taxonomies = get_object_taxonomies( $post->post_type ); // returns array of taxonomy names for post type, ex array("category", "post_tag");
			foreach ( $taxonomies as $taxonomy ) {
				$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'slugs' ) );
				wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
			}

			/*
			* duplicate all post meta in two SQL queries
			*/
			$post_meta_infos = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id" );
			if ( count( $post_meta_infos ) != 0 ) {
				foreach ( $post_meta_infos as $meta_info ) {
					$meta_key = $meta_info->meta_key;
					if ( $meta_key == '_wp_old_slug' ) {
						continue;
					}
					update_post_meta( $new_post_id, $meta_key, $meta_info->meta_value );
				}
			}

			/*
			* finally, redirect to the edit post screen for the new draft
			*/
			if ( $post->post_type !== 'aggregation' ) {
				wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
			} else {
				wp_redirect( admin_url( 'admin.php?page=wp_posts_aggregation_edit&id=' . $new_post_id ) );
			}
			exit;
		} else {
			wp_die( 'Post creation failed, could not find original post: ' . $post_id );
		}
	}

	/**
	 * Returns the duration of a video embedded in a post
	 *
	 * @param int $id
	 *
	 * @return int Duration of the video
	 */
	public static function get_video_time( $id ) {
		$output = get_the_content( null, false, $id );
		preg_match_all( '/<iframe[^>]+src="https:\/\/www.youtube.com\/embed\/([^"]+)"/', $output, $match );
		$type = 0;
		if ( is_array( $match[1] ) && ! count( $match[1] ) ) {
			preg_match_all( '/<!--\swp:embed\s(\{.*?\})\s\/?-->(?:(.*?)?<!--\s\/wp:embed\s-->)?/s', $output, $match );
			$type = 2;
		}
		if ( is_array( $match[1] ) && ! count( $match[1] ) ) {
			preg_match_all( '/{.*}/', $output, $match );
			$type = 1;
		}
		$time = 0;
		if ( is_array( $match ) && array_key_exists( 1, $match ) && $match[1] && is_array( $match[1] ) ) {
			$match_value = $match[1][0];
			if ( $type === 0 ) {
				$vidkey = $match_value;
			} elseif ( $type === 1 ) {
				$vidkey   = json_decode( $match[0][ $key ], true );
				$videoKey = explode( '=', $vidkey['url'] )[1];
				if ( empty( $vidkey ) ) {
					$videoKey = explode( '/', $vidkey['url'] );
					$vidkey   = $videoKey[ count( $videoKey ) - 1 ];
				} else {
					$vidkey = $videoKey;
				}
			} elseif ( $type === 2 ) {
				$url = json_decode( $match_value, true )['url'];

				$url_split = explode( '?', $url )[1] ?? '';
				$vidkey    = explode( '=', $url_split )[1] ?? '';
				if ( empty( $vidkey ) ) {
					$url_parts = explode( '/', $url );
					$vidkey    = $url_parts[ count( $url_parts ) - 1 ];
				}
			}
			$apikey = 'AIzaSyBRgQzEN-Hb0dSFUR8yvqf903ha9MlgDXM';
			if ( ! empty( $vidkey ) ) {
				$dur         = file_get_contents( "https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$vidkey&key=$apikey" );
				$VidDuration = json_decode( $dur, true );
				foreach ( $VidDuration['items'] as $vidTime ) {
					$VidDuration = $vidTime['contentDetails']['duration'];
				}
				$time = $time + self::convert_time( $VidDuration );
			}
		}
		$minutes = floor( $time );
		$seconds = floor( ( $time - $minutes ) * 60 );
		$seconds = str_pad( $seconds, 2, '0', STR_PAD_LEFT );
		$time    = $minutes . ':' . $seconds;
		return $time;
	}

	/**
	 * Converts a YouTube style date string into minutes
	 *
	 * @param string $yt YouTube type date string
	 *
	 * @return int Duration in minutes
	 */
	public static function convert_time( $yt ) {
		$yt = str_replace( array( 'P', 'T' ), '', $yt );
		foreach ( array( 'D', 'H', 'M', 'S' ) as $a ) {
			if ( is_string( $a ) && is_string( $yt ) ) {
				$pos = strpos( $yt, $a );
				if ( $pos !== false ) {
					${$a} = substr( $yt, 0, $pos );
				} else {
					${$a} = 0;
					continue; }
				$yt = substr( $yt, $pos + 1 );
			}
		}
		if ( $D > 0 ) {
			$M        = str_pad( $M, 2, '0', STR_PAD_LEFT );
			$S        = str_pad( $S, 2, '0', STR_PAD_LEFT );
			$str_time = ( $H + ( 24 * $D ) ) . ":$M:$S";
			sscanf( $str_time, '%d:%d:%d', $hours, $minutes, $seconds );

			$time_seconds = isset( $hours ) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
		} elseif ( $H > 0 ) {
			$M        = str_pad( $M, 2, '0', STR_PAD_LEFT );
			$S        = str_pad( $S, 2, '0', STR_PAD_LEFT );
			$str_time = "$H:$M:$S";
			sscanf( $str_time, '%d:%d:%d', $hours, $minutes, $seconds );
			$time_seconds = isset( $hours ) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
		} else {
			$S        = str_pad( $S, 2, '0', STR_PAD_LEFT );
			$str_time = "$M:$S";
			sscanf( $str_time, '%d:%d', $minutes, $seconds );
			$time_seconds = $minutes * 60 + $seconds;
		}
		return $time_seconds / 60;
	}

	/**
	 * Calculates the reading time in minutes for a given post
	 *
	 * @param int $id Post ID
	 *
	 * @return float Reading time in minutes
	 */
	public static function get_reading_time( $id ) {

		if ( get_post_format( $id ) === 'gallery' ) {
			$attachments = count( get_attached_media( 'image', $id ) );
			if ( ! $attachments || ! is_array( $attachments ) ) {
				return 0;
			}
			$count = count( $attachments );
			return round( $count / 12, 2 );
		};
		if ( get_post_type( $id ) === 'post' || get_post_type( $id ) === 'datastory' ) {
			if ( get_post_format( $id ) === 'video' ) {
				return self::get_video_time( $id );
			} else {
				$wpm          = 180;
				$post         = get_post( $id );
				$content      = $post->post_content;
				$content_size = count( explode( ' ', $content ) );
				$reading_time = $content_size / $wpm;
				$video_time   = self::get_video_time( $id );
				$reading_time = (float) $reading_time + (float) $video_time;
				return $reading_time < 1 ? round( $reading_time, 2 ) : intval( $reading_time );
			}
		} elseif ( get_post_type( $id ) == 'event' ) {
			$post_id      = $id ? $id : get_the_ID();
			$post_meta    = get_post_meta( $post_id );
			$start        = array_key_exists( 'humane_events_start_time', $post_meta ) ? strtotime( $post_meta['humane_events_start_time'][0] ) : time();
			$end          = array_key_exists( 'humane_events_end_time', $post_meta ) ? strtotime( $post_meta['humane_events_end_time'][0] ) : time();
			$reading_time = ( $end - $start ) / 60;
			return $reading_time < 1 ? round( $reading_time, 2 ) : intval( $reading_time );
		}
	}

	/**
	 * Creates the prepared statement based on where and order_by passed
	 *
	 * @param string $where
	 * @param string $order_by_field
	 *
	 * @return string
	 */
	public static function get_main_query( $where, $order_by_field = '()' ) {
		global $wpdb;
		$main_query = $wpdb->prepare(
			"SELECT * from {$wpdb->prefix}posts WHERE id IN {$where} ORDER BY FIELD {$order_by_field}"
		);
		return $main_query;
	}

	/**
	 * Returns a list of posts based on arguments passed
	 *
	 * @param array $filters Filters to apply to query
	 * @param array $sort Sort to apply to query
	 * @param int   $limit
	 * @param int   $offset
	 * @param bool  $type Post types to filter by
	 * @param array $post_status Post status to filter by
	 * @param bool  $all_data Flag that denotes if all data needs to be returned
	 *                        or only paginated results
	 * @param array $extra_filters Meta filter data
	 * @param array $extra_data Any other data required for filtering
	 *
	 * @return array List of IDs
	 */
	public static function get_filtered_post_list( $filters, $sort, $limit, $offset, $type = false, $post_status = array( 'publish', 'draft', 'future' ), $all_data = false, $extra_filters = array(), $extra_data = array() ) {
		$temporary_playlist = new Model_Playlist();
		if ( $type ) {
			$temporary_playlist->type = array( $type );
		} else {
			$temporary_playlist->type = Controller_Posts::filter_existing_post_type( 'playlist' );
		}
		$temporary_playlist = apply_filters( 'get_filtered_post_list_playlist', $temporary_playlist );
		$total_results      = $temporary_playlist->get_objects_from_query( null, null, null, $post_status, true, $extra_filters );
		$term_id            = $extra_data['term_id'];
		$total_results      = apply_filters( "get_filtered_post_list_$term_id", $total_results, $extra_data );
		$keys               = array(
			'selected_categories',
			'selected_tags',
			'selected_formats',
			'selected_authors',
		);
		foreach ( $keys as $value ) {
			if ( is_array( $filters ) && array_key_exists( $value, $filters ) ) {
				$temporary_playlist->$value = $filters[ $value ];
			}
		}
		$temporary_playlist = apply_filters( 'get_filtered_post_list_playlist', $temporary_playlist );
		if ( isset( $filters['search'][0] ) && ! empty( $filters['search'][0] ) ) {
			$extra_filters[] = array(
				'key'     => 'humane_searchable',
				'compare' => 'LIKE',
				'value'   => $filters['search'][0],
			);
		}
		if ( isset( $filters['start_date'][0] ) && ! empty( $filters['start_date'][0] ) ) {
			$temporary_playlist->start_date = $filters['start_date'][0];
		}
		if ( isset( $filters['end_date'][0] ) && ! empty( $filters['end_date'][0] ) ) {
			$temporary_playlist->end_date = $filters['end_date'][0];
		}
		$sort_order = $sort['sort'] ?? 'DESC';
		$order_by   = $sort['column'] ?? 'modified_date';
		if ( isset( $filters['search'][0] ) && ! empty( $filters['search'][0] ) ) {
			$search = $filters['search'][0];
		}
		$results = $temporary_playlist->get_objects_from_query( $search, $sort_order, $order_by, $post_status, true, $extra_filters );
		$results = apply_filters( "get_filtered_post_list_$term_id", $results, $extra_data );
		// write_log( $results, 'current_result is' );
		if ( is_array( $results ) && ! empty( $results ) ) {
			$filtered_count = count( $results );
		} else {
			$filtered_count = 0;
		}

		if ( $offset > $filtered_count ) {
			$offset = 0;
		}
		if ( $all_data ) {
			$total_filtered_results = $results;
			$total_filtered_results = '(' . join( ',', $total_filtered_results ) . ')';
		}
		$results = array_slice( $results, $offset, $limit );
		$temp    = $results;
		array_unshift( $temp, 'ID' );
		$order_by_field = '(' . join( ',', $temp ) . ')';
		$results        = '(' . join( ',', $results ) . ')';

		return array(
			'results'        => $results,
			'all_results'    => $total_filtered_results,
			'order_by_field' => $order_by_field,
			'count'          => $filtered_count,
			'total_count'    => count( $total_results ),
		);
	}

	/**
	 * Returns results based on arguments passed
	 *
	 * @param array $filters Filters to apply to query
	 * @param array $sort Sort to apply to query
	 * @param int   $limit
	 * @param int   $offset
	 * @param bool  $type Post types to filter by
	 * @param array $post_status Post status to filter by
	 * @param bool  $all_data Flag that denotes if all data needs to be returned
	 *                        or only paginated results
	 * @param array $extra_filters Meta filter data
	 * @param array $extra_data Any other data required for filtering
	 *
	 * @return array
	 */
	public static function get_from_py_posts_view( $sort = array(), $filters = array(), $page = 1, $limit = 20, $type = false, $post_status = array( 'publish', 'draft', 'future' ), $all_data = false, $extra_filters = array(), $extra_data = array() ) {
		global $wpdb;
		$offset         = ( $page - 1 ) * $limit;
		$filtered_data  = self::get_filtered_post_list( $filters, $sort, $limit, $offset, $type, $post_status, $all_data, $extra_filters, $extra_data );
		$filtered_count = $filtered_data['count'];
		$total_count    = $filtered_data['total_count'];
		$order_by_field = $filtered_data['order_by_field'];
		$where          = $filtered_data['results'];
		$all_results    = $filtered_data['all_results'];
		$results        = array();
		if ( $filtered_count ) {
			$query = self::get_main_query( $where, $order_by_field );
			try {
				$results = $wpdb->get_results( $query );
			} catch ( Exception $e ) {
				$results = array();
			}
		}
		$results = self::process_data( $results );
		if ( $all_data && $total_count ) {
			$query       = self::get_main_query( $all_results, $order_by_field );
			$all_results = $wpdb->get_results( $query );
			$all_results = array_map(
				function( $a ) {
					return $a->ID;
				},
				$all_results
			);
		}
		return array(
			'results'     => $results,
			'count'       => $filtered_count,
			'total_count' => $total_count,
			'all_results' => $all_results,
		);
	}

	/**
	 * Adds datapoints for each item in the passed array
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	public static function process_data( $data ) {
		global $wpdb;
		foreach ( $data as $datum ) {
			$creator               = get_user_by( 'ID', get_post_field( 'post_author', $datum->ID ) );
			$creator_meta          = get_user_meta( $creator->ID );
			$datum->post_title     = html_entity_decode( $datum->post_title );
			$datum->categories     = strip_tags( get_the_term_list( $datum->ID, 'category', '', ' | ' ) );
			$datum->tags           = strip_tags( get_the_term_list( $datum->ID, 'post_tag', '', ' | ' ) );
			$datum->format         = strip_tags( get_the_term_list( $datum->ID, 'writing_format', '', ' | ' ) );
			$datum->featured_image = wp_get_attachment_url( get_post_thumbnail_id( $datum->ID ), 'thumbnail' );
			$datum->author_name    = $creator_meta['first_name'][0] . ' ' . $creator_meta['last_name'][0];
			if ( $datum->post_type === 'event' ) {
				$datum->location       = get_post_meta( $datum->ID, 'humane_events_address', true );
				$datum->maps_link      = get_post_meta( $datum->ID, 'humane_events_maps_link', true );
				$datum->web_location   = get_post_meta( $datum->ID, 'humane_events_video', true );
				$datum->start_time     = get_post_meta( $datum->ID, 'humane_events_start_time', true );
				$datum->end_time       = get_post_meta( $datum->ID, 'humane_events_end_time', true );
				$datum->event_timezone = get_post_meta( $datum->ID, 'humane_events_timezone', true );
			} elseif ( $datum->post_type === 'aggregation' ) {
				$datum->redirect_to_url = get_post_meta( $datum->ID, 'humane_aggregations_redirect_to_url', true );
				$datum->domain          = get_post_meta( $datum->ID, 'humane_aggregations_domain', true );
				$datum->published_at    = get_post_meta( $datum->ID, 'humane_aggregations_published_at', true );
				$datum->author_name     = get_post_meta( $datum->ID, 'humane_aggregations_author', true );
				$datum->last_name       = '';
				$datum->featured_image  = get_post_meta( $datum->ID, 'humane_aggregations_featured_image', true );
			}
		}
		return $data;
	}
}
