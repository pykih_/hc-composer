<?php
namespace Humane_Sites;

use Humane_Sites\Model;
use Humane_Sites\Controller_Dashboard;
use Humane_Acquire\Model_Forms;
// use WhichBrowser\Parser;
use Humane_Acquire\Model_Login_Wall;
use stdClass;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * All CRUD operations and data related functions regarding 
 * User Events functionality are added in this class.
 * 
 * @package Humane Sites
 * @subpackage User Events
 */
class Model_User_Events extends Model {
    /**
     * @var object Global $wpdb
     */
    public $wpdb;
	
	/**
	 * @var string Table name
	 */
	public $table_name = '_py_user_events';
	
	/**
	 * @var int Unique Auto-incrementing ID
	 */
	public $id;

	/**
	 * @var int Stores the User ID for the person who created the form
	 */
	public $created_by;
	
	/**
	 * @var int Stores the timestamp for the creation of the form
	 */
	public $created_at;

	/**
	 * @var int Post ID where the user event occured
	 */
	public $wp_post_id;

	/**
	 * @var int Nudge ID where the user event occured
	 */
	public $py_nudges_id;

	/**
	 * @var int Form ID where the user event occured
	 */
	public $py_user_form_id;

	/**
	 * @var int Email Template to be sent when user event occurs
	 */
	public $py_email_template_id;

	/**
	 * @var string Data of the event that occured
	 */
	public $details_json;

	/**
	 * @var string Copy of the email sent to the user when the event occured
	 */
	public $email_copy;

    /**
     * @var string IP Address of the user who triggered the event
     */
    public $ip_address;

	/**
     * @var string Browser of the user who triggered the event
     */
    public $browser;

	/**
     * @var string Device of the user who triggered the event
     */
	public $device;

	/**
     * @var string OS of the user who triggered the event
     */
	public $os;

	/**
     * @var string Latitude of the user who triggered the event
     */
	public $lat;

	/**
     * @var string Longitude of the user who triggered the event
     */
	public $longitude;

	/**
     * @var string Location data of the user who triggered the event
     */
	public $location_data;

	/**
     * @var string Database table of the object which triggered the event
     */
	public $object_table;

	/**
     * @var string User agent data of the user who triggered the event
     */
	public $user_agent_data;

	/**
     * @var int Stores if the action is client facing or admin facing
     */
	public $is_client_facing_action;

	/**
     * @var string Name of the action of the event
     */
	public $action;

	/**
     * @var string Path the user went through on the site before event occured
     */
	public $user_flow;

    public function get_id() {
		return (int) $this->id;
	}

    public function get_created_by() {
		return (int) $this->created_by;
	}

    public function get_wp_post_id() {
		return (int) $this->wp_post_id;
	}
	public function get_py_nudges_id() {
		return (int) $this->py_nudges_id;
	}
	public function get_py_user_form_id() {
		return (int) $this->py_user_form_id;
	}

	public function get_py_email_template_id() {
		return (int) $this->py_email_template_id;
	}

	public function get_details_json() {
		return (string) $this->details_json;
	}
	public function get_email_copy() {
		return (string) $this->email_copy;
	}
	public function get_ip_address() {
		return (string) $this->ip_address;
	}

	public function get_browser() {
		return (string) $this->browser;
	}

	public function get_device() {
		return (string) $this->device;
	}

	public function get_os() {
		return (string) $this->os;
	}

	public function get_lat() {
		return (string) $this->lat;
	}

	public function get_location_data() {
		return (string) $this->location_data;
	}
	public function get_object_table() {
		return (string) $this->object_table;
	}
	public function get_user_agent_data() {
		return (string) $this->user_agent_data;
	}
	public function get_longitude() {
		return (string) $this->longitude;
	}

	public function get_is_client_facing_action() {
		return (bool) $this->is_client_facing_action;
	}
	
	public function get_action() {
		return (string) $this->action;
	}

	public function get_created_at() {
		return (string) $this->created_at;
	}

	public function get_user_flow() {
		return (string) maybe_unserialize($this->user_flow);
	}
	public function get_referral_source() {
		return (string) $this->referral_source;
	}

	public function set_user_flow( $user_flow ) {
		$this->user_flow = (int) $user_flow;
	}
	public function set_referral_source( $referral_source ) {
		$this->referral_source = (int) $referral_source;
	}
	public function set_id( $id ) {
		$this->id = (int) $id;
	}

	public function set_created_by($created_by) {
		$this->created_by = (int) $created_by;
	}

	public function set_wp_post_id($wp_post_id) {
		$this->wp_post_id = (int) $wp_post_id;
	}
	public function set_py_nudges_id($py_nudges_id) {
		$this->py_nudges_id = (int) $py_nudges_id;
	}
	public function set_py_user_form_id($py_user_form_id) {
		$this->py_user_form_id = (int) $py_user_form_id;
	}
	public function set_py_email_template_id($py_email_template_id) {
		$this->py_email_template_id = (int) $py_email_template_id;
	}
	public function set_details_json($details_json) {
		$this->details_json = (string) self::maybe_serialize($details_json);
	}
	public function set_email_copy($email_copy) {
		$this->email_copy = (string) $email_copy;
	}
	public function set_ip_address($ip_address) {
		$this->ip_address = (string) $ip_address;
	}

	public function set_browser($browser) {
		$this->browser = (string) $browser;
	}

	public function set_device($device) {
		$this->device = (string) $device;
	}

	public function set_os($os) {
		$this->os = (string) $os;
	}

	public function set_lat($lat) {
		$this->lat = (string) $lat;
	}

	public function set_location_data($location_data) {
		$this->location_data = (string) self::maybe_serialize($location_data);
	}
	public function set_object_table($object_table) {
		$this->object_table = (string) self::maybe_serialize($object_table);
	}
	
	public function set_user_agent_data($user_agent_data) {
		$this->user_agent_data = (string) $user_agent_data;
	}
	public function set_longitude($longitude) {
		$this->longitude = (string) $longitude;
	}

	public function set_is_client_facing_action($is_client_facing_action) {
		$this->is_client_facing_action = (string) $is_client_facing_action;
	}

	public function set_action($action) {
		$this->action = (string) $action;
	}

	public function set_created_at($created_at) {
		$this->created_at = (string) $created_at;
	}

	public static function maybe_serialize( $data ){
		if ( is_array( $data ) || is_object( $data ) ) {
			return serialize( $data );
		}
		return $data;
	}

    public function __construct( $id = 0) {
		global $wpdb;
		$this->wpdb = $wpdb;
		$this->id = $id;
		parent::__construct( $this->id );
	}

	/**
	 * Required prepare_data function for this class
	 * 
	 * @return void
	 */
	public function prepare_data() {
		$this->data = array(
			'id' => $this->id,
			'created_by' => $this->created_by,
			'wp_post_id' => $this->wp_post_id,
			'py_nudges_id' => $this->py_nudges_id,
			'py_user_form_id' => $this->py_user_form_id,
			'py_email_template_id' => $this->py_email_template_id,
			'details_json' => $this->details_json,
			'email_copy' => $this->email_copy,
            'ip_address' => $this->ip_address,
            'browser' => $this->browser,
            'device' => $this->device,
            'os' => $this->os,
            'is_client_facing_action' => $this->is_client_facing_action,
			'lat' => $this->lat,
			'location_data' => $this->location_data,
			'object_table' => $this->object_table,
			'user_agent_data' => $this->user_agent_data,
			'longitude' => $this->longitude,
			'action' => $this->action,
			'created_at' => $this->created_at,
			'user_flow' => $this->user_flow,
			'referral_source' => $this->referral_source,
		);

		$this->data_format = array(
			'%d', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s', '%s','%s'
		);
	}

	/**
     * Saves the user event data to the database table 
     * 
	 * @param int $id
	 * @param array $args
	 * 
	 * @return int New Form ID, in case of a new form being created
	 */
    public static function _save($id = 0, $args)
    {
        if($id === 0){
			$user_event = new Model_User_Events;
			if(empty(get_option('timezone_string'))) date_default_timezone_set(get_option('gmt_offset'));
        	else date_default_timezone_set(get_option('timezone_string'));
        	$current_time = current_time('timestamp');
			$user_event->set_created_at($current_time);
			$ip = $_SERVER['REMOTE_ADDR'];
			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
			$user_event->set_location_data(self::maybe_serialize($details));
			$userAgent = $_SERVER['HTTP_USER_AGENT']; // change this to the useragent you want to parse
		}else{
			$user_event = new Model_User_Events($id);
		}
		foreach ($args as $key => $value) {
			$fn = 'set_'.$key;
			if ( method_exists( __CLASS__, $fn ) ) {
				$user_event->$fn( $value );
			}
		}
		$user_event->user_flow = serialize($_SESSION["user_flow"]);
		$user_event->referral_source = $_SESSION["referral_source"];
		$device_data = new Parser(getallheaders());
		$user_event->user_agent_data = serialize(
			array(
				"browser" => $device_data->browser->toString(),
				"device" => $device_data->device->toString(),
				"os" => $device_data->os->toString()
			)
		);
		update_user_meta( $user_event->created_by, "last_seen", current_time("timestamp"));
		$user_event->save();
		if($user_event->action === "form_submission"){
			$form_id = $user_event->get_py_user_form_id();
			$form = new Model_Forms($form_id);
			if($form->is_zapier_enabled) do_action("send_form_notification", $user_event);
		}
		
		return $user_event;
	}

	/**
     * Destroys the user event entry from the database table
     *  
	 * @param mixed $id
	 * 
	 * @return void
	 */
	public static function destroy($id)
    {
        $user_event = new Model_User_Events($id);
        if($user_event){
            $user_event->delete();
			$created_by = $user_event->created_by;
			$created_by = get_user_by("id", $created_by);		
			$user_roles = get_userdata( $created_by->ID )->roles;
            $user_role = $user_roles[0];
			if($user_role === "subscriber"){
				remove_user_from_blog($created_by->ID, get_current_blog_id());
			}
		}
	}

	/**
	 * Returns the user events created by a specific user for a specific action
	 * 
	 * @param int $created_by
	 * @param string $action
	 * 
	 * @return array
	 */
	public static function get_user_action_events_by_created_by($created_by, $action){
        global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;

		$result = $wpdb->prepare(
			"SELECT * from {$user_events_table} WHERE `created_by` = %d AND action = %s ORDER BY `created_at` DESC", $created_by, $action
		);
        return $wpdb->get_results($result);
	}

	/**
	 * Returns all user events
	 * 
	 * @return array
	 */
	public static function get_all(){
        global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;

		$result = $wpdb->prepare(
			"SELECT * from {$user_events_table} ORDER BY `created_at` DESC", $created_by
		);
        return $wpdb->get_results($result);
	}

	/**
	 * Returns the user events for a specific post and action
	 * 
	 * @param int $wp_post_id
	 * @param string $action
	 * @param bool $include_nudgebot_results
	 * 
	 * @return array
	 */
	public static function get_user_events_by_post_id($wp_post_id, $action = false,  $include_nudgebot_results = true){
        global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;
		$cache_key = $wp_post_id . $action . $include_nudgebot_results;
        $result = wp_cache_get( $cache_key, "get_user_events_by_post_id" );
		if(false === $result){
			if($action){
				if($include_nudgebot_results){
					$result = $wpdb->prepare(
						"SELECT * from {$user_events_table} WHERE wp_post_id = %d AND action = %s", $wp_post_id, $action
					);
				}else{
					$result = $wpdb->prepare(
						"SELECT * from {$user_events_table} WHERE wp_post_id = %d AND py_nudges_id = 0 AND action = %s", $wp_post_id, $action
					);
				}
			}else{
				if($include_nudgebot_results){
					$result = $wpdb->prepare(
						"SELECT * from {$user_events_table} WHERE wp_post_id = %d", $wp_post_id
					);
				}else{
					$result = $wpdb->prepare(
						"SELECT * from {$user_events_table} WHERE wp_post_id = %d AND py_nudges_id = 0", $wp_post_id
					);
				}
			}
			$result = $wpdb->get_results($result);
			wp_cache_set($cache_key, $result, "get_user_events_by_post_id");
		}
        return $result;
	}

	/**
	 * Returns the user events for a specific user for an action on a specific post
	 * 
	 * @param int $created_by
	 * @param int $wp_post_id
	 * @param string $action
	 * 
	 * @return array
	 */
	public static function get_action_event($created_by, $wp_post_id, $action){
        global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;
		$cache_key = $created_by . $wp_post_id . $action;
        $result = wp_cache_get( $cache_key, "get_action_event" );
		if(false === $result){
			$result = $wpdb->prepare(
				"SELECT * from {$user_events_table} WHERE `created_by` = %d AND wp_post_id = %d AND action = %s", $created_by, $wp_post_id, $action
			);
			$result = $wpdb->get_results($result);
			wp_cache_set($cache_key, $result, "get_action_event");
		}
        return $result;
	}
	/**
	 * Returns the browser data of the user
	 * 
	 * @return array
	 */
	public static function get_browser_data() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
          $platform = 'linux';
        }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
          $platform = 'mac';
        }elseif (preg_match('/windows|win32/i', $u_agent)) {
          $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
          $bname = 'Internet Explorer';
          $ub = "MSIE";
        }elseif(preg_match('/Firefox/i',$u_agent)){
          $bname = 'Mozilla Firefox';
          $ub = "Firefox";
        }elseif(preg_match('/OPR/i',$u_agent)){
          $bname = 'Opera';
          $ub = "Opera";
        }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
          $bname = 'Google Chrome';
          $ub = "Chrome";
        }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
          $bname = 'Apple Safari';
          $ub = "Safari";
        }elseif(preg_match('/Netscape/i',$u_agent)){
          $bname = 'Netscape';
          $ub = "Netscape";
        }elseif(preg_match('/Edge/i',$u_agent)){
          $bname = 'Edge';
          $ub = "Edge";
        }elseif(preg_match('/Trident/i',$u_agent)){
          $bname = 'Internet Explorer';
          $ub = "MSIE";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
      ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
          // we have no matching number just continue
        }
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
          //we will have two since we are not using 'other' argument yet
          //see if version is before or after the name
          if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
              $version= $matches['version'][0];
          }else {
              $version= $matches['version'][1];
          }
        }else {
          $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
          'userAgent' => $u_agent,
          'name'      => $bname,
          'version'   => $version,
          'platform'  => $platform,
          'pattern'    => $pattern
        );
    }

	/**
	 * Adds an entry for an action by a user on a post
	 * 
	 * @param int $wp_post_id
	 * @param int $created_by
	 * @param string $action
	 * 
	 * @return void
	 */
	public static function add_event($wp_post_id, $created_by, $action){
		$user_events = self::get_action_event($created_by, $wp_post_id, $action);
		if(empty($user_events)){
			$ip = $_SERVER['REMOTE_ADDR'];
        	$location_data = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"), true);
			$browser = self::get_browser_data();
			$args = array(
				'action' => $action,
				'created_by' => $created_by,
				'wp_post_id' => $wp_post_id,
				'browser' => $browser['name'].' '.$browser['version'],
				'device' => $browser['platform'],
				'os' => $browser['platform'],
				'ip_address' => $location_data['ip'],
				'lat' => explode(",", $location_data['loc'])[0],
            	'longitude' => explode(",", $location_data['loc'])[1],
			);
			// $user_event = self::_save(0, $args);
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
        	$location_data = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"), true);
			$browser = self::get_browser_data();
			$args = array(
				'browser' => $browser['name'].' '.$browser['version'],
				'device' => $browser['platform'],
				'os' => $browser['platform'],
				'ip_address' => $location_data['ip'],
				'lat' => explode(",", $location_data['loc'])[0],
            	'longitude' => explode(",", $location_data['loc'])[1],
			);
			// $user_event = self::_save($user_events[0]->id, $args);
		}
	}

	/**
	 * Removes an entry for an action by a user on a post
	 * 
	 * @param int $wp_post_id
	 * @param int $created_by
	 * @param string $action
	 * 
	 * @return void
	 */
	public static function remove_event($wp_post_id, $created_by, $action){
		$user_events = self::get_action_event($created_by, $wp_post_id, $action);
		if(!empty($user_events)){
			foreach ($user_events as $event) {
				self::destroy($event->id);
			}
		}
	}

	/**
	 * Toggles an entry for an action by a user on a post
	 * 
	 * @param int $wp_post_id
	 * @param int $created_by
	 * @param string $action
	 * 
	 * @return void
	 */
	public static function toggle_event($wp_post_id, $created_by, $event){
		$user_events = self::get_action_event($created_by, $wp_post_id, $event);
		if(empty($user_events)){
			self::add_event($wp_post_id, $created_by, $event);
		}else{
			foreach ($user_events as $event) {
				self::destroy($event->id);
			}
		}
	}

	/**
	 * Returns the user events of a specific form
	 * 
	 * @param int $form_id
	 * 
	 * @return array
	 */
	public static function get_user_events_by_form_id($form_id){
		global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;

		$result = $wpdb->prepare(
			"SELECT * from {$user_events_table} WHERE py_user_form_id = %d AND action = %s ORDER BY `created_at` DESC", $form_id, "form_submission"
		);
        return $wpdb->get_results($result);
	}

	/**
	 * Returns the user events of a specific email template
	 * 
	 * @param int $email_template_id
	 * 
	 * @return array
	 */
	public static function get_user_events_by_email_template_id($email_template_id){
		global $wpdb;
		$user_events_table = DB_User_Events::$user_events_table;

		$result = $wpdb->prepare(
			"SELECT * from {$user_events_table} WHERE py_email_template_id = %d AND action = %s ORDER BY `created_at` DESC", $email_template_id, "form_submission"
		);
        return $wpdb->get_results($result);
	}

	/**
	 * Adds a download user event for the current user for the given post
	 * 
	 * @param int $post_id
	 * 
	 * @return int
	 */
	public static function register_download_action($post_id){
        if(!is_user_logged_in())
            return false;
        $user_id = get_current_user_id();
        self::toggle_event($post_id, $user_id, "downloaded");
        return $post_id;
	}
	
	/**
	 * Toggles the bookmark event for a given post and user
	 * 
	 * @param int $post_id
	 * @param bool $user_id
	 * 
	 * @return int
	 */
	public static function bookmark_unbookmark_this_post($post_id, $user_id = false){
		if($user_id === false) $user_id = get_current_user_id();
		$bookmarked_posts = get_user_option('bookmarked_posts', $user_id);
		if(!is_array($bookmarked_posts)) $bookmarked_posts = array();
		if(!in_array($post_id, $bookmarked_posts)){
			array_push($bookmarked_posts, $post_id);
			self::increment_bookmark_count($post_id);
		}elseif(($key = array_search($post_id, $bookmarked_posts)) !== false){
			unset($bookmarked_posts[$key]);
		}
		update_user_option($user_id, 'bookmarked_posts', $bookmarked_posts);
		self::toggle_event($post_id, $user_id, "bookmarked");
		Model_Login_Wall::remove_deleted_posts('bookmarked_posts');
		return $post_id;
	}

	/**
	 * Increments the bookmark count of a given post
	 * 
	 * @param int $post_id
	 * 
	 * @return void
	 */
	public static function increment_bookmark_count($post_id){
		$bookmark_count = get_post_meta($post_id, 'no_of_times_bookmarked', true);
		$bookmark_count = (int)$bookmark_count + 1;
		update_post_meta($post_id, 'no_of_times_bookmarked', $bookmark_count);
	}
}