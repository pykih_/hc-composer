<?php
namespace Humane_Sites;

use Exception;
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * Abstract model class
 * 
 * @package Humane Sites
 * @subpackage Model
 */
abstract class Model {
	/**
	 * Global $wpdb object
	 * @var object
	 */
	protected $wpdb;

	/**
	 * ID of the object
	 * @var mixed
	 */
	public $id;

	/**
	 * $this->id placeholder for db functions
	 * @var string
	 */
	private $id_format;

	/**
	 * Construct the class
	 */
	public function __construct( $id = 0, $dont_load = false) {
		if ( empty( $this->table_name ) ) {
			throw new Exception( 'No table name set', 404);
		}

		global $wpdb;
		$this->wpdb = $wpdb;
		/* 	Removed because it was conflicting with scoping prefix "py"
			if ( strpos( $this->table_name, $this->wpdb->prefix ) === 0 ) {
				$this->table_name = substr_replace( $this->table_name, '', 0, strlen( $this->wpdb->prefix ) );
			}
		*/
		$this->id_format = '%d';
		if ( ! empty( $id ) ) {
			$this->id = $id;
			$this->id_format = is_numeric( $this->id ) ? '%d' : '%s';
			if(!$dont_load)
				$this->load();
		}
	}

	/**
	 * Prepare data to be used in $wpdb methods
	 * @return void
	 */
	abstract public function prepare_data();

	/**
	 * Get table name including wpdb prefix
	 * @return string Table name
	 */
	public function get_table_name() {
		if ( strpos( $this->table_name, $this->wpdb->prefix ) === 0 ) {
			return $this->table_name;
		} else {
			return $this->wpdb->prefix . $this->table_name;
		}
	}

	/**
	 * Load data from database as object property
	 * @return void
	 */
	public function load() {
		$db_object = $this->read();

		if ( $db_object && ! empty( $db_object ) ) {
			foreach ( $db_object as $property => $value ) {
				$method = "set_{$property}";
				$is_json = false;
				if(str_split($value)[0] === "[" || str_split($value)[0] === "{"){
					json_decode($value);
					$is_json = json_last_error() === JSON_ERROR_NONE;
				}
				if($is_json === false) $value = stripslashes($value);
				if ( method_exists( $this, $method ) ) {
					$this->$method( maybe_unserialize($value));
				} else {
					$this->$property = maybe_unserialize($value);
				}
			}
		}
	}

	/**
	 * Create/insert object data into databasae
	 * @return mixed $wpdb->insert() return value
	 */
	public function _create() {
		$this->prepare_data();

		return $this->wpdb->insert(
			"{$this->wpdb->prefix}{$this->table_name}",
			$this->data,
			$this->data_format
		);
	}

	/**
	 * Get object data from database
	 * @return mixed $wpdb->get_row() return value
	 */
	public function read() {
		$object = wp_cache_get( $this->id, "{$this->table_name}" );

		if ( false === $object ) {
			$object = $this->wpdb->get_row(
				$this->wpdb->prepare(
					"SELECT * FROM `{$this->wpdb->prefix}{$this->table_name}` WHERE `id` = {$this->id_format}",
					$this->id
				)
			);
			if ( $object ) {
				wp_cache_set( $this->id, $object, "{$this->table_name}" );
			}
		}

		return $object;
	}

	/**
	 * Update object data in database
	 * @return mixed $wpdb->update() return value
	 */
	public function _update() {
		global $wpdb;
		$this->prepare_data();
		return $wpdb->update(
			"{$wpdb->prefix}{$this->table_name}",
			$this->data,
			array( 'id' => $this->id ),
			$this->data_format,
			array( $this->id_format )
		);
	}

	/**
	 * Delete object data in database
	 * @return mixed $wpdb->delete() return value
	 */
	public function delete() {
		return $this->wpdb->delete(
			"{$this->wpdb->prefix}{$this->table_name}",
			array( 'id' => $this->id ),
			array( $this->id_format )
		);
	}

	/**
	 * Save object data to database
	 * @return void
	 */
	public function save() {
		if ( $this->_update() === 0 ) {
			if ( ! $this->is_existent() ) {
				$this->_create();
			}
		}
		if ( isset( $this->related_objects ) && ! empty( $this->related_objects ) ) {
			foreach ( $this->related_objects as $object ) {
				$object->save();
			}
		}
    }

    public function read_all(){
        $objects = $this->wpdb->get_results(
            $this->wpdb->prepare(
                "SELECT * FROM `{$this->wpdb->prefix}{$this->table_name}`"
            )
        );
		return $objects;
    }

	/**
	 * Read data from database by key and value
	 * @param  string $field Field key
	 * @param  mixed  $value Field value
	 * @return object        Array of data object form database
	 */
	public function read_by( $field, $value ) {
		// $object = wp_cache_get( "read_by_{$field}_{$value}", "{$this->table_name}" );

		// if ( false === $object ) {
			$placeholder = is_numeric( $value ) ? '%d' : '%s';

			$object = $this->wpdb->get_results(
				$this->wpdb->prepare(
					"SELECT * FROM `{$this->wpdb->prefix}{$this->table_name}` WHERE `{$field}` = {$placeholder}",
					$value
				)
			);

			// wp_cache_set( "read_by_{$field}_{$value}", $object, "{$this->table_name}" );
		// }

		return $object;
	}

	/**
	 * Delete data from database by key and value
	 * @param  string $field Field key
	 * @param  mixed  $value Field value
	 * @return mixed         $wpdb->query return values
	 */
	public static function delete_by( $field, $value, $table_name ) {
		$placeholder = is_numeric( $value ) ? '%d' : '%s';
		global $wpdb;
		return $wpdb->query(
			$wpdb->prepare(
				"DELETE FROM `{$wpdb->prefix}{$table_name}` WHERE `{$field}` = {$placeholder}",
				$value
			)
		);
	}

	/**
	 * Check if the object exists in database
	 * @return boolean True if exists|false otherwise
	 */
	public function is_existent() {
		$count = $this->wpdb->get_var(
			$this->wpdb->prepare( "SELECT COUNT(*) FROM `{$this->wpdb->prefix}{$this->table_name}` WHERE `id` = {$this->id_format}",
				$this->id
			)
		);
		return $count > 0 ? true : false;
	}
	/**
	 * Check if the object exists in database
	 * @return boolean True if exists|false otherwise
	 */
	public function count() {
		$count = $this->wpdb->get_var(
			$this->wpdb->prepare( "SELECT COUNT(*) FROM `{$this->wpdb->prefix}{$this->table_name}`")
		);
		return $count;
	}
	public function find_all($order_by = false, $order = "ASC", $columns = "*"){
		global $wpdb;
		if($order_by){
			$result = $wpdb->prepare(
				"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name} ORDER BY {$order_by} {$order}"
			);
		}else{
			$result = $wpdb->prepare(
				"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name}"
			);
		}
        return $wpdb->get_results($result);
	}

	public function find_by_id($id){
        global $wpdb;

		$result = $wpdb->prepare(
			"SELECT * from {$this->wpdb->prefix}{$this->table_name} WHERE id = %d", $id
		);
        return $wpdb->get_results($result)[0];
	}
	public function count_by_query($args){
        global $wpdb;

		$where = "";
		$count = 0;
		if (isset($args['where'])) {
			$where = "";
			foreach($args['where'] as $column => $condition) {
				$val = $condition['value'];
				if(isset($condition['special_key'])) $column = $condition['special_key'];
				
				if($condition['type'] === 'string' || $condition['type'] === 'date') {
					$value = "'{$val}'";
				} else {
					$value = "{$val}";
				}
				$operator = "";
				if (isset($condition['operator']) && $count) {
					$operator = $condition['operator'] ?? "AND";
				}
				$count++;
				$comparator = "=";
				if (isset($condition['comparator'])) {
					$comparator = $condition['comparator'] ?? "=";
				}
				$where .= " {$operator} {$column} {$comparator} {$value}";
			}
		}
        $limit = isset($args['limit']) ? $args['limit'] : 10000000;
        $offset = isset($args['offset']) ? $args['offset'] : 0;
        $order = isset($args['order']) ? $args['order'] : 'DESC';
		$order_by = isset($args['order_by']) ? (string)$args['order_by'] : 'id';
		$columns = $args["columns"] ?? "*";
		if(isset($args['where'])){
			if(empty($args["search"])){
				$result = $wpdb->prepare(
					"SELECT COUNT(*) from {$this->wpdb->prefix}{$this->table_name} WHERE {$where} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $limit, $offset
				);
			}else{
				$search = $wpdb->esc_like($args["search"]["query"]); 
				$searchColumn = $args["search"]["column"];
				$result = $wpdb->prepare(
					"SELECT COUNT(*) from {$this->wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' AND {$where} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $search, $limit, $offset
				);
			}
		}else{
			if(empty($args["search"])){
				$result = $wpdb->prepare(
					"SELECT COUNT(*) from {$this->wpdb->prefix}{$this->table_name} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $limit, $offset
				);
			}else{
				$search = $wpdb->esc_like($args["search"]["query"]);
				$searchColumn = $args["search"]["column"];
				$result = $wpdb->prepare(
					"SELECT COUNT(*) from {$this->wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $search, $limit, $offset
				);
			}
		}
        return $wpdb->get_var($result);
	}
	public function find_by_query($args){
        global $wpdb;

		$where = "";
		$count = 0;
		if (isset($args['where'])) {
			$where = "";
			foreach($args['where'] as $column => $condition) {
				$val = $condition['value'];
				if(isset($condition['special_key'])) $column = $condition['special_key'];
				
				if($condition['type'] === 'string' || $condition['type'] === 'date') {
					$value = "'{$val}'";
				} else {
					$value = "{$val}";
				}
				$operator = "";
				if (isset($condition['operator']) && $count) {
					$operator = $condition['operator'] ?? "AND";
				}
				$count++;
				$comparator = "=";
				if (isset($condition['comparator'])) {
					$comparator = $condition['comparator'] ?? "=";
				}
				$where .= " {$operator} {$column} {$comparator} {$value}";
			}
		}
        $limit = isset($args['limit']) ? $args['limit'] : 10000000;
        $offset = isset($args['offset']) ? $args['offset'] : 0;
        $order = isset($args['order']) ? $args['order'] : 'DESC';
		$order_by = isset($args['order_by']) ? (string)$args['order_by'] : 'id';
		$columns = $args["columns"] ?? "*";
		if(isset($args['where'])){
			if(empty($args["search"])){
				$result = $wpdb->prepare(
					"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name} WHERE {$where} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $limit, $offset
				);
			}else{
				$search = $wpdb->esc_like($args["search"]["query"]); 
				$searchColumn = $args["search"]["column"];
				$result = $wpdb->prepare(
					"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' AND {$where} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $search, $limit, $offset
				);
			}
		}else{
			if(empty($args["search"])){
				$result = $wpdb->prepare(
					"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name} ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $limit, $offset
				);
			}else{
				$search = $wpdb->esc_like($args["search"]["query"]);
				$searchColumn = $args["search"]["column"];
				$result = $wpdb->prepare(
					"SELECT {$columns} from {$this->wpdb->prefix}{$this->table_name} WHERE {$searchColumn} LIKE '%%%s%%' ORDER BY {$order_by} {$order} LIMIT %d OFFSET %d", $search, $limit, $offset
				);
			}
		}
        return $wpdb->get_results($result);
	}
	public function initialize_empty_references($all_objects){
		global $wpdb;
		$ref_tables = array(
			"_py_10000fts",
			"_py_email_templates",
			"_py_forms",
			"_py_nudges",
			"_py_playlists"
		);
		if(!in_array($this->table_name, $ref_tables)) return;
		foreach($all_objects as $object){
			$id = $object->id;
			$table = $wpdb->prefix . $this->table_name;
			$ref = new Model_Backlinks(array( 'object_table' => $table, 'object_id' => $id));
			$referenced_in = maybe_unserialize($ref->get_referenced_in());
			if(!isset($ref->id) || $ref->id === 0){
				$referenced_in = array();
				$ref->save();
			}
		}
	}
}