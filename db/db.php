<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * Database class
 * 
 * @package Humane Sites
 * @subpackage Database
 */
class DB {
	/**
	 * Datasets table name
	 * @var string
	 */
	public static $dataset_table;

	/**
	 * Tenthousand Feet table name
	 * @var string
	 */
	public static $tenthousand_feet_table;

	/**
	 * User forms table name
	 * @var string
	 */
	public static $user_forms_table;

	/**
	 * Playlists table name
	 * @var string
	 */
	public static $playlists_table;
	
	public static $backlinks_table;

	public static $slices_table;

	public static $email_templates_table;
	
	public static $nudges_table;
	/**
	 * Init class mehtods
	 * @return void
	 */
	public static function init() {
		global $wpdb;
		self::$user_forms_table 		= "{$wpdb->prefix}_py_forms";
		self::$dataset_table 			= "{$wpdb->prefix}_py_datasets";
		self::$tenthousand_feet_table 	= "{$wpdb->prefix}_py_10000fts";
		self::$playlists_table     		= "{$wpdb->prefix}_py_playlists";
		self::$backlinks_table     	= "{$wpdb->prefix}_py_references";
		self::$slices_table     		= "{$wpdb->prefix}_py_slices";
		self::$email_templates_table    = "{$wpdb->prefix}_py_email_templates";
		self::$nudges_table 			= "{$wpdb->prefix}_py_nudges";

	}


	/**
	 * Create or update database
	 * @return void
	 */
	public static function create_custom_tables($schema) {

        global $wpdb;
		$sql = "";
		foreach ( $schema as $table => $fields ) {
			$sql .= "CREATE TABLE {$wpdb->prefix}{$table} (";

			foreach ( $fields as $key => $args ) {
				if ( $key != 'indexes' ) {
					$attr = ! empty( $args['attr'] ) ? " {$args['attr']}" : "";
					$sql .= "\n{$key} {$args['type']}{$attr},";
				} else {
					$current_array = 0;
					$count         = count( $args );
					foreach ( $args as $field => $type ) {
						$field_key = str_replace( ',', '', $field );
						$current_array++;

						if ( 'primary key' == strtolower( $type ) ) {
							$sql .= "\n{$type}  ({$field})";
						} else {
							$sql .= "\n{$type} {$field_key} ({$field})";
						}

						if ( $current_array < $count ) {
							$sql .= ",";
						}
					}
				}
			}

			$sql .= "\n) {$wpdb->get_charset_collate()};";
		}

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

	/**
	 * Get table name according to post type
	 * @param string $post_type WP post_type
	 * @return string Table name
	 */
	public static function post_type_to_table_mapper( $post_type ) {
		global $wpdb;
        $table = $wpdb->prefix . 'posts';
		if($post_type === "playlist" || $post_type === "category" || $post_type === "post_tag" || $post_type === "writing_format" || $post_type === "author" || $post_type === "humane_feed") $table = self::$playlists_table;
		if($post_type === "10000ft" || $post_type === "10000fts") $table = self::$tenthousand_feet_table;
		if($post_type === "form") $table = self::$user_forms_table;
		if($post_type === "email_template") $table =  self::$email_templates_table;
		if($post_type === "nudge") $table = self::$nudges_table;
		if($post_type === "dataset") $table = self::$dataset_table;

		return $table;
	}
	/**
	 * Get table name according to post type
	 * @param string $post_type WP post_type
	 * @return string Table name
	 */
	public static function block_type_to_table_mapper( $block_type ) {
		global $wpdb;
        $table = $wpdb->prefix . 'posts';
		if($block_type === "playlist_id") $table = self::$playlists_table;
		if($block_type === "tenthousand_id") $table = self::$tenthousand_feet_table;
		if($block_type === "contact_id") $table = self::$user_forms_table;
		if($block_type === "people_list") $table = null;
		return $table;
	}
}

DB::init();