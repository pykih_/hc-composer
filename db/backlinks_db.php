<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * DB Schema and table creation is stored
 * in this class
 * 
 * @package Humane Sites
 * @subpackage Backlinks
 */
class Database_Backlinks {
	
	/**
	 * @var string Stores the name of the custom table
	 */
	public static $backlinks_table;

	/**
     * Attaches the hooks for DB
     * 
     * @return void
     */
	public static function init() {
		global $wpdb;

		self::$backlinks_table = "{$wpdb->prefix}_py_references";
		$table_name = "{$wpdb->prefix}_py_references";
		if ( !in_array( $table_name, $wpdb->get_col( $wpdb->prepare( 'SHOW TABLES LIKE %s', $table_name ), 0 ), true ) ) {
			add_action( 'admin_init', function(){
				DB::create_custom_tables(self::get_schema());
			} );
		}		
	}

	/**
	 * Stores and returns the schema for the table.
	 * 
	 * @return array Schema of the database table
	 */
	public static function get_schema() {
		$schema = array(
			'_py_references' => array(
				'id' => array(
					'title' => "ID",
					'type' => "INT",
					'attr' => "NOT NULL AUTO_INCREMENT"
				),
				'object_table' => array(
					'title' => 'Object Table',
					'type' => "VARCHAR(100)",
					'attr' => "NOT NULL"
				),
				'object_id' => array(
					'title' => 'Object ID',
					'type' => "INT",
					'attr' => "NOT NULL"
				),
				'count' => array(
					'title' => 'Count',
					'type' => "INT",
					'attr' => "NOT NULL"
				),
				'referenced_in' => array(
					'title' => 'Referenced In',
					'type' => "TEXT",
					'attr' => ""
				),
				'indexes' => array(
					'id' => "PRIMARY KEY",
					'object_table,object_id' => "KEY",
				),
			)
		);
		return $schema;
	}
}

Database_Backlinks::init();