<?php
namespace Humane_Sites;
use Humane_Sites\DB;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}


/**
 * DB Schema and table creation is stored
 * in this class
 * 
 * @package Humane Sites
 * @subpackage User Events
 */
class DB_User_Events {

	/**
	 * @var string Stores the name of the custom table
	 */
	public static $user_events_table;

	/**
     * Attaches the hooks for DB
     * 
     * @return void
     */
	public static function init() {
		global $wpdb;
		self::$user_events_table = "{$wpdb->prefix}_py_user_events";
		$table_name = "{$wpdb->prefix}_py_user_events";
		if ( !in_array( $table_name, $wpdb->get_col( $wpdb->prepare( 'SHOW TABLES LIKE %s', $table_name ), 0 ), true ) ) {
			add_action( 'admin_init', function(){
				DB::create_custom_tables(self::get_schema());
			} );
		}	
	}

	/**
	 * Stores and returns the schema for the table.
	 * 
	 * @return array Schema of the database table
	 */
	public static function get_schema() {
		$schema = array(
			'_py_user_events' => array(
				'id' => array(
					'title' => "ID",
					'type' => "INT",
					'attr' => "NOT NULL AUTO_INCREMENT"
				),
				'created_by' => array(
					'title' => "Created By",
					'type' => "INT",
					'attr' => ""
				),
				'object_table' => array(
					'title' => "Object table",
					'type' => "TEXT"
				),
				'wp_post_id' => array(
					'title' => "WP Post ID",
					'type' => "INT",
					'attr' => ""
				),
				'py_nudges_id' => array(
					'title' => "Nudge ID",
					'type' => "INT",
					'attr' => ""
				),
				'py_user_form_id' => array(
					'title' => "User Form ID",
					'type' => "INT",
					'attr' => ""
				),
				'py_email_template_id' => array(
					'title' => "Email Template ID",
					'type' => "INT",
					'attr' => ""
				),
				'email_copy' => array(
					'title' => "Email Copy",
					'type' => "LONGTEXT",
					'attr' => ""
				),
				'details_json' => array(
					'title' => "Form Details Json",
					'type' => "LONGTEXT",
					'attr' => ""
				),
				'ip_address' => array(
					'title' => "IP Address",
					'type' => "TEXT",
					'attr' => ""
				),
				'browser' => array(
					'title' => "Browser",
					'type' => "TEXT",
					'attr' => ""
				),
				'device' => array(
					'title' => "Device",
					'type' => "TEXT",
					'attr' => ""
				),
				'os' => array(
					'title' => "OS",
					'type' => "TEXT",
					'attr' => ""
				),
				'location_data' => array(
					'title' => "Location Data",
					'type' => "TEXT",
					'attr' => ""
				),
				'user_agent_data' => array(
					'title' => "User Agent Data",
					'type' => "TEXT",
					'attr' => ""
				),
				'lat' => array(
					'title' => "Latitude",
					'type' => "TEXT",
					'attr' => ""
				),
				'longitude' => array(
					'title' => "Longitude",
					'type' => "TEXT",
					'attr' => ""
				),
				'is_client_facing_action' => array(
					'title' => "Is Client Facing Action",
					'type' => "BOOLEAN",
					'attr' => ""
				),
				'action' => array(
					'title' => "Action",
					'type' => "TEXT",
					'attr' => ""
				),
				'created_at' => array(
					'title' => "Created At",
					'type' => "TEXT",
					'attr' => ""
				),
				
				'user_flow' => array(
					'title' => "User Flow",
					'type' => "TEXT",
					'attr' => ""
				),
				'referral_source' => array(
					'title' => "Referral Source",
					'type' => "TEXT",
					'attr' => ""
				),
				'indexes' => array(
					'id' => 'PRIMARY KEY'
				)
			)
		);
		return $schema;
	}
}

DB_User_Events::init();