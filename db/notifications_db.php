<?php
namespace Humane_Sites;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * DB Schema and table creation is stored
 * in this class
 * 
 * @package Humane Sites
 * @subpackage Notifications
 */
class Database_Notifications {

	/**
	 * @var string Stores the name of the custom table
	 */
	public static $notifications_table;

	public static function init() {
		global $wpdb;
		self::$notifications_table = "{$wpdb->prefix}_py_notifications";
		$table_name = "{$wpdb->prefix}_py_notifications";
		if ( !in_array( $table_name, $wpdb->get_col( $wpdb->prepare( 'SHOW TABLES LIKE %s', $table_name ), 0 ), true ) ) {
			add_action( 'admin_init', function(){
				DB::create_custom_tables(self::get_schema());
			} );
		}
	}

	/**
	 * Stores and returns the schema for the table.
	 * 
	 * @return array Schema of the database table
	 */
	public static function get_schema() {
		$schema = array(
			'_py_notifications' => array(
				'id' => array(
					'title' => "ID",
					'type' => "INT",
					'attr' => "NOT NULL AUTO_INCREMENT"
				),
				'object_table' => array(
					'title' => "Object Table",
					'type' => "VARCHAR(100)",
					'attr' => "NOT NULL"
				),
				'object_id' => array(
					'title' => "Object ID",
					'type' => "INT",
					'attr' => "NOT NULL"
				),
				'py_user_events_action' => array(
					'title' => "Event",
					'type' => "VARCHAR(100)"
				),
				'wp_users_ids' => array(
					'title' => "User IDs",
					'type' => "LONGTEXT"
				),
				'extra_data' => array(
					'title' => "Extra Data",
					'type' => "LONGTEXT"
				),
				'created_by' => array(
					'title' => "Created By",
					'type' => "TEXT"
				),
				'created_at' => array(
					'title' => "Created At",
					'type' => "INT"
				),
				'updated_by' => array(
					'title' => "Updated By",
					'type' => "TEXT"
				),
				'updated_at' => array(
					'title' => "Updated At",
					'type' => "INT"
				),
				'indexes' => array(
					'id' => 'PRIMARY KEY',
					'object_table,object_id,py_user_events_action' => "KEY",
				)
			)
		);
		return $schema;
	}
}

Database_Notifications::init();