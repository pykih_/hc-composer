<?php
    namespace Humane_Sites;
    $args = array(
        'role__in'    => array( 'administrator', 'editor', 'author' ),
        'orderby' => 'user_nicename',
        'order'   => 'ASC'
    );
    $users = get_users( $args );
    $user_list =  maybe_unserialize($notification->wp_users_ids);
    if(!is_array($user_list)) $user_list = array();
    $current_user_id = get_current_user_id();
    $user_ids_count = count($user_list);
?>
<div class="subscribe-container hc-brand-reading hc-pb-40">
    <div class="add-subscribers-container hc-mb-8">
        <?php if(current_user_can( 'manage_options' )): ?>
            <div class="subscribed-user-list-container">
                <?php if($user_ids_count): ?>
                    <div class="subscribed-user-list">
                        <?php foreach($user_list as $user_id): ?>
                            <?php
                                $user = get_user_by("id", (int) $user_id);
                                $user_image = get_avatar_url( $user->ID );

                                $user_name = Model_Users::to_s( $user->ID );
                                $user_object = array(
                                    "src" => $user_image,
                                    "fullname" => $user_name
                                );
                            ?>
                            <?php echo Controller_Image::displayPic($user_object); ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <div class="manage-export-form">
                <div class="add-subscriber-modal-opener hc-brand-reading hc-btn-structure hc-humane-btn-secondary">
                    <?php echo Controller_Icons::get_svg_icons("bell", 24, 'white'); ?>
                    <div class="hc-supernormal-s">Keep in the loop</div>
                </div>
                <div class="add-subscriber-modal hc-bootstrap-modal hc-display-none">
                    <div class="hc-bootstrap-modal-container add-subscriber-modal-content" role="document">
                        <div class="hc-bootstrap-modal-content hc-col-8">
                            <div class="hc-fx">
                                <h5 class="hc-brand-header-s hc-my-8 hc-mr-40">Who should be notified when this event occurs?</h5>
                                <button type="button" class="hc-bootstrap-modal-close add-subscriber-modal-close">
                                    <?php echo Controller_Icons::get_svg_icons("close", 10, 'black', 'black'); ?>
                                </button>
                            </div>
                            <div class="hc-bootstrap-modal-body">
                                <div class="hc-fx">
                                    <div class="add-subsriber-select-everyone hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main hc-tab-first hc-cursor-pointer hc-humane-text-primary-main hc-text-center">Select everyone</div>
                                    <div class="add-subsriber-select-no-one hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main hc-tab-last hc-cursor-pointer hc-humane-text-primary-main hc-text-center">Select no one</div>
                                </div>
                                <div class="add-subscriber-users hc-p-20">
                                    <?php foreach($users as $user): ?>
                                        <?php
                                            $user_image = get_avatar_url( $user->ID );

                                            $user_name = Model_Users::to_s( $user->ID );
                                            $user_object = array(
                                                "src" => $user_image,
                                                "fullname" => $user_name
                                            );
                                            $checked = false;
                                            if(in_array($user->ID, $user_list)) $checked = true;
                                        ?>
                                        <div class="add-subscriber-user">
                                            <input <?php echo $checked ? "checked" : "" ?> data-val="<?php echo $user->ID; ?>" data-id="<?php echo $user_name ?>" type="checkbox" name="subscribers">
                                            <?php echo Controller_Image::displayPic($user_object); ?>
                                            <div class="subscriber-name"><?php echo $user_name; ?></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div data-row-object="<?php echo $object;?>" data-row-id="<?php echo $row_id; ?>" data-user-id="<?php echo $current_user_id; ?>" data-row-action="<?php echo $action;?>" class="save-subscribers hc-btn-structure hc-humane-btn-primary hc-no-wrap hc-col-1 hc-height-fit-content">Save</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="h-export-form hc-brand-reading hc-btn-structure hc-humane-btn-primary">
                <span class="hc-icon-container dashicons dashicons-database-export"></span>
                    <?php $action_url = filter_input( INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL ).'&form_report=entries'; ?>
                    <div class="hc-supernormal-s"><a class="hc-database-export" href="<?php echo esc_url( $action_url ) ?>">Export as CSV</a></div>
                </div>
            </div>
            </div>
           
   <?php endif; ?>
    </div>
    
</div>
