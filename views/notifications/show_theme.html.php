<?php
    namespace Humane_Sites;
?>
<?php if(is_user_logged_in()): ?>
    <div data-row-action="<?php echo $action; ?>" data-row-object="<?php echo $object; ?>" data-row-id="<?php echo $row_id; ?>" data-user-id="<?php echo $current_user_id; ?>" class="hc-cursor-pointer hc-notification-container hc-notification-button <?php echo $is_current_user_subscriber ? "hc-notification-unsubscribe" : "hc-notification-subscribe"; ?>">
        <div class="hc-notification-unsubscribe-button hc-border-rounded-8 hc-p-8 hc-bg-brightness-100 hc-text-primary-main hc-border-brightness-60 hc-fx hc-flex-align-center hc-supernormal-s">
            <div>Following</div>
        </div>
        <div class="hc-notification-subscribe-button hc-border-rounded-8 hc-p-8 hc-bg-primary-main hc-text-brightness-100 hc-fx hc-flex-align-center hc-supernormal-s">
            <div>Follow</div>
        </div>
    </div>
<?php else: ?>
    <?php
        $id = get_the_ID();
        $url = "/login?post=$id";
        if(!empty(get_option("login_page_for_follow"))){
            $url = get_permalink(get_option("login_page_for_follow"));
        }
    ?>
    <div class="hc-cursor-pointer hc-notification-container hc-notification-button">
        <a href="<?php echo $url; ?>" class="hc-notification-subscribe-button hc-border-rounded-8 hc-p-8 hc-bg-primary-main hc-text-brightness-100 hc-fx hc-flex-align-center hc-supernormal-s">
            Follow
        </a>
    </div>
<?php endif; ?>