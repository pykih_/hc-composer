<?php
    $sorts = $perspectives_json["sort"];
?>
<div class="main-sort-container" data-visibility-label="sort">
    <div class="sort-text" data-tab-label="SORTING">
        <div class="sort-title">Sort by</div>
        <div>
            <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z" fill="black"/>
            </svg>
        </div>
    </div>
</div>
<div class="sort-container sort-container-main ajax-disable" data-sort-visible="false">
    <div class="hc-fx hc-flex-between hc-show-540">
        <div class="hc-sort-title">Sort</div>
        <div class="hc-mobile-close">
            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="28" height="28" rx="14" fill="#1A1A1A"></rect>
                <path d="M21 8.41L19.59 7L14 12.59L8.41 7L7 8.41L12.59 14L7 19.59L8.41 21L14 15.41L19.59 21L21 19.59L15.41 14L21 8.41Z" fill="white"></path>
            </svg>
        </div>
    </div>
    <div id="perspectives-<?php echo $id; ?>-sort"></div>
    <?php if(!empty($sorts["description"])): ?>
        <div class="title-description-container"><?php echo $sorts["description"]; ?></div>
    <?php endif; ?>
</div>