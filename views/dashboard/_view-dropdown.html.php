<?php 
    use Humane_Sites\Controller_Icons;
?>
<div class="hc-width-fit-container hc-flex-shrink-1000 hc-flex-gap-20 hc-fx hc-mx-20-mobile hc-height-fit-content hc-flex-between">
    <div class="pyk_dropdown_views <?php echo count($perspectives_json["views"]) > 1 ? "" : "hc-single-view";?> pyk-dropdown-views hc-py-0">
        <div class="dropbtn" data-id="viewsDropdownContainer" data-tab-label="Select View">
            <div class="list-of-views">
                View: 
                <div class="view-name hc-bold"><?php echo array_values($perspectives_json["views"])[0]["scope"]; ?></div>
            </div>
            <?php if(count($perspectives_json["views"]) > 1): ?>
                <div>
                    <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z" fill="black"/>
                    </svg>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="view-dropdown-content ajax-disable" data-id="dropDown" style="display: none;">
        <div id="perspectives-<?php echo $id;?>-views"></div>
    </div>
    <div class="hc-show-540 hc-px-8 hc-open-mobile-filters hc-filter-dropdown perspectives-show hc-width-fit-content hc-flex-self-end">
        <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M4.66667 8H7.33333V6.66667H4.66667V8ZM0 0V1.33333H12V0H0ZM2 4.66667H10V3.33333H2V4.66667Z" fill="black"/>
        </svg>
        <span>Filters</span>
    </div>
    <?php if($perspectives_json["sort"] ?? false): ?>
        <div class="main-sort-container hc-open-mobile-sort hc-show-540" data-visibility-label="sort">
            <div class="sort-text" data-tab-label="SORTING">
                <div class="sort-title">Sort by</div>
                <div>
                    <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z" fill="black"/>
                    </svg>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>