<?php
    namespace Humane_Sites;

    $filtered_count = $perspectives_json["filters"]["filtered_count"];
    $page = $view["page"];
?>
<div class="table-inner">
    <div class="hc-table-row">     
        <?php foreach($view["rows"] as $key => $row): ?>
            <div class="hc-table-cell hc-table-header-cell" data-key="<?php echo $key; ?>">
                <div class="table-header-wrap">
                    <?php echo Controller_Icons::datatype($row["datatype"]);?>
                    <div><?php echo $row["label"]; ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php foreach($rows as $row): ?>
        <?php 
            $url = null;
            if($view["url"])
                $url = $view["url"]($row);
        ?>

        <div class="hc-table-row">
            <?php foreach($view["rows"] as $key => $meta): ?>
                <div class="hc-table-cell">
                    <?php if($url): ?>
                        <a href="<?php echo $url; ?>" class="hc-hyperlink hc-text-brightness-7">
                    <?php endif; ?>
                    <?php if(is_callable($meta["render"])): ?>
                        <?php echo $meta["render"]($row->$key, $row); ?>
                    <?php else: ?>
                        <?php echo $row->$key; ?>
                    <?php endif; ?>
                    <?php if($url): ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>

    <?php endforeach; ?>
</div>
<?php Controller_Pagination::addPagination($filtered_count, $page, $items_per_page);?>
