<?php
    $id = $perspectives_json["id"];
    $map_data = array_map(function($a){
        return get_the_title($a);
    }, $perspectives_json["all_data_ids"]);
    $post_data = array_map(function($a){
        return array(
            "post_title" => get_the_title($a),
            "post_url" => get_permalink($a),
            "post_image" => get_the_post_thumbnail($a),
            "post_category" => get_the_category($a)[0]->name
        );
    }, $perspectives_json["all_data_ids"]);
    $latitude = $view["latitude_key"];
    $longitude = $view["longitude_key"];
    $features = array_map(function($a) use($latitude, $longitude, $view){
        $properties = get_post_meta($a, '', true);
        $properties = array_map(function($a){
            return $a[0];
        }, $properties);
        $properties[$view["primary_key"]] = get_the_title($a);
        $feature = array(
            'type' => 'Feature',
            'geometry' => array(
                'type' => 'Point',
                'coordinates' => array(floatval(get_post_meta($a, $latitude, true)), floatval(get_post_meta($a, $longitude, true)))
            ),
            'properties' => $properties
        );
        return $feature;
    }, $perspectives_json["all_data_ids"]);
?>
<div class="hc-map-container">
    <div class="hc-map hc-row-16 hc-col-16" id="humane-map-<?php echo $id; ?>"></div>
    <script>
        (function(){
            mapboxgl.accessToken = map_object.token;
            let map = new mapboxgl.Map({
                container: 'humane-map-<?php echo $id; ?>',
                style: '<?php echo $view["style_url"]; ?>',
                zoom: 1
            });
            map.scrollZoom.disable();
            let rows = <?php echo json_encode($post_data, JSON_UNESCAPED_UNICODE); ?>;
            let features = <?php echo json_encode($features, JSON_UNESCAPED_UNICODE); ?>;
            features = features.map((a)=>{
                a["properties"]["<?php echo $view['primary_key']; ?>"] = $('<textarea />').html(a["properties"]["<?php echo $view['primary_key']; ?>"]).text();
                return a;
            });
            map.on('load', () => {
                map.loadImage('<?php echo $view["view_options"]["pin_image_url"];?>', (error, image) => {
                    map.addSource('humane_data_values', {
                        'type': 'geojson',
                        'data': {
                            'type': 'FeatureCollection',
                            'features': features
                        }
                    });
                    map.addImage('humane-pin', image);
                    map.addLayer({
                        'id': 'humane_data',
                        'type': 'symbol',
                        'source': 'humane_data_values', // reference the data source
                        'layout': {
                            'icon-image': 'humane-pin', // reference the image
                            'icon-size': 0.5,
                            'icon-allow-overlap': true
                        }
                    });
                    map.addLayer({
                        'id': 'humane_data_labels',
                        'type': 'symbol',
                        'source': 'humane_data_values',
                        'layout': {
                            'text-field': ['get', '<?php echo $view['primary_key']; ?>'],
                            'text-variable-anchor': ['top', 'bottom', 'left', 'right'],
                            'text-radial-offset': 0.5,
                            'text-justify': 'auto',
                            'text-size': 10,
                            'icon-image': ['get', 'icon']
                        }
                    });
                    map.addControl(new mapboxgl.NavigationControl(), 'top-right');
                    let filtered_values = <?php echo json_encode($map_data, JSON_UNESCAPED_UNICODE); ?>;
                    filtered_values = filtered_values.map((value)=>{
                        return $("<textarea/>").html(value).text();
                    });
                    map.setFilter('humane_data', [
                        'in',
                        ['get', '<?php echo $view["primary_key"]; ?>'],
                        ["literal", filtered_values]
                    ]);
                    map.setFilter('humane_data_labels', [
                        'in',
                        ['get', '<?php echo $view["primary_key"]; ?>'],
                        ["literal", filtered_values]
                    ]);
                    let template = `<?php echo htmlspecialchars($view["popup_template"]);?>`;
                    if(template){
                        const popup = new mapboxgl.Popup({
                            closeButton: true,
                            closeOnClick: true
                        });
                        let handler = (e) => {
                            popup.remove();
                            const coordinates = e.features[0].geometry.coordinates.slice();
                            let properties = e.features[0].properties;
                            let datapoint = {};
                            rows.forEach(row => {
                                if($("<textarea/>").html(row['post_title']).text() === properties['<?php echo $view["primary_key"]; ?>']){
                                    datapoint = row;
                                }
                            });
                            let markup = template.replace(/\%(.*?)\%/g, function(all) {
                                let key = all.replace(/%/g, "");
                                return properties[key] || "";
                            });
                            markup = markup.replace(/\$(.*?)\$/g, function(all) {
                                let key = all.replace(/\$/g, "");
                                return datapoint[key] || "";
                            });
                            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                            }
                            markup = $('<textarea />').html(markup).text();
                            popup.setLngLat(coordinates).setHTML(markup).addTo(map);
                        };
                        map.on('click', 'humane_data', handler);
                        map.on('touchend', 'humane_data', handler);
                        map.on('click', 'humane_data_labels', handler);
                        map.on('touchend', 'humane_data_labels', handler);
                        
                        // Change the cursor to a pointer when the mouse is over the places layer.
                        map.on('mouseenter', 'humane_data', () => {
                            map.getCanvas().style.cursor = 'pointer';
                        });
                        
                        // Change it back to a pointer when it leaves.
                        map.on('mouseleave', 'humane_data', () => {
                            map.getCanvas().style.cursor = '';
                        });
                    }
                });
            });
        })();
    </script>
</div>