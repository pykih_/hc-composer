<?php
    namespace Humane_Sites;

    $filtered_count = $perspectives_json["filters"]["filtered_count"];
    $page = $view["page"];
?>
<div class="automate-grid dashboard-grid1">
    <?php foreach($rows as $row): ?>
        <?php
            $mapped_values = array();
            foreach($view["mapped_values"] as $key => $value){
                $mapped_values[$key] = is_callable($value) ? trim($value($row)) : trim($row->{$value});
            }
            $current_id = $mapped_values["current_id"];
            $intrection_class = $mapped_values["intrection_class"];
            if(empty($intrection_class)){ ?>
                <a class="" href="<?php echo $mapped_values["url"]?>" <?php echo $mapped_values["new_tab"] ? "target='_blank'" : "" ?>>
           <?php }
        ?>
        <?php if(!empty($intrection_class)): ?>
            <div data-connect="<?php echo $current_id; ?>" class="<?php echo esc_attr( $intrection_class ); ?> hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">
        <?php else: ?>
            <div class="hc-card-automated-gallery hc-border-rounded-12 modal-card hc-bg-brightness-97">	
        <?php endif; ?>
            <div class="automate-grid-element hc-border-rounded-top-12 pykslice-image-container hc-row-4">
                <?php if(!empty($mapped_values["image"])):
                    if($mapped_values["current_post_type"] === "wp_block"){ ?>
                        <img class="automate-grid-element-img hc-border-rounded-top-12 hc-width-fit-container <?php echo esc_attr( ($mapped_values["is_from_datawrapper"] == 1 ) ?'hc-contain hc-datawrapper':'hc-cover' ); ?> hc-border-top-brightness-86 hc-border-bottom-brightness-86" loading="lazy" src="<?php echo $mapped_values["image"]; ?>" alt="<?php echo $mapped_values["alt_tag"]; ?>" />
                  <?php  } else{
                    ?>
                    <img class="automate-grid-element-img hc-border-rounded-top-12 hc-width-fit-container <?php echo esc_attr( ($mapped_values["is_from_datawrapper"] == 1 ) ?'hc-contain hc-datawrapper':'hc-cover' ); ?> hc-border-top-brightness-86 hc-border-bottom-brightness-86" loading="lazy" src="<?php echo $mapped_values["image"]; ?>" alt="<?php echo $mapped_values["alt_tag"]; ?>" />
                <?php
                   }    
            else:
                $wp_block_content = '';
                 if($mapped_values["current_post_type"] === 'wp_block'){
                    $wp_block_content = 'wp_block_card_content hc-supernormal-xs';
                 }
                echo sprintf( ' <div class="hc-width-fit-container hc-bg-brightness-93 hc-row-4 %s">', esc_attr($wp_block_content) );
                    if(!empty($wp_block_content)){
                        $content = apply_filters('the_content', get_post_field('post_content', $current_id));
                        echo preg_replace("/<\/?a( [^>]*)?>/i", "", $content);
                    }
                    echo '</div>';
                    ?>
                   
                <?php endif; ?>
                <?php if(!empty($mapped_values["top-left"])): ?>
                    <div class="gallery-top-left hc-supernormal-xs"><?php echo $mapped_values["top-left"]; ?></div>  
                <?php endif; ?>
            </div>
            <div class="gallery-info hc-m-12">
            <div data-lines="3" class="gallery_card_headline hc-truncate pykslice-card-title hc-mb-16">
                <?php echo $mapped_values["title"];
                ?>
            </div>
            <?php 
            $gallery_sub_title_class = '';
            if($mapped_values["current_post_type"] !== 'product'){
                $gallery_sub_title_class = 'gallery_sub-title-container';
            } ?>
            <div class="<?php echo esc_attr( $gallery_sub_title_class ); ?> sub-title-container hc-sub-title-container">
                
                <div class="gallery_date_time">
                <?php
                 if(!empty($mapped_values["pinned"])){
                    echo '<div class="hc-supernormal-xs-bold">PINNED</div>';
                } else {
            $current_id = $mapped_values["current_id"];    
        if( $mapped_values["current_post_type"]==='event'){
            $timezone = empty(get_post_meta( $current_id, 'humane_events_timezone', true )) ? "Asia/Kolkata" : get_post_meta( $current_id, 'humane_events_timezone', true );
        
        $start_date = date_create(get_post_meta($current_id, "humane_events_start_time", true), new \DateTimeZone($timezone));
        $end_date = date_create(get_post_meta($current_id, "humane_events_end_time", true), new \DateTimeZone($timezone));
        $event_data = Controller_Date::to_card($start_date, $end_date, $timezone);
            echo sprintf('<div class="hc-one-line hc-supernormal-xs">%s</div>', $event_data
        );
        } else{
            if($mapped_values["current_post_type"] === 'product' || $mapped_values["current_post_type"] === 'subscription' ) {
                if(function_exists('wc_get_product')){
                    $product_data = wc_get_product( $current_id );
                    $currency = get_woocommerce_currency_symbol(); 
                    $price = $product_data->price;
                    $show_price = "Price: ". $currency . " ". $price;
                } else{
                    $price = get_post_meta($current_id, "humane_price", true);
                    $currency = get_post_meta($current_id, "humane_currency", true);
                    $show_price = "Price: ". $currency . " ". $price;
                }
                ?>
                <div class="hc-one-line hc-supernormal-xs product_data"><?php echo $show_price; ?></div>
            <?php }
             if($mapped_values["hint"] && 'show' === $mapped_values["show_date"]): ?>
                    <div class="hc-one-line hc-supernormal-xs"><?php echo $mapped_values["hint"]; ?></div>
                <?php endif;   
        }
        $post_type_list = array( 'product', 'subscription','event', 'wp_block' );
        if(!in_array($mapped_values["current_post_type"],$post_type_list,true)){
            
                if( !empty($mapped_values["top-right"]) && 'show' === $mapped_values["show_min"]):
                    if('show' === $mapped_values["show_date"]) : 
                ?>
                    
                    <div class="hc-reusable-dot hc-bg-brightness-46"></div>
                    <?php endif; ?>
                    <div class="hc-one-line hc-supernormal-xs"><?php echo $mapped_values["top-right"]; ?></div>  
                <?php endif;
        } 
    }
                //End gallery event time.
                echo '</div>';
                echo '<div class="hc-card-footer aggregations_icon_and_domain">';
                if(!empty($mapped_values["aggregations_icon"] )  ) {
                    $show_icon ='no';
                    if( $mapped_values["current_post_type"]==='aggeregation'){
                        $show_icon ='yes';
                    } else{
                        if( $mapped_values["show_byline"]==='show'){
                            $show_icon ='yes';
                        }
                    }
                    if('yes'=== $show_icon ){
                        echo '<div class="aggregations_icon">';
                        echo sprintf('<img class="hc-dp-xs hc-flex-no-shrink" loading="lazy" src="%s" alt="%s">',$mapped_values["aggregations_icon"],Controller_Posts::humane_get_alt_tag_by_url($mapped_values["aggregations_icon"]) );
                        echo '</div>';
                    }
                    
                }
                if( $mapped_values["current_post_type"]==='aggregation'){
                    $aggregations_domain = $mapped_values["aggregations_domain"];
                if(!empty($aggregations_domain)){
                    echo '<div class="hc_gallery_aggregations_domain hc-one-line hc-supernormal-xs">';
                    echo $aggregations_domain;
                    echo '</div>';
                }
                } else{
                    echo '<div class="hc-author-and-cat hc-card-bottom">';
                    if( $mapped_values["show_byline"]==='show'){
                        if($mapped_values["subtitle"]):
                            echo sprintf(
                            '<div class="hc-one-line hc-supernormal-xs hc-gallery-author-info">By %s</div>',
                            $mapped_values["subtitle"]
                            );
                        endif;
                    }
                    if( !empty( $mapped_values["category"] )){
                        echo sprintf('<div class="hc-one-line hc-supernormal-xs">in %s</div>',ucfirst($mapped_values["category"]));
                    }
                    echo '</div>';
                }
                ?>
                </div>
            </div>
            </div>
            </div>
            <?php if(empty($intrection_class)){ ?>
            </a>
            <?php } ?>
    <?php endforeach; ?>
</div>
<?php Controller_Pagination::addPagination($filtered_count, $page, $items_per_page);?>
