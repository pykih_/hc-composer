<?php
    $id = $perspectives_json["id"];
    $selected_view = "0";
    wp_enqueue_script('humane-query-js');
    wp_enqueue_script('humane-control-js');
    wp_enqueue_script('humane-connector-js');
    wp_enqueue_script('humane-perspective-js');
    wp_enqueue_script('humane-syncscroll-js');
    wp_enqueue_script('humane-d3-js');
    wp_enqueue_style('humane-control-css');
    wp_enqueue_style('humane-vizhelper-css');
    wp_enqueue_style('humane-maps-css');
    wp_enqueue_style('humane-perspective-css');
    $filtered_count = $perspectives_json["filters"]["filtered_count"];
    $items_per_page = $attributes["items_per_page"];
    $search_data =  $perspectives_json["filters"]["filters"];
    $show_search = false;
    if( is_array($search_data) && array_key_exists('search',$search_data[0]['filters'])){
        $show_search = true;
    }
?>

<div data-id="<?php echo $perspectives_json["id"];?>" class="dashboard-container ajax-disable perspectives <?php echo $hide_filter_counts ? "hc-hide-filter-count" : "";?> <?php echo $refresh_dashboard ? "hc-refresh-dashboard" : "";?> <?php echo $filter_side ? "filter-side-dashboard" : "" ;?> hc-fy">
<?php if($show_search) : ?>
        <div class="hc-fx search-container hc-show-540 hc-mx-20-mobile">
        <div id="perspectives-<?php echo $id; ?>-search-mobile"></div>
        <div class="hc-filter-results-count hc-flex-no-shrink">
            <span class="filtered_count"><?php echo $filtered_count; ?></span> results
        </div>
    </div>
    <?php endif; ?>
    <div style="fill: var(--primary-main); stroke: var(--primary-main); position: absolute;" class="loading hc-height-fit-container hc-width-fit-container">
        <?php require HUMANE_COMPOSER_DIR . "assets/dashboard/images/loading.svg"; ?>
    </div>
    <div class="hc-fx hc-flex-gap-20">
        <?php 
         if(is_array($perspectives_json["views"]) && count($perspectives_json["views"]) > 0) :
            require HUMANE_COMPOSER_DIR.'views/dashboard/_view-dropdown.html.php';
         endif;
       
        if($show_search || !$filter_side || $perspectives_json["sort"]):
          ?>
        <div class="dashboard-header topbar hc-container-search-sorting  perspectives-dashboard-header hc-mobile-sort hc-top-header hc-fx main-dropdown <?php echo ( is_array($perspectives_json["views"]) && count($perspectives_json["views"]) > 1 ) ? "multiple-views" : "" ?>">
            <input type="hidden" value="<?php echo $perspectives_json["controller_class"]; ?>" class="controller-class">
            <?php 
            $searc_class ='search-container hc-flex-grow';
            if(!$show_search) {
                $searc_class ='search-container-hide';
            }
            ?>
            <div class="hc-fx <?php echo esc_attr(  $searc_class ); ?> hc-hide-540">
            <?php if($show_search) : ?>
                <div id="perspectives-<?php echo $id; ?>-search" style="min-width:100px;width:100%"></div>
                <div class="hc-filter-results-count hc-flex-no-shrink">
                    <span class="filtered_count"><?php echo $filtered_count; ?></span> results
                </div>
                <?php endif; ?>
            </div>
           
            <?php if(!$filter_side): ?>
                <?php if($perspectives_json["filters"]["filters"]): ?>
                    <?php $postfix = ""; ?>
                    <div class="filter-desktop-container">
                        <?php require HUMANE_COMPOSER_DIR.'views/dashboard/filter.html.php'; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($perspectives_json["sort"]): ?>
                <?php require HUMANE_COMPOSER_DIR.'views/dashboard/sort.html.php'; ?>
            <?php endif; ?>
        </div>
       <?php endif; ?>
    </div>
    <div class="<?php echo $filter_side ? "hc-fx hc-contains-sidebar" : "hc-fy" ;?>">
        <div class="hc-fx hc-justify-content-end">
            <div class="<?php echo $filter_side ? "" : "hc-show-540" ;?>">
                <div class="hc-filter-overlay"></div>
                <div class="dashboard-header <?php echo $filter_side ? "hc-position-relative sidebar hc-mr-20 hc-col-4 hc-fy hc-flex-start" : "topbar" ;?> perspectives-dashboard-header hc-mobile-filters hc-side-header main-dropdown <?php echo (is_array($perspectives_json["views"]) &&count($perspectives_json["views"]) ) > 1 ? "multiple-views" : "" ?>">
                    <div class="hc-fx hc-flex-between hc-m-20 hc-mb-0 hc-show-540">
                        <div class="hc-filters-title">Filters</div>
                        <div class="hc-mobile-close">
                            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="28" height="28" rx="14" fill="#1A1A1A"></rect>
                                <path d="M21 8.41L19.59 7L14 12.59L8.41 7L7 8.41L12.59 14L7 19.59L8.41 21L14 15.41L19.59 21L21 19.59L15.41 14L21 8.41Z" fill="white"></path>
                            </svg>
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo $perspectives_json["controller_class"]; ?>" class="controller-class">
                    <?php if($perspectives_json["filters"]["filters"]): ?>
                        <?php $postfix = "-mobile"; ?>
                        <div class="filter-mobile-container">
                            <?php require HUMANE_COMPOSER_DIR.'views/dashboard/filter.html.php'; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>    
        <div class="views-container hc-px-0 hc-mx-20-mobile <?php echo $filter_side ? "sidebar hc-col-12" : "" ;?>">
            <?php require HUMANE_COMPOSER_DIR.'views/dashboard/views.html.php'; ?>
        </div>
    </div>
</div>