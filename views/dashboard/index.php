<?php
use Humane_Sites\Controller_Date;
    $active = "dashboard";
    $header_active = "dashboard";
    $hide_csv = true;
    $reorder = array(
        "pageviews",
        "visitors",
        "visit_duration",
        "bounce_rate"
    );
?>
<div>
    <div class="hc-fx hc-col-16 hc-mt-20">
        <div class="wrap">
            <div class="headline hc-p-0 hc-m-0">
                <h2 class="hc-brand-header-m hc-text-center hc-mb-20">
                    <?php echo get_option("blogname"); ?>
                </h2>
                <p class="hc-brand-reading hc-text-center hc-mb-20"><?php echo get_option("tagline"); ?></p>
                <div class="hc-mb-20 hc-fx hc-flex-center">
                    <?php
                    $args = array(
                        'role__in'    => array('administrator', 'editor', 'author', 'contributor'),
                        'orderby' => 'user_nicename',
                        'order'   => 'ASC'
                    );
                    $users = get_users($args);
                    $more = false;
                    if (count($users) > 9) $more = true;
                    $users = array_slice($users, 0, 9);
                    ?>
                    <?php foreach ($users as $user) : ?>
                        <a target="_blank" class="hc-mr-8" href="<?php echo get_author_posts_url($user->ID); ?>"><img src="<?php echo get_avatar_url($user->ID); ?>" class="hc-dp-s"></a>
                    <?php endforeach; ?>
                    <?php if ($more) : ?>
                        ...
                    <?php endif; ?>
                </div>
            </div>
            <nav class="hc-fx hc-tab-group hc-flex-center">
                <a class="hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main hc-tab-first <?php echo $active === "dashboard" ? "hc-backend-tab-active" : ""; ?>" href="<?php echo admin_url(); ?>admin.php?page=humane_dashboard">Traffic</a>
                <a class="hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main <?php echo $active === "inbox" ? "hc-backend-tab-active" : ""; ?>" href="<?php echo admin_url(); ?>admin.php?page=humane_inbox">
                    <div class="hc-mr-8">Inbox</div>
                </a>
                <a class="hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main <?php echo $active === "post_reads" ? "hc-backend-tab-active" : ""; ?>" href="<?php echo admin_url(); ?>admin.php?page=humane_post_reads">
                    <div class="hc-mr-8">Reads (Engage)</div>
                </a>
                <a class="hc-fx hc-center hc-hyperlink hc-supernormal-s hc-p-8 hc-humane-border-primary-main hc-tab-last <?php echo $active === "index_pages_reads" ? "hc-backend-tab-active" : ""; ?>" href="<?php echo admin_url(); ?>admin.php?page=humane_page_reads">
                    <div class="hc-mr-8">Reads (Pitch)</div> 
                </a>
            </nav>
            <br />


            <div class="hc-analytics hc-p-20">
                <div class="hc-fx hint">
                    <div class="hc-width-fit-container hc-mr-20 hc-flex-grow hc-fx hc-justify-content-end">
                        <div class="hint hc-width-fit-content" style="color: black;">
                            SHOWING DATA OF LAST 30 DAYS
                        </div>
                    </div>
                </div>
                <br />
                <div class="card-group hc-fx">
                    <?php foreach ($reorder as $key) : ?>
                        <?php
                            $datum = $analytics_data[$key];
                        ?>
                        <div class="hc-flex-grow hc-p-20 hc-mr-20" style="border: 0px; background: none;">
                            <div class="hc-fy hc-flex-align-center hc-height-fit-container hc-justify-content-end">
                                <?php if ($datum["name"] === "Current Visitors") : ?>
                                    <div class="pulsating-circle" style="right: 0px; top: 8px;"></div>
                                <?php endif; ?>
                                <div class="hc-position-relative">
                                    <p class="hc-brightness-7 hc-brand-header-s"><?php echo $datum["value"]; ?></p>
                                    <?php if ($datum["change"]) : ?>
                                        <div class="hc-analytics-change hc-fx hc-center hc-supernormal-xs">
                                            <?php if($datum["change"] < 0): ?>
                                                <span style="color: var(--red); font-weight: 700;">↓</span>
                                            <?php else: ?>
                                                <span style="color: var(--green); font-weight: 700;">↑</span>
                                            <?php endif; ?>
                                            <div class="hc-text-brightness-60"><?php echo $datum["change"] ?>%</div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <p style="text-transform: uppercase; font-size: 0.7em;">
                                    <?php echo $datum["name"]; ?>
                                </p>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <br />

                <div class="hc-fx hc-tab-group hc-mb-20" id="myTab" role="tablist">
                    <a class="hc-fx  hc-center hc-supernormal-s hc-tab-first hc-p-8 hc-humane-border-primary-main hc-tab-trigger hc-backend-tab-active" id="home-tab" href="#home" role="tab" aria-controls="home" aria-selected="true" >Top Pages</a>
                    <a class="hc-fx  hc-center hc-supernormal-s hc-p-8 hc-humane-border-primary-main hc-tab-trigger" id="messages-tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Sources</a>
                    <a class="hc-fx  hc-center hc-supernormal-s hc-tab-last hc-p-8 hc-humane-border-primary-main hc-tab-trigger" id="settings-tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Technical</a>
                </div>
                <br />
                <div class="hc-tab-content">
                    <div class="hc-tab-container active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <?php if(is_array($top_page_analytics_data) && count($top_page_analytics_data)) : ?>
                            <table class="hc-table" style="font-size: 14px;">
                                <thead>
                                    <tr>
                                        <th style="width: 60%">Webpage URL</th>
                                        <th>Visitors</th>
                                        <th>Pageviews</th>
                                        <th>Bounce rate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($top_page_analytics_data as $datum) : ?>
                                        <tr>
                                            <td><a href="<?php echo $datum["page"]; ?>" target="_blank"><?php echo $datum["page"]; ?></a></td>
                                            <td><?php echo $datum["visitors"]; ?></td>
                                            <td><?php echo $datum["pageviews"]; ?></td>
                                            <td><?php echo $datum["bounce_rate"] ?? 0; ?>%</td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="hint">Data has not recorded any information yet</div>
                        <?php endif; ?>
                    </div>
                    <div class="hc-tab-container" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                        <?php if (is_array($source_analytics_data) && count($source_analytics_data)) : ?>

                            <table class="hc-table" style="font-size: 14px;">
                                <thead>
                                    <tr>
                                        <th style="width: 60%">Source</th>
                                        <th>Visitors</th>
                                        <th>Bounce Rate</th>
                                        <th>Visitor Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($source_analytics_data as $datum) : ?>
                                        <tr>
                                            <td>
                                                <img src="https://icons.duckduckgo.com/ip3/<?php echo $datum["source"]; ?>.ico" class="source-analytics-icon">
                                                <?php echo $datum["source"]; ?>
                                            </td>
                                            <td><?php echo $datum["visitors"]; ?></td>
                                            <td><?php echo $datum["bounce_rate"] ?? 0; ?>%</td>
                                            <td><?php echo Controller_Date::secondsToTime((int)$datum["visit_duration"]); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="hint">Data has not recorded any information yet</div>
                        <?php endif; ?>
                    </div>
                    <div class="hc-tab-container" id="settings" role="tabpanel" aria-labelledby="settings-tab">

                        <?php if (is_array($device_analytics_data) && count($device_analytics_data["screen-sizes"]) || count($device_analytics_data["browsers"]) || count($device_analytics_data["operating-systems"])) : ?>
                            <div class="hc-fx">
                                <div class="hc-col-4 hc-mr-20 hc-flex-grow">
                                    <table class="hc-table" style="font-size: 14px;">
                                        <thead>
                                            <tr>
                                                <th style="width: 60%">Screen size</th>
                                                <th>Visitors</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($device_analytics_data["screen-sizes"] as $datum) : ?>
                                                <tr>
                                                    <td><?php echo $datum["device"]; ?></td>
                                                    <td><?php echo $datum["visitors"]; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="hc-col-4 hc-mr-20 hc-flex-grow">
                                    <table class="hc-table" style="font-size: 14px;">
                                        <thead>
                                            <tr>
                                                <th style="width: 60%">Browser</th>
                                                <th>Visitors</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($device_analytics_data["browsers"] as $datum) : ?>
                                                <tr>
                                                    <td><?php echo $datum["browser"]; ?></td>
                                                    <td><?php echo $datum["visitors"]; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="hc-col-4 hc-mr-20 hc-flex-grow">
                                    <table class="hc-table" style="font-size: 14px;">
                                        <thead>
                                            <tr>
                                                <th style="width: 60%">OS</th>
                                                <th>Visitors</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($device_analytics_data["operating-systems"] as $datum) : ?>
                                                <tr>
                                                    <td><?php echo $datum["os"]; ?></td>
                                                    <td><?php echo $datum["visitors"]; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        <?php else : ?>
                            <div class="hint">Data has not recorded any information yet</div>
                        <?php endif; ?>
                    </div>
                </div>
                <br />
            </div>
            
        </div>
    </div>
</div>