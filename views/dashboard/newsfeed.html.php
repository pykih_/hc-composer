<?php
    namespace Humane_Sites;

    $filtered_count = $perspectives_json["filters"]["filtered_count"];
    $page = $view["page"];
    $filter_classs = 'hc_filter_top';
    if($filter_side ){
        $filter_classs = 'hc_filter_left';
    }
?>
<div class="table table-hover cognitively-newsfeed-view <?php echo $filter_classs; ?> hc-post-content">
    <?php foreach($rows as $row): ?>
        <?php
            
            $author_first_name = null;
            if($view["author_first_name"])
            $author_first_name = is_callable($view["author_first_name"]) ? $view["author_first_name"]($row) : ucwords($row->{$view["author_first_name"]});
            $author_last_name = null;
            
            $creator_name = $author_first_name;
            $updater_name = $row->{$view["updated_by_first_name"]}.' '.$row->{$view["updated_by_last_name"]};

            $user_object = array(
                "src" => $creator_image,
                "fullname" => $creator_name
            );
            $url = null;
            if(isset($view["title"]["url"]))
                $url = is_callable($view["title"]["url"]) ? $view["title"]["url"]($row) : $view["title"]['url'].$row->{$view["id"]};
            $backlinks_url = null;
            if(isset($view["backlinks"]["url"]))
                $backlinks_url = is_callable($view["backlinks"]["url"]) ? $view["backlinks"]["url"]($row) : $view["backlinks"]["url"].$row->{$view["id"]};
            $preview_url = null;
            if(isset($view["preview"]["url"]))
                $preview_url = is_callable($view["preview"]["url"]) ? $view["preview"]["url"]($row) : null;
            $container_attributes = null;
            if(isset($view["container"]["attributes"]))
                $container_attributes = $view["container"]["attributes"]($row);
            $title = null;
            
            if(isset($view["title"]["text"]))
                $title = is_callable($view["title"]["text"]) ? $view["title"]["text"]($row) : $row->{$view["title"]['text']};
            $image_src = null;
            if($view["image_src"])
                $image_src = is_callable($view["image_src"]) ? $view["image_src"]($row) : ucwords($row->{$view["image_src"]});
            $pinned = null;
            if($view["pinned"])
                $pinned = is_callable($view["pinned"]) ? $view["pinned"]($row) : ucwords($row->{$view["pinned"]});
            $post_type = null;
            if($view["post_type"])
                $post_type = is_callable($view["post_type"]) ? $view["post_type"]($row) : $row->{$view["post_type"]};
            $current_id = null;
            if($view["current_id"])
                $current_id = is_callable($view["current_id"]) ? $view["current_id"]($row) : $row->{$view["current_id"]};
            $show_date = null;
            if($view["show_date"])
                $show_date = is_callable($view["show_date"]) ? $view["show_date"]($row) : $row->{$view["show_date"]};
                $aggregations_icon = null;
                if($view["aggregations_icon"])
                    $aggregations_icon = is_callable($view["aggregations_icon"]) ? $view["aggregations_icon"]($row) : ucwords($row->{$view["aggregations_icon"]});
                $action = null;
            if($view["action"])
                $action = is_callable($view["action"]) ? $view["action"]($row) : $view["action"];
            $date = null;
            if($view["date"])
                $date = is_callable($view["date"]["function"]) ? $view["date"]["function"]($row) : date('F j, Y', strtotime($row->{$view["date"]["post_date"]}));
                $aggregations_domain = null;
                if($view["aggregations_domain"])
                    $aggregations_domain = is_callable($view["aggregations_domain"]) ? $view["aggregations_domain"]($row) : $row->{$view["aggregations_domain"]};
                    $intrection_class = null;
                    if($view["intrection_class"])
                        $intrection_class = is_callable($view["intrection_class"]) ? $view["intrection_class"]($row) : $row->{$view["intrection_class"]};
                        $alt_tag = null;
            if($view["alt_tag"])
                $alt_tag = is_callable($view["alt_tag"]) ? $view["alt_tag"]($row) : $view["alt_tag"];
                        $wp_block = false;
                        $list_table_row = 'list-table-row hc-list-table-row hc-p-20 hc-mb-20 hc-newsfeed-row hc-re-border';
                        if($post_type==='wp_block') {
                            $wp_block=true;
                            $intrection_class = '';
                            $list_table_row = 'hc-newsfeed-row';
                        }
                       
                        if(!$wp_block && isset($url) && empty($intrection_class)){
            ?>
            <a href="<?php echo $url; ?>" <?php echo $view["title"]['new_tab'] ? "target='_blank'" : "" ?> class="hc-list-title-link1">
            <object>
        <?php }
        ?>
        
        <div id="newsfeed_<?php echo $current_id; ?>"  data-src="<?php echo get_permalink( $current_id ); ?>" data-connect="<?php echo $current_id; ?>" class="<?php echo esc_attr($intrection_class);?> <?php echo esc_attr($list_table_row);?>  <?php echo !empty($image_src) ? "has_image" : ""; ?>" <?php echo $container_attributes; ?>>
        <?php  if(!$wp_block){ ?>
        <div class ='list-table-cell-left'>    
        <div class="list-table-cell" style="grid-row: 1;">
                <?php 
                if($pinned){
                    echo '<span class="hc-supernormal-xs-bold">PINNED</span>';
                }
                $external_image = '';
                if($post_type==='aggregation') {
                    $external_image = HUMANE_COMPOSER_URL . "assets/dashboard/images/external-link.png";
                }
                if(isset($url)): ?>
                                        
                    <p class="<?php echo $view["title"]["truncate"] === false ? "" : "pytitle" ;?> hc-mt-0 hc-brand-card-title-xs hc-mb-0 hc-hyperlink"><?php while($row->nesting_level--):?>—<?php endwhile; ?><?php echo stripslashes($title); ?>
                    <?php if(!empty($external_image ) ) {
                        echo sprintf('<img class="hc_external_image" src="%s" alt="%s">',$external_image,Controller_Posts::humane_get_alt_tag_by_url($external_image));
                    } ?>
                    </p>
                    
                <?php else: 
                    ?>
                    <p class="<?php echo $view["title"]["truncate"] === false ? "" : "pytitle" ;?> hc-brand-card-title-s hc-mb-8 hc-hyperlink"><?php echo stripslashes($title); ?>
                <?php if(!empty($external_image ) ) {
                        echo sprintf('<img class="hc_external_image" src="%s" alt="%s">',$external_image,Controller_Posts::humane_get_alt_tag_by_url($external_image));
                    } ?>
                </p>
                <?php endif; ?>
                <div class="hint hc-fx hc-flex-wrap hc-custom-wrap">
                    <?php 
                    if(!empty($aggregations_icon ) ) {
                        echo sprintf('<img class="hc-dp-xs hc-flex-no-shrink" src="%s" alt="%s">',$aggregations_icon,Controller_Posts::humane_get_alt_tag_by_url($aggregations_icon) );
                    }
                    echo '<div class="hc_show_date_byline_domain">';
                    echo '<div class="hc_show_date_byline">';
                    if( $post_type === 'product' || $post_type === 'subscription' ) {
                       // write_log('','insidecode');
                        if(function_exists('wc_get_product')){
                            $product_data = wc_get_product( $current_id );
                            $currency = get_woocommerce_currency_symbol(); 
                            $price = $product_data->price;
                            $show_price = "Price: ". $currency . "". $price;
                        } else{
                            $price = get_post_meta($current_id, "humane_price", true);
                            $currency = get_post_meta($current_id, "humane_currency", true);
                            $show_price = "Price: ". $currency . " ". $price;
                        }
                        echo $show_price;
                    }
                    if(!empty($creator_name) && $creator_name!=' '){
                    ?>
                    By <?php echo $creator_name;
                    }
                    if('show' === $show_date ){
                        echo " • ".$date;
                    }
                    ?>
                    <?php if($view["byline"]): ?>
                        <?php $view["byline"]($row);?>
                    <?php endif; 
                    echo '</div>';
                    ?>
                    
                    <?php
                    if(!empty($aggregations_domain)){
                        echo '<div class="hc_aggregations_domain">';
                        echo $aggregations_domain;
                        echo '</div>';
                    }
                    echo '</div>';
                    ?>
                    <?php if(isset($preview_url)): ?>
                        &nbsp;•&nbsp;
                        <a href="<?php echo $preview_url; ?>"  target="_blank">
                            <?php echo $view["preview"]["text"]; ?>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="hc-mt-20 hc-brand-reading hc-re-text-color">
                    <?php if(has_excerpt( $current_id )) 
                        echo get_the_excerpt( $current_id ); 
                    ?>
                </div>
                <?php echo $view['multilingual']; ?>
            </div>
            <div class="list-table-cell hc-py-0 hc-mb-0 list-table-row-badges" style="grid-row: 2;">
                <?php if($view["badge"]): ?>
                    <?php $view["badge"]($row);?>
                <?php endif; ?>
            </div>
            </div>
            <?php if(!empty($image_src)):
                    
                ?>
                <div class="list-table-cell rightsidebar hc-height-fit-container" style="grid-row: 1/3;">
                    <img class="hc-cover-r160 hc-list-view-img" src="<?php echo $image_src; ?>" alt="<?php echo $alt_tag; ?>" >
                </div>
            <?php 
            
        endif;
    } else {
        echo Controller_Posts::get_reusable_content($current_id);
} ?>

        </div>
        <?php 

if( !$wp_block && isset($url) && empty($intrection_class)){
    echo '</object>';
    echo '</a>';
}

                       
?>
    <?php endforeach; ?>
</div>
<?php Controller_Pagination::addPagination($filtered_count, $page, $items_per_page);?>
