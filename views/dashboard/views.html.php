<?php $view_count = 0; ?>
<?php foreach($perspectives_json["views"] as $view): ?>
    <div data-id="<?php echo $view_count?>" class="view-container <?php echo "$view_count" === $selected_view ? "" : "view-hidden"?>">
        <?php if(count($rows)): ?>
            <?php if($view["scope"] === "Table"): ?>
                <div class="inner-view-container table">
                    <?php require HUMANE_COMPOSER_DIR.'views/dashboard/table.html.php'; ?>
                </div>
            <?php elseif($view["scope"] === "Gallery"): ?>
                <div class="inner-view-container">
                    <?php require HUMANE_COMPOSER_DIR.'views/dashboard/gallery.html.php'; ?>
                </div>
            <?php elseif($view["scope"] === "List"): ?>
                <div class="inner-view-container">
                    <?php require HUMANE_COMPOSER_DIR.'views/dashboard/list.html.php'; ?>
                </div>
            <?php elseif($view["scope"] === "Map"): ?>
                <div class="inner-view-container">
                    <?php require HUMANE_COMPOSER_DIR.'views/dashboard/map.html.php'; ?>
                </div>
            <?php elseif($view["scope"] === "Newsfeed"): ?>
            <div class="inner-view-container">
                <?php require HUMANE_COMPOSER_DIR.'views/dashboard/newsfeed.html.php'; ?>
            </div>
            <?php elseif($view["scope"] === "Dictionary"): ?>
            <div class="inner-view-container">
                <?php require HUMANE_COMPOSER_DIR.'views/dashboard/dictionary.html.php'; ?>
            </div>
            <?php endif; ?>
        <?php else: ?>
            Your search returned 0 results.
        <?php endif; ?>
    </div>
    <?php $view_count++; ?>
<?php endforeach; ?>