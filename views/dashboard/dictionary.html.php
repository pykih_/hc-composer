<?php
    namespace Humane_Sites;

    $filtered_count = $perspectives_json["filters"]["filtered_count"];
    $page = $view["page"];
?>
<div class="<?php echo $filter_side ? 'dict-col-3' : 'dict-col-4'; ?> dictionary">
    <?php foreach($rows as $row): ?>
        <?php
            $url = null;
            if(isset($view["title"]["url"]))
                $url = is_callable($view["title"]["url"]) ? $view["title"]["url"]($row) : $view["title"]['url'].$row->{$view["id"]};
            $container_attributes = null;
            if(isset($view["container"]["attributes"]))
                $container_attributes = $view["container"]["attributes"]($row);
            $title = null;
            
            if(isset($view["title"]["text"]))
                $title = is_callable($view["title"]["text"]) ? $view["title"]["text"]($row) : $row->{$view["title"]['text']};
            $post_type = null;
            if($view["post_type"])
                $post_type = is_callable($view["post_type"]) ? $view["post_type"]($row) : $row->{$view["post_type"]};
            $current_id = null;
            if($view["current_id"])
                $current_id = is_callable($view["current_id"]) ? $view["current_id"]($row) : $row->{$view["current_id"]};
        ?>
        <div class="hc-px-8 hc-py-20" <?php echo $container_attributes; ?>>    
                <?php 
                if(isset($url)): ?>  
                    <a href="<?php echo $url ?>">          
                        <p class="hc-mt-0 hc-brand-card-title-xs hc-mb-0"><?php while($row->nesting_level--):?>—<?php endwhile; ?><?php echo stripslashes($title); ?>
                        </p>
                    </a>
                <?php else: 
                    ?>
                    <p class="pytitle hc-brand-card-title-xs hc-mb-8"><?php echo stripslashes($title); ?>
                    </p>
                <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
<?php Controller_Pagination::addPagination($filtered_count, $page, $items_per_page);?>