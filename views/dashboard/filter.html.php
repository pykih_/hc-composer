<?php
use Humane_Sites\Controller_Form_Fields;
use Humane_Sites\Controller_Icons;
$filters                     = $perspectives_json['filters'];
$filtered_count              = $filters['filtered_count'];
$total_count                 = $filters['total_count'];
$filter_side                 = filter_var( $filter_side, FILTER_VALIDATE_BOOLEAN );
$all_filters_items           = $perspectives_json['filters']['filters'][0]['filters'];
$all_filters_items1          = array_shift( $all_filters_items );
$filters_items_except_search = count( $all_filters_items );
if ( $filters_items_except_search > 0 ) :
	?>
<div class="<?php echo $filter_side ? 'hc-show-540' : ''; ?> hc-px-8 hc-flex-no-shrink hc-filter-dropdown perspectives-show hc-width-fit-content hc-flex-self-end main-count-container filter-dropdown-opener">
	<svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M4.66667 8H7.33333V6.66667H4.66667V8ZM0 0V1.33333H12V0H0ZM2 4.66667H10V3.33333H2V4.66667Z" fill="black"/>
	</svg>
	<span class="hc-no-wrap">Filters</span>
</div>
<?php endif; ?>
<div class="<?php echo $filter_side ? 'hc-hide-540 filter-side-container hc-width-fit-container' : 'filter-dropdown'; ?> filter-container-main">
	<?php if ( ! $filter_side ) : ?>
		<div class="title-description-container">
			<div class="main-title">Filters</div>
			<?php if ( ! empty( $filters['description'] ) ) : ?>
				<div class="main-description"><?php echo $filters['description']; ?></div>
			<?php endif; ?>
		</div>
		<div class="filter-title-container syncscroll" name="syncme">
			<?php
			foreach ( $filters['filters'] as $key => $tab ) :
				?>
				<div data-tab="<?php echo $key; ?>" class="filter-tab" style="z-index: <?php echo 5 + $key; ?>;">
					<div class="filter-tab-name"><?php echo $tab['name'] ?? ''; ?></div>
					<div class="filter-tab-description"><?php echo $tab['description'] ?? ''; ?></div>
				</div>
				<?php
				foreach ( $tab['filters'] as $key => $filter ) :
					?>
					<?php
					if ( $key === 'search' ) {
						continue;}
					?>
					<?php
					if ( $filter['scope'] === 'Dropdown' && count( $filter['data'] ) === 0 ) {
						continue;}
					?>
					<div id="perspectives-<?php echo $id; ?>-filter-<?php echo $key; ?>-title" class="filter-title">
						<div class="label"><?php echo $filter['label']; ?></div>
						<?php if ( $filter['description'] ?? false ) : ?>
							<div class="icon">
								<svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
									<ellipse cx="6.27271" cy="6" rx="6.25668" ry="6" fill="white"></ellipse>
									<path d="M5.14068 8.59998H6.35091L7.07818 4.23634H5.86795L5.14068 8.59998ZM6.59523 3.66816C6.95602 3.66816 7.27989 3.38975 7.31966 3.05452C7.35659 2.71645 7.09523 2.44373 6.73443 2.44373C6.37648 2.44373 6.04977 2.71645 6.01 3.05452C5.96739 3.38975 6.23443 3.66816 6.59523 3.66816Z" fill="black"></path>
								</svg>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<?php
	if ( $filter_side ) :
		if ( $filters_items_except_search > 0 ) :
			?>
		<div class="title-container"><?php echo $filters['title']; ?></div>
		<?php endif; ?>
	<?php endif; ?>
	<div class="<?php echo $filter_side ? '' : 'filter-container syncscroll'; ?> hc-fy hc-filter-container" name="syncme">
		<?php foreach ( $filters['filters'] as $key => $tab ) : ?>
			<?php
			if ( $key === 'search' ) {
				continue;}
			?>
			<?php foreach ( $tab['filters'] as $key => $filter ) : ?>
				<div id="perspectives-<?php echo $id; ?>-filter-<?php echo $key; ?><?php echo $postfix; ?>"></div>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</div>
	<div class="global_reset">Clear All</div>
	<div class="tooltip"></div>
</div>
