<?php
    namespace Humane_Sites;
    $action = admin_url('admin.php?page=speed_settings');
?>
<?php require HUMANE_COMPOSER_DIR . 'views/settings/header.html.php'; ?>
<div class="hc-fx hc-col-16 hc-mt-20">
    <div class="wrap">
        <h2 class="hc-mb-20">Speed</h2>
        <h3 class="hc-mb-8">Switch on caching</h3>
        <form method="post" action="<?php echo $action ?>">
            <?php wp_nonce_field('humane_edit_settings', 'humane_edit_settings', false, true); ?>
            <div class="hc-fy hc-flex-gap-20">
                <?php
                Controller_Form_Fields::input_text(
                    "humane_site_install_id",
                    "Site Installation ID",
                    true,
                    get_option('humane_site_install_id'),
                    array(
                        "class" => "",
                        "description" => "Installation id of the current site"
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "humane_site_wpengine_api_key",
                    "API Key",
                    true,
                    get_option('humane_site_wpengine_api_key'),
                    array(
                        "class" => "",
                        "description" => "WPEngine API key"
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "humane_site_wpengine_api_password",
                    "API Password",
                    true,
                    get_option('humane_site_wpengine_api_password'),
                    array(
                        "class" => "",
                        "description" => "WPEngine API password"
                    )
                );
                ?>
            </div>
            <br />
            <button type="submit" class="button button-primary">Save</button>
        </form>
    </div>
</div>
