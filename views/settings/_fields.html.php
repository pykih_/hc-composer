<?php
    namespace Humane_Sites;
?>
<div class="hc-sort-container">
    <div class="hc-sortable">
        <?php foreach($post_types as $type): ?>
            <?php 
                $filename = HUMANE_COMPOSER_DIR . "assets/posts/filters/$type.json";
                $filename = apply_filters("filters_file_name", $filename, $type);
                $loaded_filters = json_decode(file_get_contents($filename), true);
                $sort_order = array_keys($loaded_filters);
                $new_order = get_option("$type-sort_order");
                if(!empty($new_order)){
                    $new_order = json_decode(stripslashes($new_order), true);
                    $sort_order = array_map(function($a) use($type) {
                        return str_replace("$type-", "", $a);
                    }, $new_order);
                }
    
            ?>
            <?php foreach($sort_order as $key): ?>
                <?php
                    $filter = $loaded_filters[$key];
                    if(!$filter) continue;
                ?>
                <div class="hc-fx hc-flex-align-center hc-mb-20">
                    <div class="hc-mr-8" style="cursor: grab;">
                        <?php echo Controller_Icons::get_svg_icons("reorder", 16,  "var(--brightness-46)"); ?>
                    </div>
                    <?php
                        Controller_Form_Fields::input_toggle_round(
                            "$type-$key",
                            $filter["label"],
                            false,
                            get_option("$type-$key"),
                            array(
                                "class" => "form-control",
                                "default" => "on",
                                "caption_class" => "hc-mb-0 hc-ml-8",
                                "options" => array(
                                    "off" => "Hide",
                                    "on" => "Show"
                                )
                            )
                        );
                    ?>
                    <?php
                        Controller_Form_Fields::input_toggle_round(
                            "$type-$key-search",
                            "Enable Search",
                            false,
                            get_option("$type-$key-search"),
                            array(
                                "class" => "",
                                "default" => "on",
                                "caption_class" => "hc-mb-0 hc-ml-8",
                                "options" => array(
                                    "off" => "Hide",
                                    "on" => "Show"
                                )
                            )
                        );
                    ?>
                </div>
            <?php endforeach; ?>

        <?php endforeach; ?>
    </div>
    <?php
        Controller_Form_Fields::input_hidden(
            "$type-sort_order",
            '',
            false,
            get_option("$type-sort_order"),
            array(
                "class" => " sort-order-field"
            )
        );
    ?>
</div>