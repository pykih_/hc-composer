<?php
    namespace Humane_Sites;
    $action = admin_url('admin.php?page=site_settings');
?>
<?php require HUMANE_COMPOSER_DIR . 'views/settings/header.html.php'; ?>
<div class="hc-fx hc-col-16 hc-mt-20">
    <div class="wrap">
        <h2 class="hc-mb-20">Identity</h2>
        <form method="post" action="<?php echo $action ?>">
            <?php wp_nonce_field('humane_edit_settings', 'humane_edit_settings', false, true); ?>
            <div class="hc-fy hc-flex-gap-20">
                <!-- <h3 class="hc-mb-8">What's your Story</h3> DHARA_TO_CLEAN_UP_THIS_FUNCTIONALITY
                <?php
                    // Controller_Form_Fields::input_text(
                    //     "support_email",
                    //     "Support Email",
                    //     false,
                    //     get_option('support_email'),
                    //     array(
                    //         "class" => "",
                    //         "description" => ""
                    //     )
                    //);
                    ?> -->
            </div>
            <br />
            <div class="hc-fy hc-flex-gap-20">
                <h3 class="hc-mb-8">Brand Identity</h3>
                <?php
                Controller_Form_Fields::input_attach_document(
                    "site_logo",
                    "Rectangular Logo",
                    false,
                    get_option('site_logo'),
                    array(
                        'type' => 'upload',
                        "class" => "",
                        "id" => "settingsSiteLogo",
                        "description" => "This logo appears at the top of all your publication’s stories. It should have a transparent background, and be at least 600px wide and 72px tall.",
                        "placeholder" => "Choose file",
                        "has_button" => true,
                        "button" => array(
                            "class" => "custom-file-label open-media-modal"
                        )
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_attach_document(
                    "site_square_logo",
                    "Square Logo (transparent)",
                    false,
                    get_option('site_square_logo'),
                    array(
                        'type' => 'upload',
                        "class" => "",
                        "id" => "settingsSiteSquareLogo",
                        "description" => "This works like a user icon and appears in previews of your publication content. It is square and should be at least 60 × 60px in size.",
                        "placeholder" => "Choose file",
                        "has_button" => true,
                        "button" => array(
                            "class" => "custom-file-label open-media-modal"
                        )
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_attach_document(
                    "bot_icon_for_popup",
                    "Square Logo (with background)",
                    false,
                    get_option('bot_icon_for_popup'),
                    array(
                        'type' => 'upload',
                        "class" => "",
                        "id" => "settings_bot_icon_for_popup",
                        "description" => "Enter bot icon for popup",
                        "placeholder" => "Choose file",
                        "has_button" => true,
                        "button" => array(
                            "class" => "custom-file-label open-media-modal"
                        )
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_toggle(
                    "show_site_rect_logo",
                    "Which logo do you want to show in header in Laptop?",
                    false,
                    get_option('show_site_rect_logo'),
                    array(
                        "class" => " ",
                        "default" => "no",
                        "options" => array("no" => "Square", "yes" => "Rectangular")
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_toggle(
                    "show_site_rect_logo_in_mobile",
                    "Which logo do you want to show in header in Mobile?",
                    false,
                    get_option('show_site_rect_logo_in_mobile'),
                    array(
                        "class" => " ",
                        "default" => "no",
                        "options" => array("no" => "Square", "yes" => "Rectangular")
                    )
                );
                ?>
            </div>
            <br />
            <?php if (is_super_admin()) : ?>
                <div class="hc-fy hc-flex-gap-20">
                    <h3 class="hc-mb-8">Header</h3>
                    <?php
                    Controller_Form_Fields::input_toggle(
                        "settings_content_pack_search_enabled",
                        "Do you want to show a site-wide search?",
                        false,
                        get_option('settings_content_pack_search_enabled'),
                        array(
                            "class" => " ",
                            "default" => "no",
                            "options" => array("off" => "Disabled", "on" => "Enabled")
                        )
                    );
                    ?>
                </div>
            <?php endif; ?>
            <br />
            <button type="submit" class="button button-primary">Save</button>
        </form>   
    </div>
</div>
