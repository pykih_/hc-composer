<?php
    namespace Humane_Sites;
    $action = admin_url('admin.php?page=styleguide_settings');
    $active = "color";
    $check_pykih = wp_get_current_user();
    $check_pykih = $check_pykih->user_email;
?>
<?php require HUMANE_COMPOSER_DIR . 'views/settings/header.html.php'; ?>
<div class="hc-fx hc-col-16 hc-mt-20">
    <div class="wrap">
        <h2 class="hc-mb-20">Look and Feel</h2>
        <form method="Post" action="<?php echo $action ?>">
            <?php wp_nonce_field('humane_edit_settings', 'humane_edit_settings', false, true); ?>
            <input type="hidden" name="secret_type_field" value="styling" />
            <div class="section-wrapper hc-col-10">
                <h3 class="hc-mb-8">Brand Header</h3>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-header-font",
                    "Font Family",
                    false,
                    Model_Options::get_styling_option('brand-header-font'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-header-font')
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-header-size-xl",
                    "Font Extra Large Size",
                    false,
                    Model_Options::get_styling_option('brand-header-size-xl'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-header-size-xl')
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-header-size-m",
                    "Font Medium Size",
                    false,
                    Model_Options::get_styling_option('brand-header-size-m'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-header-size-m')
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-header-size-s",
                    "Font Small Size",
                    false,
                    Model_Options::get_styling_option('brand-header-size-s'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-header-size-s')
                    )
                );
                ?>
            </div>
            <br />
            <div class="section-wrapper hc-col-10">
                <h3 class="hc-mb-8">Brand Card Title</h3>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-card-title-size-s",
                    "Font Small Size",
                    false,
                    Model_Options::get_styling_option('brand-card-title-size-s'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-card-title-size-s')
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-card-title-size-xs",
                    "Font Extra Small Size",
                    false,
                    Model_Options::get_styling_option('brand-card-title-size-xs'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-card-title-size-xs')
                    )
                );
                ?>
            </div>
            <br />
            <div class="section-wrapper hc-col-10">
                <h3 class="hc-mb-8">Brand Reading</h3>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-reading-font",
                    "Font Family",
                    false,
                    Model_Options::get_styling_option('brand-reading-font'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-reading-font')
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_text(
                    "brand-reading-size",
                    "Font Size",
                    false,
                    Model_Options::get_styling_option('brand-reading-size'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('brand-reading-size')
                    )
                );
                ?>
            </div>
            <br />
            <div class="section-wrapper hc-col-10">
                <h3 class="hc-mb-8">Supernormal</h3>
                <?php
                Controller_Form_Fields::input_text(
                    "supernormal-font",
                    "Font Family",
                    false,
                    Model_Options::get_styling_option('supernormal-font'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('supernormal-font')
                    )
                );
                ?>
                <?php
                    Controller_Form_Fields::input_text(
                        "supernormal-size-m",
                        "Font Medium Size",
                        false,
                        Model_Options::get_styling_option('supernormal-size-m'),
                        array(
                            "class" => " ",
                            "default" => Model_Options::get_default('supernormal-size-m')
                        )
                    );
                    ?>
                <?php
                    Controller_Form_Fields::input_text(
                        "supernormal-size-s",
                        "Font Small Size",
                        false,
                        Model_Options::get_styling_option('supernormal-size-s'),
                        array(
                            "class" => " ",
                            "default" => Model_Options::get_default('supernormal-size-s')
                        )
                    );
                    ?>
                <?php
                Controller_Form_Fields::input_text(
                    "supernormal-size-xs",
                    "Font Extra Small Size",
                    false,
                    Model_Options::get_styling_option('supernormal-size-xs'),
                    array(
                        "class" => " ",
                        "default" => Model_Options::get_default('supernormal-size-xs')
                    )
                );
                ?>
            </div>
            <br />
            <div class="section-wrapper hc-col-10">
                <h3 class="hc-mb-8">Font Weights</h3>
                <?php
                Controller_Form_Fields::input_number(
                    "regular",
                    "Regular",
                    false,
                    Model_Options::get_styling_option('regular'),
                    array(
                        "default" => Model_Options::get_default('regular'),
                        "class" => ""
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_number(
                    "medium",
                    "Medium",
                    false,
                    Model_Options::get_styling_option('medium'),
                    array(
                        "default" => Model_Options::get_default('medium'),
                        "class" => ""
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_number(
                    "semi-bold",
                    "Semi Bold",
                    false,
                    Model_Options::get_styling_option('semi-bold'),
                    array(
                        "default" => Model_Options::get_default('semi-bold'),
                        "class" => ""
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_number(
                    "bold",
                    "Bold",
                    false,
                    Model_Options::get_styling_option('bold'),
                    array(
                        "default" => Model_Options::get_default('bold'),
                        "class" => ""
                    )
                );
                ?>
                <?php
                Controller_Form_Fields::input_number(
                    "extra-bold",
                    "Extra Bold",
                    false,
                    Model_Options::get_styling_option('extra-bold'),
                    array(
                        "default" => Model_Options::get_default('extra-bold'),
                        "class" => ""
                    )
                );
                ?>
            </div>
            <br />
            <div class="hc-fy hc-flex-gap-20">
                <h3 class="hc-mb-8">Color palette</h3>
                <div class="hc-fx hc-flex-wrap hc-col-16">
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "primary-dark",
                            "",
                            false,
                            Model_Options::get_styling_option('primary-dark'),
                            array(
                                "default" => Model_Options::get_default('primary-dark'),
                                "class" => "color-picker",
                                "description" => "Primary Dark"
                            )
                        );
                        ?>
                    </div>
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "primary-main",
                            "",
                            false,
                            Model_Options::get_styling_option('primary-main'),
                            array(
                                "default" => Model_Options::get_default('primary-main'),
                                "class" => "color-picker",
                                "description" => "Primary Main"
                            )
                        );
                        ?>
                    </div>
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "primary-pastel",
                            "",
                            false,
                            Model_Options::get_styling_option('primary-pastel'),
                            array(
                                "default" => Model_Options::get_default('primary-pastel'),
                                "class" => "color-picker",
                                "description" => "Primary Pastel"
                            )
                        );
                        ?>
                    </div>
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "primary-faded",
                            "",
                            false,
                            Model_Options::get_styling_option('primary-faded'),
                            array(
                                "default" => Model_Options::get_default('primary-faded'),
                                "class" => "color-picker",
                                "description" => "Primary Faded"
                            )
                        );
                        ?>
                    </div>
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "secondary-main",
                            "",
                            false,
                            Model_Options::get_styling_option('secondary-main'),
                            array(
                                "default" => Model_Options::get_default('secondary-main'),
                                "class" => "color-picker",
                                "description" => "Secondary Main"
                            )
                        );
                        ?>
                    </div>
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "extra-color",
                            "",
                            false,
                            Model_Options::get_styling_option('extra-color'),
                            array(
                                "default" => Model_Options::get_default('extra-color'),
                                "class" => "color-picker",
                                "description" => "Extra"
                            )
                        );
                        ?>
                    </div>
                    </table>
                </div>
                <h3 class="hc-mb-8">Reusable block color</h3>
                <div class="hc-fx hc-flex-wrap hc-col-16">
                    <div class="color-settings hc-mr-20">
                        <?php
                        Controller_Form_Fields::input_color(
                            "reusable_block_color",
                            "",
                            false,
                            Model_Options::get_styling_option('reusable_block_color'),
                            array(
                                "default" => '#FFFACD',
                                "class" => "color-picker",
                                "description" => "Reusable block color"
                            )
                        );
                        ?>
                    </div>
                    </div>
                <?php if (is_super_admin()) : ?>
                    <div class="section-wrapper hc-col-10">
                        <h3 class="hc-mb-8">Humane Attribution</h3>
                        <?php
                        Controller_Form_Fields::input_toggle(
                            "humane_branding_header",
                            "Do you want to show Humane branding in the header?",
                            false,
                            Model_Options::get_styling_option("humane_branding_header"),
                            array(
                                "class" => "",
                                "default" => "yes",
                                "options" => array("no" => "No", "yes" => "Yes")
                            )
                        );
                        ?>
                        <?php
                        Controller_Form_Fields::input_toggle(
                            "humane_branding_sticky",
                            "Do you want to show sticky Humane branding?",
                            false,
                            Model_Options::get_styling_option("humane_branding_sticky"),
                            array(
                                "class" => "",
                                "default" => "yes",
                                "options" => array("no" => "No", "yes" => "Yes")
                            )
                        );
                        ?>
                    </div>
                    <br />
                <?php endif; ?>
                <br />
                <button type="submit" class="button button-primary">Save</button>
        </form> 
    </div>
</div>
