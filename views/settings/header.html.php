<?php
    $tab = $_GET["page"];
?>
<nav class="nav-tab-wrapper">
    <a href="?page=site_settings" class="nav-tab <?php if($tab === "site_settings"):?>nav-tab-active<?php endif; ?>">Site Settings</a>
    <a href="?page=styleguide_settings" class="nav-tab <?php if($tab === 'styleguide_settings'):?>nav-tab-active<?php endif; ?>">Styleguide</a>
</nav>