<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx hc-mb-20 hc-select-container <?php echo $attributes["container_class"] ?? "" ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="hc-fy hc-width-fit-container">
    <select
        <?php echo ($attributes['multiple'] ?? false) ? "multiple" : ""?>
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-flex-grow <?php echo esc_attr( $attributes['class'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
        <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
        value="<?php echo esc_attr( $value ) ?>"
        <?php echo $is_required ? "required" : ""; ?>
    >
        <?php if(!$attributes['required'] && !$attributes["default"] && !$attributes["multiple"]): ?>
            <option selected value>--</option>
        <?php endif; 
        ?>
        <?php foreach($attributes['options'] as $key => $option): ?>
            <?php
                if(is_array($value) && ($attributes["multiple"] ?? false)){
                    $selected = in_array($key, $value);
                }else{
                    $selected = $value == $key;
                }
            ?>
            <option
                value="<?php echo esc_attr( $key ) ?>"
                <?php echo $selected ? "selected" :  "" ?>
            >
                <?php echo $option ?>
            </option>
        <?php endforeach; ?>
    </select>
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>