<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <select
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
        value="<?php echo esc_attr( $value ) ?>"
        <?php echo $is_required ? "required" : ""; ?>
    >
        <?php echo $label; ?>
        <?php if($value === ""): ?>
            <option value=""></option>
        <?php endif; ?>
        <?php foreach($attributes['optgroup'] as $optgroup): ?>
            
            <optgroup label="<?php echo esc_attr( $optgroup['label'] ) ?>">
            <?php foreach($optgroup['options'] as $option_value => $option_label):
                $selected = "";
                if ($value === $option_value) {
                    $selected = "selected";
                }
                ?>
                <option
                    value="<?php echo esc_attr( $option_value ) ?>"
                    <?php echo $selected; ?>
                >
                    <?php echo $option_label; ?>
                </option>
            <?php endforeach; ?>
            </optgroup>
        <?php endforeach; ?>
    </select>
</div>