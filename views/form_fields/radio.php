<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        data-type="radio"
        >
        <?php foreach($attributes['options'] as $option):
            $option['checked'] = "";
            if ($value === $option['value']) {
                $option['checked'] = "checked";
            }
            ?>
            <input
                type="radio"
                name="<?php echo esc_attr( $name ) ?>"
                value="<?php echo esc_attr( $option['value'] ) ?>"
                class="<?php echo esc_attr( $option['class'] ) ?>"
                <?php echo esc_attr( $option['checked'] ) ?>
                <?php echo $is_required ? "required" : ""; ?>
            >
                <?php echo $option['label']; ?>
            </option>
        <?php endforeach; ?>
    </div>
</div>