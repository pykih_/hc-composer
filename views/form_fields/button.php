<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <button
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
    >
        <?php echo $label; ?>
    </button>
</div>