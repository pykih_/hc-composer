<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="hc-fy">
    <input
        type="number"
        <?php echo $attributes["integersOnly"] ? 'onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"' : ''; ?> 
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        min="<?php echo esc_attr( $attributes['min'] ) ?>"
        max="<?php echo esc_attr( $attributes['max'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        value="<?php echo esc_attr( $value ) ?>"
        <?php echo $attributes['readonly'] ? "readonly" : ""; ?>
        <?php echo $is_required ? "required" : ""; ?>
        placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
        <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
    />
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>