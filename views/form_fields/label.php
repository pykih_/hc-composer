<?php 
    namespace Humane_Sites;
?>
<div class="hc-label hc-col-6 hc-mr-8 hc-flex-no-shrink <?php echo $attributes["caption_class"]; ?>">
    <label
        for="<?php echo esc_attr( $attributes['for'] ) ?>"
    >
        <?php echo _e( $label ) ?>
        <?php if($is_required): ?>
            <span class="hc-required">*</span>
        <?php endif; ?>
    </label>
</div>