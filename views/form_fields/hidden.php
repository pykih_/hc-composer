<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fy <?php echo $attributes["container_class"] ?? "" ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <input
        type="hidden"
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        value="<?php echo esc_attr( stripslashes($value) ) ?>"
        <?php echo $attributes['meta_id'] ? 'data-meta_id="'.esc_attr( $attributes['meta_id'] ).'"' : ""?>
    />
</div>