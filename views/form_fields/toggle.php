<?php 
    namespace Humane_Sites;
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : ""; ?>">
    <?php
        $value = !($value == "") ? $value : ($attributes["default"] ?? "");
        $count = 0;
    ?>
    <div class="hc-fx hc-align-center hc-flex-between hc-width-fit-container">
        <?php
            if ($label) {
                echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
            }
        ?>
        <input
            name="<?php echo esc_attr( $name ) ?>"
            type="hidden"
            class="hc-flex-grow  <?php echo esc_attr( $attributes['class'] ) ?> toggle-hidden"
            data-identifier="<?php echo esc_attr( $name ) ?>"
            value="<?php echo esc_attr( $value ) ?>"
            <?php echo $is_required ? "required" : ""; ?>
        />
        <span class="hc-toggle">
            <div class="hc-fx" role="group" aria-label="Button">
                <?php foreach($attributes["options"] as $option_key => $option): ?>
                    <?php
                        $count++;
                    ?>
                    <button data-value=<?php echo $option_key ?> type="button" class="hc-btn-structure hc-supernormal-s <?php echo $count === 1 ? "hc-border-rounded-left-4" : ($count === count($attributes["options"]) ? "hc-border-rounded-right-4" : "hc-border-rounded-0"); ?> <?php echo $value == $option_key ? "hc-humane-btn-primary" : "hc-humane-btn-secondary" ?> "><?php echo $option ?></button>
                <?php endforeach; ?>
            </div>
        </span>
    </div>
</div>