<?php 
    namespace Humane_Sites;
?>
<div class="hc-fy <?php echo $attributes["container_class"] ?? "" ?>">
    <?php
        $count = 0;
    ?>
    <div class="hc-fx hc-flex-align-center hc-flex-between">
        <label class="toggle-switch">
            <input 
                type='hidden' 
                value='off' 
                name="<?php echo esc_attr( $name ) ?>" 
            >
            <input
                name="<?php echo esc_attr( $name ) ?>" 
                type="checkbox"
                class="hc-flex-grow <?php echo esc_attr( $attributes['class'] ) ?>"
                data-identifier="<?php echo esc_attr( $name ) ?>"
                <?php echo $value === "on" ? "checked" : "" ?>
                <?php echo $is_required ? "required" : ""; ?>
            />
            <span class="toggle-slider toggle-round"></span>
            
        </label>
        <?php
            if ($label) {
                echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
            }
        ?>
    </div>
</div>