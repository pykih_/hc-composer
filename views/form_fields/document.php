<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="hc-attach-document">
        <input
            name="<?php echo esc_attr( $name ) ?>"
            type="<?php echo esc_attr( 'text' ) ?>"
            id="<?php echo esc_attr( 'process_custom_images' ) ?>"
            <?php echo $attributes['meta_id'] ? 'data-meta_id="'.esc_attr( $attributes['meta_id'] ).'"' : ""?>
            class="<?php echo esc_attr( 'regular-text process_custom_images input-image '.$attributes['class'] ) ?>"
            data-identifier="<?php echo esc_attr( $name ) ?>"
            placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
            value="<?php echo esc_attr( $value ) ?>"
            <?php echo $is_required ? "required" : ""; ?>
        />
        <button
            type="button"
            class="hc-no-wrap <?php echo esc_attr( 'set_custom_images' ) ?>"
        >
            <?php echo $attributes["button"]["label"] ?? "Browse"; ?>
        </button>
    </div>
</div>