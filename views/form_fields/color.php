<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
    ?>
    <div class="hc-fy">
        <div class="humane-color-picker">
            <input
                type="color"
                name="<?php echo esc_attr( $name ) ?>"
                class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
                data-identifier="<?php echo esc_attr( $name ) ?>"
                value="<?php echo esc_attr( $value ) ?>"
                <?php echo $is_required ? "required" : ""; ?>
            />
            <!-- <input type="text" class="color-textbox"></input> -->
        </div>
        <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>
</div>