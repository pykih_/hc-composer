<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx input-wrapper <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
        $maxlength = $attributes['maxlength'] ?? "";
        $tab_count = 0;
        $rand = rand();
    ?>
    <div class="hc-fy hc-width-fit-container">
    <textarea
        type="textarea"
        <?php echo $attributes["tinymce"] ? "" : 'data-replace-target="true"' ?>
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-form-textarea hc-flex-grow <?php echo esc_attr( $attributes['class'] ) ?> <?php echo $attributes["tinymce"] ? "py-form-tinymce" : "" ?>"
        <?php echo $attributes['meta_id'] ? 'data-meta_id="'.esc_attr( $attributes['meta_id'] ).'"' : ""?>
        rows = "<?php echo $attributes['rows'] ?? 3 ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
        maxlength="<?php echo esc_attr( $maxlength ) ?>"
        <?php echo $is_required ? "required" : ""; ?>
    ><?php echo $value ?></textarea>
    
    <?php if($attributes["placeholders"]): ?>
        <div class="humane-placeholders">
            <a href="#" class="placeholder-modal-opener">Insert Placeholders</a>
            <div class="placeholder-modal">
                <div class="placeholder-header hc-py-8 hc-px-20">
                    <h6>Insert Placeholders</h6>
                    <p>Click to insert Placeholders in the content which will dynamically get resolved into the appropriate data</p>
                </div>
                <div class="placeholder-container">
                    <div class="hc-fy placeholder-tabs hc-tab-group">
                        <?php foreach($attributes["placeholders"] as $tab_key => $tab_value): ?>
                            <?php $tab_count++; ?>
                            <a class="nav-item hc-fx hc-mx-8 hc-tab-trigger hc-border-rounded-8 hc-p-8 hc-supernormal-s hc-mb-8 <?php echo $tab_count == 1 ? "hc-backend-tab-active" : "" ?>" role="tab" aria-selected="<?php echo $tab_count == 1 ? "true" : "false" ?>" aria-controls="<?php echo $tab_key. "-" . $rand ?>" id="<?php echo $tab_key. "-" . $rand ?>-placeholder-tab" href="#<?php echo $tab_key. "-" . $rand ?>"><?php echo $tab_value["name"]; ?></a>
                        <?php endforeach; ?>
                        <?php $tab_count = 0; ?>
                    </div>
                    <div class="placeholders-body hc-tab-content">
                        <?php foreach($attributes["placeholders"] as $tab_key => $tab_value): ?>
                            <?php $tab_count++; ?>
                            <div id="<?php echo $tab_key. "-" . $rand ?>" role="tabpanel" aria-labelledby="<?php echo $tab_key. "-" . $rand ?>-tab" class="hc-tab-container hc-fx <?php echo $tab_count == 1 ? "active" : "" ?> placeholder-pills">
                                <?php foreach($tab_value["items"] as $item_key => $item_value): ?>
                                    <span data-replace="<?php echo $item_key?>"class="hc-mr-8 hc-mb-8 hc-badge hc-border-rounded-16 hc-bg-brightness-97 hc-supernormal-s hc-text-brightness-7 hc-width-fit-content hc-fx hc-center"><?php echo $item_value ?></span>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>