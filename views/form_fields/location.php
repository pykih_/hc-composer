<?php 
    namespace Humane_Sites;
    $city_value = Controller_Form_Fields::format_value($attributes["city_value"], $attributes);
    $city_name = $attributes["city_name"];
?>
<div class="hc-fx hc-mb-20 hc-location-container <?php echo $attributes["container_class"] ?? "" ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <div class="hc-fy hc-width-fit-container">
        <div class="hc-fx">
            <?php
                if ($label) {
                    echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
                }
            ?>
            <select
                <?php echo ($attributes['multiple'] ?? false) ? "multiple" : ""?>
                name="<?php echo esc_attr( $name ) ?>"
                class="hc-flex-grow hc-country-select <?php echo esc_attr( $attributes['class'] ) ?>"
                data-identifier="<?php echo esc_attr( $name ) ?>"
                placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
                <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
                value="<?php echo esc_attr( $value ) ?>"
                <?php echo $is_required ? "required" : ""; ?>
            >
                <?php if(!$attributes['required'] && !$attributes["default"] && !$attributes["multiple"]): ?>
                    <option selected value>--</option>
                <?php endif; ?>
                <?php foreach($attributes['options'] as $key => $option): ?>
                    <?php
                        if(is_array($value) && ($attributes["multiple"] ?? false)){
                            $selected = in_array($key, $value);
                        }else{
                            $selected = $value == $key;
                        }
                    ?>
                    <option
                        value="<?php echo esc_attr( $key ) ?>"
                        <?php echo $selected ? "selected" :  "" ?>
                    >
                        <?php echo $option ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="hc-fx hc-city-select-container <?php echo empty($value) || !count($value) ? "hc-display-none" : ""; ?>">
            <?php
                if ($attributes["city_label"]) {
                    echo Controller_Form_Fields::input_label($name, $attributes["city_label"], $is_required, $value, $attributes);
                }
            ?>
            <select
                <?php echo ($attributes['multiple'] ?? false) ? "multiple" : ""?>
                name="<?php echo esc_attr( $city_name ) ?>"
                class="hc-flex-grow hc-city-select <?php echo esc_attr( $attributes['class'] ) ?>"
                data-identifier="<?php echo esc_attr( $city_name ) ?>"
                placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
                <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
                value="<?php echo esc_attr( $city_value ) ?>"
                <?php echo $is_required ? "required" : ""; ?>
            >
                <?php if(!$attributes['required'] && !$attributes["multiple"]): ?>
                    <option selected value>--</option>
                <?php endif; ?>
                <?php foreach($attributes['city_options'] as $key => $option): ?>
                    <?php
                        if(is_array($value) && ($attributes["multiple"] ?? false)){
                            $selected = in_array($key, $city_value);
                        }else{
                            $selected = $city_value == $key;
                        }
                    ?>
                    <option
                        value="<?php echo esc_attr( $key ) ?>"
                        <?php echo $selected ? "selected" :  "" ?>
                    >
                        <?php echo $option ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>