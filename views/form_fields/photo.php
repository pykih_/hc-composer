<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="custom-file">
        <div class="hc-p-20 hc-col-4 hc-border-brightness-60 hc-border-rounded-8 hc-text-center">
            <?php if(empty($attributes["url"])): ?>
                <?php echo Controller_Icons::get_svg_icons("plus"); ?>
            <?php else: ?>
                <img class="hc-input-photo" src="<?php echo $attributes["url"]; ?>" accept="image/*"/>
            <?php endif; ?>
        </div>
        <input
            name="<?php echo esc_attr( $name ) ?>"
            type="<?php echo esc_attr( $attributes["type"] ?? "file" ) ?>"
            id="<?php echo esc_attr( $attributes['id'] ) ?>"
            class="hc-hidden-file hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
            data-identifier="<?php echo esc_attr( $name ) ?>"
            value="<?php echo esc_attr( $value ) ?>"
            accept="<?php echo esc_attr($attributes['accept']); ?>"
            <?php echo $is_required ? "required" : ""; ?>
        />
    </div>
</div>