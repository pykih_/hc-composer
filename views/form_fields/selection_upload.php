<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<script type="text/javascript">
    if(!window.modalJSONs) window.modalJSONs = {};
    window.modalJSONs["<?php echo $attributes["modal_id"] ?>"] = <?php echo json_encode($attributes["json"]); ?>;
</script>
<div class="hc-form-modal" data-id="<?php echo esc_attr( $name ); ?>">
    <?php echo Controller_Content_Selection_Modal::create_post_modal_markup($attributes["json"], $attributes["modal_id"], $attributes); ?>
</div>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
<?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="hc-fy">
    <div class="hc-attach-document">
        <input
            name="<?php echo esc_attr( $name.'_label' ) ?>"
            type="<?php echo esc_attr( 'text' ) ?>"
            id="<?php echo esc_attr( $name.'_label' ) ?>"
            class="hc-col-4 <?php echo esc_attr( 'regular-text process_custom_images input-image '.$attributes['class'] ) ?>"
            <?php echo $attributes['readonly'] ? "readonly" : ""; ?>
            data-identifier="<?php echo esc_attr( $name ) ?>"
            placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
            value="<?php echo esc_attr( $attributes["selected"] ) ?>"
            <?php echo $is_required ? "required" : ""; ?>
        />
        <input
            name="<?php echo esc_attr( $name ) ?>"
            type="hidden"
            id="<?php echo esc_attr( $name ) ?>"
            class="<?php echo esc_attr( 'regular-text linked-post-id '.$attributes['class'] ) ?>"
            data-identifier="<?php echo esc_attr( $name ) ?>"
            placeholder="<?php echo esc_attr_e( $attributes['placeholder'] ) ?>"
            value="<?php echo esc_attr( $value ) ?>"
        />
        <button
            type="button"
            data-id="<?php echo esc_attr( $name ); ?>"
            data-modal-id="<?php echo esc_attr( $attributes["modal_id"] ); ?>"
            class="hc-no-wrap <?php echo esc_attr( $attributes["button"]["class"] ) ?>"
        >
            <?php echo $attributes["button"]["label"] ?? "Browse"; ?>
        </button>
    </div>
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>