<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
        $maxlength = $attributes['maxlength'] ?? "";
    ?>
    <div class="hc-fy hc-width-fit-container">
    <input
        <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
        <?php echo $attributes['meta_id'] ? 'data-meta_id="'.esc_attr( $attributes['meta_id'] ).'"' : ""?>
        name="<?php echo esc_attr( $name ) ?>"
        type="text"
        placeholder="<?php echo esc_attr( $attributes['placeholder'] ) ?>"
        id="<?php echo esc_attr( $name ) ?>"
        class="datetime-picker hc-flex-grow  <?php echo esc_attr( $attributes['class'] ) ?>"
        value="<?php echo $value ?>"
        <?php echo $is_required ? "required" : ""; ?>
    >
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>
</div>