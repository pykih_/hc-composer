<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
        $maxlength = $attributes['maxlength'] ?? "";
    ?>
    <div class="hc-fy">
    <input
        type="url"
        name="<?php echo esc_attr( $name ) ?>"
        class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
        data-identifier="<?php echo esc_attr( $name ) ?>"
        value="<?php echo esc_attr( stripslashes($value) ) ?>"
        placeholder="<?php echo esc_attr( $attributes['placeholder'] ) ?>"
        maxlength="<?php echo esc_attr( $maxlength ) ?>"
        <?php echo $is_required ? "required" : ""; ?>
        <?php echo $attributes['disabled'] ? "disabled" : ""; ?>
    />
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>
</div>