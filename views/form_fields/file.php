<?php 
    namespace Humane_Sites;
    $value = Controller_Form_Fields::format_value($value, $attributes);
?>
<div class="hc-fx <?php echo $attributes["container_class"] ? $attributes["container_class"] : "hc-mb-20 "; ?> <?php echo $render_class ?>">
    <?php echo $attributes["before"]; ?>
    <?php
        if ($label) {
            echo Controller_Form_Fields::input_label($name, $label, $is_required, $value, $attributes);
        }
    ?>
    <div class="hc-fy">
    <div class="custom-file">
        <input
            name="<?php echo esc_attr( $name ) ?>"
            type="<?php echo esc_attr( $attributes["type"] ?? "file" ) ?>"
            id="<?php echo esc_attr( $attributes['id'] ) ?>"
            class="hc-flex-grow hc-col-4  <?php echo esc_attr( $attributes['class'] ) ?>"
            data-identifier="<?php echo esc_attr( $name ) ?>"
            value="<?php echo esc_attr( $value ) ?>"
            accept="<?php echo esc_attr($attributes['accept']); ?>"
            <?php echo $is_required ? "required" : ""; ?>
        />
        <?php
        if ($attributes['placeholder']) {
            ?>
            <label class="custom-file-label" for="<?php echo esc_attr( $attributes['id'] ) ?>"><?php echo esc_attr($attributes['placeholder']); ?></label>
            <?php
        }
        ?>
    </div>
    <?php echo Controller_Form_Fields::description($attributes); ?>
    </div>

</div>