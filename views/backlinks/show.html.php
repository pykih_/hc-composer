<?php 
    namespace Humane_Sites;
    $subtype = "posts";
    if(isset($_GET["subtype"])) $subtype = $_GET["subtype"];
?>
<div>
	<div class="hc-fx hc-pr-20 hc-mt-20">
		<div class="hc-container hc-width-fit-container">
            <div class="headline">
                <?php if($breadcrumb["type"] === "show"): ?>
                    <a href="<?php echo admin_url() ?>admin.php?page=<?php echo $breadcrumb["slug"] ?>&id=<?php echo $id; ?>"class="hc-humane-btn-link">← Back to <?php echo $title; ?></a>
                <?php else: ?>
                    <a href="<?php echo admin_url() ?>admin.php?page=<?php echo $breadcrumb["slug"] ?>"class="hc-humane-btn-link">← Back to <?php echo $breadcrumb["title"]?></a>
                <?php endif; ?>
                <h1 class="hc-my-20">
                🟩 References of <?php echo $title ?>
                </h1>
            </div>
			<?php 
			    $table = new Posts_Table();
				$table->prepare_items(); 
				$table->display(); 
			?>
		</div>
	</div>
</div>