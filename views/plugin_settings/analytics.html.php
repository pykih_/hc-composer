<?php
    use Humane_Sites\Controller_Form_Fields;
    $action = admin_url('admin.php?page=analytics_settings');
?>
<div class="hc-mt-20">
    <div class="wrap">
        <?php require HUMANE_COMPOSER_DIR . 'views/plugin_settings/header.html.php'; ?>
        <h2 class="hc-my-20">Analytics</h2>
        <form method="post" action="<?php echo $action ?>">
            <?php wp_nonce_field('humane_edit_settings', 'humane_edit_settings', false, true); ?>
            <div class="hc-fy hc-flex-gap-20">
                <?php
                    Controller_Form_Fields::input_text(
                        "humane_gtm_tracking_id",
                        "Google Tag Manager ID",
                        false,
                        get_option('humane_gtm_tracking_id'),
                        array(
                            "class" => "",
                            "description" => "Google Tag Manager tracking ID set up in your account for this site. e.g. M123456"
                        )
                    );
                    ?>
                <br />
                <?php
                    Controller_Form_Fields::input_text(
                        "humane_google_analytics_id",
                        "Google Analytics ID",
                        false,
                        get_option('humane_google_analytics_id'),
                        array(
                            "class" => "",
                            "description" => "Google Analytics ID set up in your account for this site. e.g. G-123456"
                        )
                    );
                    ?>
                <?php
                Controller_Form_Fields::input_text(
                    "humane_google_ad_id",
                    "Google Ads ID",
                    false,
                    get_option('humane_google_ad_id'),
                    array(
                        "class" => "",
                        "description" => "Google ad ID set up in your account for this site. e.g. AW-12345678"
                    )
                );
                ?>
                <?php
                    Controller_Form_Fields::input_toggle(
                        "analytics_enabled",
                        "Do you want to enable Humane Tracking Analytics?",
                        false,
                        get_option('analytics_enabled'),
                        array(
                            "class" => "",
                            "options" => array(
                                "on" => "Yes",
                                "off" => "No"
                            ),
                            "default" => "on",
                            "humane_team_only" => true
                        )
                    );
                    ?>
                <?php
                    Controller_Form_Fields::input_text(
                        "plausible_bearer",
                        "Plausible API ID",
                        true,
                        get_option('plausible_bearer'),
                        array(
                            "class" => ""
                        )
                    );
                    ?>
            </div>
                <br/>
            <button type="submit" class="button button-primary">Save</button>
        </form>        
    </div>
</div>
