<?php
    namespace Humane_Sites;
    if ( ! defined( "ABSPATH" ) ) exit;
    use Humane_Sites\Humane_List_Table;
    use Humane_Sites\Model_Users;
    use Humane_Acquire\Model_Forms;
    class User_Events_Table extends Humane_List_Table {
        function __construct() {
            parent::__construct( array(
                'singular'=> 'humane_playlist', //Singular label
                'plural' => 'humane_playlists', //plural label, also this well be one of the table css class
                'ajax'   => false //We won't support Ajax for this table
            ) );
            $this->table_name = "_py_user_events";
            $this->delete_nonce = "humane_user_events_destroy";
            $this->enable_bulk_actions = true;
        }
        function extra_tablenav($which){
            if($which === "top"){
               require HUMANE_COMPOSER_DIR . "views/user_events/_modal.html.php";
            }
        }
        function add_actions($value, $item){
            $data = $item->details_json;
            $data = maybe_unserialize($data); 
            $valid_keys = array(
                "first_name",
                "last_name",
                "email",
                "job_title",
                "tell_us_about_yourself",
                "linkedin",
                "twitter",
                "phone_number",
                "company_name",
                "company_size",
                "industry",
                "website_url",
                "street_address",
                "city",
                "country",
                "zip_postal_code",
                "source_page",
                "extra_field",
            );
            if(empty($data)) $data = array();
            if(is_array($data)){
                $data = array_filter(
                    $data,
                    function ($key) use ($valid_keys) {
                        return in_array($key, $valid_keys);
                    },
                    ARRAY_FILTER_USE_KEY
                );
            }
            $data["Email Copy"] = $item->email_copy;
            $form_id = $item->py_user_form_id;
            $form = new Model_Forms($form_id);
            $data["contacted_at"] = date("F j, Y", $item->created_at);
            $new_data = array();
            $fields = json_decode($form->fields_json, true);
            foreach($data as $key => $val){
                if(is_array($fields) && array_key_exists($key, $fields)){
                    if($key === "extra_field"){
                        $data1 = explode(',', $fields[$key]["label"]);
                        $new_data[ current($data1)] = $val;
                    } else{
                        $new_data[$fields[$key]["label"]] = $val;
                    }
                }else{
                    $new_data[$key] = $val;
                }
            }
            $name_url_slug = $this->name_url_slug;
            $id = $item->id;
            $actions = array(
                'show'      => "<a href='#' class='user-event-modal-opener' data-form-details='" . esc_attr(json_encode($new_data)) . "'>Show</a>",
                'delete'    => sprintf('<a href="?page=%s&id=%s&nonce=%s&id_deleted=%s">Delete</a>', $_GET["page"], $_GET["id"], wp_create_nonce($this->delete_nonce), $item->id),
            );
            return sprintf('%1$s %2$s', "<strong><a class='row-title user-event-modal-opener' href='#' data-form-details='" . esc_attr(json_encode($new_data)) . "'>$value</a></strong>", $this->row_actions($actions) );
        }

        function get_columns(){
            $columns = array(
                "cb" => '<input type="checkbox" />',
                "wp_post_id" => "Post",
                "email" => "Email",
                "created_by" => "Person",
                "ip_address" => "IP Address",
                "browser" => "Browser",
                "location_data" => "Location",
                "device" => "Device",
                "created_at" => "Submission Date"
            );
            if(!$this->enable_bulk_actions){
                unset($columns["cb"]);
            }
            return $columns;
        }
        function column_email($item){
            $data = maybe_unserialize($item->details_json);
            if(is_array($data))
                return $data["email"];
            $data = get_userdata($item->created_by);
            return $data->user_email;
        }
        function column_created_by($item){
            $data = maybe_unserialize($item->details_json);
            if(is_array($data))
                return $data["first_name"];
            return Model_Users::to_s($item->created_by);
        }
        function column_location_data($item){
            $data = maybe_unserialize($item->location_data);
            return $data->city . ", " . $data->country;
        }
        function column_action($item) {
            if($item->action)
                return ucwords(str_replace("_", " ", $item->action));
            return "--";
        }
        function column_device($item) {
            if($item->device)
                return ucwords($item->device);
            return "--";
        }
        function column_wp_post_id($item) {
            if($item->wp_post_id)
                $value = get_the_title($item->wp_post_id);
            else $value = "--";
            return $this->add_actions($value, $item);
        }
        function delete_record($id){
            $item = new Model_User_Events($id);
            $item->delete();
        }
        function prepare_items($condition = array()) {
            $columns = $this->get_columns();
            /** Process bulk action */
            $this->process_bulk_action();
            $hidden = array();
            $sortable = array(
                "name" => "name"
            );
            $condition["order_by"] = "created_at";
            $condition["order"] = "DESC";
            $this->_column_headers = $this->get_column_info();
            $data = $this->get_data($condition);
            $this->items = $data;
        }
    }

?>