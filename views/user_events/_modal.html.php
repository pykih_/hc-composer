<?php
    use Humane_Sites\Controller_Icons;

?>
<div class="user-event-modal-container hc-fx hc-center">
    <div class="user-event-modal hc-height-fit-content">
        <div class="user-event-modal-close"><?php echo Controller_Icons::get_svg_icons("close", 14, "black", "black"); ?></div>
        <h2>Form Details</h2>
        <div class="user-event-modal-details hc-height-fit-content"></div>
    </div>
</div>