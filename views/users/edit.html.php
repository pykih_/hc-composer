<?php
    namespace Humane_Sites;
?>
<div class="hc-col-6">
<?php
    Controller_Form_Fields::input_text(
        "humane_user_profile_photo",
        "Your Photo:",
        false,
        get_user_meta($user->ID, 'humane_user_profile_photo', true),
        array(
            "class" => "hc-col-4",
            "button"=> array(
                "label"=> "Upload",
                "class"=> "button humane-user-profile-photo-button"
            ),
            "reset_button"=> array(
                "label"=> "Reset",
                "class"=> "button humane-user-profile-photo-reset-button"
            )
        )
    );
    Controller_Form_Fields::input_text(
        "humane_user_designation",
        "Your Designation:",
        false,
        get_user_option("humane_user_designation", $user->ID),
        array(
            "class" => "hc-col-4"
        )
    );
    Controller_Form_Fields::input_url(
        "url",
        "Website URL",
        false,
        get_user_meta($user->ID, "url", true),
        array(
            "class" => "hc-col-4",
        )
    );
    Controller_Form_Fields::input_url(
        "facebook",
        "Facebook URL",
        false,
        get_user_meta($user->ID, "facebook", true),
        array(
            "class" => "hc-col-4",
        )
    );
    Controller_Form_Fields::input_url(
        "instagram",
        "Instagram URL",
        false,
        get_user_meta($user->ID, "instagram", true),
        array(
            "class" => "hc-col-4",
        )
    );
    Controller_Form_Fields::input_url(
        "linkedin",
        "LinkedIn URL",
        false,
        get_user_meta($user->ID, "linkedin", true),
        array(
            "class" => "hc-col-4",
        )
    );
    Controller_Form_Fields::input_text(
        "twitter",
        "Twitter username (without @)",
        false,
        get_user_meta($user->ID, "twitter", true),
        array(
            "class" => "hc-col-4",
        )
    );
    Controller_Form_Fields::input_url(
        "youtube",
        "YouTube profile URL",
        false,
        get_user_meta($user->ID, "youtube", true),
        array(
            "class" => "hc-col-4",
        )
    );
?>
</div>