<?php 
    namespace Humane_Sites;
    $user = get_user_by('id',$user_id);
    $user_image = get_avatar_url($user_id);
    $user_name = Model_Users::to_s($user_id);
    $user_object = array(
        "src" => $user_image,
        "fullname" => $user_name
    );
    $socials = array("facebook", "twitter", "linkedin");
    $last_seen =  get_user_meta($user_id, "last_seen", true);
    $first_seen = get_user_meta($user_id, "first_seen", true);
    $city = get_user_meta($user_id, "last_city", true);
    $country = get_user_meta($user_id, "last_location", true);
    $session_count = get_user_meta($user_id, "login_count", true);
    $role = $user->roles[0];
    $status = get_user_meta($user_id, "relationship_status", true);
    $index_page_slug = $_GET["index"];
    $edit_page_slug = "admin.php?page=wp_user_edit";
    $edit_condition = current_user_can("administrator");
    $nonce = wp_create_nonce( 'wp_users_destroy' );
    $header_active = "acquire";
    if($edit_condition){
        $edit_link_value = admin_url() . $edit_page_slug . "&id=" . $user_id;
    }
?>    
<div class="hc-mt-20">
    <div class="wrap">
        <div class="headline">
            <a href="<?php echo admin_url()."users.php"; ?>" class="hc-humane-btn-link">
                ← Back to Users
            </a>
            <h2 class="hc-brand-header-m hc-mb-20">
                <?php echo Controller_Image::displayPic($user_object); ?>
                <?php echo Model_Users::to_s($user_id); ?>
            </h2>
            <div>Last seen on <?php echo  Controller_Date::to_full($last_seen); ?>. First seen on <?php echo  Controller_Date::to_full($first_seen); ?>. So far, there have been <?php echo $session_count; ?> session(s).</div>
            <div class="hc-fx hc-mt-20">
                <div class="hc-mr-20">
                    <?php echo $city; ?>, <?php echo $country ?> - <span style="text-decoration: underline;"><?php echo $user->data->user_email; ?></span>
                </div>
            </div>
        </div>
        <form method="get">
            <?php 
                $table = new User_Events_Table();
                $table->prepare_items(array(
                    "where" => array(
                        "created_by" => array(
                            "value" => $_GET["id"]
                        )
                    )
                )); 
                $table->display(); 
            ?>
        </form>
    </div>
</div>