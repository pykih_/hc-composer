<?php
if(!isset($page)){
	$page = intval( $_POST['current_page']);
	$action = sanitize_text_field($_POST['button_action'] ?? false);
	if(empty($page)){
		$page =1;
	}
	if (!empty($action)) {
		if ('next' === $action) $page = $page + 1;
		else $page = $page - 1;
	}
	
	if ($page < 1) wp_die(); 
}
?>
<div class="objects-table-pagination" data-post_type="<?php echo esc_attr( $post_type ); ?>" data-current_page="<?php echo $page ?>">
	<span class="button previous" data-action="previous"><</span>
	<span class="current"><?php echo $page ?> / <?php echo ((int)($total_count / $per_page) + 1) ?></span>
	<span class="button next" data-action="next">></span>
</div>
<div class="clear"></div>