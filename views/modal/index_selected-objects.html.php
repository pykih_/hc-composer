<table class="objects-table">
	<thead>
		<th class="col-header sort"></th>
		<th class="col-header post_type"><?php _e( 'Type' ); ?></th>
		<th class="col-header post_id"><?php _e( 'ID' ); ?></th>
		<th class="col-header title"><?php _e( 'Title' ); ?></th>
		<th class="col-header date"><?php _e( 'Published Date' ); ?></th>
		<th class="col-header author_name"><?php _e( 'Author Name' ); ?></th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ( $items as $item ) : ?>
		<tr class="object-row slice-row" data-url="<?php echo esc_attr($item->url) ?>" data-post_type="<?php echo esc_attr( $item->post_type ); ?>" data-post_id="<?php echo esc_attr( $item->ID ); ?>" data-title="<?php echo esc_attr( $item->post_title ); ?>">
			<td class="sort">
				<span class="dashicons dashicons-arrow-up-alt2 sort-button" data-sort="up"></span>
				<span class="dashicons dashicons-arrow-down-alt2 sort-button" data-sort="down"></span>
			</td>
			<td class="post_type">
				<?php echo $item->post_type; ?>
                <?php if($item && property_exists($item, "is_pinned") && $item->is_pinned): ?>
					<i class="dashicons dashicons-admin-post"></i>
				<?php endif; ?>
			</td>
			<td class="post_id">
				<?php echo (int)$item->ID; ?>
			</td>
			<td class="title">
				<?php if($item->post_type !== "playlist" || ($was_playlist ?? "none") === "none"): ?>
					<?php echo $item->post_title; ?>
				<?php else: ?>
					<?php echo "[" . ucwords($was_playlist) . "] " . $item->post_title; ?>
				<?php endif; ?>
			</td>
			<td class="date"><?php echo $item->date; ?></td>
			<td class="author_name"><?php echo $item->author; ?></td>
			<td class="remove-object-wrapper"><span class="remove-object-button dashicons dashicons-trash"></span></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>