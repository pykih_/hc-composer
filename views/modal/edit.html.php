<div tabindex="0" class="hidden-content-selection-modal content-selection-modal-common media-modal wp-core-ui <?php echo $special_class ?>" id="<?php echo $special_class ?>">
    <button type="button" class="media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text"><?php _e( 'Close slice modal window' ) ?></span></span></button>
    <div class="media-modal-content">
        <div class="media-frame mode-select wp-core-ui" id="">
            <div class="media-frame-menu">
                <div class="media-menu">
                    <a href="#" class="media-menu-item cancel">
                        <span class="dashicons dashicons-arrow-left-alt cancel-left"></span>
                        <?php _e( 'Cancel' ) ?>
                    </a>
                    <div class="content-container">
                        <div class="separator"></div>
                        <a href="#" class="media-menu-item input-objects" data-content="input-objects">
                            <?php _e( 'Selected objects' ) ?>
                        </a>
                        <?php foreach($reference_json as $key => $menu_item):?>
                            <h4 class="media-menu-item-header hc-py-8 hc-px-20 hc-m-0">
                                <?php echo $menu_item["label"] ?>
                            </h4>
                            <?php foreach($menu_item["sections"] as $section_key => $section):?>
                                <a href="#" class="pad-left deactivated media-menu-item add-<?php echo $section_key ?>-to-slice"
                                data-single-select="<?php echo $section["single-select"] ?? "false" ?>" data-content="add-to-slice-<?php echo $section_key ?>">
                                    <?php echo $section["label"] ?>
                                </a>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="media-frame-title">
                <h2 class="hc-brand-header-m hc-mb-20 hc-m-8">
                    <div class="media-frame-title-text">
                        <?php _e( 'Edit Slice' ) ?>
                    </div>
                    <div class="media-frame-subtitle">
                        <span class="media-frame-title-notice" style="display: none; font-size: 12px; padding-left: 10px;"><?php _e( 'Click an object to select or deselect.' ) ?></span>
                    </div>
                </h2>
            </div>
            <div class="media-frame-search">
                <div class="media-toolbar-primary hc-mt-8 search-form hc-fx">
                    <input type="text" class="textbox search-post" placeholder='Search' />
                </div>
            </div>
            <div class="media-frame-router" style="display: none;">
                <?php foreach($reference_json as $menu_item): ?>
                    <?php foreach($menu_item["sections"] as $type => $section): ?>
                        <div class="media-router" data-type="<?php echo $type?>" style="display: none;">
                            <?php foreach($section["tabs"] as $key => $tab): ?>
                                <?php if($type === "post" && !post_type_exists($tab["code"])){
                                    continue;
                                } ?>
                                <a href="#" class="media-menu-item" data-tab="<?php echo $tab["code"] ?>"><?php echo  $tab["label"] ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>

            <div class="media-frame-content" data-columns="3">
                <div id="add-slice-content" class="media-frame-content-item-container">
                    <div id="input-objects" class="media-frame-content-item" style="display: none;">
                        <div class="input-objects-wrapper">

                        </div>
                    </div>
                    <?php foreach($reference_json as $menu_item): ?>
                        <?php foreach($menu_item["sections"] as $type => $section): ?>
                            <div id="add-to-slice-<?php echo $type ?>" class="media-frame-content-item" style="display: none;">
                                <?php foreach($section["tabs"] as $key => $tab): ?>
                                    <div class="media-item-tab-content" id="<?php echo $tab["code"] ?>">

                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="media-toolbar-primary-position-container">
                <div class="media-toolbar-primary hc-mt-8 update-buttons">
                    <button type="button" class="button button-secondary button-large add-object-modal" style="display: none;"><?php _e( 'Add Content' ) ?></button>
                    <button type="button" class="button button-primary button-large hc-update-slice-modal"><?php echo $attributes["update-label"] ?? "Update Slice" ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
