<table class="objects-table table hc-mt-20 hc-width-fit-container">
	<thead>
		<th><?php _e( 'Post ID' ); ?></th>
		<th><?php _e( 'Title' ); ?></th>
		<th><?php _e( 'Price' ); ?></th>
		<th><?php _e( 'Author' ); ?></th>
	</thead>
	<tbody>
		<?php foreach ( $items as $item ) : ?>
		<tr class="object-row" data-url="<?php echo esc_attr($item->url) ?>" data-post_type="<?php echo esc_attr( $item->post_type ); ?>" data-post_id="<?php echo esc_attr( $item->ID ); ?>" data-post_price="<?php echo $item->price; ?>" data-post_author="<?php echo esc_attr($item->author); ?>" data-title="<?php echo esc_attr( $item->post_title ); ?>">
			<td class="post_id"><?php echo $item->ID; ?></td>
			<td class="title"><?php echo $item->post_title; ?></td>
			<td class="price"><?php echo $item->price . " " . $item->currency; ?></td>
			<td class="author"><?php echo $item->author; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>