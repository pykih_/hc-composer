<div class="media-table table hc-fx hc-flex-wrap">
    <?php foreach ( $items as $item ) : ?>
        <div class="object-row hc-mr-8" data-post_type="<?php echo esc_attr( $item->post_type ); ?>" data-post_id="<?php echo esc_attr( $item->ID ); ?>" data-title="<?php echo esc_attr( $item->post_title ); ?>">
            <div class="img">
                <img width="100%" src="<?php echo $item->img_url; ?>" />
            </div>
        </div>
    <?php endforeach; ?>
</div>