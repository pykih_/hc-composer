<?php
    namespace Humane_Sites;
    
    global $wpdb;
    $index_page_slug = "wp_posts_posts";
    $new_page_slug = "post-new.php";
    $title = "Post";
    $nonce = wp_create_nonce( 'wp_posts_posts_destroy' );
    $current_page = $_GET["pageno"] ?? 1;
    $posts_per_page = 20;
    $total_pages = ceil($count / $posts_per_page);
    if($current_page > $total_pages) $current_page = 1;
    $offset = ($current_page - 1) * $posts_per_page;
    $rows = array_slice($rows, $offset, $posts_per_page);
?>
<?php if(count($rows)): ?>
    <table class="table table-hover hc-width-fit-container">
        <?php foreach($rows as $row): ?>
            <?php require HUMANE_COMPOSER_DIR . "views/posts/_index_one_element.html.php"; ?>
        <?php endforeach; ?>
    </table>
    <?php echo Controller_Pagination::addPagination($current_page, $count, $posts_per_page, $paginate_url); ?>

<?php else: ?>
    <div>
        No entries found. Create a <a href="<?php echo admin_url() ?><?php echo $new_page_slug ?>">new <?php echo $title ?>.</a>
    </div>
<?php endif; ?>