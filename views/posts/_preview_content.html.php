<?php 
    namespace Humane_Sites;
    $categories = get_the_terms($response_post->ID, 'category');
    $cart = get_user_option('humane_user_cart', get_current_user_id());
    $cart = maybe_unserialize($cart);
    $in_cart = false;
    if(is_array($cart) && in_array($response_post->ID, $cart)) $in_cart = true;
    $wishlist = get_user_option("humane_user_wishlist", get_current_user_id());
    $wishlist = maybe_unserialize($wishlist);
    $in_wishlist = false;
    if(is_array($wishlist) && in_array($response_post->ID, $wishlist)) $in_wishlist = true;
    $library = get_user_option("humane_user_library", get_current_user_id());
    $library = maybe_unserialize($library);
    $in_library = false;
    if(is_array($library) && in_array($response_post->ID, $library)) $in_library = true;
   
?>
<div class="hc-preview-backdrop" style="background-color: var(--primary-pastel); background-image: repeating-radial-gradient( circle at 0 0, transparent 0, var(--primary-pastel) 9px ), repeating-linear-gradient( var(--primary-main), var(--primary-main) );"></div>
<div class="hc-preview hc-fy hc-py-20">
    <div class="hc-fx hc-content">
        <div class="hc-fy hc-mr-20 hc-mb-20 hc-col-8">
            <?php if($response_post->category || $response_post->writing_format || $response_post->series): ?>
                <div class="hc-brand-reading-bold hc-mb-20 hc-text-primary-main">
                    <?php if($response_post->category_parent): ?>
                        <a href="<?php echo get_term_link($response_post->category_parent->term_id, "category"); ?>"><?php echo $response_post->category_parent->name; ?></a> /
                    <?php endif; ?>
                    <?php if($response_post->category_id): ?>
                        <a href="<?php echo $response_post->category_link; ?>"><?php echo $response_post->category; ?></a>
                    <?php endif; ?>
                    <?php if($response_post->category_id && $response_post->writing_format): ?>
                        &bull; 
                    <?php endif; ?>
                    <?php if($response_post->writing_format): ?>
                        <a href="<?php echo $response_post->writing_format_link;  ?>"><?php echo $response_post->writing_format; ?></a>
                    <?php endif; ?>
                    <?php if($response_post->series): ?>
                        &bull; <?php echo $response_post->series; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <h1 class="hc-h1-size-of-h2 hc-mt-0 hc-mb-20 hc-text-primary-main">
                <?php echo $response_post->post_title; ?>
            </h1>
            <?php if(Controller_Posts::show_socials($response_post->ID, false)): ?>
                <div class="hc-mb-20">
                    <?php Controller_Posts::show_socials($response_post->ID); ?>
                </div>
            <?php endif; ?>
            <?php if($response_post->post_type === "event"): ?>
                <?php 
                    $event_data = $response_post->event_data;
                ?>
                <div class="hc-tabs-container hc-fx hc-mb-20">
                    <span class="hc-tab hc-supernormal-s hc-text-primary-main">
                        <?php echo $event_data["day"] . " " . $event_data["month"] . " " . (date("Y") === $event_data["year"] ? "" : $event_data["year"] ); ?>
                    </span>
                    <span class="hc-tab-no-border hc-supernormal-s hc-text-primary-main">
                        <?php echo $event_data["day_words"]; ?>
                    </span>
                    <span class="hc-tab hc-supernormal-s hc-text-primary-main"><?php echo $event_data["time"]; ?> <?php echo empty($event_data["timezone"]) ? "IST" : $event_data["timezone"]; ?></span>
                </div>
                <div class="hc-brand-reading hc-text-primary-main hc-fx hc-mb-8">
                    <div class="hc-flex-no-shrink hc-mr-8">Duration : </div>
                    <div>
                        <?php echo $event_data["duration"]; ?>
                    </div>
                </div>
                <div class="hc-brand-reading hc-text-primary-main hc-fx">
                    <?php if(!empty($event_data["web_location_link"])): ?>
                        <div class="hc-flex-no-shrink hc-mr-8">Where : </div>
                        <?php 
                        echo sprintf('<a href="%s" target="_blank">%s</a>',
                        $event_data["web_location_link"],
                        $event_data["web_location_link"]
                        );
                         ?>
                    <?php elseif(!empty($event_data["location"])): ?>
                        <div class="hc-flex-no-shrink hc-mr-8">Where : </div>
                        <a class="hc-brand-reading hc-text-primary-main" href="<?php echo $event_data["location_link"]; ?>"><?php echo empty($event_data["location"]) ? $event_data["location_link"] : $event_data["location"]; ?></a>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <?php if($response_post->post_type === "product"): ?>
                    <?php 
                        $product_data = $response_post->product_data;
                        $discount = $product_data["discount"];
                        if($discount){
                            $discount = ((int) $discount / 100) * $product_data["price"];
                            $discounted_value = $product_data["price"] - $discount;
                        }
                    ?>
                    <div class="hc-tabs-container hc-fx hc-mb-20">
                        <?php if(!empty($discount) && $discount): ?>
                            <div class="hc-tab hc-bg-primary-main">
                                <span class="hc-supernormal-s hc-text-brightness-100 hc-bg-primary-main">
                                    <?php echo $product_data["currency"] . " " . $discounted_value; ?>
                                </span>
                                <span class=" hc-supernormal-xs hc-text-decoration-strikethrough hc-text-brightness-100 ">
                                    <?php echo $product_data["currency"] . " " . $product_data["price"]; ?>
                                </span>
                            </div>
                            <div class="hc-tab hc-supernormal-s hc-text-primary-main hc-fx hc-flex-align-center">
                                Save <?php echo $product_data["currency"] . " " . $discount; ?> (<?php echo $product_data["discount"]; ?>%)
                            </div>
                        <?php else: ?>
                            <span class="hc-tab hc-supernormal-s hc-text-primary-main">
                                <?php echo $product_data["currency"] . " " . $product_data["price"]; ?>
                            </span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if($response_post->post_excerpt): ?>
                    <p class="hc-brand-reading hc-text-primary-main hc-preview-description">
                        <?php echo $response_post->post_excerpt; ?>
                    </p>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if($response_post->post_format === "video"): ?>
            <iframe height="276px" width="460px" src="<?php echo $response_post->video; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <?php elseif($response_post->post_format === "gallery"): ?>
            <div id="hc-preview-gallery-container" class="hc-gallery-container">
                <?php foreach($response_post->gallery as $object): ?>
                    <?php echo $object; ?>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <?php 
                if(!empty($response_post->image)){
                ?>
            <img height="276px" width="460px" class="hc-cover hc-cover-r460" src="<?php echo $response_post->image; ?>" alt="<?php echo Controller_Posts::humane_get_alt_tag(get_the_ID()); ?>"/>
            <?php } ?>
        <?php endif; ?>
    </div>
    <div class="hc-preview-bottom hc-fx hc-mt-20">
        <div class="hc-fx hc-col-8 hc-mr-20 hc-flex-end hc-mb-20">
            <?php 
                $tag = $response_post->preview_interaction === "modal" ? "div" : "a";
            ?>
            <?php if(get_post_type($response_post->ID) === "page" && $response_post->post_format !== "video" && $response_post->post_format !== "gallery"): ?>
                    <?php if($response_post->post_type === "event"): ?>
                        <<?php echo $tag; ?> href="<?php $response_post->url ?>" id="hc_preview_primary_cta" data-connect="<?php echo $response_post->ID; ?>" class="<?php echo $response_post->preview_interaction === "modal" ? "hc-modal-trigger" : ""; ?> hc-post-type-<?php echo $response_post->post_type; ?> hc-btn-structure hc-btn-primary hc-mr-20" data-id="<?php echo $response_post->ID; ?>">
                            <?php echo ((time() - $response_post->event_data["end_date"]) > 0) ? "Read More" : "Interested"; ?>
                        </<?php echo $tag; ?>>
                    <?php elseif($response_post->post_type === "product"): ?>
                        <?php if(is_user_logged_in()): ?>
                            <?php if($in_library): ?>
                                <div id="hc_preview_primary_cta" class="hc-post-type-<?php echo $response_post->post_type; ?> hc-btn-structure hc-btn-primary hc-mr-20">
                                    Purchased
                                </div>
                            <?php elseif($in_cart): ?>
                                <div id="hc_preview_primary_cta" class="hc-remove-from-cart hc-post-type-<?php echo $response_post->post_type; ?> hc-btn-structure hc-btn-primary hc-mr-20" data-id="<?php echo $response_post->ID; ?>">
                                    Remove from Cart
                                </div>                            
                            <?php else: ?>
                                <div id="hc_preview_primary_cta" class="hc-add-to-cart hc-post-type-<?php echo $response_post->post_type; ?> hc-btn-structure hc-btn-primary hc-mr-20" data-id="<?php echo $response_post->ID; ?>">
                                    Add to Cart
                                </div>
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="/login?post=<?php echo $response_post->ID;?>" class="hc-btn-structure hc-btn-primary hc-mr-20">
                                Login
                            </a>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($response_post->restricted): ?>
                            <?php if(is_user_logged_in()): ?>
                                <a href="/pricing?post=<?php echo $response_post->ID;?>" id="hc_preview_primary_cta" class="hc-btn-structure hc-btn-primary hc-mr-20">
                                    Subscribe
                                </a>
                            <?php else: ?>
                                <a href="/login?post=<?php echo $response_post->ID;?>" class="hc-btn-structure hc-btn-primary hc-mr-20">
                                    Login
                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <<?php echo $tag; ?> href="<?php echo $response_post->url ?>" id="hc_preview_primary_cta" data-connect="<?php echo $response_post->ID; ?>" class="<?php echo $response_post->preview_interaction === "modal" && $response_post->post_type !== "product" ? "hc-modal-trigger" : ""; ?> <?php echo $response_post->post_type === "product" && $in_library && is_user_logged_in() ? ($in_cart ? "hc-remove-from-cart" : "hc-add-to-cart") : "";?> hc-post-type-<?php echo $response_post->post_type; ?> hc-btn-structure hc-btn-primary hc-mr-20" data-id="<?php echo $response_post->ID; ?>">
                                Read more
                            </<?php echo $tag; ?>>
                        <?php endif; ?>
                    <?php endif; ?>
                </a>
            <?php endif; ?>
            <?php if(!empty($response_post->cta_url)): ?>
                <a href="<?php echo $response_post->cta_url; ?>" target="_blank">
                    <div id="hc_preview_secondary_cta" class="hc-btn-structure hc-btn-secondary">
                        <?php echo $response_post->cta_label ?? "Read More"; ?>
                    </div>
                </a>
            <?php endif; ?>
            <?php if($response_post->post_type === "event"): ?>
                <a href="<?php echo $response_post->event_data["ics_link"]; ?>" class="hc-supernormal-s hc-text-primary-main hc-text-decoration-underline hc-mt-8">Add to calendar</a>
            <?php endif; ?>
        </div>
        <div class="hc-card-footer hc-fx">
            <?php if(!$response_post->hide_byline && !empty( $response_post->avatar )): ?>
                <a class="hc-dp-s hc-mr-20 hc-flex-no-shrink" href="<?php echo get_author_posts_url($response_post->post_author);?>"><img src="<?php echo $response_post->avatar; ?>" alt="<?php echo $response_post->author?>"></a>
            <?php endif; ?>
            <div class="hc-fy  hc-flex-evenly hc-height-fit-container">
                <?php if(!$response_post->hide_byline && !empty( $response_post->post_author ) ): ?>
                    <div class="hc-supernormal-s-bold hc-text-primary-main hc-no-wrap">
                        By <a href="<?php echo get_author_posts_url($response_post->post_author);?>"><?php echo $response_post->author; ?></a>.
                    </div>
                <?php endif; ?>
                <div class="hc-fx">
                    <?php if(!$response_post->hide_published_at && !empty($response_post->date)): ?>
                        <div class="hc-supernormal-s hc-text-primary-main hc-no-wrap hc-mr-20">
                            Published <?php echo $response_post->date; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(!$response_post->hide_updated_at && !empty($response_post->modified)): ?>
                        <div class="hc-supernormal-s hc-text-primary-main hc-no-wrap">
                            Updated <?php echo $response_post->modified; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if($response_post->post_type === "product"): ?>
        <?php if(!$in_wishlist): ?>
            <div class="hc-hyperlink hc-supernormal-s hc-text-primary-main hc-text-decoration-underline hc-mt-8 hc-add-to-wishlist" data-id="<?php echo $response_post->ID; ?>">Add to wishlist</div>
        <?php else: ?>
            <div class="hc-hyperlink hc-supernormal-s hc-text-primary-main hc-text-decoration-underline hc-mt-8 hc-remove-from-wishlist" data-id="<?php echo $response_post->ID; ?>">Remove from wishlist</div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<?php if($preview_class === "hc-multiple-preview"): ?>
    <button class="hc-preview-close">
        <svg width="20" height="20" x="0px" y="0px" viewBox="0 0 47.971 47.971">
            <g>
                <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path>
            </g>
        </svg>
    </button>
<?php endif; ?>