<div class="hc-preview-container <?php echo $preview_class; ?> <?php  echo $preview_class === "hc-multiple-preview" ? "preview-closed" : "";?>" id="<?php echo $preview_class; ?>">
    <?php include HUMANE_COMPOSER_DIR . "views/posts/_preview_content.html.php"; ?>
</div>