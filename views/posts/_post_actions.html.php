<?php 
    namespace Humane_Sites;
?>
<div class="hc-fx hc-supernormal-s">
    <div class="hc-fx hc-mr-20 hc-flex-align-center">
        <?php
            $user_id = get_current_user_id();
            $active = !empty(Model_User_Events::get_action_event($user_id, $id, "upvote"));
        ?>
        <div class="hc-post-upvote hc-hyperlink hc-toggle-event <?php echo $active ? "active" : "" ;?>" data-event="upvote" data-id="<?php echo $id; ?>">
            <div class="hc-inactive-div">
                <?php echo Controller_Icons::get_svg_icons("upvote", 18, "transparent"); ?>
            </div>  
            <div class="hc-active-div">
                <?php echo Controller_Icons::get_svg_icons("upvote_active", 18, "var(--primary-main)"); ?>
            </div>  
        </div>
        <?php
            $active = !empty(Model_User_Events::get_action_event($user_id, $id, "downvote"));
        ?>
        <div class="hc-post-downvote hc-hyperlink hc-toggle-event <?php echo $active ? "active" : "" ;?>" data-event="downvote" data-id="<?php echo $id; ?>">
            <div class="hc-inactive-div">
                <?php echo Controller_Icons::get_svg_icons("downvote", 18, "transparent"); ?>
            </div>  
            <div class="hc-active-div">
                <?php echo Controller_Icons::get_svg_icons("downvote_active", 18, "var(--brightness-0)"); ?>
            </div>  
        </div>
        <?php
            $upvote_count = count(Model_User_Events::get_user_events_by_post_id($id, "upvote"));
            $downvote_count = count(Model_User_Events::get_user_events_by_post_id($id, "downvote"));
        ?>
        <div class="hc-post-vote-count hc-ml-8">
            <?php echo $upvote_count - $downvote_count; ?>
        </div>
    </div>
    <?php
        $bookmarked_posts = get_user_option( 'bookmarked_posts', $user_id);
        $inactive_svg = Controller_Icons::get_svg_icons("bookmark", 18);
        $inactive_title = "Save";
        $active_svg = Controller_Icons::get_svg_icons("bookmarked", 18);
        $active_title = "Unsave";
        $active = !empty(Model_User_Events::get_action_event($user_id, $id, "bookmarked"));
    ?>
    <div class="hc-fx hc-mr-20 hc-hyperlink hc-flex-align-center hc-bookmark hc-toggle-event <?php echo $active ? "active" : "" ;?>" data-event="bookmarked" data-id="<?php echo $id; ?>">
        <div class="hc-active-div hc-flex-align-center">
            <div class="hc-read-icon"><?php echo $active_svg; ?></div>
            <div class="hc-read-title hc-no-wrap"><?php echo $active_title; ?></div>
        </div>
        <div class="hc-inactive-div hc-flex-align-center">
            <div class="hc-read-icon"><?php echo $inactive_svg; ?></div>
            <div class="hc-read-title hc-no-wrap"><?php echo $inactive_title; ?></div>
        </div>
    </div>
    <?php
        $read_posts = get_user_option( 'read_posts', $user_id);
        $inactive_svg = Controller_Icons::get_svg_icons("unread", 16);
        $inactive_title = "Mark as read";
        $active_svg = Controller_Icons::get_svg_icons("read", 16, "var(--green)");
        $active_title = "Mark as unread";
        $active = !empty(Model_User_Events::get_action_event($user_id, $id, "read"));
    ?>
    <div class="hc-read hc-fx hc-hyperlink hc-toggle-event <?php echo $active ? "active" : "" ;?>" data-event="read" data-id="<?php echo $id; ?>">
        <div class="hc-active-div hc-flex-align-center">
            <div class="hc-read-icon" style="padding: 0 3px;"><?php echo $active_svg; ?></div>
            <div class="hc-read-title hc-no-wrap"><?php echo $active_title; ?></div>
        </div>
        <div class="hc-inactive-div hc-flex-align-center">
            <div class="hc-read-icon" style="padding: 0 3px;"><?php echo $inactive_svg; ?></div>
            <div class="hc-read-title hc-no-wrap"><?php echo $inactive_title; ?></div>
        </div>
    </div>
</div>