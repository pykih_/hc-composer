<?php
    namespace Humane_Sites;
?>
<div>
    <div class="hc-width-fit-container">
        <h1 class="hc-my-20">
            Reads (Pitch)
        </h1>
        <form method="get">
            <?php 
                $table = new User_Events_Table();
                $table->prepare_items(array(
                    "where" => array(
                        "action" => array(
                            "value" => "read",
                            "type" => "string"
                        )
                    )
                )); 
                $table->display(); 
            ?>
        </form>
    </div>
</div>