<?php
namespace Humane_Acquire;

/**
 * REST handling for Wp_blocks.
 * Check all gui documentation https://humane.club/rest-api/docs/
 *
 * Used mainly in Gutenberg to display list of Wp_blocks
 *
 * @package Humane Acquire
 * @subpackage Wp_blocks
 */
class API_Wp_blocks {
	/**
	 * Attaches the REST API hook
	 *
	 * @return void
	 *
	 * @uses API_Wp_blocks::add_atomic_note()
	 */
	public static function init() {
		add_action( 'rest_api_init', array( __CLASS__, 'add_rest_route_for_blocks' ) );
		add_action( 'rest_api_init', array( __CLASS__, 'hc_rest_allow_all_cors' ), 15 );
		add_filter( 'rest_post_query', array( __CLASS__, 'hc_allow_specific_meta_query_in_api' ), 99, 2 );
		add_filter( 'rest_wp_block_query', array( __CLASS__, 'hc_allow_specific_meta_query_in_api' ), 99, 2 );
		add_filter( 'rest_aggregation_query', array( __CLASS__, 'hc_allow_specific_meta_query_in_api' ), 99, 2 );
		
		add_action( 'init', array( __CLASS__, 'humane_register_custom_meta' ) );
		add_action( 'pre_get_terms', array( __CLASS__, 'humane_custom_pre_get_posts' ), 10, 2 );
		add_filter( 'rest_user_query', array( __CLASS__, 'hc_allow_specific_meta_query_in_user_api' ), 99, 2 );
		add_filter('determine_current_user', array( __CLASS__,'hc_json_basic_auth_handler'), 14);
		add_filter('rest_authentication_errors', array( __CLASS__, 'json_basic_auth_error'));

	}

	/**
	 * Adds REST API route for Wp_blocks
	 *
	 * @return void
	 *
	 * @uses Model_Wp_blocks::get_all()
	 *
	 * @see API_Wp_blocks::init()
	 */
	public static function add_rest_route_for_blocks() {
		register_rest_route(
			'humane/v1',
			'wp_get_all_pages',
			array(
				'methods'             => 'GET',
				'callback'            => function( $data ) {
					return self::humane_get_all_pages();
				},
				'permission_callback' => '__return_true',
			)
		);
		register_rest_route( 
			'humane/v1', 
			'is_plugin_active', 
			array(
				'methods' => 'GET',
				'callback' => function(){
					if (in_array( 'humane-acquire/humane-acquire.php', apply_filters('active_plugins', get_option('active_plugins')))) {
						return true;
					} else {
						return false;
					}
				},
				'permission_callback' => '__return_true', // Make it publicly accessible
    		)
		);

	}
	/**
	 * Get all pages.
	 *
	 * @return $all_pages_for_api
	 */
	public static function humane_get_all_pages() {
		$cache_name  = 'wp_block_all_pages';
		$cache_group = 'group_wp_block_all_pages';
		$all_pages   = wp_cache_get( $cache_name, $cache_group );
		if ( empty( $all_pages ) ) {
			$all_pages = get_pages();
			wp_cache_set( $cache_name, $all_pages, $cache_group, 60 * MINUTE_IN_SECONDS );
		}
		$all_pages_for_api = array_map(
			function( $a ) {
				return array(
					'label' => $a->post_title,
					'value' => $a->ID,
				);
			},
			$all_pages
		);
		return $all_pages_for_api;
	}
	/**
	 * Allow all CORS.
	 */
	public static function hc_rest_allow_all_cors() {
		// Remove the default filter.
		remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

		// Add a Custom filter.
		add_filter(
			'rest_pre_serve_request',
			function( $value ) {
				header( 'Access-Control-Allow-Origin: *' );
				header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
				header( 'Access-Control-Allow-Credentials: true' );
				return $value;
			}
		);
	}
	/**
	 * Allow specific meta query in api.
	 *
	 * @param array $args args for query.
	 * @param array $request request.
	 *
	 * @return array $args
	 */
	public static function hc_allow_specific_meta_query_in_api( $args, $request ) {
		$allowed_meta_keys = array(
			'humane_roam_post_id',
			'humane_is_from_roam',
			'humane_datawrapper_block_notes',
			'humane_datawrapper_block_data_source',
			'humane_datawrapper_block_link_data_source',
			'humane_datawrapper_block_iframe',
			'humane_datawrapper_block_id',
			'humane_is_from_datawrapper',
			'humane_roam_content_id',
			'humane_roam_user_id',
		);
		$args             += array(
			'meta_key'   => $request['meta_key'],
			'meta_value' => $request['meta_value'],
			'meta_query' => $request['meta_query'],
		);
		if ( ! in_array( $args['meta_key'], $allowed_meta_keys ) ) {
			unset( $args['meta_key'] );
			unset( $args['meta_value'] );
		}
		return $args;
	}
	public static function humane_register_custom_meta() {
		register_term_meta(
			'category',
			'humane_is_from_roam',
			array(
				'show_in_rest' => array(
					'schema' => array(
						'type'    => 'boolean',
						'context' => array( 'view', 'edit' ),
					),
				),
				'type'         => 'boolean',
				'single'       => true,
			)
		);
		register_term_meta(
			'category',
			'humane_roam_content_id',
			array(
				'show_in_rest' => array(
					'schema' => array(
						'type'    => 'string',
						'context' => array( 'view', 'edit' ),
					),
				),
				'type'         => 'string',
				'single'       => true,
			)
		);
		register_term_meta(
			'post_tag',
			'humane_is_from_roam',
			array(
				'show_in_rest' => array(
					'schema' => array(
						'type'    => 'boolean',
						'context' => array( 'view', 'edit' ),
					),
				),
				'type'         => 'boolean',
				'single'       => true,
			)
		);
		register_term_meta(
			'post_tag',
			'humane_roam_content_id',
			array(
				'show_in_rest' => array(
					'schema' => array(
						'type'    => 'string',
						'context' => array( 'view', 'edit' ),
					),
				),
				'type'         => 'string',
				'single'       => true,
			)
		);
		register_meta('user', 'humane_is_from_roam', array(
			'type'         => 'boolean',
			'show_in_rest' => array(
				'schema' => array(
					'type'    => 'boolean',
					'context' => array( 'view', 'edit' ),
				),
			),
			'single'       => true,
		  ));
		  register_meta('user', 'humane_roam_content_id', array(
			'type'         => 'string',
			"show_in_rest" => true,
			'single'       => true,
		  ));
		  register_meta('user', 'humane_roam_user_id', array(
			'type'         => 'string',
			"show_in_rest" => true,
			'single'       => true,
		  ));

	}
	/**
	 * Filter WP_Term_Query meta query
	 * - a better drill down for filtering term meta data
	 * - used in category api
	 *
	 * @param   object $query  WP_Term_Query
	 */
	public static function humane_custom_pre_get_posts( $query ) {

		if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) {
			$current_path = filter_input( INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL );
			$current_url  = wp_parse_url( $current_path );
			if ( ( ( strpos( $current_url['path'], 'categories' ) !== false || strpos( $current_url['path'], 'tags' ) !== false) && strpos( $current_url['query'], 'meta_key' ) !== false ) ) {
				// Store current query vars.
				$query_vars = $query->query_vars;
				$args       = array();
				// Set meta query.
				$args['meta_query'] = array(
					array(
						'key'     => $_GET['meta_key'],
						'value'   => $_GET['meta_value'],
						'compare' => '=',
					),
				);
				// Refresh query vars.
				$query->query_vars = array_merge( $query_vars, $args );
			}
		}
	}
	/**
	 * Allow specific meta query in user api.
	 *
	 * @param array $args args for query.
	 * @param array $request request.
	 *
	 * @return array $args
	 */
	public static function hc_allow_specific_meta_query_in_user_api($args, $request){
		if(isset($_GET['meta_key']) && isset($_GET['meta_value'])){
			$args['meta_query'] = array(
				array(
					'key'     => $_GET['meta_key'],
					'value'   => $_GET['meta_value'],
					'compare' => '=',
				),
			);
		}
		return $args;
	}
	/**
	 * Json basic auth error.
	 *
	 * @param array $error error.
	 *
	 * @return array error.
	 */
	public static function json_basic_auth_error($error)
	{
		// Passthrough other errors.
		if (!empty($error)) {
			return $error;
		}

		global $wp_json_basic_auth_error;

		return $wp_json_basic_auth_error;
	}
	/**
	 * Json basic auth handler.
	 * This is for handle error rest_can_not_edit_user error.
	 *
	 * @param array $user acurrent user.
	 *
	 * @return int user id.
	 */
	public static function hc_json_basic_auth_handler($user){
		global $wp_json_basic_auth_error;
		
		$wp_json_basic_auth_error = null;
		
		// Don't authenticate twice
		if (!empty($user)) {
			return $user;
		}
		// Check that we're trying to authenticate
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			return $user;
		}
		
		$username = $_SERVER['PHP_AUTH_USER'];
		$password = $_SERVER['PHP_AUTH_PW'];
		/**
		 * In multi-site, wp_authenticate_spam_check filter is run on authentication. This filter calls
		 * get_currentuserinfo which in turn calls the determine_current_user filter. This leads to infinite
		 * recursion and a stack overflow unless the current function is removed from the determine_current_user
		 * filter during authentication.
		 */
		remove_filter('determine_current_user',  array( __CLASS__,'hc_json_basic_auth_handler'), 14);
		$user = wp_authenticate($username, $password);
		add_filter('determine_current_user', array( __CLASS__,'hc_json_basic_auth_handler'), 14);
		if (is_wp_error($user)) {
			$wp_json_basic_auth_error = $user;
			return null;
		}
		$wp_json_basic_auth_error = true;
		return $user->ID;
	}
}

API_Wp_blocks::init();
