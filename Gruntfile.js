module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            dev: {
                options: {
                    mangle: false,
                    compress: true
                },
                files: {
                    "assets/core/dist/core.min.js": ["assets/core/js/*.js"],
                    "assets/form_fields/dist/form_fields.min.js": ["assets/form_fields/js/*.js"],
                    "assets/dashboard/dist/dashboard.min.js": ["assets/dashboard/js/*.js"],
                    "assets/menu_options/dist/menu_options.min.js": ["assets/menu_options/js/*.js"],
                    "assets/modal/dist/modal.min.js": ["assets/modal/js/*.js"],
                    "assets/notifications/dist/notifications.min.js": ["assets/notifications/js/*.js"],
                    "assets/settings/dist/settings.min.js": ["assets/settings/js/*.js"],
                    "assets/users/dist/users.min.js": ["assets/users/js/*.js"],
                }
            },
            prod: {
                options: {
                    mangle: true,
                    sourceMap: false,
                    beautify: false, // Specify beautify: true to beautify your code for debugging/troubleshooting purposes. Pass an object to manually configure any other output options.
                    compress: {
                        unused: true,
                        drop_console: true
                    }
                },
                files: {
                    "assets/core/dist/core.min.js": ["assets/core/js/*.js"],
                    "assets/form_fields/dist/form_fields.min.js": ["assets/form_fields/js/*.js"],
                    "assets/dashboard/dist/dashboard.min.js": ["assets/dashboard/js/*.js"],
                    "assets/menu_options/dist/menu_options.min.js": ["assets/menu_options/js/*.js"],
                    "assets/modal/dist/modal.min.js": ["assets/modal/js/*.js"],
                    "assets/notifications/dist/notifications.min.js": ["assets/notifications/js/*.js"],
                    "assets/settings/dist/settings.min.js": ["assets/settings/js/*.js"],
                    "assets/users/dist/users.min.js": ["assets/users/js/*.js"],
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    "assets/core/dist/core.min.css": ["assets/core/css/*.css"],
                    "assets/dashboard/dist/dashboard.min.css": ["assets/dashboard/css/*.css"],
                    "assets/menu_options/dist/menu_options.min.css": ["assets/menu_options/css/*.css"],
                    "assets/modal/dist/modal.min.css": ["assets/modal/css/*.css"],
                    "assets/notifications/dist/notifications.min.css": ["assets/notifications/css/*.css"],
                    "assets/users/dist/users.min.css": ["assets/users/css/*.css"],
                }
            }
        },

        watch: {
            js: {
                files: [
                    "assets/core/js/*.js",
                    "assets/dashboard/js/*.js",
                    "assets/form_fields/js/*.js",
                    "assets/menu_options/js/*.js",
                    "assets/modal/js/*.js",
                    "assets/notifications/js/*.js",
                    "assets/settings/js/*.js",
                    "assets/users/js/*.js"
                ],
                tasks: ['dev:js'],
                options: {
                    livereload: false
                }
            },
            css: {
                files: [
                    "assets/core/css/*.css",
                    "assets/dashboard/css/*.css",
                    "assets/menu_options/css/*.css",
                    "assets/modal/css/*.css",
                    "assets/notifications/css/*.css",
                    "assets/users/css/*.css"
                ],
                tasks: ['dev:css'],
                options: {
                    livereload: false
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('hotload', ['watch:js', 'watch:css']);
    grunt.registerTask('dev:js', ['uglify:dev', 'cssmin']);
    grunt.registerTask('dev:css', ['cssmin']);
    grunt.registerTask('prod', ['uglify:prod', 'cssmin']);
};
