var Rsync = require('rsync');
var env = process.argv[2];
let site = "stagpykih";
if(env === "prod") site = "pykih";
if(env === "dev") site = "devpykih";

var rsync = Rsync.build({
    source:      "./",
    destination: `${site}@pykih.ssh.wpengine.net:/home/wpe-user/sites/${site}/wp-content/mu-plugins/hc-composer/`,
    exclude:     ['.git', 'node_modules'],
    flags:       'avz',
    progress:   true,
    shell:       'ssh'
});
rsync.output(
    function(data){
        console.log(data.toString());
        // do things like parse progress
    }, function(data) {
        // do things like parse error output
    }
);
rsync.execute(function(error, stdout, stderr) {
    console.log(stdout);
// we're done
});